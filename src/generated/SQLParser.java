// Generated from D:/Desktop/chapter2/compiler/Compiler-1-7-2020/compiler-project2/src\SQL.g4 by ANTLR 4.8
package generated;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SQLParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, SCOL=6, DOT=7, OPA=8, CLA=9, OPEN_PAR=10, 
		CLOSE_PAR=11, COMMA=12, ASSIGN=13, STAR=14, PLUS=15, PLUS2=16, MINUS2=17, 
		MINUS=18, TILDE=19, AMP2=20, PIPE2=21, DIV=22, MOD=23, LT2=24, GT2=25, 
		AMP=26, PIPE=27, LT=28, LT_EQ=29, GT=30, GT_EQ=31, EQ=32, NOT_EQ1=33, 
		NOT_EQ2=34, K_ACTION=35, K_ADD=36, K_ALL=37, K_ALTER=38, K_AND=39, K_AS=40, 
		K_ASC=41, K_AUTOINCREMENT=42, K_BY=43, K_CASCADE=44, K_CHECK=45, K_COLLATE=46, 
		K_COLUMN=47, K_CONSTRAINT=48, K_CREATE=49, K_CROSS=50, K_CURRENT_DATE=51, 
		K_CURRENT_TIME=52, K_CURRENT_TIMESTAMP=53, K_DATABASE=54, K_DEFAULT=55, 
		K_DEFERRABLE=56, K_DEFERRED=57, K_DELETE=58, K_DESC=59, K_DISTINCT=60, 
		K_DROP=61, K_EACH=62, K_ELSE=63, K_ENABLE=64, K_FOR=65, K_FOREIGN=66, 
		K_FROM=67, K_GLOB=68, K_GROUP=69, K_HAVING=70, K_IF=71, K_IMMEDIATE=72, 
		K_IN=73, K_INDEXED=74, K_INITIALLY=75, K_INNER=76, K_INSERT=77, K_INTO=78, 
		K_IS=79, K_ISNULL=80, K_JOIN=81, K_KEY=82, K_LEFT=83, K_LIKE=84, K_LIMIT=85, 
		K_MATCH=86, K_NEXTVAL=87, K_NO=88, K_NOT=89, K_NOTNULL=90, K_NULL=91, 
		K_OF=92, K_OFFSET=93, K_ON=94, K_OR=95, K_ORDER=96, K_OUTER=97, K_PRIMARY=98, 
		K_QUERY=99, K_REFERENCES=100, K_REGEXP=101, K_RENAME=102, K_REPLACE=103, 
		K_RESTRICT=104, K_RIGHT=105, K_ROW=106, K_SELECT=107, K_SET=108, K_TABLE=109, 
		K_TYPE=110, K_THEN=111, K_TO=112, K_UNION=113, K_UNIQUE=114, K_UPDATE=115, 
		K_VALUES=116, K_VIEW=117, K_WHERE=118, K_METHODNAME=119, K_CLASSNAME=120, 
		K_JARPATH=121, K_AGGREGATION=122, K_PATH=123, K_PRINT=124, K_VAR=125, 
		K_NEW=126, K_TRUE=127, K_FALSE=128, K_FUNCTION=129, K_RETURN=130, K_WHILE=131, 
		K_SWITCH=132, K_CASE=133, K_BREAK=134, K_CONTINUE=135, K_DO=136, K_EXISTS=137, 
		VARNAME=138, IDENTIFIER=139, NUMERIC_LITERAL=140, BIND_PARAMETER=141, 
		STRING_LITERAL=142, BLOB_LITERAL=143, MULTILINE_COMMENT=144, LINE_COMMENT=145, 
		SPACES=146, UNEXPECTED_CHAR=147;
	public static final int
		RULE_parse = 0, RULE_error = 1, RULE_function_call = 2, RULE_argument_list = 3, 
		RULE_argument = 4, RULE_function_def = 5, RULE_parameter_list = 6, RULE_parameter = 7, 
		RULE_default_var = 8, RULE_body = 9, RULE_java_stmt = 10, RULE_not_need_scol = 11, 
		RULE_need_scol = 12, RULE_incriment_stmt = 13, RULE_print = 14, RULE_end_body = 15, 
		RULE_base_body = 16, RULE_for_stmt = 17, RULE_for_loop = 18, RULE_for_each_loop = 19, 
		RULE_switch_stmt = 20, RULE_case_stmt = 21, RULE_default_stmt = 22, RULE_if_stmt = 23, 
		RULE_else_if_stmt = 24, RULE_else_stmt = 25, RULE_if_inline = 26, RULE_ifinlin_side = 27, 
		RULE_while_stmt = 28, RULE_do_while_stmt = 29, RULE_return_stmt = 30, 
		RULE_sql_stmt_list = 31, RULE_sql_stmt = 32, RULE_create_table_stmt = 33, 
		RULE_table_type = 34, RULE_table_path = 35, RULE_create_type_stmt = 36, 
		RULE_select_stmt = 37, RULE_column_def = 38, RULE_type_name = 39, RULE_create_aggregation_function = 40, 
		RULE_jarPath = 41, RULE_jarName = 42, RULE_className = 43, RULE_methodName = 44, 
		RULE_returnType = 45, RULE_parameters = 46, RULE_condition_exp = 47, RULE_num_exp = 48, 
		RULE_expr = 49, RULE_function_expr = 50, RULE_not_in_expr = 51, RULE_foreign_key_clause = 52, 
		RULE_foreign_on_condition = 53, RULE_fk_target_column_name = 54, RULE_indexed_column = 55, 
		RULE_table_constraint = 56, RULE_table_constraint_primary_key = 57, RULE_table_constraint_foreign_key = 58, 
		RULE_table_constraint_unique = 59, RULE_table_constraint_key = 60, RULE_fk_origin_column_name = 61, 
		RULE_qualified_table_name = 62, RULE_ordering_term = 63, RULE_result_column = 64, 
		RULE_table_or_subquery = 65, RULE_join_clause = 66, RULE_join_operator = 67, 
		RULE_join_constraint = 68, RULE_select_core = 69, RULE_multivar_def = 70, 
		RULE_multivar_def_sub = 71, RULE_var_def = 72, RULE_array_def = 73, RULE_array_dec = 74, 
		RULE_var_deceleration = 75, RULE_decliration = 76, RULE_java_value = 77, 
		RULE_boolean_var = 78, RULE_num_var = 79, RULE_variable = 80, RULE_string_val = 81, 
		RULE_boolean_val = 82, RULE_num_val = 83, RULE_signed_number = 84, RULE_literal_value = 85, 
		RULE_assign_op = 86, RULE_num_op = 87, RULE_comparison_op = 88, RULE_incriment_op = 89, 
		RULE_unary_operator = 90, RULE_error_message = 91, RULE_column_alias = 92, 
		RULE_keyword = 93, RULE_java_function_name = 94, RULE_java_var_name = 95, 
		RULE_unknown = 96, RULE_name = 97, RULE_function_name = 98, RULE_database_name = 99, 
		RULE_source_table_name = 100, RULE_table_name = 101, RULE_new_table_name = 102, 
		RULE_column_name = 103, RULE_collation_name = 104, RULE_foreign_table = 105, 
		RULE_index_name = 106, RULE_table_alias = 107, RULE_any_name = 108;
	private static String[] makeRuleNames() {
		return new String[] {
			"parse", "error", "function_call", "argument_list", "argument", "function_def", 
			"parameter_list", "parameter", "default_var", "body", "java_stmt", "not_need_scol", 
			"need_scol", "incriment_stmt", "print", "end_body", "base_body", "for_stmt", 
			"for_loop", "for_each_loop", "switch_stmt", "case_stmt", "default_stmt", 
			"if_stmt", "else_if_stmt", "else_stmt", "if_inline", "ifinlin_side", 
			"while_stmt", "do_while_stmt", "return_stmt", "sql_stmt_list", "sql_stmt", 
			"create_table_stmt", "table_type", "table_path", "create_type_stmt", 
			"select_stmt", "column_def", "type_name", "create_aggregation_function", 
			"jarPath", "jarName", "className", "methodName", "returnType", "parameters", 
			"condition_exp", "num_exp", "expr", "function_expr", "not_in_expr", "foreign_key_clause", 
			"foreign_on_condition", "fk_target_column_name", "indexed_column", "table_constraint", 
			"table_constraint_primary_key", "table_constraint_foreign_key", "table_constraint_unique", 
			"table_constraint_key", "fk_origin_column_name", "qualified_table_name", 
			"ordering_term", "result_column", "table_or_subquery", "join_clause", 
			"join_operator", "join_constraint", "select_core", "multivar_def", "multivar_def_sub", 
			"var_def", "array_def", "array_dec", "var_deceleration", "decliration", 
			"java_value", "boolean_var", "num_var", "variable", "string_val", "boolean_val", 
			"num_val", "signed_number", "literal_value", "assign_op", "num_op", "comparison_op", 
			"incriment_op", "unary_operator", "error_message", "column_alias", "keyword", 
			"java_function_name", "java_var_name", "unknown", "name", "function_name", 
			"database_name", "source_table_name", "table_name", "new_table_name", 
			"column_name", "collation_name", "foreign_table", "index_name", "table_alias", 
			"any_name"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'{'", "'}'", "':'", "'?'", "'!'", "';'", "'.'", "'['", "']'", 
			"'('", "')'", "','", "'='", "'*'", "'+'", "'++'", "'--'", "'-'", "'~'", 
			"'&&'", "'||'", "'/'", "'%'", "'<<'", "'>>'", "'&'", "'|'", "'<'", "'<='", 
			"'>'", "'>='", "'=='", "'!='", "'<>'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, "SCOL", "DOT", "OPA", "CLA", "OPEN_PAR", 
			"CLOSE_PAR", "COMMA", "ASSIGN", "STAR", "PLUS", "PLUS2", "MINUS2", "MINUS", 
			"TILDE", "AMP2", "PIPE2", "DIV", "MOD", "LT2", "GT2", "AMP", "PIPE", 
			"LT", "LT_EQ", "GT", "GT_EQ", "EQ", "NOT_EQ1", "NOT_EQ2", "K_ACTION", 
			"K_ADD", "K_ALL", "K_ALTER", "K_AND", "K_AS", "K_ASC", "K_AUTOINCREMENT", 
			"K_BY", "K_CASCADE", "K_CHECK", "K_COLLATE", "K_COLUMN", "K_CONSTRAINT", 
			"K_CREATE", "K_CROSS", "K_CURRENT_DATE", "K_CURRENT_TIME", "K_CURRENT_TIMESTAMP", 
			"K_DATABASE", "K_DEFAULT", "K_DEFERRABLE", "K_DEFERRED", "K_DELETE", 
			"K_DESC", "K_DISTINCT", "K_DROP", "K_EACH", "K_ELSE", "K_ENABLE", "K_FOR", 
			"K_FOREIGN", "K_FROM", "K_GLOB", "K_GROUP", "K_HAVING", "K_IF", "K_IMMEDIATE", 
			"K_IN", "K_INDEXED", "K_INITIALLY", "K_INNER", "K_INSERT", "K_INTO", 
			"K_IS", "K_ISNULL", "K_JOIN", "K_KEY", "K_LEFT", "K_LIKE", "K_LIMIT", 
			"K_MATCH", "K_NEXTVAL", "K_NO", "K_NOT", "K_NOTNULL", "K_NULL", "K_OF", 
			"K_OFFSET", "K_ON", "K_OR", "K_ORDER", "K_OUTER", "K_PRIMARY", "K_QUERY", 
			"K_REFERENCES", "K_REGEXP", "K_RENAME", "K_REPLACE", "K_RESTRICT", "K_RIGHT", 
			"K_ROW", "K_SELECT", "K_SET", "K_TABLE", "K_TYPE", "K_THEN", "K_TO", 
			"K_UNION", "K_UNIQUE", "K_UPDATE", "K_VALUES", "K_VIEW", "K_WHERE", "K_METHODNAME", 
			"K_CLASSNAME", "K_JARPATH", "K_AGGREGATION", "K_PATH", "K_PRINT", "K_VAR", 
			"K_NEW", "K_TRUE", "K_FALSE", "K_FUNCTION", "K_RETURN", "K_WHILE", "K_SWITCH", 
			"K_CASE", "K_BREAK", "K_CONTINUE", "K_DO", "K_EXISTS", "VARNAME", "IDENTIFIER", 
			"NUMERIC_LITERAL", "BIND_PARAMETER", "STRING_LITERAL", "BLOB_LITERAL", 
			"MULTILINE_COMMENT", "LINE_COMMENT", "SPACES", "UNEXPECTED_CHAR"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "SQL.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public SQLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ParseContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(SQLParser.EOF, 0); }
		public List<Sql_stmt_listContext> sql_stmt_list() {
			return getRuleContexts(Sql_stmt_listContext.class);
		}
		public Sql_stmt_listContext sql_stmt_list(int i) {
			return getRuleContext(Sql_stmt_listContext.class,i);
		}
		public List<Function_defContext> function_def() {
			return getRuleContexts(Function_defContext.class);
		}
		public Function_defContext function_def(int i) {
			return getRuleContext(Function_defContext.class,i);
		}
		public List<Function_callContext> function_call() {
			return getRuleContexts(Function_callContext.class);
		}
		public Function_callContext function_call(int i) {
			return getRuleContext(Function_callContext.class,i);
		}
		public List<ErrorContext> error() {
			return getRuleContexts(ErrorContext.class);
		}
		public ErrorContext error(int i) {
			return getRuleContext(ErrorContext.class,i);
		}
		public List<TerminalNode> SCOL() { return getTokens(SQLParser.SCOL); }
		public TerminalNode SCOL(int i) {
			return getToken(SQLParser.SCOL, i);
		}
		public ParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitParse(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitParse(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParseContext parse() throws RecognitionException {
		ParseContext _localctx = new ParseContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_parse);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(227);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SCOL) | (1L << OPEN_PAR) | (1L << K_CREATE))) != 0) || ((((_la - 107)) & ~0x3f) == 0 && ((1L << (_la - 107)) & ((1L << (K_SELECT - 107)) | (1L << (K_VALUES - 107)) | (1L << (K_FUNCTION - 107)) | (1L << (VARNAME - 107)) | (1L << (UNEXPECTED_CHAR - 107)))) != 0)) {
				{
				setState(225);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
				case 1:
					{
					setState(218);
					sql_stmt_list();
					}
					break;
				case 2:
					{
					setState(219);
					function_def();
					}
					break;
				case 3:
					{
					setState(220);
					function_call();
					setState(222);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
					case 1:
						{
						setState(221);
						match(SCOL);
						}
						break;
					}
					}
					break;
				case 4:
					{
					setState(224);
					error();
					}
					break;
				}
				}
				setState(229);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(230);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ErrorContext extends ParserRuleContext {
		public Token UNEXPECTED_CHAR;
		public TerminalNode UNEXPECTED_CHAR() { return getToken(SQLParser.UNEXPECTED_CHAR, 0); }
		public ErrorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_error; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterError(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitError(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitError(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ErrorContext error() throws RecognitionException {
		ErrorContext _localctx = new ErrorContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_error);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(232);
			((ErrorContext)_localctx).UNEXPECTED_CHAR = match(UNEXPECTED_CHAR);

			     throw new RuntimeException("UNEXPECTED_CHAR=" + (((ErrorContext)_localctx).UNEXPECTED_CHAR!=null?((ErrorContext)_localctx).UNEXPECTED_CHAR.getText():null));
			   
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_callContext extends ParserRuleContext {
		public Java_function_nameContext java_function_name() {
			return getRuleContext(Java_function_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public TerminalNode DOT() { return getToken(SQLParser.DOT, 0); }
		public Argument_listContext argument_list() {
			return getRuleContext(Argument_listContext.class,0);
		}
		public Function_callContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_call; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterFunction_call(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitFunction_call(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitFunction_call(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_callContext function_call() throws RecognitionException {
		Function_callContext _localctx = new Function_callContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_function_call);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(238);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				{
				setState(235);
				variable(0);
				setState(236);
				match(DOT);
				}
				break;
			}
			setState(240);
			java_function_name();
			setState(241);
			match(OPEN_PAR);
			{
			setState(243);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << OPEN_PAR) | (1L << STAR) | (1L << PLUS) | (1L << PLUS2) | (1L << MINUS2) | (1L << MINUS))) != 0) || ((((_la - 91)) & ~0x3f) == 0 && ((1L << (_la - 91)) & ((1L << (K_NULL - 91)) | (1L << (K_TRUE - 91)) | (1L << (K_FALSE - 91)) | (1L << (VARNAME - 91)) | (1L << (IDENTIFIER - 91)) | (1L << (NUMERIC_LITERAL - 91)) | (1L << (STRING_LITERAL - 91)))) != 0)) {
				{
				setState(242);
				argument_list();
				}
			}

			}
			setState(245);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Argument_listContext extends ParserRuleContext {
		public List<ArgumentContext> argument() {
			return getRuleContexts(ArgumentContext.class);
		}
		public ArgumentContext argument(int i) {
			return getRuleContext(ArgumentContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Argument_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argument_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterArgument_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitArgument_list(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitArgument_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Argument_listContext argument_list() throws RecognitionException {
		Argument_listContext _localctx = new Argument_listContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_argument_list);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(247);
			argument();
			setState(252);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(248);
				match(COMMA);
				setState(249);
				argument();
				}
				}
				setState(254);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgumentContext extends ParserRuleContext {
		public DeclirationContext decliration() {
			return getRuleContext(DeclirationContext.class,0);
		}
		public ArgumentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argument; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterArgument(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitArgument(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitArgument(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgumentContext argument() throws RecognitionException {
		ArgumentContext _localctx = new ArgumentContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_argument);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(255);
			decliration();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_defContext extends ParserRuleContext {
		public Java_function_nameContext java_function_name() {
			return getRuleContext(Java_function_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode K_FUNCTION() { return getToken(SQLParser.K_FUNCTION, 0); }
		public Parameter_listContext parameter_list() {
			return getRuleContext(Parameter_listContext.class,0);
		}
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public End_bodyContext end_body() {
			return getRuleContext(End_bodyContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(SQLParser.SCOL, 0); }
		public Function_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterFunction_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitFunction_def(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitFunction_def(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_defContext function_def() throws RecognitionException {
		Function_defContext _localctx = new Function_defContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_function_def);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(258);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_FUNCTION) {
				{
				setState(257);
				match(K_FUNCTION);
				}
			}

			setState(260);
			java_function_name();
			setState(261);
			match(OPEN_PAR);
			setState(263);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_VAR) {
				{
				setState(262);
				parameter_list();
				}
			}

			setState(265);
			match(CLOSE_PAR);
			setState(266);
			match(T__0);
			{
			setState(268);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__4) | (1L << OPEN_PAR) | (1L << STAR) | (1L << PLUS) | (1L << PLUS2) | (1L << MINUS2) | (1L << MINUS))) != 0) || ((((_la - 65)) & ~0x3f) == 0 && ((1L << (_la - 65)) & ((1L << (K_FOR - 65)) | (1L << (K_IF - 65)) | (1L << (K_NULL - 65)) | (1L << (K_PRINT - 65)) | (1L << (K_VAR - 65)) | (1L << (K_TRUE - 65)) | (1L << (K_FALSE - 65)))) != 0) || ((((_la - 131)) & ~0x3f) == 0 && ((1L << (_la - 131)) & ((1L << (K_WHILE - 131)) | (1L << (K_SWITCH - 131)) | (1L << (K_DO - 131)) | (1L << (VARNAME - 131)) | (1L << (IDENTIFIER - 131)) | (1L << (NUMERIC_LITERAL - 131)) | (1L << (STRING_LITERAL - 131)))) != 0)) {
				{
				setState(267);
				body(0);
				}
			}

			setState(273);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (((((_la - 130)) & ~0x3f) == 0 && ((1L << (_la - 130)) & ((1L << (K_RETURN - 130)) | (1L << (K_BREAK - 130)) | (1L << (K_CONTINUE - 130)))) != 0)) {
				{
				setState(270);
				end_body();
				setState(271);
				match(SCOL);
				}
			}

			}
			setState(275);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Parameter_listContext extends ParserRuleContext {
		public List<ParameterContext> parameter() {
			return getRuleContexts(ParameterContext.class);
		}
		public ParameterContext parameter(int i) {
			return getRuleContext(ParameterContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public List<Default_varContext> default_var() {
			return getRuleContexts(Default_varContext.class);
		}
		public Default_varContext default_var(int i) {
			return getRuleContext(Default_varContext.class,i);
		}
		public Parameter_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterParameter_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitParameter_list(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitParameter_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Parameter_listContext parameter_list() throws RecognitionException {
		Parameter_listContext _localctx = new Parameter_listContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_parameter_list);
		int _la;
		try {
			int _alt;
			setState(308);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(277);
				parameter();
				setState(282);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(278);
					match(COMMA);
					setState(279);
					parameter();
					}
					}
					setState(284);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(285);
				default_var();
				setState(290);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(286);
					match(COMMA);
					setState(287);
					default_var();
					}
					}
					setState(292);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(293);
				parameter();
				setState(298);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(294);
						match(COMMA);
						setState(295);
						parameter();
						}
						} 
					}
					setState(300);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
				}
				}
				setState(305);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(301);
					match(COMMA);
					setState(302);
					default_var();
					}
					}
					setState(307);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterContext extends ParserRuleContext {
		public Var_defContext var_def() {
			return getRuleContext(Var_defContext.class,0);
		}
		public ParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitParameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitParameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParameterContext parameter() throws RecognitionException {
		ParameterContext _localctx = new ParameterContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_parameter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(310);
			var_def();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Default_varContext extends ParserRuleContext {
		public Var_defContext var_def() {
			return getRuleContext(Var_defContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public DeclirationContext decliration() {
			return getRuleContext(DeclirationContext.class,0);
		}
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public Default_varContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_default_var; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterDefault_var(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitDefault_var(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitDefault_var(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Default_varContext default_var() throws RecognitionException {
		Default_varContext _localctx = new Default_varContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_default_var);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(312);
			var_def();
			setState(313);
			match(ASSIGN);
			setState(316);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__4:
			case OPEN_PAR:
			case STAR:
			case PLUS:
			case PLUS2:
			case MINUS2:
			case MINUS:
			case K_NULL:
			case K_TRUE:
			case K_FALSE:
			case VARNAME:
			case IDENTIFIER:
			case NUMERIC_LITERAL:
			case STRING_LITERAL:
				{
				setState(314);
				decliration();
				}
				break;
			case K_SELECT:
			case K_VALUES:
				{
				setState(315);
				select_stmt();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BodyContext extends ParserRuleContext {
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public List<Java_stmtContext> java_stmt() {
			return getRuleContexts(Java_stmtContext.class);
		}
		public Java_stmtContext java_stmt(int i) {
			return getRuleContext(Java_stmtContext.class,i);
		}
		public BodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_body; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitBody(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BodyContext body() throws RecognitionException {
		return body(0);
	}

	private BodyContext body(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		BodyContext _localctx = new BodyContext(_ctx, _parentState);
		BodyContext _prevctx = _localctx;
		int _startState = 18;
		enterRecursionRule(_localctx, 18, RULE_body, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(332);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__0:
				{
				{
				setState(319);
				match(T__0);
				setState(321);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__4) | (1L << OPEN_PAR) | (1L << STAR) | (1L << PLUS) | (1L << PLUS2) | (1L << MINUS2) | (1L << MINUS))) != 0) || ((((_la - 65)) & ~0x3f) == 0 && ((1L << (_la - 65)) & ((1L << (K_FOR - 65)) | (1L << (K_IF - 65)) | (1L << (K_NULL - 65)) | (1L << (K_PRINT - 65)) | (1L << (K_VAR - 65)) | (1L << (K_TRUE - 65)) | (1L << (K_FALSE - 65)))) != 0) || ((((_la - 131)) & ~0x3f) == 0 && ((1L << (_la - 131)) & ((1L << (K_WHILE - 131)) | (1L << (K_SWITCH - 131)) | (1L << (K_DO - 131)) | (1L << (VARNAME - 131)) | (1L << (IDENTIFIER - 131)) | (1L << (NUMERIC_LITERAL - 131)) | (1L << (STRING_LITERAL - 131)))) != 0)) {
					{
					setState(320);
					body(0);
					}
				}

				setState(323);
				match(T__1);
				}
				}
				break;
			case T__4:
			case OPEN_PAR:
			case STAR:
			case PLUS:
			case PLUS2:
			case MINUS2:
			case MINUS:
			case K_FOR:
			case K_IF:
			case K_NULL:
			case K_PRINT:
			case K_VAR:
			case K_TRUE:
			case K_FALSE:
			case K_WHILE:
			case K_SWITCH:
			case K_DO:
			case VARNAME:
			case IDENTIFIER:
			case NUMERIC_LITERAL:
			case STRING_LITERAL:
				{
				setState(325); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(324);
						java_stmt();
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(327); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				setState(330);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
				case 1:
					{
					setState(329);
					body(0);
					}
					break;
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(342);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new BodyContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_body);
					setState(334);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(336); 
					_errHandler.sync(this);
					_alt = 1;
					do {
						switch (_alt) {
						case 1:
							{
							{
							setState(335);
							java_stmt();
							}
							}
							break;
						default:
							throw new NoViableAltException(this);
						}
						setState(338); 
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,20,_ctx);
					} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
					}
					} 
				}
				setState(344);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Java_stmtContext extends ParserRuleContext {
		public Need_scolContext need_scol() {
			return getRuleContext(Need_scolContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(SQLParser.SCOL, 0); }
		public Not_need_scolContext not_need_scol() {
			return getRuleContext(Not_need_scolContext.class,0);
		}
		public Java_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_java_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterJava_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitJava_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitJava_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Java_stmtContext java_stmt() throws RecognitionException {
		Java_stmtContext _localctx = new Java_stmtContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_java_stmt);
		try {
			setState(349);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__4:
			case OPEN_PAR:
			case STAR:
			case PLUS:
			case PLUS2:
			case MINUS2:
			case MINUS:
			case K_NULL:
			case K_PRINT:
			case K_VAR:
			case K_TRUE:
			case K_FALSE:
			case K_DO:
			case VARNAME:
			case IDENTIFIER:
			case NUMERIC_LITERAL:
			case STRING_LITERAL:
				enterOuterAlt(_localctx, 1);
				{
				setState(345);
				need_scol();
				setState(346);
				match(SCOL);
				}
				break;
			case K_FOR:
			case K_IF:
			case K_WHILE:
			case K_SWITCH:
				enterOuterAlt(_localctx, 2);
				{
				setState(348);
				not_need_scol();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Not_need_scolContext extends ParserRuleContext {
		public Switch_stmtContext switch_stmt() {
			return getRuleContext(Switch_stmtContext.class,0);
		}
		public For_stmtContext for_stmt() {
			return getRuleContext(For_stmtContext.class,0);
		}
		public If_stmtContext if_stmt() {
			return getRuleContext(If_stmtContext.class,0);
		}
		public While_stmtContext while_stmt() {
			return getRuleContext(While_stmtContext.class,0);
		}
		public Not_need_scolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_not_need_scol; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterNot_need_scol(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitNot_need_scol(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitNot_need_scol(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Not_need_scolContext not_need_scol() throws RecognitionException {
		Not_need_scolContext _localctx = new Not_need_scolContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_not_need_scol);
		try {
			setState(355);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_SWITCH:
				enterOuterAlt(_localctx, 1);
				{
				setState(351);
				switch_stmt();
				}
				break;
			case K_FOR:
				enterOuterAlt(_localctx, 2);
				{
				setState(352);
				for_stmt();
				}
				break;
			case K_IF:
				enterOuterAlt(_localctx, 3);
				{
				setState(353);
				if_stmt();
				}
				break;
			case K_WHILE:
				enterOuterAlt(_localctx, 4);
				{
				setState(354);
				while_stmt();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Need_scolContext extends ParserRuleContext {
		public Var_defContext var_def() {
			return getRuleContext(Var_defContext.class,0);
		}
		public Default_varContext default_var() {
			return getRuleContext(Default_varContext.class,0);
		}
		public Var_decelerationContext var_deceleration() {
			return getRuleContext(Var_decelerationContext.class,0);
		}
		public Multivar_defContext multivar_def() {
			return getRuleContext(Multivar_defContext.class,0);
		}
		public Function_callContext function_call() {
			return getRuleContext(Function_callContext.class,0);
		}
		public PrintContext print() {
			return getRuleContext(PrintContext.class,0);
		}
		public Incriment_stmtContext incriment_stmt() {
			return getRuleContext(Incriment_stmtContext.class,0);
		}
		public If_inlineContext if_inline() {
			return getRuleContext(If_inlineContext.class,0);
		}
		public Do_while_stmtContext do_while_stmt() {
			return getRuleContext(Do_while_stmtContext.class,0);
		}
		public Need_scolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_need_scol; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterNeed_scol(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitNeed_scol(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitNeed_scol(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Need_scolContext need_scol() throws RecognitionException {
		Need_scolContext _localctx = new Need_scolContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_need_scol);
		try {
			setState(366);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(357);
				var_def();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(358);
				default_var();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(359);
				var_deceleration();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(360);
				multivar_def();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(361);
				function_call();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(362);
				print();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(363);
				incriment_stmt();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(364);
				if_inline();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(365);
				do_while_stmt();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Incriment_stmtContext extends ParserRuleContext {
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public Incriment_opContext incriment_op() {
			return getRuleContext(Incriment_opContext.class,0);
		}
		public Incriment_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_incriment_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterIncriment_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitIncriment_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitIncriment_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Incriment_stmtContext incriment_stmt() throws RecognitionException {
		Incriment_stmtContext _localctx = new Incriment_stmtContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_incriment_stmt);
		try {
			setState(374);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OPEN_PAR:
			case VARNAME:
				enterOuterAlt(_localctx, 1);
				{
				setState(368);
				variable(0);
				setState(369);
				incriment_op();
				}
				break;
			case PLUS2:
			case MINUS2:
				enterOuterAlt(_localctx, 2);
				{
				setState(371);
				incriment_op();
				setState(372);
				variable(0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrintContext extends ParserRuleContext {
		public TerminalNode K_PRINT() { return getToken(SQLParser.K_PRINT, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public List<Java_valueContext> java_value() {
			return getRuleContexts(Java_valueContext.class);
		}
		public Java_valueContext java_value(int i) {
			return getRuleContext(Java_valueContext.class,i);
		}
		public List<TerminalNode> PLUS() { return getTokens(SQLParser.PLUS); }
		public TerminalNode PLUS(int i) {
			return getToken(SQLParser.PLUS, i);
		}
		public PrintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_print; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterPrint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitPrint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitPrint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrintContext print() throws RecognitionException {
		PrintContext _localctx = new PrintContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_print);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(376);
			match(K_PRINT);
			setState(377);
			match(OPEN_PAR);
			setState(386);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OPEN_PAR) | (1L << STAR) | (1L << PLUS) | (1L << MINUS))) != 0) || ((((_la - 91)) & ~0x3f) == 0 && ((1L << (_la - 91)) & ((1L << (K_NULL - 91)) | (1L << (K_TRUE - 91)) | (1L << (K_FALSE - 91)) | (1L << (VARNAME - 91)) | (1L << (IDENTIFIER - 91)) | (1L << (NUMERIC_LITERAL - 91)) | (1L << (STRING_LITERAL - 91)))) != 0)) {
				{
				{
				setState(378);
				java_value();
				}
				setState(383);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==PLUS) {
					{
					{
					setState(379);
					match(PLUS);
					setState(380);
					java_value();
					}
					}
					setState(385);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(388);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class End_bodyContext extends ParserRuleContext {
		public TerminalNode K_BREAK() { return getToken(SQLParser.K_BREAK, 0); }
		public TerminalNode K_CONTINUE() { return getToken(SQLParser.K_CONTINUE, 0); }
		public Return_stmtContext return_stmt() {
			return getRuleContext(Return_stmtContext.class,0);
		}
		public End_bodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_end_body; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterEnd_body(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitEnd_body(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitEnd_body(this);
			else return visitor.visitChildren(this);
		}
	}

	public final End_bodyContext end_body() throws RecognitionException {
		End_bodyContext _localctx = new End_bodyContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_end_body);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(393);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_BREAK:
				{
				setState(390);
				match(K_BREAK);
				}
				break;
			case K_CONTINUE:
				{
				setState(391);
				match(K_CONTINUE);
				}
				break;
			case K_RETURN:
				{
				setState(392);
				return_stmt();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Base_bodyContext extends ParserRuleContext {
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public End_bodyContext end_body() {
			return getRuleContext(End_bodyContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(SQLParser.SCOL, 0); }
		public Java_stmtContext java_stmt() {
			return getRuleContext(Java_stmtContext.class,0);
		}
		public Base_bodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_base_body; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterBase_body(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitBase_body(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitBase_body(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Base_bodyContext base_body() throws RecognitionException {
		Base_bodyContext _localctx = new Base_bodyContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_base_body);
		int _la;
		try {
			setState(411);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__0:
				enterOuterAlt(_localctx, 1);
				{
				setState(395);
				match(T__0);
				{
				setState(397);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__4) | (1L << OPEN_PAR) | (1L << STAR) | (1L << PLUS) | (1L << PLUS2) | (1L << MINUS2) | (1L << MINUS))) != 0) || ((((_la - 65)) & ~0x3f) == 0 && ((1L << (_la - 65)) & ((1L << (K_FOR - 65)) | (1L << (K_IF - 65)) | (1L << (K_NULL - 65)) | (1L << (K_PRINT - 65)) | (1L << (K_VAR - 65)) | (1L << (K_TRUE - 65)) | (1L << (K_FALSE - 65)))) != 0) || ((((_la - 131)) & ~0x3f) == 0 && ((1L << (_la - 131)) & ((1L << (K_WHILE - 131)) | (1L << (K_SWITCH - 131)) | (1L << (K_DO - 131)) | (1L << (VARNAME - 131)) | (1L << (IDENTIFIER - 131)) | (1L << (NUMERIC_LITERAL - 131)) | (1L << (STRING_LITERAL - 131)))) != 0)) {
					{
					setState(396);
					body(0);
					}
				}

				setState(402);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (((((_la - 130)) & ~0x3f) == 0 && ((1L << (_la - 130)) & ((1L << (K_RETURN - 130)) | (1L << (K_BREAK - 130)) | (1L << (K_CONTINUE - 130)))) != 0)) {
					{
					setState(399);
					end_body();
					setState(400);
					match(SCOL);
					}
				}

				}
				setState(404);
				match(T__1);
				}
				break;
			case T__4:
			case OPEN_PAR:
			case STAR:
			case PLUS:
			case PLUS2:
			case MINUS2:
			case MINUS:
			case K_FOR:
			case K_IF:
			case K_NULL:
			case K_PRINT:
			case K_VAR:
			case K_TRUE:
			case K_FALSE:
			case K_RETURN:
			case K_WHILE:
			case K_SWITCH:
			case K_BREAK:
			case K_CONTINUE:
			case K_DO:
			case VARNAME:
			case IDENTIFIER:
			case NUMERIC_LITERAL:
			case STRING_LITERAL:
				enterOuterAlt(_localctx, 2);
				{
				setState(409);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__4:
				case OPEN_PAR:
				case STAR:
				case PLUS:
				case PLUS2:
				case MINUS2:
				case MINUS:
				case K_FOR:
				case K_IF:
				case K_NULL:
				case K_PRINT:
				case K_VAR:
				case K_TRUE:
				case K_FALSE:
				case K_WHILE:
				case K_SWITCH:
				case K_DO:
				case VARNAME:
				case IDENTIFIER:
				case NUMERIC_LITERAL:
				case STRING_LITERAL:
					{
					setState(405);
					java_stmt();
					}
					break;
				case K_RETURN:
				case K_BREAK:
				case K_CONTINUE:
					{
					{
					setState(406);
					end_body();
					setState(407);
					match(SCOL);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class For_stmtContext extends ParserRuleContext {
		public For_loopContext for_loop() {
			return getRuleContext(For_loopContext.class,0);
		}
		public For_each_loopContext for_each_loop() {
			return getRuleContext(For_each_loopContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(SQLParser.SCOL, 0); }
		public Base_bodyContext base_body() {
			return getRuleContext(Base_bodyContext.class,0);
		}
		public For_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_for_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterFor_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitFor_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitFor_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final For_stmtContext for_stmt() throws RecognitionException {
		For_stmtContext _localctx = new For_stmtContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_for_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(415);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,33,_ctx) ) {
			case 1:
				{
				setState(413);
				for_loop();
				}
				break;
			case 2:
				{
				setState(414);
				for_each_loop();
				}
				break;
			}
			setState(419);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__0:
			case T__4:
			case OPEN_PAR:
			case STAR:
			case PLUS:
			case PLUS2:
			case MINUS2:
			case MINUS:
			case K_FOR:
			case K_IF:
			case K_NULL:
			case K_PRINT:
			case K_VAR:
			case K_TRUE:
			case K_FALSE:
			case K_RETURN:
			case K_WHILE:
			case K_SWITCH:
			case K_BREAK:
			case K_CONTINUE:
			case K_DO:
			case VARNAME:
			case IDENTIFIER:
			case NUMERIC_LITERAL:
			case STRING_LITERAL:
				{
				{
				setState(417);
				base_body();
				}
				}
				break;
			case SCOL:
				{
				setState(418);
				match(SCOL);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class For_loopContext extends ParserRuleContext {
		public TerminalNode K_FOR() { return getToken(SQLParser.K_FOR, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<VariableContext> variable() {
			return getRuleContexts(VariableContext.class);
		}
		public VariableContext variable(int i) {
			return getRuleContext(VariableContext.class,i);
		}
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public List<Num_expContext> num_exp() {
			return getRuleContexts(Num_expContext.class);
		}
		public Num_expContext num_exp(int i) {
			return getRuleContext(Num_expContext.class,i);
		}
		public List<TerminalNode> SCOL() { return getTokens(SQLParser.SCOL); }
		public TerminalNode SCOL(int i) {
			return getToken(SQLParser.SCOL, i);
		}
		public Comparison_opContext comparison_op() {
			return getRuleContext(Comparison_opContext.class,0);
		}
		public Assign_opContext assign_op() {
			return getRuleContext(Assign_opContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode K_VAR() { return getToken(SQLParser.K_VAR, 0); }
		public For_loopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_for_loop; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterFor_loop(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitFor_loop(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitFor_loop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final For_loopContext for_loop() throws RecognitionException {
		For_loopContext _localctx = new For_loopContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_for_loop);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(421);
			match(K_FOR);
			setState(422);
			match(OPEN_PAR);
			setState(424);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_VAR) {
				{
				setState(423);
				match(K_VAR);
				}
			}

			setState(426);
			variable(0);
			setState(427);
			match(ASSIGN);
			setState(428);
			num_exp(0);
			setState(429);
			match(SCOL);
			setState(430);
			variable(0);
			setState(431);
			comparison_op();
			setState(432);
			num_exp(0);
			setState(433);
			match(SCOL);
			setState(434);
			variable(0);
			setState(435);
			assign_op();
			setState(436);
			num_exp(0);
			setState(437);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class For_each_loopContext extends ParserRuleContext {
		public TerminalNode K_FOR() { return getToken(SQLParser.K_FOR, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public Java_var_nameContext java_var_name() {
			return getRuleContext(Java_var_nameContext.class,0);
		}
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode K_VAR() { return getToken(SQLParser.K_VAR, 0); }
		public For_each_loopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_for_each_loop; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterFor_each_loop(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitFor_each_loop(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitFor_each_loop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final For_each_loopContext for_each_loop() throws RecognitionException {
		For_each_loopContext _localctx = new For_each_loopContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_for_each_loop);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(439);
			match(K_FOR);
			setState(440);
			match(OPEN_PAR);
			setState(442);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_VAR) {
				{
				setState(441);
				match(K_VAR);
				}
			}

			setState(444);
			java_var_name();
			setState(445);
			match(T__2);
			setState(446);
			variable(0);
			setState(447);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Switch_stmtContext extends ParserRuleContext {
		public TerminalNode K_SWITCH() { return getToken(SQLParser.K_SWITCH, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public Num_expContext num_exp() {
			return getRuleContext(Num_expContext.class,0);
		}
		public String_valContext string_val() {
			return getRuleContext(String_valContext.class,0);
		}
		public List<Case_stmtContext> case_stmt() {
			return getRuleContexts(Case_stmtContext.class);
		}
		public Case_stmtContext case_stmt(int i) {
			return getRuleContext(Case_stmtContext.class,i);
		}
		public Default_stmtContext default_stmt() {
			return getRuleContext(Default_stmtContext.class,0);
		}
		public Switch_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_switch_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterSwitch_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitSwitch_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitSwitch_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Switch_stmtContext switch_stmt() throws RecognitionException {
		Switch_stmtContext _localctx = new Switch_stmtContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_switch_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(449);
			match(K_SWITCH);
			setState(450);
			match(OPEN_PAR);
			setState(454);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,37,_ctx) ) {
			case 1:
				{
				setState(451);
				variable(0);
				}
				break;
			case 2:
				{
				setState(452);
				num_exp(0);
				}
				break;
			case 3:
				{
				setState(453);
				string_val();
				}
				break;
			}
			setState(456);
			match(CLOSE_PAR);
			setState(457);
			match(T__0);
			setState(461);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==K_CASE) {
				{
				{
				setState(458);
				case_stmt();
				}
				}
				setState(463);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(465);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_DEFAULT) {
				{
				setState(464);
				default_stmt();
				}
			}

			setState(467);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Case_stmtContext extends ParserRuleContext {
		public TerminalNode K_CASE() { return getToken(SQLParser.K_CASE, 0); }
		public Num_expContext num_exp() {
			return getRuleContext(Num_expContext.class,0);
		}
		public String_valContext string_val() {
			return getRuleContext(String_valContext.class,0);
		}
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public End_bodyContext end_body() {
			return getRuleContext(End_bodyContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(SQLParser.SCOL, 0); }
		public Case_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_case_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCase_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCase_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitCase_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Case_stmtContext case_stmt() throws RecognitionException {
		Case_stmtContext _localctx = new Case_stmtContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_case_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(469);
			match(K_CASE);
			setState(472);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OPEN_PAR:
			case STAR:
			case PLUS:
			case PLUS2:
			case MINUS2:
			case MINUS:
			case VARNAME:
			case NUMERIC_LITERAL:
				{
				setState(470);
				num_exp(0);
				}
				break;
			case IDENTIFIER:
			case STRING_LITERAL:
				{
				setState(471);
				string_val();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(474);
			match(T__2);
			setState(493);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,45,_ctx) ) {
			case 1:
				{
				setState(475);
				match(T__0);
				{
				setState(477);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__4) | (1L << OPEN_PAR) | (1L << STAR) | (1L << PLUS) | (1L << PLUS2) | (1L << MINUS2) | (1L << MINUS))) != 0) || ((((_la - 65)) & ~0x3f) == 0 && ((1L << (_la - 65)) & ((1L << (K_FOR - 65)) | (1L << (K_IF - 65)) | (1L << (K_NULL - 65)) | (1L << (K_PRINT - 65)) | (1L << (K_VAR - 65)) | (1L << (K_TRUE - 65)) | (1L << (K_FALSE - 65)))) != 0) || ((((_la - 131)) & ~0x3f) == 0 && ((1L << (_la - 131)) & ((1L << (K_WHILE - 131)) | (1L << (K_SWITCH - 131)) | (1L << (K_DO - 131)) | (1L << (VARNAME - 131)) | (1L << (IDENTIFIER - 131)) | (1L << (NUMERIC_LITERAL - 131)) | (1L << (STRING_LITERAL - 131)))) != 0)) {
					{
					setState(476);
					body(0);
					}
				}

				setState(482);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (((((_la - 130)) & ~0x3f) == 0 && ((1L << (_la - 130)) & ((1L << (K_RETURN - 130)) | (1L << (K_BREAK - 130)) | (1L << (K_CONTINUE - 130)))) != 0)) {
					{
					setState(479);
					end_body();
					setState(480);
					match(SCOL);
					}
				}

				}
				setState(484);
				match(T__1);
				}
				break;
			case 2:
				{
				{
				setState(486);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__4) | (1L << OPEN_PAR) | (1L << STAR) | (1L << PLUS) | (1L << PLUS2) | (1L << MINUS2) | (1L << MINUS))) != 0) || ((((_la - 65)) & ~0x3f) == 0 && ((1L << (_la - 65)) & ((1L << (K_FOR - 65)) | (1L << (K_IF - 65)) | (1L << (K_NULL - 65)) | (1L << (K_PRINT - 65)) | (1L << (K_VAR - 65)) | (1L << (K_TRUE - 65)) | (1L << (K_FALSE - 65)))) != 0) || ((((_la - 131)) & ~0x3f) == 0 && ((1L << (_la - 131)) & ((1L << (K_WHILE - 131)) | (1L << (K_SWITCH - 131)) | (1L << (K_DO - 131)) | (1L << (VARNAME - 131)) | (1L << (IDENTIFIER - 131)) | (1L << (NUMERIC_LITERAL - 131)) | (1L << (STRING_LITERAL - 131)))) != 0)) {
					{
					setState(485);
					body(0);
					}
				}

				setState(491);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (((((_la - 130)) & ~0x3f) == 0 && ((1L << (_la - 130)) & ((1L << (K_RETURN - 130)) | (1L << (K_BREAK - 130)) | (1L << (K_CONTINUE - 130)))) != 0)) {
					{
					setState(488);
					end_body();
					setState(489);
					match(SCOL);
					}
				}

				}
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Default_stmtContext extends ParserRuleContext {
		public TerminalNode K_DEFAULT() { return getToken(SQLParser.K_DEFAULT, 0); }
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public End_bodyContext end_body() {
			return getRuleContext(End_bodyContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(SQLParser.SCOL, 0); }
		public Default_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_default_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterDefault_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitDefault_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitDefault_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Default_stmtContext default_stmt() throws RecognitionException {
		Default_stmtContext _localctx = new Default_stmtContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_default_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(495);
			match(K_DEFAULT);
			setState(496);
			match(T__2);
			setState(515);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,50,_ctx) ) {
			case 1:
				{
				setState(497);
				match(T__0);
				{
				setState(499);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__4) | (1L << OPEN_PAR) | (1L << STAR) | (1L << PLUS) | (1L << PLUS2) | (1L << MINUS2) | (1L << MINUS))) != 0) || ((((_la - 65)) & ~0x3f) == 0 && ((1L << (_la - 65)) & ((1L << (K_FOR - 65)) | (1L << (K_IF - 65)) | (1L << (K_NULL - 65)) | (1L << (K_PRINT - 65)) | (1L << (K_VAR - 65)) | (1L << (K_TRUE - 65)) | (1L << (K_FALSE - 65)))) != 0) || ((((_la - 131)) & ~0x3f) == 0 && ((1L << (_la - 131)) & ((1L << (K_WHILE - 131)) | (1L << (K_SWITCH - 131)) | (1L << (K_DO - 131)) | (1L << (VARNAME - 131)) | (1L << (IDENTIFIER - 131)) | (1L << (NUMERIC_LITERAL - 131)) | (1L << (STRING_LITERAL - 131)))) != 0)) {
					{
					setState(498);
					body(0);
					}
				}

				setState(504);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (((((_la - 130)) & ~0x3f) == 0 && ((1L << (_la - 130)) & ((1L << (K_RETURN - 130)) | (1L << (K_BREAK - 130)) | (1L << (K_CONTINUE - 130)))) != 0)) {
					{
					setState(501);
					end_body();
					setState(502);
					match(SCOL);
					}
				}

				}
				setState(506);
				match(T__1);
				}
				break;
			case 2:
				{
				{
				setState(508);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__4) | (1L << OPEN_PAR) | (1L << STAR) | (1L << PLUS) | (1L << PLUS2) | (1L << MINUS2) | (1L << MINUS))) != 0) || ((((_la - 65)) & ~0x3f) == 0 && ((1L << (_la - 65)) & ((1L << (K_FOR - 65)) | (1L << (K_IF - 65)) | (1L << (K_NULL - 65)) | (1L << (K_PRINT - 65)) | (1L << (K_VAR - 65)) | (1L << (K_TRUE - 65)) | (1L << (K_FALSE - 65)))) != 0) || ((((_la - 131)) & ~0x3f) == 0 && ((1L << (_la - 131)) & ((1L << (K_WHILE - 131)) | (1L << (K_SWITCH - 131)) | (1L << (K_DO - 131)) | (1L << (VARNAME - 131)) | (1L << (IDENTIFIER - 131)) | (1L << (NUMERIC_LITERAL - 131)) | (1L << (STRING_LITERAL - 131)))) != 0)) {
					{
					setState(507);
					body(0);
					}
				}

				setState(513);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (((((_la - 130)) & ~0x3f) == 0 && ((1L << (_la - 130)) & ((1L << (K_RETURN - 130)) | (1L << (K_BREAK - 130)) | (1L << (K_CONTINUE - 130)))) != 0)) {
					{
					setState(510);
					end_body();
					setState(511);
					match(SCOL);
					}
				}

				}
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_stmtContext extends ParserRuleContext {
		public TerminalNode K_IF() { return getToken(SQLParser.K_IF, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Condition_expContext condition_exp() {
			return getRuleContext(Condition_expContext.class,0);
		}
		public Base_bodyContext base_body() {
			return getRuleContext(Base_bodyContext.class,0);
		}
		public List<Else_if_stmtContext> else_if_stmt() {
			return getRuleContexts(Else_if_stmtContext.class);
		}
		public Else_if_stmtContext else_if_stmt(int i) {
			return getRuleContext(Else_if_stmtContext.class,i);
		}
		public Else_stmtContext else_stmt() {
			return getRuleContext(Else_stmtContext.class,0);
		}
		public If_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterIf_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitIf_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitIf_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final If_stmtContext if_stmt() throws RecognitionException {
		If_stmtContext _localctx = new If_stmtContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_if_stmt);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(517);
			match(K_IF);
			setState(518);
			match(OPEN_PAR);
			{
			setState(519);
			condition_exp(0);
			}
			setState(520);
			match(CLOSE_PAR);
			{
			setState(521);
			base_body();
			}
			setState(525);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,51,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(522);
					else_if_stmt();
					}
					} 
				}
				setState(527);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,51,_ctx);
			}
			setState(529);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,52,_ctx) ) {
			case 1:
				{
				setState(528);
				else_stmt();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Else_if_stmtContext extends ParserRuleContext {
		public TerminalNode K_ELSE() { return getToken(SQLParser.K_ELSE, 0); }
		public TerminalNode K_IF() { return getToken(SQLParser.K_IF, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Condition_expContext condition_exp() {
			return getRuleContext(Condition_expContext.class,0);
		}
		public Base_bodyContext base_body() {
			return getRuleContext(Base_bodyContext.class,0);
		}
		public Else_if_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_else_if_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterElse_if_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitElse_if_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitElse_if_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Else_if_stmtContext else_if_stmt() throws RecognitionException {
		Else_if_stmtContext _localctx = new Else_if_stmtContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_else_if_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(531);
			match(K_ELSE);
			setState(532);
			match(K_IF);
			setState(533);
			match(OPEN_PAR);
			{
			setState(534);
			condition_exp(0);
			}
			setState(535);
			match(CLOSE_PAR);
			{
			setState(536);
			base_body();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Else_stmtContext extends ParserRuleContext {
		public TerminalNode K_ELSE() { return getToken(SQLParser.K_ELSE, 0); }
		public Base_bodyContext base_body() {
			return getRuleContext(Base_bodyContext.class,0);
		}
		public Else_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_else_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterElse_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitElse_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitElse_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Else_stmtContext else_stmt() throws RecognitionException {
		Else_stmtContext _localctx = new Else_stmtContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_else_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(538);
			match(K_ELSE);
			{
			setState(539);
			base_body();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_inlineContext extends ParserRuleContext {
		public Condition_expContext condition_exp() {
			return getRuleContext(Condition_expContext.class,0);
		}
		public List<Ifinlin_sideContext> ifinlin_side() {
			return getRuleContexts(Ifinlin_sideContext.class);
		}
		public Ifinlin_sideContext ifinlin_side(int i) {
			return getRuleContext(Ifinlin_sideContext.class,i);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public If_inlineContext if_inline() {
			return getRuleContext(If_inlineContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public If_inlineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_inline; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterIf_inline(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitIf_inline(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitIf_inline(this);
			else return visitor.visitChildren(this);
		}
	}

	public final If_inlineContext if_inline() throws RecognitionException {
		If_inlineContext _localctx = new If_inlineContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_if_inline);
		try {
			setState(551);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,53,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(541);
				condition_exp(0);
				}
				setState(542);
				match(T__3);
				{
				setState(543);
				ifinlin_side();
				setState(544);
				match(T__2);
				setState(545);
				ifinlin_side();
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(547);
				match(OPEN_PAR);
				setState(548);
				if_inline();
				setState(549);
				match(CLOSE_PAR);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Ifinlin_sideContext extends ParserRuleContext {
		public Java_valueContext java_value() {
			return getRuleContext(Java_valueContext.class,0);
		}
		public Num_expContext num_exp() {
			return getRuleContext(Num_expContext.class,0);
		}
		public Condition_expContext condition_exp() {
			return getRuleContext(Condition_expContext.class,0);
		}
		public If_inlineContext if_inline() {
			return getRuleContext(If_inlineContext.class,0);
		}
		public Var_decelerationContext var_deceleration() {
			return getRuleContext(Var_decelerationContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public Ifinlin_sideContext ifinlin_side() {
			return getRuleContext(Ifinlin_sideContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Ifinlin_sideContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifinlin_side; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterIfinlin_side(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitIfinlin_side(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitIfinlin_side(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Ifinlin_sideContext ifinlin_side() throws RecognitionException {
		Ifinlin_sideContext _localctx = new Ifinlin_sideContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_ifinlin_side);
		try {
			setState(562);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,54,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(553);
				java_value();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(554);
				num_exp(0);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(555);
				condition_exp(0);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(556);
				if_inline();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(557);
				var_deceleration();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(558);
				match(OPEN_PAR);
				setState(559);
				ifinlin_side();
				setState(560);
				match(CLOSE_PAR);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class While_stmtContext extends ParserRuleContext {
		public TerminalNode K_WHILE() { return getToken(SQLParser.K_WHILE, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public Condition_expContext condition_exp() {
			return getRuleContext(Condition_expContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Base_bodyContext base_body() {
			return getRuleContext(Base_bodyContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(SQLParser.SCOL, 0); }
		public While_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_while_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterWhile_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitWhile_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitWhile_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final While_stmtContext while_stmt() throws RecognitionException {
		While_stmtContext _localctx = new While_stmtContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_while_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(564);
			match(K_WHILE);
			setState(565);
			match(OPEN_PAR);
			setState(566);
			condition_exp(0);
			setState(567);
			match(CLOSE_PAR);
			setState(570);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__0:
			case T__4:
			case OPEN_PAR:
			case STAR:
			case PLUS:
			case PLUS2:
			case MINUS2:
			case MINUS:
			case K_FOR:
			case K_IF:
			case K_NULL:
			case K_PRINT:
			case K_VAR:
			case K_TRUE:
			case K_FALSE:
			case K_RETURN:
			case K_WHILE:
			case K_SWITCH:
			case K_BREAK:
			case K_CONTINUE:
			case K_DO:
			case VARNAME:
			case IDENTIFIER:
			case NUMERIC_LITERAL:
			case STRING_LITERAL:
				{
				setState(568);
				base_body();
				}
				break;
			case SCOL:
				{
				setState(569);
				match(SCOL);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Do_while_stmtContext extends ParserRuleContext {
		public TerminalNode K_DO() { return getToken(SQLParser.K_DO, 0); }
		public TerminalNode K_WHILE() { return getToken(SQLParser.K_WHILE, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public Condition_expContext condition_exp() {
			return getRuleContext(Condition_expContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Base_bodyContext base_body() {
			return getRuleContext(Base_bodyContext.class,0);
		}
		public Do_while_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_do_while_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterDo_while_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitDo_while_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitDo_while_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Do_while_stmtContext do_while_stmt() throws RecognitionException {
		Do_while_stmtContext _localctx = new Do_while_stmtContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_do_while_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(572);
			match(K_DO);
			{
			setState(573);
			base_body();
			}
			setState(574);
			match(K_WHILE);
			setState(575);
			match(OPEN_PAR);
			setState(576);
			condition_exp(0);
			setState(577);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Return_stmtContext extends ParserRuleContext {
		public TerminalNode K_RETURN() { return getToken(SQLParser.K_RETURN, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public Java_valueContext java_value() {
			return getRuleContext(Java_valueContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public If_inlineContext if_inline() {
			return getRuleContext(If_inlineContext.class,0);
		}
		public Num_expContext num_exp() {
			return getRuleContext(Num_expContext.class,0);
		}
		public Condition_expContext condition_exp() {
			return getRuleContext(Condition_expContext.class,0);
		}
		public Return_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_return_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterReturn_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitReturn_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitReturn_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Return_stmtContext return_stmt() throws RecognitionException {
		Return_stmtContext _localctx = new Return_stmtContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_return_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(579);
			match(K_RETURN);
			setState(590);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,57,_ctx) ) {
			case 1:
				{
				setState(580);
				match(OPEN_PAR);
				setState(581);
				java_value();
				setState(582);
				match(CLOSE_PAR);
				}
				break;
			case 2:
				{
				setState(588);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,56,_ctx) ) {
				case 1:
					{
					setState(584);
					java_value();
					}
					break;
				case 2:
					{
					setState(585);
					if_inline();
					}
					break;
				case 3:
					{
					setState(586);
					num_exp(0);
					}
					break;
				case 4:
					{
					setState(587);
					condition_exp(0);
					}
					break;
				}
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sql_stmt_listContext extends ParserRuleContext {
		public List<Sql_stmtContext> sql_stmt() {
			return getRuleContexts(Sql_stmtContext.class);
		}
		public Sql_stmtContext sql_stmt(int i) {
			return getRuleContext(Sql_stmtContext.class,i);
		}
		public List<TerminalNode> SCOL() { return getTokens(SQLParser.SCOL); }
		public TerminalNode SCOL(int i) {
			return getToken(SQLParser.SCOL, i);
		}
		public Sql_stmt_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sql_stmt_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterSql_stmt_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitSql_stmt_list(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitSql_stmt_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sql_stmt_listContext sql_stmt_list() throws RecognitionException {
		Sql_stmt_listContext _localctx = new Sql_stmt_listContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_sql_stmt_list);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(595);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SCOL) {
				{
				{
				setState(592);
				match(SCOL);
				}
				}
				setState(597);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(598);
			sql_stmt();
			setState(607);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,60,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(600); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(599);
						match(SCOL);
						}
						}
						setState(602); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==SCOL );
					setState(604);
					sql_stmt();
					}
					} 
				}
				setState(609);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,60,_ctx);
			}
			setState(613);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,61,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(610);
					match(SCOL);
					}
					} 
				}
				setState(615);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,61,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sql_stmtContext extends ParserRuleContext {
		public Create_table_stmtContext create_table_stmt() {
			return getRuleContext(Create_table_stmtContext.class,0);
		}
		public Create_type_stmtContext create_type_stmt() {
			return getRuleContext(Create_type_stmtContext.class,0);
		}
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public Create_aggregation_functionContext create_aggregation_function() {
			return getRuleContext(Create_aggregation_functionContext.class,0);
		}
		public Sql_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sql_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterSql_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitSql_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitSql_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sql_stmtContext sql_stmt() throws RecognitionException {
		Sql_stmtContext _localctx = new Sql_stmtContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_sql_stmt);
		try {
			setState(620);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,62,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(616);
				create_table_stmt();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(617);
				create_type_stmt();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(618);
				select_stmt();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(619);
				create_aggregation_function();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Create_table_stmtContext extends ParserRuleContext {
		public TerminalNode K_CREATE() { return getToken(SQLParser.K_CREATE, 0); }
		public TerminalNode K_TABLE() { return getToken(SQLParser.K_TABLE, 0); }
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public TerminalNode K_TYPE() { return getToken(SQLParser.K_TYPE, 0); }
		public List<TerminalNode> ASSIGN() { return getTokens(SQLParser.ASSIGN); }
		public TerminalNode ASSIGN(int i) {
			return getToken(SQLParser.ASSIGN, i);
		}
		public Table_typeContext table_type() {
			return getRuleContext(Table_typeContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public TerminalNode K_PATH() { return getToken(SQLParser.K_PATH, 0); }
		public Table_pathContext table_path() {
			return getRuleContext(Table_pathContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<Column_defContext> column_def() {
			return getRuleContexts(Column_defContext.class);
		}
		public Column_defContext column_def(int i) {
			return getRuleContext(Column_defContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode K_IF() { return getToken(SQLParser.K_IF, 0); }
		public TerminalNode K_NOT() { return getToken(SQLParser.K_NOT, 0); }
		public TerminalNode K_EXISTS() { return getToken(SQLParser.K_EXISTS, 0); }
		public Create_table_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_create_table_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCreate_table_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCreate_table_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitCreate_table_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Create_table_stmtContext create_table_stmt() throws RecognitionException {
		Create_table_stmtContext _localctx = new Create_table_stmtContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_create_table_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(622);
			match(K_CREATE);
			setState(623);
			match(K_TABLE);
			setState(627);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,63,_ctx) ) {
			case 1:
				{
				setState(624);
				match(K_IF);
				setState(625);
				match(K_NOT);
				setState(626);
				match(K_EXISTS);
				}
				break;
			}
			setState(629);
			table_name();
			{
			setState(630);
			match(OPEN_PAR);
			setState(631);
			column_def();
			setState(636);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(632);
				match(COMMA);
				setState(633);
				column_def();
				}
				}
				setState(638);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(639);
			match(CLOSE_PAR);
			}
			setState(641);
			match(K_TYPE);
			setState(642);
			match(ASSIGN);
			setState(643);
			table_type();
			setState(644);
			match(COMMA);
			setState(645);
			match(K_PATH);
			setState(646);
			match(ASSIGN);
			setState(647);
			table_path();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_typeContext extends ParserRuleContext {
		public String_valContext string_val() {
			return getRuleContext(String_valContext.class,0);
		}
		public Table_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTable_type(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTable_type(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTable_type(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_typeContext table_type() throws RecognitionException {
		Table_typeContext _localctx = new Table_typeContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_table_type);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(649);
			string_val();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_pathContext extends ParserRuleContext {
		public String_valContext string_val() {
			return getRuleContext(String_valContext.class,0);
		}
		public Table_pathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_path; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTable_path(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTable_path(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTable_path(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_pathContext table_path() throws RecognitionException {
		Table_pathContext _localctx = new Table_pathContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_table_path);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(651);
			string_val();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Create_type_stmtContext extends ParserRuleContext {
		public TerminalNode K_CREATE() { return getToken(SQLParser.K_CREATE, 0); }
		public TerminalNode K_TYPE() { return getToken(SQLParser.K_TYPE, 0); }
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<Column_defContext> column_def() {
			return getRuleContexts(Column_defContext.class);
		}
		public Column_defContext column_def(int i) {
			return getRuleContext(Column_defContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode K_IF() { return getToken(SQLParser.K_IF, 0); }
		public TerminalNode K_NOT() { return getToken(SQLParser.K_NOT, 0); }
		public TerminalNode K_EXISTS() { return getToken(SQLParser.K_EXISTS, 0); }
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Create_type_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_create_type_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCreate_type_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCreate_type_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitCreate_type_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Create_type_stmtContext create_type_stmt() throws RecognitionException {
		Create_type_stmtContext _localctx = new Create_type_stmtContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_create_type_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(653);
			match(K_CREATE);
			setState(654);
			match(K_TYPE);
			setState(658);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,65,_ctx) ) {
			case 1:
				{
				setState(655);
				match(K_IF);
				setState(656);
				match(K_NOT);
				setState(657);
				match(K_EXISTS);
				}
				break;
			}
			setState(660);
			table_name();
			setState(661);
			match(OPEN_PAR);
			setState(662);
			column_def();
			setState(667);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(663);
				match(COMMA);
				setState(664);
				column_def();
				}
				}
				setState(669);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(670);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Select_stmtContext extends ParserRuleContext {
		public Select_coreContext select_core() {
			return getRuleContext(Select_coreContext.class,0);
		}
		public TerminalNode K_ORDER() { return getToken(SQLParser.K_ORDER, 0); }
		public TerminalNode K_BY() { return getToken(SQLParser.K_BY, 0); }
		public List<Ordering_termContext> ordering_term() {
			return getRuleContexts(Ordering_termContext.class);
		}
		public Ordering_termContext ordering_term(int i) {
			return getRuleContext(Ordering_termContext.class,i);
		}
		public TerminalNode K_LIMIT() { return getToken(SQLParser.K_LIMIT, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public TerminalNode K_OFFSET() { return getToken(SQLParser.K_OFFSET, 0); }
		public Select_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_select_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterSelect_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitSelect_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitSelect_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Select_stmtContext select_stmt() throws RecognitionException {
		Select_stmtContext _localctx = new Select_stmtContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_select_stmt);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(672);
			select_core();
			setState(683);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_ORDER) {
				{
				setState(673);
				match(K_ORDER);
				setState(674);
				match(K_BY);
				setState(675);
				ordering_term();
				setState(680);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,67,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(676);
						match(COMMA);
						setState(677);
						ordering_term();
						}
						} 
					}
					setState(682);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,67,_ctx);
				}
				}
			}

			setState(691);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_LIMIT) {
				{
				setState(685);
				match(K_LIMIT);
				setState(686);
				expr(0);
				setState(689);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,69,_ctx) ) {
				case 1:
					{
					setState(687);
					_la = _input.LA(1);
					if ( !(_la==COMMA || _la==K_OFFSET) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(688);
					expr(0);
					}
					break;
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_defContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public Type_nameContext type_name() {
			return getRuleContext(Type_nameContext.class,0);
		}
		public Column_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterColumn_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitColumn_def(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitColumn_def(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_defContext column_def() throws RecognitionException {
		Column_defContext _localctx = new Column_defContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_column_def);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(693);
			column_name();
			{
			setState(694);
			type_name();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_nameContext extends ParserRuleContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<Signed_numberContext> signed_number() {
			return getRuleContexts(Signed_numberContext.class);
		}
		public Signed_numberContext signed_number(int i) {
			return getRuleContext(Signed_numberContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode COMMA() { return getToken(SQLParser.COMMA, 0); }
		public List<Any_nameContext> any_name() {
			return getRuleContexts(Any_nameContext.class);
		}
		public Any_nameContext any_name(int i) {
			return getRuleContext(Any_nameContext.class,i);
		}
		public Type_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterType_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitType_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitType_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Type_nameContext type_name() throws RecognitionException {
		Type_nameContext _localctx = new Type_nameContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_type_name);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(696);
			name();
			setState(716);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,74,_ctx) ) {
			case 1:
				{
				setState(697);
				match(OPEN_PAR);
				setState(698);
				signed_number();
				setState(700);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OPEN_PAR) | (1L << K_ACTION) | (1L << K_ADD) | (1L << K_ALL) | (1L << K_ALTER) | (1L << K_AND) | (1L << K_AS) | (1L << K_ASC) | (1L << K_AUTOINCREMENT) | (1L << K_BY) | (1L << K_CASCADE) | (1L << K_CHECK) | (1L << K_COLLATE) | (1L << K_COLUMN) | (1L << K_CONSTRAINT) | (1L << K_CREATE) | (1L << K_CROSS) | (1L << K_CURRENT_DATE) | (1L << K_CURRENT_TIME) | (1L << K_CURRENT_TIMESTAMP) | (1L << K_DATABASE) | (1L << K_DEFAULT) | (1L << K_DEFERRABLE) | (1L << K_DEFERRED) | (1L << K_DELETE) | (1L << K_DESC) | (1L << K_DISTINCT) | (1L << K_DROP) | (1L << K_EACH) | (1L << K_ELSE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (K_ENABLE - 64)) | (1L << (K_FOR - 64)) | (1L << (K_FOREIGN - 64)) | (1L << (K_FROM - 64)) | (1L << (K_GLOB - 64)) | (1L << (K_GROUP - 64)) | (1L << (K_HAVING - 64)) | (1L << (K_IF - 64)) | (1L << (K_IMMEDIATE - 64)) | (1L << (K_IN - 64)) | (1L << (K_INDEXED - 64)) | (1L << (K_INITIALLY - 64)) | (1L << (K_INNER - 64)) | (1L << (K_INSERT - 64)) | (1L << (K_INTO - 64)) | (1L << (K_IS - 64)) | (1L << (K_ISNULL - 64)) | (1L << (K_JOIN - 64)) | (1L << (K_KEY - 64)) | (1L << (K_LEFT - 64)) | (1L << (K_LIKE - 64)) | (1L << (K_LIMIT - 64)) | (1L << (K_MATCH - 64)) | (1L << (K_NEXTVAL - 64)) | (1L << (K_NO - 64)) | (1L << (K_NOT - 64)) | (1L << (K_NOTNULL - 64)) | (1L << (K_NULL - 64)) | (1L << (K_OF - 64)) | (1L << (K_OFFSET - 64)) | (1L << (K_ON - 64)) | (1L << (K_OR - 64)) | (1L << (K_ORDER - 64)) | (1L << (K_OUTER - 64)) | (1L << (K_PRIMARY - 64)) | (1L << (K_QUERY - 64)) | (1L << (K_REFERENCES - 64)) | (1L << (K_REGEXP - 64)) | (1L << (K_RENAME - 64)) | (1L << (K_REPLACE - 64)) | (1L << (K_RESTRICT - 64)) | (1L << (K_RIGHT - 64)) | (1L << (K_ROW - 64)) | (1L << (K_SELECT - 64)) | (1L << (K_SET - 64)) | (1L << (K_TABLE - 64)) | (1L << (K_THEN - 64)) | (1L << (K_TO - 64)) | (1L << (K_UNION - 64)) | (1L << (K_UNIQUE - 64)) | (1L << (K_UPDATE - 64)) | (1L << (K_VALUES - 64)) | (1L << (K_VIEW - 64)) | (1L << (K_WHERE - 64)) | (1L << (K_METHODNAME - 64)) | (1L << (K_CLASSNAME - 64)) | (1L << (K_JARPATH - 64)) | (1L << (K_AGGREGATION - 64)) | (1L << (K_VAR - 64)) | (1L << (K_NEW - 64)) | (1L << (K_TRUE - 64)))) != 0) || ((((_la - 128)) & ~0x3f) == 0 && ((1L << (_la - 128)) & ((1L << (K_FALSE - 128)) | (1L << (K_FUNCTION - 128)) | (1L << (K_RETURN - 128)) | (1L << (K_WHILE - 128)) | (1L << (K_SWITCH - 128)) | (1L << (K_CASE - 128)) | (1L << (K_BREAK - 128)) | (1L << (K_CONTINUE - 128)) | (1L << (K_DO - 128)) | (1L << (K_EXISTS - 128)) | (1L << (VARNAME - 128)) | (1L << (IDENTIFIER - 128)) | (1L << (STRING_LITERAL - 128)))) != 0)) {
					{
					setState(699);
					any_name();
					}
				}

				setState(702);
				match(CLOSE_PAR);
				}
				break;
			case 2:
				{
				setState(704);
				match(OPEN_PAR);
				setState(705);
				signed_number();
				setState(707);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OPEN_PAR) | (1L << K_ACTION) | (1L << K_ADD) | (1L << K_ALL) | (1L << K_ALTER) | (1L << K_AND) | (1L << K_AS) | (1L << K_ASC) | (1L << K_AUTOINCREMENT) | (1L << K_BY) | (1L << K_CASCADE) | (1L << K_CHECK) | (1L << K_COLLATE) | (1L << K_COLUMN) | (1L << K_CONSTRAINT) | (1L << K_CREATE) | (1L << K_CROSS) | (1L << K_CURRENT_DATE) | (1L << K_CURRENT_TIME) | (1L << K_CURRENT_TIMESTAMP) | (1L << K_DATABASE) | (1L << K_DEFAULT) | (1L << K_DEFERRABLE) | (1L << K_DEFERRED) | (1L << K_DELETE) | (1L << K_DESC) | (1L << K_DISTINCT) | (1L << K_DROP) | (1L << K_EACH) | (1L << K_ELSE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (K_ENABLE - 64)) | (1L << (K_FOR - 64)) | (1L << (K_FOREIGN - 64)) | (1L << (K_FROM - 64)) | (1L << (K_GLOB - 64)) | (1L << (K_GROUP - 64)) | (1L << (K_HAVING - 64)) | (1L << (K_IF - 64)) | (1L << (K_IMMEDIATE - 64)) | (1L << (K_IN - 64)) | (1L << (K_INDEXED - 64)) | (1L << (K_INITIALLY - 64)) | (1L << (K_INNER - 64)) | (1L << (K_INSERT - 64)) | (1L << (K_INTO - 64)) | (1L << (K_IS - 64)) | (1L << (K_ISNULL - 64)) | (1L << (K_JOIN - 64)) | (1L << (K_KEY - 64)) | (1L << (K_LEFT - 64)) | (1L << (K_LIKE - 64)) | (1L << (K_LIMIT - 64)) | (1L << (K_MATCH - 64)) | (1L << (K_NEXTVAL - 64)) | (1L << (K_NO - 64)) | (1L << (K_NOT - 64)) | (1L << (K_NOTNULL - 64)) | (1L << (K_NULL - 64)) | (1L << (K_OF - 64)) | (1L << (K_OFFSET - 64)) | (1L << (K_ON - 64)) | (1L << (K_OR - 64)) | (1L << (K_ORDER - 64)) | (1L << (K_OUTER - 64)) | (1L << (K_PRIMARY - 64)) | (1L << (K_QUERY - 64)) | (1L << (K_REFERENCES - 64)) | (1L << (K_REGEXP - 64)) | (1L << (K_RENAME - 64)) | (1L << (K_REPLACE - 64)) | (1L << (K_RESTRICT - 64)) | (1L << (K_RIGHT - 64)) | (1L << (K_ROW - 64)) | (1L << (K_SELECT - 64)) | (1L << (K_SET - 64)) | (1L << (K_TABLE - 64)) | (1L << (K_THEN - 64)) | (1L << (K_TO - 64)) | (1L << (K_UNION - 64)) | (1L << (K_UNIQUE - 64)) | (1L << (K_UPDATE - 64)) | (1L << (K_VALUES - 64)) | (1L << (K_VIEW - 64)) | (1L << (K_WHERE - 64)) | (1L << (K_METHODNAME - 64)) | (1L << (K_CLASSNAME - 64)) | (1L << (K_JARPATH - 64)) | (1L << (K_AGGREGATION - 64)) | (1L << (K_VAR - 64)) | (1L << (K_NEW - 64)) | (1L << (K_TRUE - 64)))) != 0) || ((((_la - 128)) & ~0x3f) == 0 && ((1L << (_la - 128)) & ((1L << (K_FALSE - 128)) | (1L << (K_FUNCTION - 128)) | (1L << (K_RETURN - 128)) | (1L << (K_WHILE - 128)) | (1L << (K_SWITCH - 128)) | (1L << (K_CASE - 128)) | (1L << (K_BREAK - 128)) | (1L << (K_CONTINUE - 128)) | (1L << (K_DO - 128)) | (1L << (K_EXISTS - 128)) | (1L << (VARNAME - 128)) | (1L << (IDENTIFIER - 128)) | (1L << (STRING_LITERAL - 128)))) != 0)) {
					{
					setState(706);
					any_name();
					}
				}

				setState(709);
				match(COMMA);
				setState(710);
				signed_number();
				setState(712);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OPEN_PAR) | (1L << K_ACTION) | (1L << K_ADD) | (1L << K_ALL) | (1L << K_ALTER) | (1L << K_AND) | (1L << K_AS) | (1L << K_ASC) | (1L << K_AUTOINCREMENT) | (1L << K_BY) | (1L << K_CASCADE) | (1L << K_CHECK) | (1L << K_COLLATE) | (1L << K_COLUMN) | (1L << K_CONSTRAINT) | (1L << K_CREATE) | (1L << K_CROSS) | (1L << K_CURRENT_DATE) | (1L << K_CURRENT_TIME) | (1L << K_CURRENT_TIMESTAMP) | (1L << K_DATABASE) | (1L << K_DEFAULT) | (1L << K_DEFERRABLE) | (1L << K_DEFERRED) | (1L << K_DELETE) | (1L << K_DESC) | (1L << K_DISTINCT) | (1L << K_DROP) | (1L << K_EACH) | (1L << K_ELSE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (K_ENABLE - 64)) | (1L << (K_FOR - 64)) | (1L << (K_FOREIGN - 64)) | (1L << (K_FROM - 64)) | (1L << (K_GLOB - 64)) | (1L << (K_GROUP - 64)) | (1L << (K_HAVING - 64)) | (1L << (K_IF - 64)) | (1L << (K_IMMEDIATE - 64)) | (1L << (K_IN - 64)) | (1L << (K_INDEXED - 64)) | (1L << (K_INITIALLY - 64)) | (1L << (K_INNER - 64)) | (1L << (K_INSERT - 64)) | (1L << (K_INTO - 64)) | (1L << (K_IS - 64)) | (1L << (K_ISNULL - 64)) | (1L << (K_JOIN - 64)) | (1L << (K_KEY - 64)) | (1L << (K_LEFT - 64)) | (1L << (K_LIKE - 64)) | (1L << (K_LIMIT - 64)) | (1L << (K_MATCH - 64)) | (1L << (K_NEXTVAL - 64)) | (1L << (K_NO - 64)) | (1L << (K_NOT - 64)) | (1L << (K_NOTNULL - 64)) | (1L << (K_NULL - 64)) | (1L << (K_OF - 64)) | (1L << (K_OFFSET - 64)) | (1L << (K_ON - 64)) | (1L << (K_OR - 64)) | (1L << (K_ORDER - 64)) | (1L << (K_OUTER - 64)) | (1L << (K_PRIMARY - 64)) | (1L << (K_QUERY - 64)) | (1L << (K_REFERENCES - 64)) | (1L << (K_REGEXP - 64)) | (1L << (K_RENAME - 64)) | (1L << (K_REPLACE - 64)) | (1L << (K_RESTRICT - 64)) | (1L << (K_RIGHT - 64)) | (1L << (K_ROW - 64)) | (1L << (K_SELECT - 64)) | (1L << (K_SET - 64)) | (1L << (K_TABLE - 64)) | (1L << (K_THEN - 64)) | (1L << (K_TO - 64)) | (1L << (K_UNION - 64)) | (1L << (K_UNIQUE - 64)) | (1L << (K_UPDATE - 64)) | (1L << (K_VALUES - 64)) | (1L << (K_VIEW - 64)) | (1L << (K_WHERE - 64)) | (1L << (K_METHODNAME - 64)) | (1L << (K_CLASSNAME - 64)) | (1L << (K_JARPATH - 64)) | (1L << (K_AGGREGATION - 64)) | (1L << (K_VAR - 64)) | (1L << (K_NEW - 64)) | (1L << (K_TRUE - 64)))) != 0) || ((((_la - 128)) & ~0x3f) == 0 && ((1L << (_la - 128)) & ((1L << (K_FALSE - 128)) | (1L << (K_FUNCTION - 128)) | (1L << (K_RETURN - 128)) | (1L << (K_WHILE - 128)) | (1L << (K_SWITCH - 128)) | (1L << (K_CASE - 128)) | (1L << (K_BREAK - 128)) | (1L << (K_CONTINUE - 128)) | (1L << (K_DO - 128)) | (1L << (K_EXISTS - 128)) | (1L << (VARNAME - 128)) | (1L << (IDENTIFIER - 128)) | (1L << (STRING_LITERAL - 128)))) != 0)) {
					{
					setState(711);
					any_name();
					}
				}

				setState(714);
				match(CLOSE_PAR);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Create_aggregation_functionContext extends ParserRuleContext {
		public TerminalNode K_CREATE() { return getToken(SQLParser.K_CREATE, 0); }
		public TerminalNode K_AGGREGATION() { return getToken(SQLParser.K_AGGREGATION, 0); }
		public TerminalNode K_FUNCTION() { return getToken(SQLParser.K_FUNCTION, 0); }
		public Function_nameContext function_name() {
			return getRuleContext(Function_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public JarPathContext jarPath() {
			return getRuleContext(JarPathContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public ClassNameContext className() {
			return getRuleContext(ClassNameContext.class,0);
		}
		public MethodNameContext methodName() {
			return getRuleContext(MethodNameContext.class,0);
		}
		public ReturnTypeContext returnType() {
			return getRuleContext(ReturnTypeContext.class,0);
		}
		public ParametersContext parameters() {
			return getRuleContext(ParametersContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Create_aggregation_functionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_create_aggregation_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCreate_aggregation_function(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCreate_aggregation_function(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitCreate_aggregation_function(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Create_aggregation_functionContext create_aggregation_function() throws RecognitionException {
		Create_aggregation_functionContext _localctx = new Create_aggregation_functionContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_create_aggregation_function);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(718);
			match(K_CREATE);
			setState(719);
			match(K_AGGREGATION);
			setState(720);
			match(K_FUNCTION);
			setState(721);
			function_name();
			setState(722);
			match(OPEN_PAR);
			setState(723);
			jarPath();
			setState(724);
			match(COMMA);
			setState(725);
			className();
			setState(726);
			match(COMMA);
			setState(727);
			methodName();
			setState(728);
			match(COMMA);
			setState(729);
			returnType();
			setState(730);
			match(COMMA);
			setState(731);
			parameters();
			setState(732);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class JarPathContext extends ParserRuleContext {
		public String_valContext string_val() {
			return getRuleContext(String_valContext.class,0);
		}
		public JarPathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_jarPath; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterJarPath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitJarPath(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitJarPath(this);
			else return visitor.visitChildren(this);
		}
	}

	public final JarPathContext jarPath() throws RecognitionException {
		JarPathContext _localctx = new JarPathContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_jarPath);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(734);
			string_val();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class JarNameContext extends ParserRuleContext {
		public String_valContext string_val() {
			return getRuleContext(String_valContext.class,0);
		}
		public JarNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_jarName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterJarName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitJarName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitJarName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final JarNameContext jarName() throws RecognitionException {
		JarNameContext _localctx = new JarNameContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_jarName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(736);
			string_val();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassNameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public ClassNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_className; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterClassName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitClassName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitClassName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClassNameContext className() throws RecognitionException {
		ClassNameContext _localctx = new ClassNameContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_className);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(738);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodNameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public MethodNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterMethodName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitMethodName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitMethodName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodNameContext methodName() throws RecognitionException {
		MethodNameContext _localctx = new MethodNameContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_methodName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(740);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReturnTypeContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public ReturnTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_returnType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterReturnType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitReturnType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitReturnType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReturnTypeContext returnType() throws RecognitionException {
		ReturnTypeContext _localctx = new ReturnTypeContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_returnType);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(742);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParametersContext extends ParserRuleContext {
		public TerminalNode OPA() { return getToken(SQLParser.OPA, 0); }
		public TerminalNode CLA() { return getToken(SQLParser.CLA, 0); }
		public List<Any_nameContext> any_name() {
			return getRuleContexts(Any_nameContext.class);
		}
		public Any_nameContext any_name(int i) {
			return getRuleContext(Any_nameContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public ParametersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameters; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterParameters(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitParameters(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitParameters(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParametersContext parameters() throws RecognitionException {
		ParametersContext _localctx = new ParametersContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_parameters);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(744);
			match(OPA);
			setState(753);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OPEN_PAR) | (1L << K_ACTION) | (1L << K_ADD) | (1L << K_ALL) | (1L << K_ALTER) | (1L << K_AND) | (1L << K_AS) | (1L << K_ASC) | (1L << K_AUTOINCREMENT) | (1L << K_BY) | (1L << K_CASCADE) | (1L << K_CHECK) | (1L << K_COLLATE) | (1L << K_COLUMN) | (1L << K_CONSTRAINT) | (1L << K_CREATE) | (1L << K_CROSS) | (1L << K_CURRENT_DATE) | (1L << K_CURRENT_TIME) | (1L << K_CURRENT_TIMESTAMP) | (1L << K_DATABASE) | (1L << K_DEFAULT) | (1L << K_DEFERRABLE) | (1L << K_DEFERRED) | (1L << K_DELETE) | (1L << K_DESC) | (1L << K_DISTINCT) | (1L << K_DROP) | (1L << K_EACH) | (1L << K_ELSE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (K_ENABLE - 64)) | (1L << (K_FOR - 64)) | (1L << (K_FOREIGN - 64)) | (1L << (K_FROM - 64)) | (1L << (K_GLOB - 64)) | (1L << (K_GROUP - 64)) | (1L << (K_HAVING - 64)) | (1L << (K_IF - 64)) | (1L << (K_IMMEDIATE - 64)) | (1L << (K_IN - 64)) | (1L << (K_INDEXED - 64)) | (1L << (K_INITIALLY - 64)) | (1L << (K_INNER - 64)) | (1L << (K_INSERT - 64)) | (1L << (K_INTO - 64)) | (1L << (K_IS - 64)) | (1L << (K_ISNULL - 64)) | (1L << (K_JOIN - 64)) | (1L << (K_KEY - 64)) | (1L << (K_LEFT - 64)) | (1L << (K_LIKE - 64)) | (1L << (K_LIMIT - 64)) | (1L << (K_MATCH - 64)) | (1L << (K_NEXTVAL - 64)) | (1L << (K_NO - 64)) | (1L << (K_NOT - 64)) | (1L << (K_NOTNULL - 64)) | (1L << (K_NULL - 64)) | (1L << (K_OF - 64)) | (1L << (K_OFFSET - 64)) | (1L << (K_ON - 64)) | (1L << (K_OR - 64)) | (1L << (K_ORDER - 64)) | (1L << (K_OUTER - 64)) | (1L << (K_PRIMARY - 64)) | (1L << (K_QUERY - 64)) | (1L << (K_REFERENCES - 64)) | (1L << (K_REGEXP - 64)) | (1L << (K_RENAME - 64)) | (1L << (K_REPLACE - 64)) | (1L << (K_RESTRICT - 64)) | (1L << (K_RIGHT - 64)) | (1L << (K_ROW - 64)) | (1L << (K_SELECT - 64)) | (1L << (K_SET - 64)) | (1L << (K_TABLE - 64)) | (1L << (K_THEN - 64)) | (1L << (K_TO - 64)) | (1L << (K_UNION - 64)) | (1L << (K_UNIQUE - 64)) | (1L << (K_UPDATE - 64)) | (1L << (K_VALUES - 64)) | (1L << (K_VIEW - 64)) | (1L << (K_WHERE - 64)) | (1L << (K_METHODNAME - 64)) | (1L << (K_CLASSNAME - 64)) | (1L << (K_JARPATH - 64)) | (1L << (K_AGGREGATION - 64)) | (1L << (K_VAR - 64)) | (1L << (K_NEW - 64)) | (1L << (K_TRUE - 64)))) != 0) || ((((_la - 128)) & ~0x3f) == 0 && ((1L << (_la - 128)) & ((1L << (K_FALSE - 128)) | (1L << (K_FUNCTION - 128)) | (1L << (K_RETURN - 128)) | (1L << (K_WHILE - 128)) | (1L << (K_SWITCH - 128)) | (1L << (K_CASE - 128)) | (1L << (K_BREAK - 128)) | (1L << (K_CONTINUE - 128)) | (1L << (K_DO - 128)) | (1L << (K_EXISTS - 128)) | (1L << (VARNAME - 128)) | (1L << (IDENTIFIER - 128)) | (1L << (STRING_LITERAL - 128)))) != 0)) {
				{
				setState(745);
				any_name();
				setState(750);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(746);
					match(COMMA);
					setState(747);
					any_name();
					}
					}
					setState(752);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(755);
			match(CLA);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Condition_expContext extends ParserRuleContext {
		public Function_callContext function_call() {
			return getRuleContext(Function_callContext.class,0);
		}
		public Boolean_varContext boolean_var() {
			return getRuleContext(Boolean_varContext.class,0);
		}
		public Boolean_valContext boolean_val() {
			return getRuleContext(Boolean_valContext.class,0);
		}
		public List<Num_expContext> num_exp() {
			return getRuleContexts(Num_expContext.class);
		}
		public Num_expContext num_exp(int i) {
			return getRuleContext(Num_expContext.class,i);
		}
		public TerminalNode LT() { return getToken(SQLParser.LT, 0); }
		public TerminalNode LT_EQ() { return getToken(SQLParser.LT_EQ, 0); }
		public TerminalNode GT() { return getToken(SQLParser.GT, 0); }
		public TerminalNode GT_EQ() { return getToken(SQLParser.GT_EQ, 0); }
		public TerminalNode EQ() { return getToken(SQLParser.EQ, 0); }
		public TerminalNode NOT_EQ1() { return getToken(SQLParser.NOT_EQ1, 0); }
		public TerminalNode NOT_EQ2() { return getToken(SQLParser.NOT_EQ2, 0); }
		public List<Java_valueContext> java_value() {
			return getRuleContexts(Java_valueContext.class);
		}
		public Java_valueContext java_value(int i) {
			return getRuleContext(Java_valueContext.class,i);
		}
		public List<Condition_expContext> condition_exp() {
			return getRuleContexts(Condition_expContext.class);
		}
		public Condition_expContext condition_exp(int i) {
			return getRuleContext(Condition_expContext.class,i);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode AMP2() { return getToken(SQLParser.AMP2, 0); }
		public TerminalNode AMP() { return getToken(SQLParser.AMP, 0); }
		public TerminalNode PIPE2() { return getToken(SQLParser.PIPE2, 0); }
		public TerminalNode PIPE() { return getToken(SQLParser.PIPE, 0); }
		public Condition_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condition_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCondition_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCondition_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitCondition_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Condition_expContext condition_exp() throws RecognitionException {
		return condition_exp(0);
	}

	private Condition_expContext condition_exp(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Condition_expContext _localctx = new Condition_expContext(_ctx, _parentState);
		Condition_expContext _prevctx = _localctx;
		int _startState = 94;
		enterRecursionRule(_localctx, 94, RULE_condition_exp, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(790);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,82,_ctx) ) {
			case 1:
				{
				setState(761);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,77,_ctx) ) {
				case 1:
					{
					setState(758);
					function_call();
					}
					break;
				case 2:
					{
					setState(759);
					boolean_var();
					}
					break;
				case 3:
					{
					setState(760);
					boolean_val();
					}
					break;
				}
				}
				break;
			case 2:
				{
				setState(763);
				num_exp(0);
				setState(764);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LT) | (1L << LT_EQ) | (1L << GT) | (1L << GT_EQ))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(765);
				num_exp(0);
				}
				break;
			case 3:
				{
				setState(769);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,78,_ctx) ) {
				case 1:
					{
					setState(767);
					num_exp(0);
					}
					break;
				case 2:
					{
					setState(768);
					java_value();
					}
					break;
				}
				setState(771);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQ) | (1L << NOT_EQ1) | (1L << NOT_EQ2))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(774);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,79,_ctx) ) {
				case 1:
					{
					setState(772);
					num_exp(0);
					}
					break;
				case 2:
					{
					setState(773);
					java_value();
					}
					break;
				}
				}
				break;
			case 4:
				{
				setState(776);
				match(T__4);
				setState(780);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,80,_ctx) ) {
				case 1:
					{
					setState(777);
					condition_exp(0);
					}
					break;
				case 2:
					{
					setState(778);
					function_call();
					}
					break;
				case 3:
					{
					setState(779);
					boolean_var();
					}
					break;
				}
				}
				break;
			case 5:
				{
				setState(782);
				match(OPEN_PAR);
				setState(786);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,81,_ctx) ) {
				case 1:
					{
					setState(783);
					condition_exp(0);
					}
					break;
				case 2:
					{
					setState(784);
					function_call();
					}
					break;
				case 3:
					{
					setState(785);
					boolean_var();
					}
					break;
				}
				setState(788);
				match(CLOSE_PAR);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(800);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,84,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(798);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,83,_ctx) ) {
					case 1:
						{
						_localctx = new Condition_expContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_condition_exp);
						setState(792);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(793);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << AMP2) | (1L << PIPE2) | (1L << AMP) | (1L << PIPE))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(794);
						condition_exp(7);
						}
						break;
					case 2:
						{
						_localctx = new Condition_expContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_condition_exp);
						setState(795);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(796);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQ) | (1L << NOT_EQ1) | (1L << NOT_EQ2))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(797);
						condition_exp(4);
						}
						break;
					}
					} 
				}
				setState(802);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,84,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Num_expContext extends ParserRuleContext {
		public Num_valContext num_val() {
			return getRuleContext(Num_valContext.class,0);
		}
		public Function_callContext function_call() {
			return getRuleContext(Function_callContext.class,0);
		}
		public Num_varContext num_var() {
			return getRuleContext(Num_varContext.class,0);
		}
		public Incriment_opContext incriment_op() {
			return getRuleContext(Incriment_opContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<Num_expContext> num_exp() {
			return getRuleContexts(Num_expContext.class);
		}
		public Num_expContext num_exp(int i) {
			return getRuleContext(Num_expContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode STAR() { return getToken(SQLParser.STAR, 0); }
		public TerminalNode DIV() { return getToken(SQLParser.DIV, 0); }
		public TerminalNode MOD() { return getToken(SQLParser.MOD, 0); }
		public TerminalNode PLUS() { return getToken(SQLParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(SQLParser.MINUS, 0); }
		public Num_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_num_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterNum_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitNum_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitNum_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Num_expContext num_exp() throws RecognitionException {
		return num_exp(0);
	}

	private Num_expContext num_exp(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Num_expContext _localctx = new Num_expContext(_ctx, _parentState);
		Num_expContext _prevctx = _localctx;
		int _startState = 96;
		enterRecursionRule(_localctx, 96, RULE_num_exp, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(819);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,87,_ctx) ) {
			case 1:
				{
				setState(806);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case STAR:
				case PLUS:
				case MINUS:
				case NUMERIC_LITERAL:
					{
					setState(804);
					num_val();
					}
					break;
				case OPEN_PAR:
				case VARNAME:
					{
					setState(805);
					function_call();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case 2:
				{
				setState(808);
				num_var();
				setState(810);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,86,_ctx) ) {
				case 1:
					{
					setState(809);
					incriment_op();
					}
					break;
				}
				}
				break;
			case 3:
				{
				setState(812);
				incriment_op();
				setState(813);
				num_var();
				}
				break;
			case 4:
				{
				setState(815);
				match(OPEN_PAR);
				setState(816);
				num_exp(0);
				setState(817);
				match(CLOSE_PAR);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(829);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,89,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(827);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,88,_ctx) ) {
					case 1:
						{
						_localctx = new Num_expContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_num_exp);
						setState(821);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(822);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STAR) | (1L << DIV) | (1L << MOD))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(823);
						num_exp(6);
						}
						break;
					case 2:
						{
						_localctx = new Num_expContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_num_exp);
						setState(824);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(825);
						_la = _input.LA(1);
						if ( !(_la==PLUS || _la==MINUS) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(826);
						num_exp(5);
						}
						break;
					}
					} 
				}
				setState(831);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,89,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public Literal_valueContext literal_value() {
			return getRuleContext(Literal_valueContext.class,0);
		}
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public List<TerminalNode> DOT() { return getTokens(SQLParser.DOT); }
		public TerminalNode DOT(int i) {
			return getToken(SQLParser.DOT, i);
		}
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public Unary_operatorContext unary_operator() {
			return getRuleContext(Unary_operatorContext.class,0);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public Function_exprContext function_expr() {
			return getRuleContext(Function_exprContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public TerminalNode K_EXISTS() { return getToken(SQLParser.K_EXISTS, 0); }
		public TerminalNode K_NOT() { return getToken(SQLParser.K_NOT, 0); }
		public TerminalNode PIPE2() { return getToken(SQLParser.PIPE2, 0); }
		public TerminalNode STAR() { return getToken(SQLParser.STAR, 0); }
		public TerminalNode DIV() { return getToken(SQLParser.DIV, 0); }
		public TerminalNode MOD() { return getToken(SQLParser.MOD, 0); }
		public TerminalNode PLUS() { return getToken(SQLParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(SQLParser.MINUS, 0); }
		public TerminalNode LT2() { return getToken(SQLParser.LT2, 0); }
		public TerminalNode GT2() { return getToken(SQLParser.GT2, 0); }
		public TerminalNode AMP() { return getToken(SQLParser.AMP, 0); }
		public TerminalNode PIPE() { return getToken(SQLParser.PIPE, 0); }
		public TerminalNode LT() { return getToken(SQLParser.LT, 0); }
		public TerminalNode LT_EQ() { return getToken(SQLParser.LT_EQ, 0); }
		public TerminalNode GT() { return getToken(SQLParser.GT, 0); }
		public TerminalNode GT_EQ() { return getToken(SQLParser.GT_EQ, 0); }
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public TerminalNode EQ() { return getToken(SQLParser.EQ, 0); }
		public TerminalNode NOT_EQ1() { return getToken(SQLParser.NOT_EQ1, 0); }
		public TerminalNode NOT_EQ2() { return getToken(SQLParser.NOT_EQ2, 0); }
		public TerminalNode K_IS() { return getToken(SQLParser.K_IS, 0); }
		public TerminalNode K_IN() { return getToken(SQLParser.K_IN, 0); }
		public TerminalNode K_LIKE() { return getToken(SQLParser.K_LIKE, 0); }
		public TerminalNode K_GLOB() { return getToken(SQLParser.K_GLOB, 0); }
		public TerminalNode K_MATCH() { return getToken(SQLParser.K_MATCH, 0); }
		public TerminalNode K_REGEXP() { return getToken(SQLParser.K_REGEXP, 0); }
		public TerminalNode K_AND() { return getToken(SQLParser.K_AND, 0); }
		public TerminalNode K_OR() { return getToken(SQLParser.K_OR, 0); }
		public Not_in_exprContext not_in_expr() {
			return getRuleContext(Not_in_exprContext.class,0);
		}
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 98;
		enterRecursionRule(_localctx, 98, RULE_expr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(863);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,94,_ctx) ) {
			case 1:
				{
				setState(833);
				literal_value();
				}
				break;
			case 2:
				{
				setState(842);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,91,_ctx) ) {
				case 1:
					{
					setState(837);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,90,_ctx) ) {
					case 1:
						{
						setState(834);
						database_name();
						setState(835);
						match(DOT);
						}
						break;
					}
					setState(839);
					table_name();
					setState(840);
					match(DOT);
					}
					break;
				}
				setState(844);
				column_name();
				}
				break;
			case 3:
				{
				setState(845);
				unary_operator();
				setState(846);
				expr(13);
				}
				break;
			case 4:
				{
				setState(848);
				function_expr();
				}
				break;
			case 5:
				{
				setState(849);
				match(OPEN_PAR);
				setState(850);
				expr(0);
				setState(851);
				match(CLOSE_PAR);
				}
				break;
			case 6:
				{
				setState(857);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_NOT || _la==K_EXISTS) {
					{
					setState(854);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_NOT) {
						{
						setState(853);
						match(K_NOT);
						}
					}

					setState(856);
					match(K_EXISTS);
					}
				}

				setState(859);
				match(OPEN_PAR);
				setState(860);
				select_stmt();
				setState(861);
				match(CLOSE_PAR);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(906);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,97,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(904);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,96,_ctx) ) {
					case 1:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(865);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(866);
						match(PIPE2);
						setState(867);
						expr(13);
						}
						break;
					case 2:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(868);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(869);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STAR) | (1L << DIV) | (1L << MOD))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(870);
						expr(12);
						}
						break;
					case 3:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(871);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(872);
						_la = _input.LA(1);
						if ( !(_la==PLUS || _la==MINUS) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(873);
						expr(11);
						}
						break;
					case 4:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(874);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(875);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LT2) | (1L << GT2) | (1L << AMP) | (1L << PIPE))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(876);
						expr(10);
						}
						break;
					case 5:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(877);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(878);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LT) | (1L << LT_EQ) | (1L << GT) | (1L << GT_EQ))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(879);
						expr(9);
						}
						break;
					case 6:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(880);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(893);
						_errHandler.sync(this);
						switch ( getInterpreter().adaptivePredict(_input,95,_ctx) ) {
						case 1:
							{
							setState(881);
							match(ASSIGN);
							}
							break;
						case 2:
							{
							setState(882);
							match(EQ);
							}
							break;
						case 3:
							{
							setState(883);
							match(NOT_EQ1);
							}
							break;
						case 4:
							{
							setState(884);
							match(NOT_EQ2);
							}
							break;
						case 5:
							{
							setState(885);
							match(K_IS);
							}
							break;
						case 6:
							{
							setState(886);
							match(K_IS);
							setState(887);
							match(K_NOT);
							}
							break;
						case 7:
							{
							setState(888);
							match(K_IN);
							}
							break;
						case 8:
							{
							setState(889);
							match(K_LIKE);
							}
							break;
						case 9:
							{
							setState(890);
							match(K_GLOB);
							}
							break;
						case 10:
							{
							setState(891);
							match(K_MATCH);
							}
							break;
						case 11:
							{
							setState(892);
							match(K_REGEXP);
							}
							break;
						}
						setState(895);
						expr(8);
						}
						break;
					case 7:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(896);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(897);
						match(K_AND);
						setState(898);
						expr(7);
						}
						break;
					case 8:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(899);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(900);
						match(K_OR);
						setState(901);
						expr(6);
						}
						break;
					case 9:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(902);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(903);
						not_in_expr();
						}
						break;
					}
					} 
				}
				setState(908);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,97,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Function_exprContext extends ParserRuleContext {
		public Function_nameContext function_name() {
			return getRuleContext(Function_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode STAR() { return getToken(SQLParser.STAR, 0); }
		public TerminalNode K_DISTINCT() { return getToken(SQLParser.K_DISTINCT, 0); }
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Function_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterFunction_expr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitFunction_expr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitFunction_expr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_exprContext function_expr() throws RecognitionException {
		Function_exprContext _localctx = new Function_exprContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_function_expr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(909);
			function_name();
			setState(910);
			match(OPEN_PAR);
			setState(923);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OPEN_PAR:
			case PLUS:
			case MINUS:
			case TILDE:
			case K_ACTION:
			case K_ADD:
			case K_ALL:
			case K_ALTER:
			case K_AND:
			case K_AS:
			case K_ASC:
			case K_AUTOINCREMENT:
			case K_BY:
			case K_CASCADE:
			case K_CHECK:
			case K_COLLATE:
			case K_COLUMN:
			case K_CONSTRAINT:
			case K_CREATE:
			case K_CROSS:
			case K_CURRENT_DATE:
			case K_CURRENT_TIME:
			case K_CURRENT_TIMESTAMP:
			case K_DATABASE:
			case K_DEFAULT:
			case K_DEFERRABLE:
			case K_DEFERRED:
			case K_DELETE:
			case K_DESC:
			case K_DISTINCT:
			case K_DROP:
			case K_EACH:
			case K_ELSE:
			case K_ENABLE:
			case K_FOR:
			case K_FOREIGN:
			case K_FROM:
			case K_GLOB:
			case K_GROUP:
			case K_HAVING:
			case K_IF:
			case K_IMMEDIATE:
			case K_IN:
			case K_INDEXED:
			case K_INITIALLY:
			case K_INNER:
			case K_INSERT:
			case K_INTO:
			case K_IS:
			case K_ISNULL:
			case K_JOIN:
			case K_KEY:
			case K_LEFT:
			case K_LIKE:
			case K_LIMIT:
			case K_MATCH:
			case K_NEXTVAL:
			case K_NO:
			case K_NOT:
			case K_NOTNULL:
			case K_NULL:
			case K_OF:
			case K_OFFSET:
			case K_ON:
			case K_OR:
			case K_ORDER:
			case K_OUTER:
			case K_PRIMARY:
			case K_QUERY:
			case K_REFERENCES:
			case K_REGEXP:
			case K_RENAME:
			case K_REPLACE:
			case K_RESTRICT:
			case K_RIGHT:
			case K_ROW:
			case K_SELECT:
			case K_SET:
			case K_TABLE:
			case K_THEN:
			case K_TO:
			case K_UNION:
			case K_UNIQUE:
			case K_UPDATE:
			case K_VALUES:
			case K_VIEW:
			case K_WHERE:
			case K_METHODNAME:
			case K_CLASSNAME:
			case K_JARPATH:
			case K_AGGREGATION:
			case K_VAR:
			case K_NEW:
			case K_TRUE:
			case K_FALSE:
			case K_FUNCTION:
			case K_RETURN:
			case K_WHILE:
			case K_SWITCH:
			case K_CASE:
			case K_BREAK:
			case K_CONTINUE:
			case K_DO:
			case K_EXISTS:
			case VARNAME:
			case IDENTIFIER:
			case NUMERIC_LITERAL:
			case STRING_LITERAL:
			case BLOB_LITERAL:
				{
				setState(912);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,98,_ctx) ) {
				case 1:
					{
					setState(911);
					match(K_DISTINCT);
					}
					break;
				}
				setState(914);
				expr(0);
				setState(919);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(915);
					match(COMMA);
					setState(916);
					expr(0);
					}
					}
					setState(921);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case STAR:
				{
				setState(922);
				match(STAR);
				}
				break;
			case CLOSE_PAR:
				break;
			default:
				break;
			}
			setState(925);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Not_in_exprContext extends ParserRuleContext {
		public TerminalNode K_IN() { return getToken(SQLParser.K_IN, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public TerminalNode K_NOT() { return getToken(SQLParser.K_NOT, 0); }
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(SQLParser.DOT, 0); }
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Not_in_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_not_in_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterNot_in_expr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitNot_in_expr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitNot_in_expr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Not_in_exprContext not_in_expr() throws RecognitionException {
		Not_in_exprContext _localctx = new Not_in_exprContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_not_in_expr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(928);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_NOT) {
				{
				setState(927);
				match(K_NOT);
				}
			}

			setState(930);
			match(K_IN);
			setState(950);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,105,_ctx) ) {
			case 1:
				{
				setState(931);
				match(OPEN_PAR);
				setState(941);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,103,_ctx) ) {
				case 1:
					{
					setState(932);
					select_stmt();
					}
					break;
				case 2:
					{
					setState(933);
					expr(0);
					setState(938);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(934);
						match(COMMA);
						setState(935);
						expr(0);
						}
						}
						setState(940);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
					break;
				}
				setState(943);
				match(CLOSE_PAR);
				}
				break;
			case 2:
				{
				setState(947);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,104,_ctx) ) {
				case 1:
					{
					setState(944);
					database_name();
					setState(945);
					match(DOT);
					}
					break;
				}
				setState(949);
				table_name();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Foreign_key_clauseContext extends ParserRuleContext {
		public TerminalNode K_REFERENCES() { return getToken(SQLParser.K_REFERENCES, 0); }
		public Foreign_tableContext foreign_table() {
			return getRuleContext(Foreign_tableContext.class,0);
		}
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(SQLParser.DOT, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<Fk_target_column_nameContext> fk_target_column_name() {
			return getRuleContexts(Fk_target_column_nameContext.class);
		}
		public Fk_target_column_nameContext fk_target_column_name(int i) {
			return getRuleContext(Fk_target_column_nameContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode K_DEFERRABLE() { return getToken(SQLParser.K_DEFERRABLE, 0); }
		public List<Foreign_on_conditionContext> foreign_on_condition() {
			return getRuleContexts(Foreign_on_conditionContext.class);
		}
		public Foreign_on_conditionContext foreign_on_condition(int i) {
			return getRuleContext(Foreign_on_conditionContext.class,i);
		}
		public List<TerminalNode> K_MATCH() { return getTokens(SQLParser.K_MATCH); }
		public TerminalNode K_MATCH(int i) {
			return getToken(SQLParser.K_MATCH, i);
		}
		public List<NameContext> name() {
			return getRuleContexts(NameContext.class);
		}
		public NameContext name(int i) {
			return getRuleContext(NameContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public TerminalNode K_NOT() { return getToken(SQLParser.K_NOT, 0); }
		public TerminalNode K_INITIALLY() { return getToken(SQLParser.K_INITIALLY, 0); }
		public TerminalNode K_DEFERRED() { return getToken(SQLParser.K_DEFERRED, 0); }
		public TerminalNode K_IMMEDIATE() { return getToken(SQLParser.K_IMMEDIATE, 0); }
		public TerminalNode K_ENABLE() { return getToken(SQLParser.K_ENABLE, 0); }
		public Foreign_key_clauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_foreign_key_clause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterForeign_key_clause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitForeign_key_clause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitForeign_key_clause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Foreign_key_clauseContext foreign_key_clause() throws RecognitionException {
		Foreign_key_clauseContext _localctx = new Foreign_key_clauseContext(_ctx, getState());
		enterRule(_localctx, 104, RULE_foreign_key_clause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(952);
			match(K_REFERENCES);
			setState(956);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,106,_ctx) ) {
			case 1:
				{
				setState(953);
				database_name();
				setState(954);
				match(DOT);
				}
				break;
			}
			setState(958);
			foreign_table();
			setState(970);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==OPEN_PAR) {
				{
				setState(959);
				match(OPEN_PAR);
				setState(960);
				fk_target_column_name();
				setState(965);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(961);
					match(COMMA);
					setState(962);
					fk_target_column_name();
					}
					}
					setState(967);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(968);
				match(CLOSE_PAR);
				}
			}

			setState(979);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==K_MATCH || _la==K_ON) {
				{
				{
				setState(975);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case K_ON:
					{
					setState(972);
					foreign_on_condition();
					}
					break;
				case K_MATCH:
					{
					setState(973);
					match(K_MATCH);
					setState(974);
					name();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				}
				setState(981);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(995);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_DEFERRABLE || _la==K_NOT) {
				{
				setState(983);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_NOT) {
					{
					setState(982);
					match(K_NOT);
					}
				}

				setState(985);
				match(K_DEFERRABLE);
				setState(990);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,112,_ctx) ) {
				case 1:
					{
					setState(986);
					match(K_INITIALLY);
					setState(987);
					match(K_DEFERRED);
					}
					break;
				case 2:
					{
					setState(988);
					match(K_INITIALLY);
					setState(989);
					match(K_IMMEDIATE);
					}
					break;
				}
				setState(993);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_ENABLE) {
					{
					setState(992);
					match(K_ENABLE);
					}
				}

				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Foreign_on_conditionContext extends ParserRuleContext {
		public TerminalNode K_ON() { return getToken(SQLParser.K_ON, 0); }
		public TerminalNode K_DELETE() { return getToken(SQLParser.K_DELETE, 0); }
		public TerminalNode K_UPDATE() { return getToken(SQLParser.K_UPDATE, 0); }
		public TerminalNode K_SET() { return getToken(SQLParser.K_SET, 0); }
		public TerminalNode K_NULL() { return getToken(SQLParser.K_NULL, 0); }
		public TerminalNode K_DEFAULT() { return getToken(SQLParser.K_DEFAULT, 0); }
		public TerminalNode K_CASCADE() { return getToken(SQLParser.K_CASCADE, 0); }
		public TerminalNode K_RESTRICT() { return getToken(SQLParser.K_RESTRICT, 0); }
		public TerminalNode K_NO() { return getToken(SQLParser.K_NO, 0); }
		public TerminalNode K_ACTION() { return getToken(SQLParser.K_ACTION, 0); }
		public Foreign_on_conditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_foreign_on_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterForeign_on_condition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitForeign_on_condition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitForeign_on_condition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Foreign_on_conditionContext foreign_on_condition() throws RecognitionException {
		Foreign_on_conditionContext _localctx = new Foreign_on_conditionContext(_ctx, getState());
		enterRule(_localctx, 106, RULE_foreign_on_condition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(997);
			match(K_ON);
			setState(998);
			_la = _input.LA(1);
			if ( !(_la==K_DELETE || _la==K_UPDATE) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(1007);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,115,_ctx) ) {
			case 1:
				{
				setState(999);
				match(K_SET);
				setState(1000);
				match(K_NULL);
				}
				break;
			case 2:
				{
				setState(1001);
				match(K_SET);
				setState(1002);
				match(K_DEFAULT);
				}
				break;
			case 3:
				{
				setState(1003);
				match(K_CASCADE);
				}
				break;
			case 4:
				{
				setState(1004);
				match(K_RESTRICT);
				}
				break;
			case 5:
				{
				setState(1005);
				match(K_NO);
				setState(1006);
				match(K_ACTION);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Fk_target_column_nameContext extends ParserRuleContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public Fk_target_column_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fk_target_column_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterFk_target_column_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitFk_target_column_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitFk_target_column_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Fk_target_column_nameContext fk_target_column_name() throws RecognitionException {
		Fk_target_column_nameContext _localctx = new Fk_target_column_nameContext(_ctx, getState());
		enterRule(_localctx, 108, RULE_fk_target_column_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1009);
			name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Indexed_columnContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public TerminalNode K_COLLATE() { return getToken(SQLParser.K_COLLATE, 0); }
		public Collation_nameContext collation_name() {
			return getRuleContext(Collation_nameContext.class,0);
		}
		public TerminalNode K_ASC() { return getToken(SQLParser.K_ASC, 0); }
		public TerminalNode K_DESC() { return getToken(SQLParser.K_DESC, 0); }
		public Indexed_columnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_indexed_column; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterIndexed_column(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitIndexed_column(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitIndexed_column(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Indexed_columnContext indexed_column() throws RecognitionException {
		Indexed_columnContext _localctx = new Indexed_columnContext(_ctx, getState());
		enterRule(_localctx, 110, RULE_indexed_column);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1011);
			column_name();
			setState(1014);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_COLLATE) {
				{
				setState(1012);
				match(K_COLLATE);
				setState(1013);
				collation_name();
				}
			}

			setState(1017);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_ASC || _la==K_DESC) {
				{
				setState(1016);
				_la = _input.LA(1);
				if ( !(_la==K_ASC || _la==K_DESC) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_constraintContext extends ParserRuleContext {
		public Table_constraint_primary_keyContext table_constraint_primary_key() {
			return getRuleContext(Table_constraint_primary_keyContext.class,0);
		}
		public Table_constraint_keyContext table_constraint_key() {
			return getRuleContext(Table_constraint_keyContext.class,0);
		}
		public Table_constraint_uniqueContext table_constraint_unique() {
			return getRuleContext(Table_constraint_uniqueContext.class,0);
		}
		public TerminalNode K_CHECK() { return getToken(SQLParser.K_CHECK, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Table_constraint_foreign_keyContext table_constraint_foreign_key() {
			return getRuleContext(Table_constraint_foreign_keyContext.class,0);
		}
		public TerminalNode K_CONSTRAINT() { return getToken(SQLParser.K_CONSTRAINT, 0); }
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public Table_constraintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_constraint; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTable_constraint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTable_constraint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTable_constraint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_constraintContext table_constraint() throws RecognitionException {
		Table_constraintContext _localctx = new Table_constraintContext(_ctx, getState());
		enterRule(_localctx, 112, RULE_table_constraint);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1021);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_CONSTRAINT) {
				{
				setState(1019);
				match(K_CONSTRAINT);
				setState(1020);
				name();
				}
			}

			setState(1032);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_PRIMARY:
				{
				setState(1023);
				table_constraint_primary_key();
				}
				break;
			case K_KEY:
				{
				setState(1024);
				table_constraint_key();
				}
				break;
			case K_UNIQUE:
				{
				setState(1025);
				table_constraint_unique();
				}
				break;
			case K_CHECK:
				{
				setState(1026);
				match(K_CHECK);
				setState(1027);
				match(OPEN_PAR);
				setState(1028);
				expr(0);
				setState(1029);
				match(CLOSE_PAR);
				}
				break;
			case K_FOREIGN:
				{
				setState(1031);
				table_constraint_foreign_key();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_constraint_primary_keyContext extends ParserRuleContext {
		public TerminalNode K_PRIMARY() { return getToken(SQLParser.K_PRIMARY, 0); }
		public TerminalNode K_KEY() { return getToken(SQLParser.K_KEY, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<Indexed_columnContext> indexed_column() {
			return getRuleContexts(Indexed_columnContext.class);
		}
		public Indexed_columnContext indexed_column(int i) {
			return getRuleContext(Indexed_columnContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Table_constraint_primary_keyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_constraint_primary_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTable_constraint_primary_key(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTable_constraint_primary_key(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTable_constraint_primary_key(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_constraint_primary_keyContext table_constraint_primary_key() throws RecognitionException {
		Table_constraint_primary_keyContext _localctx = new Table_constraint_primary_keyContext(_ctx, getState());
		enterRule(_localctx, 114, RULE_table_constraint_primary_key);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1034);
			match(K_PRIMARY);
			setState(1035);
			match(K_KEY);
			setState(1036);
			match(OPEN_PAR);
			setState(1037);
			indexed_column();
			setState(1042);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1038);
				match(COMMA);
				setState(1039);
				indexed_column();
				}
				}
				setState(1044);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1045);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_constraint_foreign_keyContext extends ParserRuleContext {
		public TerminalNode K_FOREIGN() { return getToken(SQLParser.K_FOREIGN, 0); }
		public TerminalNode K_KEY() { return getToken(SQLParser.K_KEY, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<Fk_origin_column_nameContext> fk_origin_column_name() {
			return getRuleContexts(Fk_origin_column_nameContext.class);
		}
		public Fk_origin_column_nameContext fk_origin_column_name(int i) {
			return getRuleContext(Fk_origin_column_nameContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Foreign_key_clauseContext foreign_key_clause() {
			return getRuleContext(Foreign_key_clauseContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Table_constraint_foreign_keyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_constraint_foreign_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTable_constraint_foreign_key(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTable_constraint_foreign_key(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTable_constraint_foreign_key(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_constraint_foreign_keyContext table_constraint_foreign_key() throws RecognitionException {
		Table_constraint_foreign_keyContext _localctx = new Table_constraint_foreign_keyContext(_ctx, getState());
		enterRule(_localctx, 116, RULE_table_constraint_foreign_key);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1047);
			match(K_FOREIGN);
			setState(1048);
			match(K_KEY);
			setState(1049);
			match(OPEN_PAR);
			setState(1050);
			fk_origin_column_name();
			setState(1055);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1051);
				match(COMMA);
				setState(1052);
				fk_origin_column_name();
				}
				}
				setState(1057);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1058);
			match(CLOSE_PAR);
			setState(1059);
			foreign_key_clause();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_constraint_uniqueContext extends ParserRuleContext {
		public TerminalNode K_UNIQUE() { return getToken(SQLParser.K_UNIQUE, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<Indexed_columnContext> indexed_column() {
			return getRuleContexts(Indexed_columnContext.class);
		}
		public Indexed_columnContext indexed_column(int i) {
			return getRuleContext(Indexed_columnContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode K_KEY() { return getToken(SQLParser.K_KEY, 0); }
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Table_constraint_uniqueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_constraint_unique; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTable_constraint_unique(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTable_constraint_unique(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTable_constraint_unique(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_constraint_uniqueContext table_constraint_unique() throws RecognitionException {
		Table_constraint_uniqueContext _localctx = new Table_constraint_uniqueContext(_ctx, getState());
		enterRule(_localctx, 118, RULE_table_constraint_unique);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1061);
			match(K_UNIQUE);
			setState(1063);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,122,_ctx) ) {
			case 1:
				{
				setState(1062);
				match(K_KEY);
				}
				break;
			}
			setState(1066);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,123,_ctx) ) {
			case 1:
				{
				setState(1065);
				name();
				}
				break;
			}
			setState(1068);
			match(OPEN_PAR);
			setState(1069);
			indexed_column();
			setState(1074);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1070);
				match(COMMA);
				setState(1071);
				indexed_column();
				}
				}
				setState(1076);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1077);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_constraint_keyContext extends ParserRuleContext {
		public TerminalNode K_KEY() { return getToken(SQLParser.K_KEY, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<Indexed_columnContext> indexed_column() {
			return getRuleContexts(Indexed_columnContext.class);
		}
		public Indexed_columnContext indexed_column(int i) {
			return getRuleContext(Indexed_columnContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Table_constraint_keyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_constraint_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTable_constraint_key(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTable_constraint_key(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTable_constraint_key(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_constraint_keyContext table_constraint_key() throws RecognitionException {
		Table_constraint_keyContext _localctx = new Table_constraint_keyContext(_ctx, getState());
		enterRule(_localctx, 120, RULE_table_constraint_key);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1079);
			match(K_KEY);
			setState(1081);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,125,_ctx) ) {
			case 1:
				{
				setState(1080);
				name();
				}
				break;
			}
			setState(1083);
			match(OPEN_PAR);
			setState(1084);
			indexed_column();
			setState(1089);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1085);
				match(COMMA);
				setState(1086);
				indexed_column();
				}
				}
				setState(1091);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1092);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Fk_origin_column_nameContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public Fk_origin_column_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fk_origin_column_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterFk_origin_column_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitFk_origin_column_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitFk_origin_column_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Fk_origin_column_nameContext fk_origin_column_name() throws RecognitionException {
		Fk_origin_column_nameContext _localctx = new Fk_origin_column_nameContext(_ctx, getState());
		enterRule(_localctx, 122, RULE_fk_origin_column_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1094);
			column_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Qualified_table_nameContext extends ParserRuleContext {
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(SQLParser.DOT, 0); }
		public TerminalNode K_INDEXED() { return getToken(SQLParser.K_INDEXED, 0); }
		public TerminalNode K_BY() { return getToken(SQLParser.K_BY, 0); }
		public Index_nameContext index_name() {
			return getRuleContext(Index_nameContext.class,0);
		}
		public TerminalNode K_NOT() { return getToken(SQLParser.K_NOT, 0); }
		public Qualified_table_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_qualified_table_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterQualified_table_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitQualified_table_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitQualified_table_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Qualified_table_nameContext qualified_table_name() throws RecognitionException {
		Qualified_table_nameContext _localctx = new Qualified_table_nameContext(_ctx, getState());
		enterRule(_localctx, 124, RULE_qualified_table_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1099);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,127,_ctx) ) {
			case 1:
				{
				setState(1096);
				database_name();
				setState(1097);
				match(DOT);
				}
				break;
			}
			setState(1101);
			table_name();
			setState(1107);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_INDEXED:
				{
				setState(1102);
				match(K_INDEXED);
				setState(1103);
				match(K_BY);
				setState(1104);
				index_name();
				}
				break;
			case K_NOT:
				{
				setState(1105);
				match(K_NOT);
				setState(1106);
				match(K_INDEXED);
				}
				break;
			case EOF:
				break;
			default:
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Ordering_termContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode K_ASC() { return getToken(SQLParser.K_ASC, 0); }
		public TerminalNode K_DESC() { return getToken(SQLParser.K_DESC, 0); }
		public Ordering_termContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ordering_term; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterOrdering_term(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitOrdering_term(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitOrdering_term(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Ordering_termContext ordering_term() throws RecognitionException {
		Ordering_termContext _localctx = new Ordering_termContext(_ctx, getState());
		enterRule(_localctx, 126, RULE_ordering_term);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1109);
			expr(0);
			setState(1111);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_ASC || _la==K_DESC) {
				{
				setState(1110);
				_la = _input.LA(1);
				if ( !(_la==K_ASC || _la==K_DESC) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Result_columnContext extends ParserRuleContext {
		public TerminalNode STAR() { return getToken(SQLParser.STAR, 0); }
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(SQLParser.DOT, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Column_aliasContext column_alias() {
			return getRuleContext(Column_aliasContext.class,0);
		}
		public TerminalNode K_AS() { return getToken(SQLParser.K_AS, 0); }
		public Result_columnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_result_column; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterResult_column(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitResult_column(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitResult_column(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Result_columnContext result_column() throws RecognitionException {
		Result_columnContext _localctx = new Result_columnContext(_ctx, getState());
		enterRule(_localctx, 128, RULE_result_column);
		int _la;
		try {
			setState(1125);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,132,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1113);
				match(STAR);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1114);
				table_name();
				setState(1115);
				match(DOT);
				setState(1116);
				match(STAR);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1118);
				expr(0);
				setState(1123);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,131,_ctx) ) {
				case 1:
					{
					setState(1120);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_AS) {
						{
						setState(1119);
						match(K_AS);
						}
					}

					setState(1122);
					column_alias();
					}
					break;
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_or_subqueryContext extends ParserRuleContext {
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(SQLParser.DOT, 0); }
		public Table_aliasContext table_alias() {
			return getRuleContext(Table_aliasContext.class,0);
		}
		public TerminalNode K_INDEXED() { return getToken(SQLParser.K_INDEXED, 0); }
		public TerminalNode K_BY() { return getToken(SQLParser.K_BY, 0); }
		public Index_nameContext index_name() {
			return getRuleContext(Index_nameContext.class,0);
		}
		public TerminalNode K_NOT() { return getToken(SQLParser.K_NOT, 0); }
		public TerminalNode K_AS() { return getToken(SQLParser.K_AS, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public List<Table_or_subqueryContext> table_or_subquery() {
			return getRuleContexts(Table_or_subqueryContext.class);
		}
		public Table_or_subqueryContext table_or_subquery(int i) {
			return getRuleContext(Table_or_subqueryContext.class,i);
		}
		public Join_clauseContext join_clause() {
			return getRuleContext(Join_clauseContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public Table_or_subqueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_or_subquery; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTable_or_subquery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTable_or_subquery(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTable_or_subquery(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_or_subqueryContext table_or_subquery() throws RecognitionException {
		Table_or_subqueryContext _localctx = new Table_or_subqueryContext(_ctx, getState());
		enterRule(_localctx, 130, RULE_table_or_subquery);
		int _la;
		try {
			setState(1174);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,143,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1130);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,133,_ctx) ) {
				case 1:
					{
					setState(1127);
					database_name();
					setState(1128);
					match(DOT);
					}
					break;
				}
				setState(1132);
				table_name();
				setState(1137);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,135,_ctx) ) {
				case 1:
					{
					setState(1134);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_AS) {
						{
						setState(1133);
						match(K_AS);
						}
					}

					setState(1136);
					table_alias();
					}
					break;
				}
				setState(1144);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case K_INDEXED:
					{
					setState(1139);
					match(K_INDEXED);
					setState(1140);
					match(K_BY);
					setState(1141);
					index_name();
					}
					break;
				case K_NOT:
					{
					setState(1142);
					match(K_NOT);
					setState(1143);
					match(K_INDEXED);
					}
					break;
				case EOF:
				case T__2:
				case SCOL:
				case OPEN_PAR:
				case CLOSE_PAR:
				case COMMA:
				case K_CREATE:
				case K_GROUP:
				case K_INNER:
				case K_JOIN:
				case K_LEFT:
				case K_LIMIT:
				case K_ON:
				case K_ORDER:
				case K_SELECT:
				case K_VALUES:
				case K_WHERE:
				case K_FUNCTION:
				case VARNAME:
				case UNEXPECTED_CHAR:
					break;
				default:
					break;
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1146);
				match(OPEN_PAR);
				setState(1156);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,138,_ctx) ) {
				case 1:
					{
					setState(1147);
					table_or_subquery();
					setState(1152);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(1148);
						match(COMMA);
						setState(1149);
						table_or_subquery();
						}
						}
						setState(1154);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
					break;
				case 2:
					{
					setState(1155);
					join_clause();
					}
					break;
				}
				setState(1158);
				match(CLOSE_PAR);
				setState(1163);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,140,_ctx) ) {
				case 1:
					{
					setState(1160);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_AS) {
						{
						setState(1159);
						match(K_AS);
						}
					}

					setState(1162);
					table_alias();
					}
					break;
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1165);
				match(OPEN_PAR);
				setState(1166);
				select_stmt();
				setState(1167);
				match(CLOSE_PAR);
				setState(1172);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,142,_ctx) ) {
				case 1:
					{
					setState(1169);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_AS) {
						{
						setState(1168);
						match(K_AS);
						}
					}

					setState(1171);
					table_alias();
					}
					break;
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Join_clauseContext extends ParserRuleContext {
		public List<Table_or_subqueryContext> table_or_subquery() {
			return getRuleContexts(Table_or_subqueryContext.class);
		}
		public Table_or_subqueryContext table_or_subquery(int i) {
			return getRuleContext(Table_or_subqueryContext.class,i);
		}
		public List<Join_operatorContext> join_operator() {
			return getRuleContexts(Join_operatorContext.class);
		}
		public Join_operatorContext join_operator(int i) {
			return getRuleContext(Join_operatorContext.class,i);
		}
		public List<Join_constraintContext> join_constraint() {
			return getRuleContexts(Join_constraintContext.class);
		}
		public Join_constraintContext join_constraint(int i) {
			return getRuleContext(Join_constraintContext.class,i);
		}
		public Join_clauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_join_clause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterJoin_clause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitJoin_clause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitJoin_clause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Join_clauseContext join_clause() throws RecognitionException {
		Join_clauseContext _localctx = new Join_clauseContext(_ctx, getState());
		enterRule(_localctx, 132, RULE_join_clause);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1176);
			table_or_subquery();
			setState(1183);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,144,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1177);
					join_operator();
					setState(1178);
					table_or_subquery();
					setState(1179);
					join_constraint();
					}
					} 
				}
				setState(1185);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,144,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Join_operatorContext extends ParserRuleContext {
		public TerminalNode COMMA() { return getToken(SQLParser.COMMA, 0); }
		public TerminalNode K_JOIN() { return getToken(SQLParser.K_JOIN, 0); }
		public TerminalNode K_LEFT() { return getToken(SQLParser.K_LEFT, 0); }
		public TerminalNode K_INNER() { return getToken(SQLParser.K_INNER, 0); }
		public TerminalNode K_OUTER() { return getToken(SQLParser.K_OUTER, 0); }
		public Join_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_join_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterJoin_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitJoin_operator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitJoin_operator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Join_operatorContext join_operator() throws RecognitionException {
		Join_operatorContext _localctx = new Join_operatorContext(_ctx, getState());
		enterRule(_localctx, 134, RULE_join_operator);
		int _la;
		try {
			setState(1195);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case COMMA:
				enterOuterAlt(_localctx, 1);
				{
				setState(1186);
				match(COMMA);
				}
				break;
			case K_INNER:
			case K_JOIN:
			case K_LEFT:
				enterOuterAlt(_localctx, 2);
				{
				setState(1192);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case K_LEFT:
					{
					setState(1187);
					match(K_LEFT);
					setState(1189);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_OUTER) {
						{
						setState(1188);
						match(K_OUTER);
						}
					}

					}
					break;
				case K_INNER:
					{
					setState(1191);
					match(K_INNER);
					}
					break;
				case K_JOIN:
					break;
				default:
					break;
				}
				setState(1194);
				match(K_JOIN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Join_constraintContext extends ParserRuleContext {
		public TerminalNode K_ON() { return getToken(SQLParser.K_ON, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Join_constraintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_join_constraint; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterJoin_constraint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitJoin_constraint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitJoin_constraint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Join_constraintContext join_constraint() throws RecognitionException {
		Join_constraintContext _localctx = new Join_constraintContext(_ctx, getState());
		enterRule(_localctx, 136, RULE_join_constraint);
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(1197);
			match(K_ON);
			setState(1198);
			expr(0);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Select_coreContext extends ParserRuleContext {
		public TerminalNode K_SELECT() { return getToken(SQLParser.K_SELECT, 0); }
		public List<Result_columnContext> result_column() {
			return getRuleContexts(Result_columnContext.class);
		}
		public Result_columnContext result_column(int i) {
			return getRuleContext(Result_columnContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public TerminalNode K_FROM() { return getToken(SQLParser.K_FROM, 0); }
		public TerminalNode K_WHERE() { return getToken(SQLParser.K_WHERE, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode K_GROUP() { return getToken(SQLParser.K_GROUP, 0); }
		public TerminalNode K_BY() { return getToken(SQLParser.K_BY, 0); }
		public TerminalNode K_DISTINCT() { return getToken(SQLParser.K_DISTINCT, 0); }
		public TerminalNode K_ALL() { return getToken(SQLParser.K_ALL, 0); }
		public List<Table_or_subqueryContext> table_or_subquery() {
			return getRuleContexts(Table_or_subqueryContext.class);
		}
		public Table_or_subqueryContext table_or_subquery(int i) {
			return getRuleContext(Table_or_subqueryContext.class,i);
		}
		public Join_clauseContext join_clause() {
			return getRuleContext(Join_clauseContext.class,0);
		}
		public TerminalNode K_HAVING() { return getToken(SQLParser.K_HAVING, 0); }
		public TerminalNode K_VALUES() { return getToken(SQLParser.K_VALUES, 0); }
		public List<TerminalNode> OPEN_PAR() { return getTokens(SQLParser.OPEN_PAR); }
		public TerminalNode OPEN_PAR(int i) {
			return getToken(SQLParser.OPEN_PAR, i);
		}
		public List<TerminalNode> CLOSE_PAR() { return getTokens(SQLParser.CLOSE_PAR); }
		public TerminalNode CLOSE_PAR(int i) {
			return getToken(SQLParser.CLOSE_PAR, i);
		}
		public Select_coreContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_select_core; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterSelect_core(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitSelect_core(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitSelect_core(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Select_coreContext select_core() throws RecognitionException {
		Select_coreContext _localctx = new Select_coreContext(_ctx, getState());
		enterRule(_localctx, 138, RULE_select_core);
		int _la;
		try {
			int _alt;
			setState(1274);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_SELECT:
				enterOuterAlt(_localctx, 1);
				{
				setState(1200);
				match(K_SELECT);
				setState(1202);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,148,_ctx) ) {
				case 1:
					{
					setState(1201);
					_la = _input.LA(1);
					if ( !(_la==K_ALL || _la==K_DISTINCT) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
					break;
				}
				setState(1204);
				result_column();
				setState(1209);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,149,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1205);
						match(COMMA);
						setState(1206);
						result_column();
						}
						} 
					}
					setState(1211);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,149,_ctx);
				}
				setState(1224);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_FROM) {
					{
					setState(1212);
					match(K_FROM);
					setState(1222);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,151,_ctx) ) {
					case 1:
						{
						setState(1213);
						table_or_subquery();
						setState(1218);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,150,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(1214);
								match(COMMA);
								setState(1215);
								table_or_subquery();
								}
								} 
							}
							setState(1220);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,150,_ctx);
						}
						}
						break;
					case 2:
						{
						setState(1221);
						join_clause();
						}
						break;
					}
					}
				}

				setState(1228);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_WHERE) {
					{
					setState(1226);
					match(K_WHERE);
					setState(1227);
					expr(0);
					}
				}

				setState(1244);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_GROUP) {
					{
					setState(1230);
					match(K_GROUP);
					setState(1231);
					match(K_BY);
					setState(1232);
					expr(0);
					setState(1237);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,154,_ctx);
					while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
						if ( _alt==1 ) {
							{
							{
							setState(1233);
							match(COMMA);
							setState(1234);
							expr(0);
							}
							} 
						}
						setState(1239);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,154,_ctx);
					}
					setState(1242);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_HAVING) {
						{
						setState(1240);
						match(K_HAVING);
						setState(1241);
						expr(0);
						}
					}

					}
				}

				}
				break;
			case K_VALUES:
				enterOuterAlt(_localctx, 2);
				{
				setState(1246);
				match(K_VALUES);
				setState(1247);
				match(OPEN_PAR);
				setState(1248);
				expr(0);
				setState(1253);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(1249);
					match(COMMA);
					setState(1250);
					expr(0);
					}
					}
					setState(1255);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(1256);
				match(CLOSE_PAR);
				setState(1271);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,159,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1257);
						match(COMMA);
						setState(1258);
						match(OPEN_PAR);
						setState(1259);
						expr(0);
						setState(1264);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==COMMA) {
							{
							{
							setState(1260);
							match(COMMA);
							setState(1261);
							expr(0);
							}
							}
							setState(1266);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(1267);
						match(CLOSE_PAR);
						}
						} 
					}
					setState(1273);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,159,_ctx);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Multivar_defContext extends ParserRuleContext {
		public TerminalNode K_VAR() { return getToken(SQLParser.K_VAR, 0); }
		public List<Multivar_def_subContext> multivar_def_sub() {
			return getRuleContexts(Multivar_def_subContext.class);
		}
		public Multivar_def_subContext multivar_def_sub(int i) {
			return getRuleContext(Multivar_def_subContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Multivar_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multivar_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterMultivar_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitMultivar_def(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitMultivar_def(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Multivar_defContext multivar_def() throws RecognitionException {
		Multivar_defContext _localctx = new Multivar_defContext(_ctx, getState());
		enterRule(_localctx, 140, RULE_multivar_def);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1276);
			match(K_VAR);
			{
			setState(1277);
			multivar_def_sub();
			}
			setState(1280); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(1278);
				match(COMMA);
				{
				setState(1279);
				multivar_def_sub();
				}
				}
				}
				setState(1282); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==COMMA );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Multivar_def_subContext extends ParserRuleContext {
		public Java_var_nameContext java_var_name() {
			return getRuleContext(Java_var_nameContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public DeclirationContext decliration() {
			return getRuleContext(DeclirationContext.class,0);
		}
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public Multivar_def_subContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multivar_def_sub; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterMultivar_def_sub(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitMultivar_def_sub(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitMultivar_def_sub(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Multivar_def_subContext multivar_def_sub() throws RecognitionException {
		Multivar_def_subContext _localctx = new Multivar_def_subContext(_ctx, getState());
		enterRule(_localctx, 142, RULE_multivar_def_sub);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(1284);
			java_var_name();
			}
			setState(1290);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ASSIGN) {
				{
				setState(1285);
				match(ASSIGN);
				setState(1288);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__4:
				case OPEN_PAR:
				case STAR:
				case PLUS:
				case PLUS2:
				case MINUS2:
				case MINUS:
				case K_NULL:
				case K_TRUE:
				case K_FALSE:
				case VARNAME:
				case IDENTIFIER:
				case NUMERIC_LITERAL:
				case STRING_LITERAL:
					{
					setState(1286);
					decliration();
					}
					break;
				case K_SELECT:
				case K_VALUES:
					{
					setState(1287);
					select_stmt();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Var_defContext extends ParserRuleContext {
		public TerminalNode K_VAR() { return getToken(SQLParser.K_VAR, 0); }
		public Java_var_nameContext java_var_name() {
			return getRuleContext(Java_var_nameContext.class,0);
		}
		public Var_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterVar_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitVar_def(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitVar_def(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Var_defContext var_def() throws RecognitionException {
		Var_defContext _localctx = new Var_defContext(_ctx, getState());
		enterRule(_localctx, 144, RULE_var_def);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1292);
			match(K_VAR);
			{
			setState(1293);
			java_var_name();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Array_defContext extends ParserRuleContext {
		public Java_var_nameContext java_var_name() {
			return getRuleContext(Java_var_nameContext.class,0);
		}
		public TerminalNode OPA() { return getToken(SQLParser.OPA, 0); }
		public TerminalNode CLA() { return getToken(SQLParser.CLA, 0); }
		public TerminalNode NUMERIC_LITERAL() { return getToken(SQLParser.NUMERIC_LITERAL, 0); }
		public Array_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_array_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterArray_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitArray_def(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitArray_def(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Array_defContext array_def() throws RecognitionException {
		Array_defContext _localctx = new Array_defContext(_ctx, getState());
		enterRule(_localctx, 146, RULE_array_def);
		int _la;
		try {
			setState(1306);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OPA:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(1295);
				match(OPA);
				setState(1296);
				match(CLA);
				}
				setState(1298);
				java_var_name();
				}
				break;
			case VARNAME:
				enterOuterAlt(_localctx, 2);
				{
				setState(1299);
				java_var_name();
				{
				setState(1300);
				match(OPA);
				setState(1302);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==NUMERIC_LITERAL) {
					{
					setState(1301);
					match(NUMERIC_LITERAL);
					}
				}

				setState(1304);
				match(CLA);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Array_decContext extends ParserRuleContext {
		public TerminalNode K_NEW() { return getToken(SQLParser.K_NEW, 0); }
		public TerminalNode K_VAR() { return getToken(SQLParser.K_VAR, 0); }
		public TerminalNode OPA() { return getToken(SQLParser.OPA, 0); }
		public Num_valContext num_val() {
			return getRuleContext(Num_valContext.class,0);
		}
		public TerminalNode CLA() { return getToken(SQLParser.CLA, 0); }
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public Array_decContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_array_dec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterArray_dec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitArray_dec(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitArray_dec(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Array_decContext array_dec() throws RecognitionException {
		Array_decContext _localctx = new Array_decContext(_ctx, getState());
		enterRule(_localctx, 148, RULE_array_dec);
		try {
			setState(1315);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_NEW:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(1308);
				match(K_NEW);
				setState(1309);
				match(K_VAR);
				{
				setState(1310);
				match(OPA);
				setState(1311);
				num_val();
				setState(1312);
				match(CLA);
				}
				}
				}
				break;
			case OPEN_PAR:
			case VARNAME:
				enterOuterAlt(_localctx, 2);
				{
				setState(1314);
				variable(0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Var_decelerationContext extends ParserRuleContext {
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public Assign_opContext assign_op() {
			return getRuleContext(Assign_opContext.class,0);
		}
		public DeclirationContext decliration() {
			return getRuleContext(DeclirationContext.class,0);
		}
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public Var_decelerationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var_deceleration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterVar_deceleration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitVar_deceleration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitVar_deceleration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Var_decelerationContext var_deceleration() throws RecognitionException {
		Var_decelerationContext _localctx = new Var_decelerationContext(_ctx, getState());
		enterRule(_localctx, 150, RULE_var_deceleration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1317);
			variable(0);
			setState(1318);
			assign_op();
			setState(1321);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__4:
			case OPEN_PAR:
			case STAR:
			case PLUS:
			case PLUS2:
			case MINUS2:
			case MINUS:
			case K_NULL:
			case K_TRUE:
			case K_FALSE:
			case VARNAME:
			case IDENTIFIER:
			case NUMERIC_LITERAL:
			case STRING_LITERAL:
				{
				setState(1319);
				decliration();
				}
				break;
			case K_SELECT:
			case K_VALUES:
				{
				setState(1320);
				select_stmt();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclirationContext extends ParserRuleContext {
		public Java_valueContext java_value() {
			return getRuleContext(Java_valueContext.class,0);
		}
		public Num_expContext num_exp() {
			return getRuleContext(Num_expContext.class,0);
		}
		public Condition_expContext condition_exp() {
			return getRuleContext(Condition_expContext.class,0);
		}
		public If_inlineContext if_inline() {
			return getRuleContext(If_inlineContext.class,0);
		}
		public DeclirationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_decliration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterDecliration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitDecliration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitDecliration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclirationContext decliration() throws RecognitionException {
		DeclirationContext _localctx = new DeclirationContext(_ctx, getState());
		enterRule(_localctx, 152, RULE_decliration);
		try {
			setState(1327);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,168,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1323);
				java_value();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1324);
				num_exp(0);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1325);
				condition_exp(0);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1326);
				if_inline();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Java_valueContext extends ParserRuleContext {
		public Num_valContext num_val() {
			return getRuleContext(Num_valContext.class,0);
		}
		public Boolean_valContext boolean_val() {
			return getRuleContext(Boolean_valContext.class,0);
		}
		public String_valContext string_val() {
			return getRuleContext(String_valContext.class,0);
		}
		public TerminalNode K_NULL() { return getToken(SQLParser.K_NULL, 0); }
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public Function_callContext function_call() {
			return getRuleContext(Function_callContext.class,0);
		}
		public Java_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_java_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterJava_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitJava_value(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitJava_value(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Java_valueContext java_value() throws RecognitionException {
		Java_valueContext _localctx = new Java_valueContext(_ctx, getState());
		enterRule(_localctx, 154, RULE_java_value);
		try {
			setState(1335);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,169,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1329);
				num_val();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1330);
				boolean_val();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1331);
				string_val();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1332);
				match(K_NULL);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(1333);
				variable(0);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(1334);
				function_call();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Boolean_varContext extends ParserRuleContext {
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public Boolean_varContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolean_var; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterBoolean_var(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitBoolean_var(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitBoolean_var(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Boolean_varContext boolean_var() throws RecognitionException {
		Boolean_varContext _localctx = new Boolean_varContext(_ctx, getState());
		enterRule(_localctx, 156, RULE_boolean_var);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1337);
			variable(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Num_varContext extends ParserRuleContext {
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public Num_varContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_num_var; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterNum_var(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitNum_var(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitNum_var(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Num_varContext num_var() throws RecognitionException {
		Num_varContext _localctx = new Num_varContext(_ctx, getState());
		enterRule(_localctx, 158, RULE_num_var);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1339);
			variable(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableContext extends ParserRuleContext {
		public Java_var_nameContext java_var_name() {
			return getRuleContext(Java_var_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<VariableContext> variable() {
			return getRuleContexts(VariableContext.class);
		}
		public VariableContext variable(int i) {
			return getRuleContext(VariableContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode DOT() { return getToken(SQLParser.DOT, 0); }
		public VariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitVariable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitVariable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableContext variable() throws RecognitionException {
		return variable(0);
	}

	private VariableContext variable(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		VariableContext _localctx = new VariableContext(_ctx, _parentState);
		VariableContext _prevctx = _localctx;
		int _startState = 160;
		enterRecursionRule(_localctx, 160, RULE_variable, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1347);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case VARNAME:
				{
				setState(1342);
				java_var_name();
				}
				break;
			case OPEN_PAR:
				{
				setState(1343);
				match(OPEN_PAR);
				setState(1344);
				variable(0);
				setState(1345);
				match(CLOSE_PAR);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(1354);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,171,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new VariableContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_variable);
					setState(1349);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(1350);
					match(DOT);
					setState(1351);
					variable(3);
					}
					} 
				}
				setState(1356);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,171,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class String_valContext extends ParserRuleContext {
		public TerminalNode STRING_LITERAL() { return getToken(SQLParser.STRING_LITERAL, 0); }
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public List<TerminalNode> PLUS() { return getTokens(SQLParser.PLUS); }
		public TerminalNode PLUS(int i) {
			return getToken(SQLParser.PLUS, i);
		}
		public List<Java_valueContext> java_value() {
			return getRuleContexts(Java_valueContext.class);
		}
		public Java_valueContext java_value(int i) {
			return getRuleContext(Java_valueContext.class,i);
		}
		public String_valContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_string_val; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterString_val(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitString_val(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitString_val(this);
			else return visitor.visitChildren(this);
		}
	}

	public final String_valContext string_val() throws RecognitionException {
		String_valContext _localctx = new String_valContext(_ctx, getState());
		enterRule(_localctx, 162, RULE_string_val);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1357);
			_la = _input.LA(1);
			if ( !(_la==IDENTIFIER || _la==STRING_LITERAL) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(1362);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,172,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1358);
					match(PLUS);
					{
					setState(1359);
					java_value();
					}
					}
					} 
				}
				setState(1364);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,172,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Boolean_valContext extends ParserRuleContext {
		public TerminalNode K_FALSE() { return getToken(SQLParser.K_FALSE, 0); }
		public TerminalNode K_TRUE() { return getToken(SQLParser.K_TRUE, 0); }
		public Boolean_valContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolean_val; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterBoolean_val(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitBoolean_val(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitBoolean_val(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Boolean_valContext boolean_val() throws RecognitionException {
		Boolean_valContext _localctx = new Boolean_valContext(_ctx, getState());
		enterRule(_localctx, 164, RULE_boolean_val);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1365);
			_la = _input.LA(1);
			if ( !(_la==K_TRUE || _la==K_FALSE) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Num_valContext extends ParserRuleContext {
		public TerminalNode NUMERIC_LITERAL() { return getToken(SQLParser.NUMERIC_LITERAL, 0); }
		public Signed_numberContext signed_number() {
			return getRuleContext(Signed_numberContext.class,0);
		}
		public Num_valContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_num_val; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterNum_val(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitNum_val(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitNum_val(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Num_valContext num_val() throws RecognitionException {
		Num_valContext _localctx = new Num_valContext(_ctx, getState());
		enterRule(_localctx, 166, RULE_num_val);
		try {
			setState(1369);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,173,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1367);
				match(NUMERIC_LITERAL);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1368);
				signed_number();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Signed_numberContext extends ParserRuleContext {
		public TerminalNode NUMERIC_LITERAL() { return getToken(SQLParser.NUMERIC_LITERAL, 0); }
		public TerminalNode STAR() { return getToken(SQLParser.STAR, 0); }
		public TerminalNode PLUS() { return getToken(SQLParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(SQLParser.MINUS, 0); }
		public Signed_numberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_signed_number; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterSigned_number(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitSigned_number(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitSigned_number(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Signed_numberContext signed_number() throws RecognitionException {
		Signed_numberContext _localctx = new Signed_numberContext(_ctx, getState());
		enterRule(_localctx, 168, RULE_signed_number);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1376);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case PLUS:
			case MINUS:
			case NUMERIC_LITERAL:
				{
				setState(1372);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==PLUS || _la==MINUS) {
					{
					setState(1371);
					_la = _input.LA(1);
					if ( !(_la==PLUS || _la==MINUS) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				setState(1374);
				match(NUMERIC_LITERAL);
				}
				break;
			case STAR:
				{
				setState(1375);
				match(STAR);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Literal_valueContext extends ParserRuleContext {
		public TerminalNode NUMERIC_LITERAL() { return getToken(SQLParser.NUMERIC_LITERAL, 0); }
		public TerminalNode STRING_LITERAL() { return getToken(SQLParser.STRING_LITERAL, 0); }
		public TerminalNode BLOB_LITERAL() { return getToken(SQLParser.BLOB_LITERAL, 0); }
		public TerminalNode K_NULL() { return getToken(SQLParser.K_NULL, 0); }
		public TerminalNode K_CURRENT_TIME() { return getToken(SQLParser.K_CURRENT_TIME, 0); }
		public TerminalNode K_CURRENT_DATE() { return getToken(SQLParser.K_CURRENT_DATE, 0); }
		public TerminalNode K_CURRENT_TIMESTAMP() { return getToken(SQLParser.K_CURRENT_TIMESTAMP, 0); }
		public Literal_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterLiteral_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitLiteral_value(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitLiteral_value(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Literal_valueContext literal_value() throws RecognitionException {
		Literal_valueContext _localctx = new Literal_valueContext(_ctx, getState());
		enterRule(_localctx, 170, RULE_literal_value);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1378);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << K_CURRENT_DATE) | (1L << K_CURRENT_TIME) | (1L << K_CURRENT_TIMESTAMP))) != 0) || ((((_la - 91)) & ~0x3f) == 0 && ((1L << (_la - 91)) & ((1L << (K_NULL - 91)) | (1L << (NUMERIC_LITERAL - 91)) | (1L << (STRING_LITERAL - 91)) | (1L << (BLOB_LITERAL - 91)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assign_opContext extends ParserRuleContext {
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public Num_opContext num_op() {
			return getRuleContext(Num_opContext.class,0);
		}
		public Assign_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assign_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterAssign_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitAssign_op(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitAssign_op(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Assign_opContext assign_op() throws RecognitionException {
		Assign_opContext _localctx = new Assign_opContext(_ctx, getState());
		enterRule(_localctx, 172, RULE_assign_op);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1381);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STAR) | (1L << PLUS) | (1L << MINUS) | (1L << DIV) | (1L << MOD))) != 0)) {
				{
				setState(1380);
				num_op();
				}
			}

			setState(1383);
			match(ASSIGN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Num_opContext extends ParserRuleContext {
		public TerminalNode STAR() { return getToken(SQLParser.STAR, 0); }
		public TerminalNode DIV() { return getToken(SQLParser.DIV, 0); }
		public TerminalNode MOD() { return getToken(SQLParser.MOD, 0); }
		public TerminalNode PLUS() { return getToken(SQLParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(SQLParser.MINUS, 0); }
		public Num_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_num_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterNum_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitNum_op(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitNum_op(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Num_opContext num_op() throws RecognitionException {
		Num_opContext _localctx = new Num_opContext(_ctx, getState());
		enterRule(_localctx, 174, RULE_num_op);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1385);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STAR) | (1L << PLUS) | (1L << MINUS) | (1L << DIV) | (1L << MOD))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Comparison_opContext extends ParserRuleContext {
		public TerminalNode LT() { return getToken(SQLParser.LT, 0); }
		public TerminalNode GT() { return getToken(SQLParser.GT, 0); }
		public TerminalNode GT_EQ() { return getToken(SQLParser.GT_EQ, 0); }
		public TerminalNode LT_EQ() { return getToken(SQLParser.LT_EQ, 0); }
		public Comparison_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comparison_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterComparison_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitComparison_op(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitComparison_op(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Comparison_opContext comparison_op() throws RecognitionException {
		Comparison_opContext _localctx = new Comparison_opContext(_ctx, getState());
		enterRule(_localctx, 176, RULE_comparison_op);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1387);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LT) | (1L << LT_EQ) | (1L << GT) | (1L << GT_EQ))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Incriment_opContext extends ParserRuleContext {
		public TerminalNode PLUS2() { return getToken(SQLParser.PLUS2, 0); }
		public TerminalNode MINUS2() { return getToken(SQLParser.MINUS2, 0); }
		public Incriment_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_incriment_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterIncriment_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitIncriment_op(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitIncriment_op(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Incriment_opContext incriment_op() throws RecognitionException {
		Incriment_opContext _localctx = new Incriment_opContext(_ctx, getState());
		enterRule(_localctx, 178, RULE_incriment_op);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1389);
			_la = _input.LA(1);
			if ( !(_la==PLUS2 || _la==MINUS2) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Unary_operatorContext extends ParserRuleContext {
		public TerminalNode MINUS() { return getToken(SQLParser.MINUS, 0); }
		public TerminalNode PLUS() { return getToken(SQLParser.PLUS, 0); }
		public TerminalNode TILDE() { return getToken(SQLParser.TILDE, 0); }
		public TerminalNode K_NOT() { return getToken(SQLParser.K_NOT, 0); }
		public Unary_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unary_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterUnary_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitUnary_operator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitUnary_operator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Unary_operatorContext unary_operator() throws RecognitionException {
		Unary_operatorContext _localctx = new Unary_operatorContext(_ctx, getState());
		enterRule(_localctx, 180, RULE_unary_operator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1391);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PLUS) | (1L << MINUS) | (1L << TILDE))) != 0) || _la==K_NOT) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Error_messageContext extends ParserRuleContext {
		public TerminalNode STRING_LITERAL() { return getToken(SQLParser.STRING_LITERAL, 0); }
		public Error_messageContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_error_message; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterError_message(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitError_message(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitError_message(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Error_messageContext error_message() throws RecognitionException {
		Error_messageContext _localctx = new Error_messageContext(_ctx, getState());
		enterRule(_localctx, 182, RULE_error_message);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1393);
			match(STRING_LITERAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_aliasContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public TerminalNode VARNAME() { return getToken(SQLParser.VARNAME, 0); }
		public TerminalNode STRING_LITERAL() { return getToken(SQLParser.STRING_LITERAL, 0); }
		public Column_aliasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_alias; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterColumn_alias(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitColumn_alias(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitColumn_alias(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_aliasContext column_alias() throws RecognitionException {
		Column_aliasContext _localctx = new Column_aliasContext(_ctx, getState());
		enterRule(_localctx, 184, RULE_column_alias);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1395);
			_la = _input.LA(1);
			if ( !(((((_la - 138)) & ~0x3f) == 0 && ((1L << (_la - 138)) & ((1L << (VARNAME - 138)) | (1L << (IDENTIFIER - 138)) | (1L << (STRING_LITERAL - 138)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KeywordContext extends ParserRuleContext {
		public TerminalNode K_ACTION() { return getToken(SQLParser.K_ACTION, 0); }
		public TerminalNode K_ADD() { return getToken(SQLParser.K_ADD, 0); }
		public TerminalNode K_ALL() { return getToken(SQLParser.K_ALL, 0); }
		public TerminalNode K_ALTER() { return getToken(SQLParser.K_ALTER, 0); }
		public TerminalNode K_AND() { return getToken(SQLParser.K_AND, 0); }
		public TerminalNode K_AS() { return getToken(SQLParser.K_AS, 0); }
		public TerminalNode K_ASC() { return getToken(SQLParser.K_ASC, 0); }
		public TerminalNode K_AUTOINCREMENT() { return getToken(SQLParser.K_AUTOINCREMENT, 0); }
		public TerminalNode K_BY() { return getToken(SQLParser.K_BY, 0); }
		public TerminalNode K_CASCADE() { return getToken(SQLParser.K_CASCADE, 0); }
		public TerminalNode K_CASE() { return getToken(SQLParser.K_CASE, 0); }
		public TerminalNode K_CHECK() { return getToken(SQLParser.K_CHECK, 0); }
		public TerminalNode K_COLLATE() { return getToken(SQLParser.K_COLLATE, 0); }
		public TerminalNode K_COLUMN() { return getToken(SQLParser.K_COLUMN, 0); }
		public TerminalNode K_CONSTRAINT() { return getToken(SQLParser.K_CONSTRAINT, 0); }
		public TerminalNode K_CREATE() { return getToken(SQLParser.K_CREATE, 0); }
		public TerminalNode K_CROSS() { return getToken(SQLParser.K_CROSS, 0); }
		public TerminalNode K_CURRENT_DATE() { return getToken(SQLParser.K_CURRENT_DATE, 0); }
		public TerminalNode K_CURRENT_TIME() { return getToken(SQLParser.K_CURRENT_TIME, 0); }
		public TerminalNode K_CURRENT_TIMESTAMP() { return getToken(SQLParser.K_CURRENT_TIMESTAMP, 0); }
		public TerminalNode K_DATABASE() { return getToken(SQLParser.K_DATABASE, 0); }
		public TerminalNode K_DEFAULT() { return getToken(SQLParser.K_DEFAULT, 0); }
		public TerminalNode K_DEFERRABLE() { return getToken(SQLParser.K_DEFERRABLE, 0); }
		public TerminalNode K_DEFERRED() { return getToken(SQLParser.K_DEFERRED, 0); }
		public TerminalNode K_DELETE() { return getToken(SQLParser.K_DELETE, 0); }
		public TerminalNode K_DESC() { return getToken(SQLParser.K_DESC, 0); }
		public TerminalNode K_DISTINCT() { return getToken(SQLParser.K_DISTINCT, 0); }
		public TerminalNode K_DROP() { return getToken(SQLParser.K_DROP, 0); }
		public TerminalNode K_EACH() { return getToken(SQLParser.K_EACH, 0); }
		public TerminalNode K_ELSE() { return getToken(SQLParser.K_ELSE, 0); }
		public TerminalNode K_ENABLE() { return getToken(SQLParser.K_ENABLE, 0); }
		public TerminalNode K_EXISTS() { return getToken(SQLParser.K_EXISTS, 0); }
		public TerminalNode K_FOR() { return getToken(SQLParser.K_FOR, 0); }
		public TerminalNode K_FOREIGN() { return getToken(SQLParser.K_FOREIGN, 0); }
		public TerminalNode K_FROM() { return getToken(SQLParser.K_FROM, 0); }
		public TerminalNode K_GLOB() { return getToken(SQLParser.K_GLOB, 0); }
		public TerminalNode K_GROUP() { return getToken(SQLParser.K_GROUP, 0); }
		public TerminalNode K_HAVING() { return getToken(SQLParser.K_HAVING, 0); }
		public TerminalNode K_IF() { return getToken(SQLParser.K_IF, 0); }
		public TerminalNode K_IMMEDIATE() { return getToken(SQLParser.K_IMMEDIATE, 0); }
		public TerminalNode K_IN() { return getToken(SQLParser.K_IN, 0); }
		public TerminalNode K_INDEXED() { return getToken(SQLParser.K_INDEXED, 0); }
		public TerminalNode K_INITIALLY() { return getToken(SQLParser.K_INITIALLY, 0); }
		public TerminalNode K_INNER() { return getToken(SQLParser.K_INNER, 0); }
		public TerminalNode K_INSERT() { return getToken(SQLParser.K_INSERT, 0); }
		public TerminalNode K_INTO() { return getToken(SQLParser.K_INTO, 0); }
		public TerminalNode K_IS() { return getToken(SQLParser.K_IS, 0); }
		public TerminalNode K_ISNULL() { return getToken(SQLParser.K_ISNULL, 0); }
		public TerminalNode K_JOIN() { return getToken(SQLParser.K_JOIN, 0); }
		public TerminalNode K_KEY() { return getToken(SQLParser.K_KEY, 0); }
		public TerminalNode K_LEFT() { return getToken(SQLParser.K_LEFT, 0); }
		public TerminalNode K_LIKE() { return getToken(SQLParser.K_LIKE, 0); }
		public TerminalNode K_LIMIT() { return getToken(SQLParser.K_LIMIT, 0); }
		public TerminalNode K_MATCH() { return getToken(SQLParser.K_MATCH, 0); }
		public TerminalNode K_NO() { return getToken(SQLParser.K_NO, 0); }
		public TerminalNode K_NOT() { return getToken(SQLParser.K_NOT, 0); }
		public TerminalNode K_NOTNULL() { return getToken(SQLParser.K_NOTNULL, 0); }
		public TerminalNode K_NULL() { return getToken(SQLParser.K_NULL, 0); }
		public TerminalNode K_OF() { return getToken(SQLParser.K_OF, 0); }
		public TerminalNode K_OFFSET() { return getToken(SQLParser.K_OFFSET, 0); }
		public TerminalNode K_ON() { return getToken(SQLParser.K_ON, 0); }
		public TerminalNode K_OR() { return getToken(SQLParser.K_OR, 0); }
		public TerminalNode K_ORDER() { return getToken(SQLParser.K_ORDER, 0); }
		public TerminalNode K_OUTER() { return getToken(SQLParser.K_OUTER, 0); }
		public TerminalNode K_PRIMARY() { return getToken(SQLParser.K_PRIMARY, 0); }
		public TerminalNode K_QUERY() { return getToken(SQLParser.K_QUERY, 0); }
		public TerminalNode K_REFERENCES() { return getToken(SQLParser.K_REFERENCES, 0); }
		public TerminalNode K_REGEXP() { return getToken(SQLParser.K_REGEXP, 0); }
		public TerminalNode K_RENAME() { return getToken(SQLParser.K_RENAME, 0); }
		public TerminalNode K_REPLACE() { return getToken(SQLParser.K_REPLACE, 0); }
		public TerminalNode K_RESTRICT() { return getToken(SQLParser.K_RESTRICT, 0); }
		public TerminalNode K_RIGHT() { return getToken(SQLParser.K_RIGHT, 0); }
		public TerminalNode K_ROW() { return getToken(SQLParser.K_ROW, 0); }
		public TerminalNode K_SELECT() { return getToken(SQLParser.K_SELECT, 0); }
		public TerminalNode K_SET() { return getToken(SQLParser.K_SET, 0); }
		public TerminalNode K_TABLE() { return getToken(SQLParser.K_TABLE, 0); }
		public TerminalNode K_THEN() { return getToken(SQLParser.K_THEN, 0); }
		public TerminalNode K_TO() { return getToken(SQLParser.K_TO, 0); }
		public TerminalNode K_UNION() { return getToken(SQLParser.K_UNION, 0); }
		public TerminalNode K_UNIQUE() { return getToken(SQLParser.K_UNIQUE, 0); }
		public TerminalNode K_UPDATE() { return getToken(SQLParser.K_UPDATE, 0); }
		public TerminalNode K_VALUES() { return getToken(SQLParser.K_VALUES, 0); }
		public TerminalNode K_VIEW() { return getToken(SQLParser.K_VIEW, 0); }
		public TerminalNode K_WHERE() { return getToken(SQLParser.K_WHERE, 0); }
		public TerminalNode K_NEXTVAL() { return getToken(SQLParser.K_NEXTVAL, 0); }
		public TerminalNode K_METHODNAME() { return getToken(SQLParser.K_METHODNAME, 0); }
		public TerminalNode K_CLASSNAME() { return getToken(SQLParser.K_CLASSNAME, 0); }
		public TerminalNode K_JARPATH() { return getToken(SQLParser.K_JARPATH, 0); }
		public TerminalNode K_AGGREGATION() { return getToken(SQLParser.K_AGGREGATION, 0); }
		public TerminalNode K_VAR() { return getToken(SQLParser.K_VAR, 0); }
		public TerminalNode K_NEW() { return getToken(SQLParser.K_NEW, 0); }
		public TerminalNode K_TRUE() { return getToken(SQLParser.K_TRUE, 0); }
		public TerminalNode K_FALSE() { return getToken(SQLParser.K_FALSE, 0); }
		public TerminalNode K_FUNCTION() { return getToken(SQLParser.K_FUNCTION, 0); }
		public TerminalNode K_RETURN() { return getToken(SQLParser.K_RETURN, 0); }
		public TerminalNode K_WHILE() { return getToken(SQLParser.K_WHILE, 0); }
		public TerminalNode K_SWITCH() { return getToken(SQLParser.K_SWITCH, 0); }
		public TerminalNode K_BREAK() { return getToken(SQLParser.K_BREAK, 0); }
		public TerminalNode K_CONTINUE() { return getToken(SQLParser.K_CONTINUE, 0); }
		public TerminalNode K_DO() { return getToken(SQLParser.K_DO, 0); }
		public KeywordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_keyword; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterKeyword(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitKeyword(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitKeyword(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KeywordContext keyword() throws RecognitionException {
		KeywordContext _localctx = new KeywordContext(_ctx, getState());
		enterRule(_localctx, 186, RULE_keyword);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1397);
			_la = _input.LA(1);
			if ( !(((((_la - 35)) & ~0x3f) == 0 && ((1L << (_la - 35)) & ((1L << (K_ACTION - 35)) | (1L << (K_ADD - 35)) | (1L << (K_ALL - 35)) | (1L << (K_ALTER - 35)) | (1L << (K_AND - 35)) | (1L << (K_AS - 35)) | (1L << (K_ASC - 35)) | (1L << (K_AUTOINCREMENT - 35)) | (1L << (K_BY - 35)) | (1L << (K_CASCADE - 35)) | (1L << (K_CHECK - 35)) | (1L << (K_COLLATE - 35)) | (1L << (K_COLUMN - 35)) | (1L << (K_CONSTRAINT - 35)) | (1L << (K_CREATE - 35)) | (1L << (K_CROSS - 35)) | (1L << (K_CURRENT_DATE - 35)) | (1L << (K_CURRENT_TIME - 35)) | (1L << (K_CURRENT_TIMESTAMP - 35)) | (1L << (K_DATABASE - 35)) | (1L << (K_DEFAULT - 35)) | (1L << (K_DEFERRABLE - 35)) | (1L << (K_DEFERRED - 35)) | (1L << (K_DELETE - 35)) | (1L << (K_DESC - 35)) | (1L << (K_DISTINCT - 35)) | (1L << (K_DROP - 35)) | (1L << (K_EACH - 35)) | (1L << (K_ELSE - 35)) | (1L << (K_ENABLE - 35)) | (1L << (K_FOR - 35)) | (1L << (K_FOREIGN - 35)) | (1L << (K_FROM - 35)) | (1L << (K_GLOB - 35)) | (1L << (K_GROUP - 35)) | (1L << (K_HAVING - 35)) | (1L << (K_IF - 35)) | (1L << (K_IMMEDIATE - 35)) | (1L << (K_IN - 35)) | (1L << (K_INDEXED - 35)) | (1L << (K_INITIALLY - 35)) | (1L << (K_INNER - 35)) | (1L << (K_INSERT - 35)) | (1L << (K_INTO - 35)) | (1L << (K_IS - 35)) | (1L << (K_ISNULL - 35)) | (1L << (K_JOIN - 35)) | (1L << (K_KEY - 35)) | (1L << (K_LEFT - 35)) | (1L << (K_LIKE - 35)) | (1L << (K_LIMIT - 35)) | (1L << (K_MATCH - 35)) | (1L << (K_NEXTVAL - 35)) | (1L << (K_NO - 35)) | (1L << (K_NOT - 35)) | (1L << (K_NOTNULL - 35)) | (1L << (K_NULL - 35)) | (1L << (K_OF - 35)) | (1L << (K_OFFSET - 35)) | (1L << (K_ON - 35)) | (1L << (K_OR - 35)) | (1L << (K_ORDER - 35)) | (1L << (K_OUTER - 35)) | (1L << (K_PRIMARY - 35)))) != 0) || ((((_la - 99)) & ~0x3f) == 0 && ((1L << (_la - 99)) & ((1L << (K_QUERY - 99)) | (1L << (K_REFERENCES - 99)) | (1L << (K_REGEXP - 99)) | (1L << (K_RENAME - 99)) | (1L << (K_REPLACE - 99)) | (1L << (K_RESTRICT - 99)) | (1L << (K_RIGHT - 99)) | (1L << (K_ROW - 99)) | (1L << (K_SELECT - 99)) | (1L << (K_SET - 99)) | (1L << (K_TABLE - 99)) | (1L << (K_THEN - 99)) | (1L << (K_TO - 99)) | (1L << (K_UNION - 99)) | (1L << (K_UNIQUE - 99)) | (1L << (K_UPDATE - 99)) | (1L << (K_VALUES - 99)) | (1L << (K_VIEW - 99)) | (1L << (K_WHERE - 99)) | (1L << (K_METHODNAME - 99)) | (1L << (K_CLASSNAME - 99)) | (1L << (K_JARPATH - 99)) | (1L << (K_AGGREGATION - 99)) | (1L << (K_VAR - 99)) | (1L << (K_NEW - 99)) | (1L << (K_TRUE - 99)) | (1L << (K_FALSE - 99)) | (1L << (K_FUNCTION - 99)) | (1L << (K_RETURN - 99)) | (1L << (K_WHILE - 99)) | (1L << (K_SWITCH - 99)) | (1L << (K_CASE - 99)) | (1L << (K_BREAK - 99)) | (1L << (K_CONTINUE - 99)) | (1L << (K_DO - 99)) | (1L << (K_EXISTS - 99)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Java_function_nameContext extends ParserRuleContext {
		public Java_var_nameContext java_var_name() {
			return getRuleContext(Java_var_nameContext.class,0);
		}
		public Java_function_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_java_function_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterJava_function_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitJava_function_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitJava_function_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Java_function_nameContext java_function_name() throws RecognitionException {
		Java_function_nameContext _localctx = new Java_function_nameContext(_ctx, getState());
		enterRule(_localctx, 188, RULE_java_function_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1399);
			java_var_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Java_var_nameContext extends ParserRuleContext {
		public TerminalNode VARNAME() { return getToken(SQLParser.VARNAME, 0); }
		public Java_var_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_java_var_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterJava_var_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitJava_var_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitJava_var_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Java_var_nameContext java_var_name() throws RecognitionException {
		Java_var_nameContext _localctx = new Java_var_nameContext(_ctx, getState());
		enterRule(_localctx, 190, RULE_java_var_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1401);
			match(VARNAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnknownContext extends ParserRuleContext {
		public UnknownContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unknown; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterUnknown(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitUnknown(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitUnknown(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnknownContext unknown() throws RecognitionException {
		UnknownContext _localctx = new UnknownContext(_ctx, getState());
		enterRule(_localctx, 192, RULE_unknown);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1404); 
			_errHandler.sync(this);
			_alt = 1+1;
			do {
				switch (_alt) {
				case 1+1:
					{
					{
					setState(1403);
					matchWildcard();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(1406); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,177,_ctx);
			} while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public NameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NameContext name() throws RecognitionException {
		NameContext _localctx = new NameContext(_ctx, getState());
		enterRule(_localctx, 194, RULE_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1408);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Function_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterFunction_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitFunction_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitFunction_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_nameContext function_name() throws RecognitionException {
		Function_nameContext _localctx = new Function_nameContext(_ctx, getState());
		enterRule(_localctx, 196, RULE_function_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1410);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Database_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Database_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_database_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterDatabase_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitDatabase_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitDatabase_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Database_nameContext database_name() throws RecognitionException {
		Database_nameContext _localctx = new Database_nameContext(_ctx, getState());
		enterRule(_localctx, 198, RULE_database_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1412);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Source_table_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Source_table_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_source_table_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterSource_table_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitSource_table_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitSource_table_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Source_table_nameContext source_table_name() throws RecognitionException {
		Source_table_nameContext _localctx = new Source_table_nameContext(_ctx, getState());
		enterRule(_localctx, 200, RULE_source_table_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1414);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Table_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTable_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTable_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTable_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_nameContext table_name() throws RecognitionException {
		Table_nameContext _localctx = new Table_nameContext(_ctx, getState());
		enterRule(_localctx, 202, RULE_table_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1416);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class New_table_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public New_table_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_new_table_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterNew_table_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitNew_table_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitNew_table_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final New_table_nameContext new_table_name() throws RecognitionException {
		New_table_nameContext _localctx = new New_table_nameContext(_ctx, getState());
		enterRule(_localctx, 204, RULE_new_table_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1418);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Column_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterColumn_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitColumn_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitColumn_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_nameContext column_name() throws RecognitionException {
		Column_nameContext _localctx = new Column_nameContext(_ctx, getState());
		enterRule(_localctx, 206, RULE_column_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1420);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Collation_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Collation_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_collation_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCollation_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCollation_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitCollation_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Collation_nameContext collation_name() throws RecognitionException {
		Collation_nameContext _localctx = new Collation_nameContext(_ctx, getState());
		enterRule(_localctx, 208, RULE_collation_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1422);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Foreign_tableContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Foreign_tableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_foreign_table; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterForeign_table(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitForeign_table(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitForeign_table(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Foreign_tableContext foreign_table() throws RecognitionException {
		Foreign_tableContext _localctx = new Foreign_tableContext(_ctx, getState());
		enterRule(_localctx, 210, RULE_foreign_table);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1424);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Index_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Index_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_index_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterIndex_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitIndex_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitIndex_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Index_nameContext index_name() throws RecognitionException {
		Index_nameContext _localctx = new Index_nameContext(_ctx, getState());
		enterRule(_localctx, 212, RULE_index_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1426);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_aliasContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public TerminalNode VARNAME() { return getToken(SQLParser.VARNAME, 0); }
		public Table_aliasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_alias; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTable_alias(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTable_alias(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTable_alias(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_aliasContext table_alias() throws RecognitionException {
		Table_aliasContext _localctx = new Table_aliasContext(_ctx, getState());
		enterRule(_localctx, 214, RULE_table_alias);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1428);
			_la = _input.LA(1);
			if ( !(_la==VARNAME || _la==IDENTIFIER) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Any_nameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public TerminalNode VARNAME() { return getToken(SQLParser.VARNAME, 0); }
		public KeywordContext keyword() {
			return getRuleContext(KeywordContext.class,0);
		}
		public TerminalNode STRING_LITERAL() { return getToken(SQLParser.STRING_LITERAL, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Any_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_any_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterAny_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitAny_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitAny_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Any_nameContext any_name() throws RecognitionException {
		Any_nameContext _localctx = new Any_nameContext(_ctx, getState());
		enterRule(_localctx, 216, RULE_any_name);
		try {
			setState(1438);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(1430);
				match(IDENTIFIER);
				}
				break;
			case VARNAME:
				enterOuterAlt(_localctx, 2);
				{
				setState(1431);
				match(VARNAME);
				}
				break;
			case K_ACTION:
			case K_ADD:
			case K_ALL:
			case K_ALTER:
			case K_AND:
			case K_AS:
			case K_ASC:
			case K_AUTOINCREMENT:
			case K_BY:
			case K_CASCADE:
			case K_CHECK:
			case K_COLLATE:
			case K_COLUMN:
			case K_CONSTRAINT:
			case K_CREATE:
			case K_CROSS:
			case K_CURRENT_DATE:
			case K_CURRENT_TIME:
			case K_CURRENT_TIMESTAMP:
			case K_DATABASE:
			case K_DEFAULT:
			case K_DEFERRABLE:
			case K_DEFERRED:
			case K_DELETE:
			case K_DESC:
			case K_DISTINCT:
			case K_DROP:
			case K_EACH:
			case K_ELSE:
			case K_ENABLE:
			case K_FOR:
			case K_FOREIGN:
			case K_FROM:
			case K_GLOB:
			case K_GROUP:
			case K_HAVING:
			case K_IF:
			case K_IMMEDIATE:
			case K_IN:
			case K_INDEXED:
			case K_INITIALLY:
			case K_INNER:
			case K_INSERT:
			case K_INTO:
			case K_IS:
			case K_ISNULL:
			case K_JOIN:
			case K_KEY:
			case K_LEFT:
			case K_LIKE:
			case K_LIMIT:
			case K_MATCH:
			case K_NEXTVAL:
			case K_NO:
			case K_NOT:
			case K_NOTNULL:
			case K_NULL:
			case K_OF:
			case K_OFFSET:
			case K_ON:
			case K_OR:
			case K_ORDER:
			case K_OUTER:
			case K_PRIMARY:
			case K_QUERY:
			case K_REFERENCES:
			case K_REGEXP:
			case K_RENAME:
			case K_REPLACE:
			case K_RESTRICT:
			case K_RIGHT:
			case K_ROW:
			case K_SELECT:
			case K_SET:
			case K_TABLE:
			case K_THEN:
			case K_TO:
			case K_UNION:
			case K_UNIQUE:
			case K_UPDATE:
			case K_VALUES:
			case K_VIEW:
			case K_WHERE:
			case K_METHODNAME:
			case K_CLASSNAME:
			case K_JARPATH:
			case K_AGGREGATION:
			case K_VAR:
			case K_NEW:
			case K_TRUE:
			case K_FALSE:
			case K_FUNCTION:
			case K_RETURN:
			case K_WHILE:
			case K_SWITCH:
			case K_CASE:
			case K_BREAK:
			case K_CONTINUE:
			case K_DO:
			case K_EXISTS:
				enterOuterAlt(_localctx, 3);
				{
				setState(1432);
				keyword();
				}
				break;
			case STRING_LITERAL:
				enterOuterAlt(_localctx, 4);
				{
				setState(1433);
				match(STRING_LITERAL);
				}
				break;
			case OPEN_PAR:
				enterOuterAlt(_localctx, 5);
				{
				setState(1434);
				match(OPEN_PAR);
				setState(1435);
				any_name();
				setState(1436);
				match(CLOSE_PAR);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 9:
			return body_sempred((BodyContext)_localctx, predIndex);
		case 47:
			return condition_exp_sempred((Condition_expContext)_localctx, predIndex);
		case 48:
			return num_exp_sempred((Num_expContext)_localctx, predIndex);
		case 49:
			return expr_sempred((ExprContext)_localctx, predIndex);
		case 80:
			return variable_sempred((VariableContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean body_sempred(BodyContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean condition_exp_sempred(Condition_expContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 6);
		case 2:
			return precpred(_ctx, 3);
		}
		return true;
	}
	private boolean num_exp_sempred(Num_expContext _localctx, int predIndex) {
		switch (predIndex) {
		case 3:
			return precpred(_ctx, 5);
		case 4:
			return precpred(_ctx, 4);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 5:
			return precpred(_ctx, 12);
		case 6:
			return precpred(_ctx, 11);
		case 7:
			return precpred(_ctx, 10);
		case 8:
			return precpred(_ctx, 9);
		case 9:
			return precpred(_ctx, 8);
		case 10:
			return precpred(_ctx, 7);
		case 11:
			return precpred(_ctx, 6);
		case 12:
			return precpred(_ctx, 5);
		case 13:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean variable_sempred(VariableContext _localctx, int predIndex) {
		switch (predIndex) {
		case 14:
			return precpred(_ctx, 2);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\u0095\u05a3\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"+
		"\tI\4J\tJ\4K\tK\4L\tL\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT"+
		"\4U\tU\4V\tV\4W\tW\4X\tX\4Y\tY\4Z\tZ\4[\t[\4\\\t\\\4]\t]\4^\t^\4_\t_\4"+
		"`\t`\4a\ta\4b\tb\4c\tc\4d\td\4e\te\4f\tf\4g\tg\4h\th\4i\ti\4j\tj\4k\t"+
		"k\4l\tl\4m\tm\4n\tn\3\2\3\2\3\2\3\2\5\2\u00e1\n\2\3\2\7\2\u00e4\n\2\f"+
		"\2\16\2\u00e7\13\2\3\2\3\2\3\3\3\3\3\3\3\4\3\4\3\4\5\4\u00f1\n\4\3\4\3"+
		"\4\3\4\5\4\u00f6\n\4\3\4\3\4\3\5\3\5\3\5\7\5\u00fd\n\5\f\5\16\5\u0100"+
		"\13\5\3\6\3\6\3\7\5\7\u0105\n\7\3\7\3\7\3\7\5\7\u010a\n\7\3\7\3\7\3\7"+
		"\5\7\u010f\n\7\3\7\3\7\3\7\5\7\u0114\n\7\3\7\3\7\3\b\3\b\3\b\7\b\u011b"+
		"\n\b\f\b\16\b\u011e\13\b\3\b\3\b\3\b\7\b\u0123\n\b\f\b\16\b\u0126\13\b"+
		"\3\b\3\b\3\b\7\b\u012b\n\b\f\b\16\b\u012e\13\b\3\b\3\b\7\b\u0132\n\b\f"+
		"\b\16\b\u0135\13\b\5\b\u0137\n\b\3\t\3\t\3\n\3\n\3\n\3\n\5\n\u013f\n\n"+
		"\3\13\3\13\3\13\5\13\u0144\n\13\3\13\3\13\6\13\u0148\n\13\r\13\16\13\u0149"+
		"\3\13\5\13\u014d\n\13\5\13\u014f\n\13\3\13\3\13\6\13\u0153\n\13\r\13\16"+
		"\13\u0154\7\13\u0157\n\13\f\13\16\13\u015a\13\13\3\f\3\f\3\f\3\f\5\f\u0160"+
		"\n\f\3\r\3\r\3\r\3\r\5\r\u0166\n\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16"+
		"\3\16\3\16\5\16\u0171\n\16\3\17\3\17\3\17\3\17\3\17\3\17\5\17\u0179\n"+
		"\17\3\20\3\20\3\20\3\20\3\20\7\20\u0180\n\20\f\20\16\20\u0183\13\20\5"+
		"\20\u0185\n\20\3\20\3\20\3\21\3\21\3\21\5\21\u018c\n\21\3\22\3\22\5\22"+
		"\u0190\n\22\3\22\3\22\3\22\5\22\u0195\n\22\3\22\3\22\3\22\3\22\3\22\5"+
		"\22\u019c\n\22\5\22\u019e\n\22\3\23\3\23\5\23\u01a2\n\23\3\23\3\23\5\23"+
		"\u01a6\n\23\3\24\3\24\3\24\5\24\u01ab\n\24\3\24\3\24\3\24\3\24\3\24\3"+
		"\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3\25\5\25\u01bd\n\25"+
		"\3\25\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26\5\26\u01c9\n\26\3\26"+
		"\3\26\3\26\7\26\u01ce\n\26\f\26\16\26\u01d1\13\26\3\26\5\26\u01d4\n\26"+
		"\3\26\3\26\3\27\3\27\3\27\5\27\u01db\n\27\3\27\3\27\3\27\5\27\u01e0\n"+
		"\27\3\27\3\27\3\27\5\27\u01e5\n\27\3\27\3\27\5\27\u01e9\n\27\3\27\3\27"+
		"\3\27\5\27\u01ee\n\27\5\27\u01f0\n\27\3\30\3\30\3\30\3\30\5\30\u01f6\n"+
		"\30\3\30\3\30\3\30\5\30\u01fb\n\30\3\30\3\30\5\30\u01ff\n\30\3\30\3\30"+
		"\3\30\5\30\u0204\n\30\5\30\u0206\n\30\3\31\3\31\3\31\3\31\3\31\3\31\7"+
		"\31\u020e\n\31\f\31\16\31\u0211\13\31\3\31\5\31\u0214\n\31\3\32\3\32\3"+
		"\32\3\32\3\32\3\32\3\32\3\33\3\33\3\33\3\34\3\34\3\34\3\34\3\34\3\34\3"+
		"\34\3\34\3\34\3\34\5\34\u022a\n\34\3\35\3\35\3\35\3\35\3\35\3\35\3\35"+
		"\3\35\3\35\5\35\u0235\n\35\3\36\3\36\3\36\3\36\3\36\3\36\5\36\u023d\n"+
		"\36\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3 \3 \3 \3 \3 \3 \3 \3 \3 \5 \u024f"+
		"\n \5 \u0251\n \3!\7!\u0254\n!\f!\16!\u0257\13!\3!\3!\6!\u025b\n!\r!\16"+
		"!\u025c\3!\7!\u0260\n!\f!\16!\u0263\13!\3!\7!\u0266\n!\f!\16!\u0269\13"+
		"!\3\"\3\"\3\"\3\"\5\"\u026f\n\"\3#\3#\3#\3#\3#\5#\u0276\n#\3#\3#\3#\3"+
		"#\3#\7#\u027d\n#\f#\16#\u0280\13#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3$\3$"+
		"\3%\3%\3&\3&\3&\3&\3&\5&\u0295\n&\3&\3&\3&\3&\3&\7&\u029c\n&\f&\16&\u029f"+
		"\13&\3&\3&\3\'\3\'\3\'\3\'\3\'\3\'\7\'\u02a9\n\'\f\'\16\'\u02ac\13\'\5"+
		"\'\u02ae\n\'\3\'\3\'\3\'\3\'\5\'\u02b4\n\'\5\'\u02b6\n\'\3(\3(\3(\3)\3"+
		")\3)\3)\5)\u02bf\n)\3)\3)\3)\3)\3)\5)\u02c6\n)\3)\3)\3)\5)\u02cb\n)\3"+
		")\3)\5)\u02cf\n)\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3+\3"+
		"+\3,\3,\3-\3-\3.\3.\3/\3/\3\60\3\60\3\60\3\60\7\60\u02ef\n\60\f\60\16"+
		"\60\u02f2\13\60\5\60\u02f4\n\60\3\60\3\60\3\61\3\61\3\61\3\61\5\61\u02fc"+
		"\n\61\3\61\3\61\3\61\3\61\3\61\3\61\5\61\u0304\n\61\3\61\3\61\3\61\5\61"+
		"\u0309\n\61\3\61\3\61\3\61\3\61\5\61\u030f\n\61\3\61\3\61\3\61\3\61\5"+
		"\61\u0315\n\61\3\61\3\61\5\61\u0319\n\61\3\61\3\61\3\61\3\61\3\61\3\61"+
		"\7\61\u0321\n\61\f\61\16\61\u0324\13\61\3\62\3\62\3\62\5\62\u0329\n\62"+
		"\3\62\3\62\5\62\u032d\n\62\3\62\3\62\3\62\3\62\3\62\3\62\3\62\5\62\u0336"+
		"\n\62\3\62\3\62\3\62\3\62\3\62\3\62\7\62\u033e\n\62\f\62\16\62\u0341\13"+
		"\62\3\63\3\63\3\63\3\63\3\63\5\63\u0348\n\63\3\63\3\63\3\63\5\63\u034d"+
		"\n\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\5\63\u0359\n\63"+
		"\3\63\5\63\u035c\n\63\3\63\3\63\3\63\3\63\5\63\u0362\n\63\3\63\3\63\3"+
		"\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3"+
		"\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\5\63\u0380"+
		"\n\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\7\63\u038b\n\63\f\63"+
		"\16\63\u038e\13\63\3\64\3\64\3\64\5\64\u0393\n\64\3\64\3\64\3\64\7\64"+
		"\u0398\n\64\f\64\16\64\u039b\13\64\3\64\5\64\u039e\n\64\3\64\3\64\3\65"+
		"\5\65\u03a3\n\65\3\65\3\65\3\65\3\65\3\65\3\65\7\65\u03ab\n\65\f\65\16"+
		"\65\u03ae\13\65\5\65\u03b0\n\65\3\65\3\65\3\65\3\65\5\65\u03b6\n\65\3"+
		"\65\5\65\u03b9\n\65\3\66\3\66\3\66\3\66\5\66\u03bf\n\66\3\66\3\66\3\66"+
		"\3\66\3\66\7\66\u03c6\n\66\f\66\16\66\u03c9\13\66\3\66\3\66\5\66\u03cd"+
		"\n\66\3\66\3\66\3\66\5\66\u03d2\n\66\7\66\u03d4\n\66\f\66\16\66\u03d7"+
		"\13\66\3\66\5\66\u03da\n\66\3\66\3\66\3\66\3\66\3\66\5\66\u03e1\n\66\3"+
		"\66\5\66\u03e4\n\66\5\66\u03e6\n\66\3\67\3\67\3\67\3\67\3\67\3\67\3\67"+
		"\3\67\3\67\3\67\5\67\u03f2\n\67\38\38\39\39\39\59\u03f9\n9\39\59\u03fc"+
		"\n9\3:\3:\5:\u0400\n:\3:\3:\3:\3:\3:\3:\3:\3:\3:\5:\u040b\n:\3;\3;\3;"+
		"\3;\3;\3;\7;\u0413\n;\f;\16;\u0416\13;\3;\3;\3<\3<\3<\3<\3<\3<\7<\u0420"+
		"\n<\f<\16<\u0423\13<\3<\3<\3<\3=\3=\5=\u042a\n=\3=\5=\u042d\n=\3=\3=\3"+
		"=\3=\7=\u0433\n=\f=\16=\u0436\13=\3=\3=\3>\3>\5>\u043c\n>\3>\3>\3>\3>"+
		"\7>\u0442\n>\f>\16>\u0445\13>\3>\3>\3?\3?\3@\3@\3@\5@\u044e\n@\3@\3@\3"+
		"@\3@\3@\3@\5@\u0456\n@\3A\3A\5A\u045a\nA\3B\3B\3B\3B\3B\3B\3B\5B\u0463"+
		"\nB\3B\5B\u0466\nB\5B\u0468\nB\3C\3C\3C\5C\u046d\nC\3C\3C\5C\u0471\nC"+
		"\3C\5C\u0474\nC\3C\3C\3C\3C\3C\5C\u047b\nC\3C\3C\3C\3C\7C\u0481\nC\fC"+
		"\16C\u0484\13C\3C\5C\u0487\nC\3C\3C\5C\u048b\nC\3C\5C\u048e\nC\3C\3C\3"+
		"C\3C\5C\u0494\nC\3C\5C\u0497\nC\5C\u0499\nC\3D\3D\3D\3D\3D\7D\u04a0\n"+
		"D\fD\16D\u04a3\13D\3E\3E\3E\5E\u04a8\nE\3E\5E\u04ab\nE\3E\5E\u04ae\nE"+
		"\3F\3F\3F\3G\3G\5G\u04b5\nG\3G\3G\3G\7G\u04ba\nG\fG\16G\u04bd\13G\3G\3"+
		"G\3G\3G\7G\u04c3\nG\fG\16G\u04c6\13G\3G\5G\u04c9\nG\5G\u04cb\nG\3G\3G"+
		"\5G\u04cf\nG\3G\3G\3G\3G\3G\7G\u04d6\nG\fG\16G\u04d9\13G\3G\3G\5G\u04dd"+
		"\nG\5G\u04df\nG\3G\3G\3G\3G\3G\7G\u04e6\nG\fG\16G\u04e9\13G\3G\3G\3G\3"+
		"G\3G\3G\7G\u04f1\nG\fG\16G\u04f4\13G\3G\3G\7G\u04f8\nG\fG\16G\u04fb\13"+
		"G\5G\u04fd\nG\3H\3H\3H\3H\6H\u0503\nH\rH\16H\u0504\3I\3I\3I\3I\5I\u050b"+
		"\nI\5I\u050d\nI\3J\3J\3J\3K\3K\3K\3K\3K\3K\3K\5K\u0519\nK\3K\3K\5K\u051d"+
		"\nK\3L\3L\3L\3L\3L\3L\3L\5L\u0526\nL\3M\3M\3M\3M\5M\u052c\nM\3N\3N\3N"+
		"\3N\5N\u0532\nN\3O\3O\3O\3O\3O\3O\5O\u053a\nO\3P\3P\3Q\3Q\3R\3R\3R\3R"+
		"\3R\3R\5R\u0546\nR\3R\3R\3R\7R\u054b\nR\fR\16R\u054e\13R\3S\3S\3S\7S\u0553"+
		"\nS\fS\16S\u0556\13S\3T\3T\3U\3U\5U\u055c\nU\3V\5V\u055f\nV\3V\3V\5V\u0563"+
		"\nV\3W\3W\3X\5X\u0568\nX\3X\3X\3Y\3Y\3Z\3Z\3[\3[\3\\\3\\\3]\3]\3^\3^\3"+
		"_\3_\3`\3`\3a\3a\3b\6b\u057f\nb\rb\16b\u0580\3c\3c\3d\3d\3e\3e\3f\3f\3"+
		"g\3g\3h\3h\3i\3i\3j\3j\3k\3k\3l\3l\3m\3m\3n\3n\3n\3n\3n\3n\3n\3n\5n\u05a1"+
		"\nn\3n\3\u0580\7\24`bd\u00a2o\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 "+
		"\"$&(*,.\60\62\64\668:<>@BDFHJLNPRTVXZ\\^`bdfhjlnprtvxz|~\u0080\u0082"+
		"\u0084\u0086\u0088\u008a\u008c\u008e\u0090\u0092\u0094\u0096\u0098\u009a"+
		"\u009c\u009e\u00a0\u00a2\u00a4\u00a6\u00a8\u00aa\u00ac\u00ae\u00b0\u00b2"+
		"\u00b4\u00b6\u00b8\u00ba\u00bc\u00be\u00c0\u00c2\u00c4\u00c6\u00c8\u00ca"+
		"\u00cc\u00ce\u00d0\u00d2\u00d4\u00d6\u00d8\u00da\2\25\4\2\16\16__\3\2"+
		"\36!\3\2\"$\4\2\26\27\34\35\4\2\20\20\30\31\4\2\21\21\24\24\3\2\32\35"+
		"\4\2<<uu\4\2++==\4\2\'\'>>\4\2\u008d\u008d\u0090\u0090\3\2\u0081\u0082"+
		"\6\2\65\67]]\u008e\u008e\u0090\u0091\5\2\20\21\24\24\30\31\3\2\22\23\5"+
		"\2\21\21\24\25[[\4\2\u008c\u008d\u0090\u0090\5\2%oq|\177\u008b\3\2\u008c"+
		"\u008d\2\u0632\2\u00e5\3\2\2\2\4\u00ea\3\2\2\2\6\u00f0\3\2\2\2\b\u00f9"+
		"\3\2\2\2\n\u0101\3\2\2\2\f\u0104\3\2\2\2\16\u0136\3\2\2\2\20\u0138\3\2"+
		"\2\2\22\u013a\3\2\2\2\24\u014e\3\2\2\2\26\u015f\3\2\2\2\30\u0165\3\2\2"+
		"\2\32\u0170\3\2\2\2\34\u0178\3\2\2\2\36\u017a\3\2\2\2 \u018b\3\2\2\2\""+
		"\u019d\3\2\2\2$\u01a1\3\2\2\2&\u01a7\3\2\2\2(\u01b9\3\2\2\2*\u01c3\3\2"+
		"\2\2,\u01d7\3\2\2\2.\u01f1\3\2\2\2\60\u0207\3\2\2\2\62\u0215\3\2\2\2\64"+
		"\u021c\3\2\2\2\66\u0229\3\2\2\28\u0234\3\2\2\2:\u0236\3\2\2\2<\u023e\3"+
		"\2\2\2>\u0245\3\2\2\2@\u0255\3\2\2\2B\u026e\3\2\2\2D\u0270\3\2\2\2F\u028b"+
		"\3\2\2\2H\u028d\3\2\2\2J\u028f\3\2\2\2L\u02a2\3\2\2\2N\u02b7\3\2\2\2P"+
		"\u02ba\3\2\2\2R\u02d0\3\2\2\2T\u02e0\3\2\2\2V\u02e2\3\2\2\2X\u02e4\3\2"+
		"\2\2Z\u02e6\3\2\2\2\\\u02e8\3\2\2\2^\u02ea\3\2\2\2`\u0318\3\2\2\2b\u0335"+
		"\3\2\2\2d\u0361\3\2\2\2f\u038f\3\2\2\2h\u03a2\3\2\2\2j\u03ba\3\2\2\2l"+
		"\u03e7\3\2\2\2n\u03f3\3\2\2\2p\u03f5\3\2\2\2r\u03ff\3\2\2\2t\u040c\3\2"+
		"\2\2v\u0419\3\2\2\2x\u0427\3\2\2\2z\u0439\3\2\2\2|\u0448\3\2\2\2~\u044d"+
		"\3\2\2\2\u0080\u0457\3\2\2\2\u0082\u0467\3\2\2\2\u0084\u0498\3\2\2\2\u0086"+
		"\u049a\3\2\2\2\u0088\u04ad\3\2\2\2\u008a\u04af\3\2\2\2\u008c\u04fc\3\2"+
		"\2\2\u008e\u04fe\3\2\2\2\u0090\u0506\3\2\2\2\u0092\u050e\3\2\2\2\u0094"+
		"\u051c\3\2\2\2\u0096\u0525\3\2\2\2\u0098\u0527\3\2\2\2\u009a\u0531\3\2"+
		"\2\2\u009c\u0539\3\2\2\2\u009e\u053b\3\2\2\2\u00a0\u053d\3\2\2\2\u00a2"+
		"\u0545\3\2\2\2\u00a4\u054f\3\2\2\2\u00a6\u0557\3\2\2\2\u00a8\u055b\3\2"+
		"\2\2\u00aa\u0562\3\2\2\2\u00ac\u0564\3\2\2\2\u00ae\u0567\3\2\2\2\u00b0"+
		"\u056b\3\2\2\2\u00b2\u056d\3\2\2\2\u00b4\u056f\3\2\2\2\u00b6\u0571\3\2"+
		"\2\2\u00b8\u0573\3\2\2\2\u00ba\u0575\3\2\2\2\u00bc\u0577\3\2\2\2\u00be"+
		"\u0579\3\2\2\2\u00c0\u057b\3\2\2\2\u00c2\u057e\3\2\2\2\u00c4\u0582\3\2"+
		"\2\2\u00c6\u0584\3\2\2\2\u00c8\u0586\3\2\2\2\u00ca\u0588\3\2\2\2\u00cc"+
		"\u058a\3\2\2\2\u00ce\u058c\3\2\2\2\u00d0\u058e\3\2\2\2\u00d2\u0590\3\2"+
		"\2\2\u00d4\u0592\3\2\2\2\u00d6\u0594\3\2\2\2\u00d8\u0596\3\2\2\2\u00da"+
		"\u05a0\3\2\2\2\u00dc\u00e4\5@!\2\u00dd\u00e4\5\f\7\2\u00de\u00e0\5\6\4"+
		"\2\u00df\u00e1\7\b\2\2\u00e0\u00df\3\2\2\2\u00e0\u00e1\3\2\2\2\u00e1\u00e4"+
		"\3\2\2\2\u00e2\u00e4\5\4\3\2\u00e3\u00dc\3\2\2\2\u00e3\u00dd\3\2\2\2\u00e3"+
		"\u00de\3\2\2\2\u00e3\u00e2\3\2\2\2\u00e4\u00e7\3\2\2\2\u00e5\u00e3\3\2"+
		"\2\2\u00e5\u00e6\3\2\2\2\u00e6\u00e8\3\2\2\2\u00e7\u00e5\3\2\2\2\u00e8"+
		"\u00e9\7\2\2\3\u00e9\3\3\2\2\2\u00ea\u00eb\7\u0095\2\2\u00eb\u00ec\b\3"+
		"\1\2\u00ec\5\3\2\2\2\u00ed\u00ee\5\u00a2R\2\u00ee\u00ef\7\t\2\2\u00ef"+
		"\u00f1\3\2\2\2\u00f0\u00ed\3\2\2\2\u00f0\u00f1\3\2\2\2\u00f1\u00f2\3\2"+
		"\2\2\u00f2\u00f3\5\u00be`\2\u00f3\u00f5\7\f\2\2\u00f4\u00f6\5\b\5\2\u00f5"+
		"\u00f4\3\2\2\2\u00f5\u00f6\3\2\2\2\u00f6\u00f7\3\2\2\2\u00f7\u00f8\7\r"+
		"\2\2\u00f8\7\3\2\2\2\u00f9\u00fe\5\n\6\2\u00fa\u00fb\7\16\2\2\u00fb\u00fd"+
		"\5\n\6\2\u00fc\u00fa\3\2\2\2\u00fd\u0100\3\2\2\2\u00fe\u00fc\3\2\2\2\u00fe"+
		"\u00ff\3\2\2\2\u00ff\t\3\2\2\2\u0100\u00fe\3\2\2\2\u0101\u0102\5\u009a"+
		"N\2\u0102\13\3\2\2\2\u0103\u0105\7\u0083\2\2\u0104\u0103\3\2\2\2\u0104"+
		"\u0105\3\2\2\2\u0105\u0106\3\2\2\2\u0106\u0107\5\u00be`\2\u0107\u0109"+
		"\7\f\2\2\u0108\u010a\5\16\b\2\u0109\u0108\3\2\2\2\u0109\u010a\3\2\2\2"+
		"\u010a\u010b\3\2\2\2\u010b\u010c\7\r\2\2\u010c\u010e\7\3\2\2\u010d\u010f"+
		"\5\24\13\2\u010e\u010d\3\2\2\2\u010e\u010f\3\2\2\2\u010f\u0113\3\2\2\2"+
		"\u0110\u0111\5 \21\2\u0111\u0112\7\b\2\2\u0112\u0114\3\2\2\2\u0113\u0110"+
		"\3\2\2\2\u0113\u0114\3\2\2\2\u0114\u0115\3\2\2\2\u0115\u0116\7\4\2\2\u0116"+
		"\r\3\2\2\2\u0117\u011c\5\20\t\2\u0118\u0119\7\16\2\2\u0119\u011b\5\20"+
		"\t\2\u011a\u0118\3\2\2\2\u011b\u011e\3\2\2\2\u011c\u011a\3\2\2\2\u011c"+
		"\u011d\3\2\2\2\u011d\u0137\3\2\2\2\u011e\u011c\3\2\2\2\u011f\u0124\5\22"+
		"\n\2\u0120\u0121\7\16\2\2\u0121\u0123\5\22\n\2\u0122\u0120\3\2\2\2\u0123"+
		"\u0126\3\2\2\2\u0124\u0122\3\2\2\2\u0124\u0125\3\2\2\2\u0125\u0137\3\2"+
		"\2\2\u0126\u0124\3\2\2\2\u0127\u012c\5\20\t\2\u0128\u0129\7\16\2\2\u0129"+
		"\u012b\5\20\t\2\u012a\u0128\3\2\2\2\u012b\u012e\3\2\2\2\u012c\u012a\3"+
		"\2\2\2\u012c\u012d\3\2\2\2\u012d\u0133\3\2\2\2\u012e\u012c\3\2\2\2\u012f"+
		"\u0130\7\16\2\2\u0130\u0132\5\22\n\2\u0131\u012f\3\2\2\2\u0132\u0135\3"+
		"\2\2\2\u0133\u0131\3\2\2\2\u0133\u0134\3\2\2\2\u0134\u0137\3\2\2\2\u0135"+
		"\u0133\3\2\2\2\u0136\u0117\3\2\2\2\u0136\u011f\3\2\2\2\u0136\u0127\3\2"+
		"\2\2\u0137\17\3\2\2\2\u0138\u0139\5\u0092J\2\u0139\21\3\2\2\2\u013a\u013b"+
		"\5\u0092J\2\u013b\u013e\7\17\2\2\u013c\u013f\5\u009aN\2\u013d\u013f\5"+
		"L\'\2\u013e\u013c\3\2\2\2\u013e\u013d\3\2\2\2\u013f\23\3\2\2\2\u0140\u0141"+
		"\b\13\1\2\u0141\u0143\7\3\2\2\u0142\u0144\5\24\13\2\u0143\u0142\3\2\2"+
		"\2\u0143\u0144\3\2\2\2\u0144\u0145\3\2\2\2\u0145\u014f\7\4\2\2\u0146\u0148"+
		"\5\26\f\2\u0147\u0146\3\2\2\2\u0148\u0149\3\2\2\2\u0149\u0147\3\2\2\2"+
		"\u0149\u014a\3\2\2\2\u014a\u014c\3\2\2\2\u014b\u014d\5\24\13\2\u014c\u014b"+
		"\3\2\2\2\u014c\u014d\3\2\2\2\u014d\u014f\3\2\2\2\u014e\u0140\3\2\2\2\u014e"+
		"\u0147\3\2\2\2\u014f\u0158\3\2\2\2\u0150\u0152\f\3\2\2\u0151\u0153\5\26"+
		"\f\2\u0152\u0151\3\2\2\2\u0153\u0154\3\2\2\2\u0154\u0152\3\2\2\2\u0154"+
		"\u0155\3\2\2\2\u0155\u0157\3\2\2\2\u0156\u0150\3\2\2\2\u0157\u015a\3\2"+
		"\2\2\u0158\u0156\3\2\2\2\u0158\u0159\3\2\2\2\u0159\25\3\2\2\2\u015a\u0158"+
		"\3\2\2\2\u015b\u015c\5\32\16\2\u015c\u015d\7\b\2\2\u015d\u0160\3\2\2\2"+
		"\u015e\u0160\5\30\r\2\u015f\u015b\3\2\2\2\u015f\u015e\3\2\2\2\u0160\27"+
		"\3\2\2\2\u0161\u0166\5*\26\2\u0162\u0166\5$\23\2\u0163\u0166\5\60\31\2"+
		"\u0164\u0166\5:\36\2\u0165\u0161\3\2\2\2\u0165\u0162\3\2\2\2\u0165\u0163"+
		"\3\2\2\2\u0165\u0164\3\2\2\2\u0166\31\3\2\2\2\u0167\u0171\5\u0092J\2\u0168"+
		"\u0171\5\22\n\2\u0169\u0171\5\u0098M\2\u016a\u0171\5\u008eH\2\u016b\u0171"+
		"\5\6\4\2\u016c\u0171\5\36\20\2\u016d\u0171\5\34\17\2\u016e\u0171\5\66"+
		"\34\2\u016f\u0171\5<\37\2\u0170\u0167\3\2\2\2\u0170\u0168\3\2\2\2\u0170"+
		"\u0169\3\2\2\2\u0170\u016a\3\2\2\2\u0170\u016b\3\2\2\2\u0170\u016c\3\2"+
		"\2\2\u0170\u016d\3\2\2\2\u0170\u016e\3\2\2\2\u0170\u016f\3\2\2\2\u0171"+
		"\33\3\2\2\2\u0172\u0173\5\u00a2R\2\u0173\u0174\5\u00b4[\2\u0174\u0179"+
		"\3\2\2\2\u0175\u0176\5\u00b4[\2\u0176\u0177\5\u00a2R\2\u0177\u0179\3\2"+
		"\2\2\u0178\u0172\3\2\2\2\u0178\u0175\3\2\2\2\u0179\35\3\2\2\2\u017a\u017b"+
		"\7~\2\2\u017b\u0184\7\f\2\2\u017c\u0181\5\u009cO\2\u017d\u017e\7\21\2"+
		"\2\u017e\u0180\5\u009cO\2\u017f\u017d\3\2\2\2\u0180\u0183\3\2\2\2\u0181"+
		"\u017f\3\2\2\2\u0181\u0182\3\2\2\2\u0182\u0185\3\2\2\2\u0183\u0181\3\2"+
		"\2\2\u0184\u017c\3\2\2\2\u0184\u0185\3\2\2\2\u0185\u0186\3\2\2\2\u0186"+
		"\u0187\7\r\2\2\u0187\37\3\2\2\2\u0188\u018c\7\u0088\2\2\u0189\u018c\7"+
		"\u0089\2\2\u018a\u018c\5> \2\u018b\u0188\3\2\2\2\u018b\u0189\3\2\2\2\u018b"+
		"\u018a\3\2\2\2\u018c!\3\2\2\2\u018d\u018f\7\3\2\2\u018e\u0190\5\24\13"+
		"\2\u018f\u018e\3\2\2\2\u018f\u0190\3\2\2\2\u0190\u0194\3\2\2\2\u0191\u0192"+
		"\5 \21\2\u0192\u0193\7\b\2\2\u0193\u0195\3\2\2\2\u0194\u0191\3\2\2\2\u0194"+
		"\u0195\3\2\2\2\u0195\u0196\3\2\2\2\u0196\u019e\7\4\2\2\u0197\u019c\5\26"+
		"\f\2\u0198\u0199\5 \21\2\u0199\u019a\7\b\2\2\u019a\u019c\3\2\2\2\u019b"+
		"\u0197\3\2\2\2\u019b\u0198\3\2\2\2\u019c\u019e\3\2\2\2\u019d\u018d\3\2"+
		"\2\2\u019d\u019b\3\2\2\2\u019e#\3\2\2\2\u019f\u01a2\5&\24\2\u01a0\u01a2"+
		"\5(\25\2\u01a1\u019f\3\2\2\2\u01a1\u01a0\3\2\2\2\u01a2\u01a5\3\2\2\2\u01a3"+
		"\u01a6\5\"\22\2\u01a4\u01a6\7\b\2\2\u01a5\u01a3\3\2\2\2\u01a5\u01a4\3"+
		"\2\2\2\u01a6%\3\2\2\2\u01a7\u01a8\7C\2\2\u01a8\u01aa\7\f\2\2\u01a9\u01ab"+
		"\7\177\2\2\u01aa\u01a9\3\2\2\2\u01aa\u01ab\3\2\2\2\u01ab\u01ac\3\2\2\2"+
		"\u01ac\u01ad\5\u00a2R\2\u01ad\u01ae\7\17\2\2\u01ae\u01af\5b\62\2\u01af"+
		"\u01b0\7\b\2\2\u01b0\u01b1\5\u00a2R\2\u01b1\u01b2\5\u00b2Z\2\u01b2\u01b3"+
		"\5b\62\2\u01b3\u01b4\7\b\2\2\u01b4\u01b5\5\u00a2R\2\u01b5\u01b6\5\u00ae"+
		"X\2\u01b6\u01b7\5b\62\2\u01b7\u01b8\7\r\2\2\u01b8\'\3\2\2\2\u01b9\u01ba"+
		"\7C\2\2\u01ba\u01bc\7\f\2\2\u01bb\u01bd\7\177\2\2\u01bc\u01bb\3\2\2\2"+
		"\u01bc\u01bd\3\2\2\2\u01bd\u01be\3\2\2\2\u01be\u01bf\5\u00c0a\2\u01bf"+
		"\u01c0\7\5\2\2\u01c0\u01c1\5\u00a2R\2\u01c1\u01c2\7\r\2\2\u01c2)\3\2\2"+
		"\2\u01c3\u01c4\7\u0086\2\2\u01c4\u01c8\7\f\2\2\u01c5\u01c9\5\u00a2R\2"+
		"\u01c6\u01c9\5b\62\2\u01c7\u01c9\5\u00a4S\2\u01c8\u01c5\3\2\2\2\u01c8"+
		"\u01c6\3\2\2\2\u01c8\u01c7\3\2\2\2\u01c9\u01ca\3\2\2\2\u01ca\u01cb\7\r"+
		"\2\2\u01cb\u01cf\7\3\2\2\u01cc\u01ce\5,\27\2\u01cd\u01cc\3\2\2\2\u01ce"+
		"\u01d1\3\2\2\2\u01cf\u01cd\3\2\2\2\u01cf\u01d0\3\2\2\2\u01d0\u01d3\3\2"+
		"\2\2\u01d1\u01cf\3\2\2\2\u01d2\u01d4\5.\30\2\u01d3\u01d2\3\2\2\2\u01d3"+
		"\u01d4\3\2\2\2\u01d4\u01d5\3\2\2\2\u01d5\u01d6\7\4\2\2\u01d6+\3\2\2\2"+
		"\u01d7\u01da\7\u0087\2\2\u01d8\u01db\5b\62\2\u01d9\u01db\5\u00a4S\2\u01da"+
		"\u01d8\3\2\2\2\u01da\u01d9\3\2\2\2\u01db\u01dc\3\2\2\2\u01dc\u01ef\7\5"+
		"\2\2\u01dd\u01df\7\3\2\2\u01de\u01e0\5\24\13\2\u01df\u01de\3\2\2\2\u01df"+
		"\u01e0\3\2\2\2\u01e0\u01e4\3\2\2\2\u01e1\u01e2\5 \21\2\u01e2\u01e3\7\b"+
		"\2\2\u01e3\u01e5\3\2\2\2\u01e4\u01e1\3\2\2\2\u01e4\u01e5\3\2\2\2\u01e5"+
		"\u01e6\3\2\2\2\u01e6\u01f0\7\4\2\2\u01e7\u01e9\5\24\13\2\u01e8\u01e7\3"+
		"\2\2\2\u01e8\u01e9\3\2\2\2\u01e9\u01ed\3\2\2\2\u01ea\u01eb\5 \21\2\u01eb"+
		"\u01ec\7\b\2\2\u01ec\u01ee\3\2\2\2\u01ed\u01ea\3\2\2\2\u01ed\u01ee\3\2"+
		"\2\2\u01ee\u01f0\3\2\2\2\u01ef\u01dd\3\2\2\2\u01ef\u01e8\3\2\2\2\u01f0"+
		"-\3\2\2\2\u01f1\u01f2\79\2\2\u01f2\u0205\7\5\2\2\u01f3\u01f5\7\3\2\2\u01f4"+
		"\u01f6\5\24\13\2\u01f5\u01f4\3\2\2\2\u01f5\u01f6\3\2\2\2\u01f6\u01fa\3"+
		"\2\2\2\u01f7\u01f8\5 \21\2\u01f8\u01f9\7\b\2\2\u01f9\u01fb\3\2\2\2\u01fa"+
		"\u01f7\3\2\2\2\u01fa\u01fb\3\2\2\2\u01fb\u01fc\3\2\2\2\u01fc\u0206\7\4"+
		"\2\2\u01fd\u01ff\5\24\13\2\u01fe\u01fd\3\2\2\2\u01fe\u01ff\3\2\2\2\u01ff"+
		"\u0203\3\2\2\2\u0200\u0201\5 \21\2\u0201\u0202\7\b\2\2\u0202\u0204\3\2"+
		"\2\2\u0203\u0200\3\2\2\2\u0203\u0204\3\2\2\2\u0204\u0206\3\2\2\2\u0205"+
		"\u01f3\3\2\2\2\u0205\u01fe\3\2\2\2\u0206/\3\2\2\2\u0207\u0208\7I\2\2\u0208"+
		"\u0209\7\f\2\2\u0209\u020a\5`\61\2\u020a\u020b\7\r\2\2\u020b\u020f\5\""+
		"\22\2\u020c\u020e\5\62\32\2\u020d\u020c\3\2\2\2\u020e\u0211\3\2\2\2\u020f"+
		"\u020d\3\2\2\2\u020f\u0210\3\2\2\2\u0210\u0213\3\2\2\2\u0211\u020f\3\2"+
		"\2\2\u0212\u0214\5\64\33\2\u0213\u0212\3\2\2\2\u0213\u0214\3\2\2\2\u0214"+
		"\61\3\2\2\2\u0215\u0216\7A\2\2\u0216\u0217\7I\2\2\u0217\u0218\7\f\2\2"+
		"\u0218\u0219\5`\61\2\u0219\u021a\7\r\2\2\u021a\u021b\5\"\22\2\u021b\63"+
		"\3\2\2\2\u021c\u021d\7A\2\2\u021d\u021e\5\"\22\2\u021e\65\3\2\2\2\u021f"+
		"\u0220\5`\61\2\u0220\u0221\7\6\2\2\u0221\u0222\58\35\2\u0222\u0223\7\5"+
		"\2\2\u0223\u0224\58\35\2\u0224\u022a\3\2\2\2\u0225\u0226\7\f\2\2\u0226"+
		"\u0227\5\66\34\2\u0227\u0228\7\r\2\2\u0228\u022a\3\2\2\2\u0229\u021f\3"+
		"\2\2\2\u0229\u0225\3\2\2\2\u022a\67\3\2\2\2\u022b\u0235\5\u009cO\2\u022c"+
		"\u0235\5b\62\2\u022d\u0235\5`\61\2\u022e\u0235\5\66\34\2\u022f\u0235\5"+
		"\u0098M\2\u0230\u0231\7\f\2\2\u0231\u0232\58\35\2\u0232\u0233\7\r\2\2"+
		"\u0233\u0235\3\2\2\2\u0234\u022b\3\2\2\2\u0234\u022c\3\2\2\2\u0234\u022d"+
		"\3\2\2\2\u0234\u022e\3\2\2\2\u0234\u022f\3\2\2\2\u0234\u0230\3\2\2\2\u0235"+
		"9\3\2\2\2\u0236\u0237\7\u0085\2\2\u0237\u0238\7\f\2\2\u0238\u0239\5`\61"+
		"\2\u0239\u023c\7\r\2\2\u023a\u023d\5\"\22\2\u023b\u023d\7\b\2\2\u023c"+
		"\u023a\3\2\2\2\u023c\u023b\3\2\2\2\u023d;\3\2\2\2\u023e\u023f\7\u008a"+
		"\2\2\u023f\u0240\5\"\22\2\u0240\u0241\7\u0085\2\2\u0241\u0242\7\f\2\2"+
		"\u0242\u0243\5`\61\2\u0243\u0244\7\r\2\2\u0244=\3\2\2\2\u0245\u0250\7"+
		"\u0084\2\2\u0246\u0247\7\f\2\2\u0247\u0248\5\u009cO\2\u0248\u0249\7\r"+
		"\2\2\u0249\u0251\3\2\2\2\u024a\u024f\5\u009cO\2\u024b\u024f\5\66\34\2"+
		"\u024c\u024f\5b\62\2\u024d\u024f\5`\61\2\u024e\u024a\3\2\2\2\u024e\u024b"+
		"\3\2\2\2\u024e\u024c\3\2\2\2\u024e\u024d\3\2\2\2\u024f\u0251\3\2\2\2\u0250"+
		"\u0246\3\2\2\2\u0250\u024e\3\2\2\2\u0251?\3\2\2\2\u0252\u0254\7\b\2\2"+
		"\u0253\u0252\3\2\2\2\u0254\u0257\3\2\2\2\u0255\u0253\3\2\2\2\u0255\u0256"+
		"\3\2\2\2\u0256\u0258\3\2\2\2\u0257\u0255\3\2\2\2\u0258\u0261\5B\"\2\u0259"+
		"\u025b\7\b\2\2\u025a\u0259\3\2\2\2\u025b\u025c\3\2\2\2\u025c\u025a\3\2"+
		"\2\2\u025c\u025d\3\2\2\2\u025d\u025e\3\2\2\2\u025e\u0260\5B\"\2\u025f"+
		"\u025a\3\2\2\2\u0260\u0263\3\2\2\2\u0261\u025f\3\2\2\2\u0261\u0262\3\2"+
		"\2\2\u0262\u0267\3\2\2\2\u0263\u0261\3\2\2\2\u0264\u0266\7\b\2\2\u0265"+
		"\u0264\3\2\2\2\u0266\u0269\3\2\2\2\u0267\u0265\3\2\2\2\u0267\u0268\3\2"+
		"\2\2\u0268A\3\2\2\2\u0269\u0267\3\2\2\2\u026a\u026f\5D#\2\u026b\u026f"+
		"\5J&\2\u026c\u026f\5L\'\2\u026d\u026f\5R*\2\u026e\u026a\3\2\2\2\u026e"+
		"\u026b\3\2\2\2\u026e\u026c\3\2\2\2\u026e\u026d\3\2\2\2\u026fC\3\2\2\2"+
		"\u0270\u0271\7\63\2\2\u0271\u0275\7o\2\2\u0272\u0273\7I\2\2\u0273\u0274"+
		"\7[\2\2\u0274\u0276\7\u008b\2\2\u0275\u0272\3\2\2\2\u0275\u0276\3\2\2"+
		"\2\u0276\u0277\3\2\2\2\u0277\u0278\5\u00ccg\2\u0278\u0279\7\f\2\2\u0279"+
		"\u027e\5N(\2\u027a\u027b\7\16\2\2\u027b\u027d\5N(\2\u027c\u027a\3\2\2"+
		"\2\u027d\u0280\3\2\2\2\u027e\u027c\3\2\2\2\u027e\u027f\3\2\2\2\u027f\u0281"+
		"\3\2\2\2\u0280\u027e\3\2\2\2\u0281\u0282\7\r\2\2\u0282\u0283\3\2\2\2\u0283"+
		"\u0284\7p\2\2\u0284\u0285\7\17\2\2\u0285\u0286\5F$\2\u0286\u0287\7\16"+
		"\2\2\u0287\u0288\7}\2\2\u0288\u0289\7\17\2\2\u0289\u028a\5H%\2\u028aE"+
		"\3\2\2\2\u028b\u028c\5\u00a4S\2\u028cG\3\2\2\2\u028d\u028e\5\u00a4S\2"+
		"\u028eI\3\2\2\2\u028f\u0290\7\63\2\2\u0290\u0294\7p\2\2\u0291\u0292\7"+
		"I\2\2\u0292\u0293\7[\2\2\u0293\u0295\7\u008b\2\2\u0294\u0291\3\2\2\2\u0294"+
		"\u0295\3\2\2\2\u0295\u0296\3\2\2\2\u0296\u0297\5\u00ccg\2\u0297\u0298"+
		"\7\f\2\2\u0298\u029d\5N(\2\u0299\u029a\7\16\2\2\u029a\u029c\5N(\2\u029b"+
		"\u0299\3\2\2\2\u029c\u029f\3\2\2\2\u029d\u029b\3\2\2\2\u029d\u029e\3\2"+
		"\2\2\u029e\u02a0\3\2\2\2\u029f\u029d\3\2\2\2\u02a0\u02a1\7\r\2\2\u02a1"+
		"K\3\2\2\2\u02a2\u02ad\5\u008cG\2\u02a3\u02a4\7b\2\2\u02a4\u02a5\7-\2\2"+
		"\u02a5\u02aa\5\u0080A\2\u02a6\u02a7\7\16\2\2\u02a7\u02a9\5\u0080A\2\u02a8"+
		"\u02a6\3\2\2\2\u02a9\u02ac\3\2\2\2\u02aa\u02a8\3\2\2\2\u02aa\u02ab\3\2"+
		"\2\2\u02ab\u02ae\3\2\2\2\u02ac\u02aa\3\2\2\2\u02ad\u02a3\3\2\2\2\u02ad"+
		"\u02ae\3\2\2\2\u02ae\u02b5\3\2\2\2\u02af\u02b0\7W\2\2\u02b0\u02b3\5d\63"+
		"\2\u02b1\u02b2\t\2\2\2\u02b2\u02b4\5d\63\2\u02b3\u02b1\3\2\2\2\u02b3\u02b4"+
		"\3\2\2\2\u02b4\u02b6\3\2\2\2\u02b5\u02af\3\2\2\2\u02b5\u02b6\3\2\2\2\u02b6"+
		"M\3\2\2\2\u02b7\u02b8\5\u00d0i\2\u02b8\u02b9\5P)\2\u02b9O\3\2\2\2\u02ba"+
		"\u02ce\5\u00c4c\2\u02bb\u02bc\7\f\2\2\u02bc\u02be\5\u00aaV\2\u02bd\u02bf"+
		"\5\u00dan\2\u02be\u02bd\3\2\2\2\u02be\u02bf\3\2\2\2\u02bf\u02c0\3\2\2"+
		"\2\u02c0\u02c1\7\r\2\2\u02c1\u02cf\3\2\2\2\u02c2\u02c3\7\f\2\2\u02c3\u02c5"+
		"\5\u00aaV\2\u02c4\u02c6\5\u00dan\2\u02c5\u02c4\3\2\2\2\u02c5\u02c6\3\2"+
		"\2\2\u02c6\u02c7\3\2\2\2\u02c7\u02c8\7\16\2\2\u02c8\u02ca\5\u00aaV\2\u02c9"+
		"\u02cb\5\u00dan\2\u02ca\u02c9\3\2\2\2\u02ca\u02cb\3\2\2\2\u02cb\u02cc"+
		"\3\2\2\2\u02cc\u02cd\7\r\2\2\u02cd\u02cf\3\2\2\2\u02ce\u02bb\3\2\2\2\u02ce"+
		"\u02c2\3\2\2\2\u02ce\u02cf\3\2\2\2\u02cfQ\3\2\2\2\u02d0\u02d1\7\63\2\2"+
		"\u02d1\u02d2\7|\2\2\u02d2\u02d3\7\u0083\2\2\u02d3\u02d4\5\u00c6d\2\u02d4"+
		"\u02d5\7\f\2\2\u02d5\u02d6\5T+\2\u02d6\u02d7\7\16\2\2\u02d7\u02d8\5X-"+
		"\2\u02d8\u02d9\7\16\2\2\u02d9\u02da\5Z.\2\u02da\u02db\7\16\2\2\u02db\u02dc"+
		"\5\\/\2\u02dc\u02dd\7\16\2\2\u02dd\u02de\5^\60\2\u02de\u02df\7\r\2\2\u02df"+
		"S\3\2\2\2\u02e0\u02e1\5\u00a4S\2\u02e1U\3\2\2\2\u02e2\u02e3\5\u00a4S\2"+
		"\u02e3W\3\2\2\2\u02e4\u02e5\5\u00dan\2\u02e5Y\3\2\2\2\u02e6\u02e7\5\u00da"+
		"n\2\u02e7[\3\2\2\2\u02e8\u02e9\5\u00dan\2\u02e9]\3\2\2\2\u02ea\u02f3\7"+
		"\n\2\2\u02eb\u02f0\5\u00dan\2\u02ec\u02ed\7\16\2\2\u02ed\u02ef\5\u00da"+
		"n\2\u02ee\u02ec\3\2\2\2\u02ef\u02f2\3\2\2\2\u02f0\u02ee\3\2\2\2\u02f0"+
		"\u02f1\3\2\2\2\u02f1\u02f4\3\2\2\2\u02f2\u02f0\3\2\2\2\u02f3\u02eb\3\2"+
		"\2\2\u02f3\u02f4\3\2\2\2\u02f4\u02f5\3\2\2\2\u02f5\u02f6\7\13\2\2\u02f6"+
		"_\3\2\2\2\u02f7\u02fb\b\61\1\2\u02f8\u02fc\5\6\4\2\u02f9\u02fc\5\u009e"+
		"P\2\u02fa\u02fc\5\u00a6T\2\u02fb\u02f8\3\2\2\2\u02fb\u02f9\3\2\2\2\u02fb"+
		"\u02fa\3\2\2\2\u02fc\u0319\3\2\2\2\u02fd\u02fe\5b\62\2\u02fe\u02ff\t\3"+
		"\2\2\u02ff\u0300\5b\62\2\u0300\u0319\3\2\2\2\u0301\u0304\5b\62\2\u0302"+
		"\u0304\5\u009cO\2\u0303\u0301\3\2\2\2\u0303\u0302\3\2\2\2\u0304\u0305"+
		"\3\2\2\2\u0305\u0308\t\4\2\2\u0306\u0309\5b\62\2\u0307\u0309\5\u009cO"+
		"\2\u0308\u0306\3\2\2\2\u0308\u0307\3\2\2\2\u0309\u0319\3\2\2\2\u030a\u030e"+
		"\7\7\2\2\u030b\u030f\5`\61\2\u030c\u030f\5\6\4\2\u030d\u030f\5\u009eP"+
		"\2\u030e\u030b\3\2\2\2\u030e\u030c\3\2\2\2\u030e\u030d\3\2\2\2\u030f\u0319"+
		"\3\2\2\2\u0310\u0314\7\f\2\2\u0311\u0315\5`\61\2\u0312\u0315\5\6\4\2\u0313"+
		"\u0315\5\u009eP\2\u0314\u0311\3\2\2\2\u0314\u0312\3\2\2\2\u0314\u0313"+
		"\3\2\2\2\u0315\u0316\3\2\2\2\u0316\u0317\7\r\2\2\u0317\u0319\3\2\2\2\u0318"+
		"\u02f7\3\2\2\2\u0318\u02fd\3\2\2\2\u0318\u0303\3\2\2\2\u0318\u030a\3\2"+
		"\2\2\u0318\u0310\3\2\2\2\u0319\u0322\3\2\2\2\u031a\u031b\f\b\2\2\u031b"+
		"\u031c\t\5\2\2\u031c\u0321\5`\61\t\u031d\u031e\f\5\2\2\u031e\u031f\t\4"+
		"\2\2\u031f\u0321\5`\61\6\u0320\u031a\3\2\2\2\u0320\u031d\3\2\2\2\u0321"+
		"\u0324\3\2\2\2\u0322\u0320\3\2\2\2\u0322\u0323\3\2\2\2\u0323a\3\2\2\2"+
		"\u0324\u0322\3\2\2\2\u0325\u0328\b\62\1\2\u0326\u0329\5\u00a8U\2\u0327"+
		"\u0329\5\6\4\2\u0328\u0326\3\2\2\2\u0328\u0327\3\2\2\2\u0329\u0336\3\2"+
		"\2\2\u032a\u032c\5\u00a0Q\2\u032b\u032d\5\u00b4[\2\u032c\u032b\3\2\2\2"+
		"\u032c\u032d\3\2\2\2\u032d\u0336\3\2\2\2\u032e\u032f\5\u00b4[\2\u032f"+
		"\u0330\5\u00a0Q\2\u0330\u0336\3\2\2\2\u0331\u0332\7\f\2\2\u0332\u0333"+
		"\5b\62\2\u0333\u0334\7\r\2\2\u0334\u0336\3\2\2\2\u0335\u0325\3\2\2\2\u0335"+
		"\u032a\3\2\2\2\u0335\u032e\3\2\2\2\u0335\u0331\3\2\2\2\u0336\u033f\3\2"+
		"\2\2\u0337\u0338\f\7\2\2\u0338\u0339\t\6\2\2\u0339\u033e\5b\62\b\u033a"+
		"\u033b\f\6\2\2\u033b\u033c\t\7\2\2\u033c\u033e\5b\62\7\u033d\u0337\3\2"+
		"\2\2\u033d\u033a\3\2\2\2\u033e\u0341\3\2\2\2\u033f\u033d\3\2\2\2\u033f"+
		"\u0340\3\2\2\2\u0340c\3\2\2\2\u0341\u033f\3\2\2\2\u0342\u0343\b\63\1\2"+
		"\u0343\u0362\5\u00acW\2\u0344\u0345\5\u00c8e\2\u0345\u0346\7\t\2\2\u0346"+
		"\u0348\3\2\2\2\u0347\u0344\3\2\2\2\u0347\u0348\3\2\2\2\u0348\u0349\3\2"+
		"\2\2\u0349\u034a\5\u00ccg\2\u034a\u034b\7\t\2\2\u034b\u034d\3\2\2\2\u034c"+
		"\u0347\3\2\2\2\u034c\u034d\3\2\2\2\u034d\u034e\3\2\2\2\u034e\u0362\5\u00d0"+
		"i\2\u034f\u0350\5\u00b6\\\2\u0350\u0351\5d\63\17\u0351\u0362\3\2\2\2\u0352"+
		"\u0362\5f\64\2\u0353\u0354\7\f\2\2\u0354\u0355\5d\63\2\u0355\u0356\7\r"+
		"\2\2\u0356\u0362\3\2\2\2\u0357\u0359\7[\2\2\u0358\u0357\3\2\2\2\u0358"+
		"\u0359\3\2\2\2\u0359\u035a\3\2\2\2\u035a\u035c\7\u008b\2\2\u035b\u0358"+
		"\3\2\2\2\u035b\u035c\3\2\2\2\u035c\u035d\3\2\2\2\u035d\u035e\7\f\2\2\u035e"+
		"\u035f\5L\'\2\u035f\u0360\7\r\2\2\u0360\u0362\3\2\2\2\u0361\u0342\3\2"+
		"\2\2\u0361\u034c\3\2\2\2\u0361\u034f\3\2\2\2\u0361\u0352\3\2\2\2\u0361"+
		"\u0353\3\2\2\2\u0361\u035b\3\2\2\2\u0362\u038c\3\2\2\2\u0363\u0364\f\16"+
		"\2\2\u0364\u0365\7\27\2\2\u0365\u038b\5d\63\17\u0366\u0367\f\r\2\2\u0367"+
		"\u0368\t\6\2\2\u0368\u038b\5d\63\16\u0369\u036a\f\f\2\2\u036a\u036b\t"+
		"\7\2\2\u036b\u038b\5d\63\r\u036c\u036d\f\13\2\2\u036d\u036e\t\b\2\2\u036e"+
		"\u038b\5d\63\f\u036f\u0370\f\n\2\2\u0370\u0371\t\3\2\2\u0371\u038b\5d"+
		"\63\13\u0372\u037f\f\t\2\2\u0373\u0380\7\17\2\2\u0374\u0380\7\"\2\2\u0375"+
		"\u0380\7#\2\2\u0376\u0380\7$\2\2\u0377\u0380\7Q\2\2\u0378\u0379\7Q\2\2"+
		"\u0379\u0380\7[\2\2\u037a\u0380\7K\2\2\u037b\u0380\7V\2\2\u037c\u0380"+
		"\7F\2\2\u037d\u0380\7X\2\2\u037e\u0380\7g\2\2\u037f\u0373\3\2\2\2\u037f"+
		"\u0374\3\2\2\2\u037f\u0375\3\2\2\2\u037f\u0376\3\2\2\2\u037f\u0377\3\2"+
		"\2\2\u037f\u0378\3\2\2\2\u037f\u037a\3\2\2\2\u037f\u037b\3\2\2\2\u037f"+
		"\u037c\3\2\2\2\u037f\u037d\3\2\2\2\u037f\u037e\3\2\2\2\u0380\u0381\3\2"+
		"\2\2\u0381\u038b\5d\63\n\u0382\u0383\f\b\2\2\u0383\u0384\7)\2\2\u0384"+
		"\u038b\5d\63\t\u0385\u0386\f\7\2\2\u0386\u0387\7a\2\2\u0387\u038b\5d\63"+
		"\b\u0388\u0389\f\4\2\2\u0389\u038b\5h\65\2\u038a\u0363\3\2\2\2\u038a\u0366"+
		"\3\2\2\2\u038a\u0369\3\2\2\2\u038a\u036c\3\2\2\2\u038a\u036f\3\2\2\2\u038a"+
		"\u0372\3\2\2\2\u038a\u0382\3\2\2\2\u038a\u0385\3\2\2\2\u038a\u0388\3\2"+
		"\2\2\u038b\u038e\3\2\2\2\u038c\u038a\3\2\2\2\u038c\u038d\3\2\2\2\u038d"+
		"e\3\2\2\2\u038e\u038c\3\2\2\2\u038f\u0390\5\u00c6d\2\u0390\u039d\7\f\2"+
		"\2\u0391\u0393\7>\2\2\u0392\u0391\3\2\2\2\u0392\u0393\3\2\2\2\u0393\u0394"+
		"\3\2\2\2\u0394\u0399\5d\63\2\u0395\u0396\7\16\2\2\u0396\u0398\5d\63\2"+
		"\u0397\u0395\3\2\2\2\u0398\u039b\3\2\2\2\u0399\u0397\3\2\2\2\u0399\u039a"+
		"\3\2\2\2\u039a\u039e\3\2\2\2\u039b\u0399\3\2\2\2\u039c\u039e\7\20\2\2"+
		"\u039d\u0392\3\2\2\2\u039d\u039c\3\2\2\2\u039d\u039e\3\2\2\2\u039e\u039f"+
		"\3\2\2\2\u039f\u03a0\7\r\2\2\u03a0g\3\2\2\2\u03a1\u03a3\7[\2\2\u03a2\u03a1"+
		"\3\2\2\2\u03a2\u03a3\3\2\2\2\u03a3\u03a4\3\2\2\2\u03a4\u03b8\7K\2\2\u03a5"+
		"\u03af\7\f\2\2\u03a6\u03b0\5L\'\2\u03a7\u03ac\5d\63\2\u03a8\u03a9\7\16"+
		"\2\2\u03a9\u03ab\5d\63\2\u03aa\u03a8\3\2\2\2\u03ab\u03ae\3\2\2\2\u03ac"+
		"\u03aa\3\2\2\2\u03ac\u03ad\3\2\2\2\u03ad\u03b0\3\2\2\2\u03ae\u03ac\3\2"+
		"\2\2\u03af\u03a6\3\2\2\2\u03af\u03a7\3\2\2\2\u03af\u03b0\3\2\2\2\u03b0"+
		"\u03b1\3\2\2\2\u03b1\u03b9\7\r\2\2\u03b2\u03b3\5\u00c8e\2\u03b3\u03b4"+
		"\7\t\2\2\u03b4\u03b6\3\2\2\2\u03b5\u03b2\3\2\2\2\u03b5\u03b6\3\2\2\2\u03b6"+
		"\u03b7\3\2\2\2\u03b7\u03b9\5\u00ccg\2\u03b8\u03a5\3\2\2\2\u03b8\u03b5"+
		"\3\2\2\2\u03b9i\3\2\2\2\u03ba\u03be\7f\2\2\u03bb\u03bc\5\u00c8e\2\u03bc"+
		"\u03bd\7\t\2\2\u03bd\u03bf\3\2\2\2\u03be\u03bb\3\2\2\2\u03be\u03bf\3\2"+
		"\2\2\u03bf\u03c0\3\2\2\2\u03c0\u03cc\5\u00d4k\2\u03c1\u03c2\7\f\2\2\u03c2"+
		"\u03c7\5n8\2\u03c3\u03c4\7\16\2\2\u03c4\u03c6\5n8\2\u03c5\u03c3\3\2\2"+
		"\2\u03c6\u03c9\3\2\2\2\u03c7\u03c5\3\2\2\2\u03c7\u03c8\3\2\2\2\u03c8\u03ca"+
		"\3\2\2\2\u03c9\u03c7\3\2\2\2\u03ca\u03cb\7\r\2\2\u03cb\u03cd\3\2\2\2\u03cc"+
		"\u03c1\3\2\2\2\u03cc\u03cd\3\2\2\2\u03cd\u03d5\3\2\2\2\u03ce\u03d2\5l"+
		"\67\2\u03cf\u03d0\7X\2\2\u03d0\u03d2\5\u00c4c\2\u03d1\u03ce\3\2\2\2\u03d1"+
		"\u03cf\3\2\2\2\u03d2\u03d4\3\2\2\2\u03d3\u03d1\3\2\2\2\u03d4\u03d7\3\2"+
		"\2\2\u03d5\u03d3\3\2\2\2\u03d5\u03d6\3\2\2\2\u03d6\u03e5\3\2\2\2\u03d7"+
		"\u03d5\3\2\2\2\u03d8\u03da\7[\2\2\u03d9\u03d8\3\2\2\2\u03d9\u03da\3\2"+
		"\2\2\u03da\u03db\3\2\2\2\u03db\u03e0\7:\2\2\u03dc\u03dd\7M\2\2\u03dd\u03e1"+
		"\7;\2\2\u03de\u03df\7M\2\2\u03df\u03e1\7J\2\2\u03e0\u03dc\3\2\2\2\u03e0"+
		"\u03de\3\2\2\2\u03e0\u03e1\3\2\2\2\u03e1\u03e3\3\2\2\2\u03e2\u03e4\7B"+
		"\2\2\u03e3\u03e2\3\2\2\2\u03e3\u03e4\3\2\2\2\u03e4\u03e6\3\2\2\2\u03e5"+
		"\u03d9\3\2\2\2\u03e5\u03e6\3\2\2\2\u03e6k\3\2\2\2\u03e7\u03e8\7`\2\2\u03e8"+
		"\u03f1\t\t\2\2\u03e9\u03ea\7n\2\2\u03ea\u03f2\7]\2\2\u03eb\u03ec\7n\2"+
		"\2\u03ec\u03f2\79\2\2\u03ed\u03f2\7.\2\2\u03ee\u03f2\7j\2\2\u03ef\u03f0"+
		"\7Z\2\2\u03f0\u03f2\7%\2\2\u03f1\u03e9\3\2\2\2\u03f1\u03eb\3\2\2\2\u03f1"+
		"\u03ed\3\2\2\2\u03f1\u03ee\3\2\2\2\u03f1\u03ef\3\2\2\2\u03f2m\3\2\2\2"+
		"\u03f3\u03f4\5\u00c4c\2\u03f4o\3\2\2\2\u03f5\u03f8\5\u00d0i\2\u03f6\u03f7"+
		"\7\60\2\2\u03f7\u03f9\5\u00d2j\2\u03f8\u03f6\3\2\2\2\u03f8\u03f9\3\2\2"+
		"\2\u03f9\u03fb\3\2\2\2\u03fa\u03fc\t\n\2\2\u03fb\u03fa\3\2\2\2\u03fb\u03fc"+
		"\3\2\2\2\u03fcq\3\2\2\2\u03fd\u03fe\7\62\2\2\u03fe\u0400\5\u00c4c\2\u03ff"+
		"\u03fd\3\2\2\2\u03ff\u0400\3\2\2\2\u0400\u040a\3\2\2\2\u0401\u040b\5t"+
		";\2\u0402\u040b\5z>\2\u0403\u040b\5x=\2\u0404\u0405\7/\2\2\u0405\u0406"+
		"\7\f\2\2\u0406\u0407\5d\63\2\u0407\u0408\7\r\2\2\u0408\u040b\3\2\2\2\u0409"+
		"\u040b\5v<\2\u040a\u0401\3\2\2\2\u040a\u0402\3\2\2\2\u040a\u0403\3\2\2"+
		"\2\u040a\u0404\3\2\2\2\u040a\u0409\3\2\2\2\u040bs\3\2\2\2\u040c\u040d"+
		"\7d\2\2\u040d\u040e\7T\2\2\u040e\u040f\7\f\2\2\u040f\u0414\5p9\2\u0410"+
		"\u0411\7\16\2\2\u0411\u0413\5p9\2\u0412\u0410\3\2\2\2\u0413\u0416\3\2"+
		"\2\2\u0414\u0412\3\2\2\2\u0414\u0415\3\2\2\2\u0415\u0417\3\2\2\2\u0416"+
		"\u0414\3\2\2\2\u0417\u0418\7\r\2\2\u0418u\3\2\2\2\u0419\u041a\7D\2\2\u041a"+
		"\u041b\7T\2\2\u041b\u041c\7\f\2\2\u041c\u0421\5|?\2\u041d\u041e\7\16\2"+
		"\2\u041e\u0420\5|?\2\u041f\u041d\3\2\2\2\u0420\u0423\3\2\2\2\u0421\u041f"+
		"\3\2\2\2\u0421\u0422\3\2\2\2\u0422\u0424\3\2\2\2\u0423\u0421\3\2\2\2\u0424"+
		"\u0425\7\r\2\2\u0425\u0426\5j\66\2\u0426w\3\2\2\2\u0427\u0429\7t\2\2\u0428"+
		"\u042a\7T\2\2\u0429\u0428\3\2\2\2\u0429\u042a\3\2\2\2\u042a\u042c\3\2"+
		"\2\2\u042b\u042d\5\u00c4c\2\u042c\u042b\3\2\2\2\u042c\u042d\3\2\2\2\u042d"+
		"\u042e\3\2\2\2\u042e\u042f\7\f\2\2\u042f\u0434\5p9\2\u0430\u0431\7\16"+
		"\2\2\u0431\u0433\5p9\2\u0432\u0430\3\2\2\2\u0433\u0436\3\2\2\2\u0434\u0432"+
		"\3\2\2\2\u0434\u0435\3\2\2\2\u0435\u0437\3\2\2\2\u0436\u0434\3\2\2\2\u0437"+
		"\u0438\7\r\2\2\u0438y\3\2\2\2\u0439\u043b\7T\2\2\u043a\u043c\5\u00c4c"+
		"\2\u043b\u043a\3\2\2\2\u043b\u043c\3\2\2\2\u043c\u043d\3\2\2\2\u043d\u043e"+
		"\7\f\2\2\u043e\u0443\5p9\2\u043f\u0440\7\16\2\2\u0440\u0442\5p9\2\u0441"+
		"\u043f\3\2\2\2\u0442\u0445\3\2\2\2\u0443\u0441\3\2\2\2\u0443\u0444\3\2"+
		"\2\2\u0444\u0446\3\2\2\2\u0445\u0443\3\2\2\2\u0446\u0447\7\r\2\2\u0447"+
		"{\3\2\2\2\u0448\u0449\5\u00d0i\2\u0449}\3\2\2\2\u044a\u044b\5\u00c8e\2"+
		"\u044b\u044c\7\t\2\2\u044c\u044e\3\2\2\2\u044d\u044a\3\2\2\2\u044d\u044e"+
		"\3\2\2\2\u044e\u044f\3\2\2\2\u044f\u0455\5\u00ccg\2\u0450\u0451\7L\2\2"+
		"\u0451\u0452\7-\2\2\u0452\u0456\5\u00d6l\2\u0453\u0454\7[\2\2\u0454\u0456"+
		"\7L\2\2\u0455\u0450\3\2\2\2\u0455\u0453\3\2\2\2\u0455\u0456\3\2\2\2\u0456"+
		"\177\3\2\2\2\u0457\u0459\5d\63\2\u0458\u045a\t\n\2\2\u0459\u0458\3\2\2"+
		"\2\u0459\u045a\3\2\2\2\u045a\u0081\3\2\2\2\u045b\u0468\7\20\2\2\u045c"+
		"\u045d\5\u00ccg\2\u045d\u045e\7\t\2\2\u045e\u045f\7\20\2\2\u045f\u0468"+
		"\3\2\2\2\u0460\u0465\5d\63\2\u0461\u0463\7*\2\2\u0462\u0461\3\2\2\2\u0462"+
		"\u0463\3\2\2\2\u0463\u0464\3\2\2\2\u0464\u0466\5\u00ba^\2\u0465\u0462"+
		"\3\2\2\2\u0465\u0466\3\2\2\2\u0466\u0468\3\2\2\2\u0467\u045b\3\2\2\2\u0467"+
		"\u045c\3\2\2\2\u0467\u0460\3\2\2\2\u0468\u0083\3\2\2\2\u0469\u046a\5\u00c8"+
		"e\2\u046a\u046b\7\t\2\2\u046b\u046d\3\2\2\2\u046c\u0469\3\2\2\2\u046c"+
		"\u046d\3\2\2\2\u046d\u046e\3\2\2\2\u046e\u0473\5\u00ccg\2\u046f\u0471"+
		"\7*\2\2\u0470\u046f\3\2\2\2\u0470\u0471\3\2\2\2\u0471\u0472\3\2\2\2\u0472"+
		"\u0474\5\u00d8m\2\u0473\u0470\3\2\2\2\u0473\u0474\3\2\2\2\u0474\u047a"+
		"\3\2\2\2\u0475\u0476\7L\2\2\u0476\u0477\7-\2\2\u0477\u047b\5\u00d6l\2"+
		"\u0478\u0479\7[\2\2\u0479\u047b\7L\2\2\u047a\u0475\3\2\2\2\u047a\u0478"+
		"\3\2\2\2\u047a\u047b\3\2\2\2\u047b\u0499\3\2\2\2\u047c\u0486\7\f\2\2\u047d"+
		"\u0482\5\u0084C\2\u047e\u047f\7\16\2\2\u047f\u0481\5\u0084C\2\u0480\u047e"+
		"\3\2\2\2\u0481\u0484\3\2\2\2\u0482\u0480\3\2\2\2\u0482\u0483\3\2\2\2\u0483"+
		"\u0487\3\2\2\2\u0484\u0482\3\2\2\2\u0485\u0487\5\u0086D\2\u0486\u047d"+
		"\3\2\2\2\u0486\u0485\3\2\2\2\u0487\u0488\3\2\2\2\u0488\u048d\7\r\2\2\u0489"+
		"\u048b\7*\2\2\u048a\u0489\3\2\2\2\u048a\u048b\3\2\2\2\u048b\u048c\3\2"+
		"\2\2\u048c\u048e\5\u00d8m\2\u048d\u048a\3\2\2\2\u048d\u048e\3\2\2\2\u048e"+
		"\u0499\3\2\2\2\u048f\u0490\7\f\2\2\u0490\u0491\5L\'\2\u0491\u0496\7\r"+
		"\2\2\u0492\u0494\7*\2\2\u0493\u0492\3\2\2\2\u0493\u0494\3\2\2\2\u0494"+
		"\u0495\3\2\2\2\u0495\u0497\5\u00d8m\2\u0496\u0493\3\2\2\2\u0496\u0497"+
		"\3\2\2\2\u0497\u0499\3\2\2\2\u0498\u046c\3\2\2\2\u0498\u047c\3\2\2\2\u0498"+
		"\u048f\3\2\2\2\u0499\u0085\3\2\2\2\u049a\u04a1\5\u0084C\2\u049b\u049c"+
		"\5\u0088E\2\u049c\u049d\5\u0084C\2\u049d\u049e\5\u008aF\2\u049e\u04a0"+
		"\3\2\2\2\u049f\u049b\3\2\2\2\u04a0\u04a3\3\2\2\2\u04a1\u049f\3\2\2\2\u04a1"+
		"\u04a2\3\2\2\2\u04a2\u0087\3\2\2\2\u04a3\u04a1\3\2\2\2\u04a4\u04ae\7\16"+
		"\2\2\u04a5\u04a7\7U\2\2\u04a6\u04a8\7c\2\2\u04a7\u04a6\3\2\2\2\u04a7\u04a8"+
		"\3\2\2\2\u04a8\u04ab\3\2\2\2\u04a9\u04ab\7N\2\2\u04aa\u04a5\3\2\2\2\u04aa"+
		"\u04a9\3\2\2\2\u04aa\u04ab\3\2\2\2\u04ab\u04ac\3\2\2\2\u04ac\u04ae\7S"+
		"\2\2\u04ad\u04a4\3\2\2\2\u04ad\u04aa\3\2\2\2\u04ae\u0089\3\2\2\2\u04af"+
		"\u04b0\7`\2\2\u04b0\u04b1\5d\63\2\u04b1\u008b\3\2\2\2\u04b2\u04b4\7m\2"+
		"\2\u04b3\u04b5\t\13\2\2\u04b4\u04b3\3\2\2\2\u04b4\u04b5\3\2\2\2\u04b5"+
		"\u04b6\3\2\2\2\u04b6\u04bb\5\u0082B\2\u04b7\u04b8\7\16\2\2\u04b8\u04ba"+
		"\5\u0082B\2\u04b9\u04b7\3\2\2\2\u04ba\u04bd\3\2\2\2\u04bb\u04b9\3\2\2"+
		"\2\u04bb\u04bc\3\2\2\2\u04bc\u04ca\3\2\2\2\u04bd\u04bb\3\2\2\2\u04be\u04c8"+
		"\7E\2\2\u04bf\u04c4\5\u0084C\2\u04c0\u04c1\7\16\2\2\u04c1\u04c3\5\u0084"+
		"C\2\u04c2\u04c0\3\2\2\2\u04c3\u04c6\3\2\2\2\u04c4\u04c2\3\2\2\2\u04c4"+
		"\u04c5\3\2\2\2\u04c5\u04c9\3\2\2\2\u04c6\u04c4\3\2\2\2\u04c7\u04c9\5\u0086"+
		"D\2\u04c8\u04bf\3\2\2\2\u04c8\u04c7\3\2\2\2\u04c9\u04cb\3\2\2\2\u04ca"+
		"\u04be\3\2\2\2\u04ca\u04cb\3\2\2\2\u04cb\u04ce\3\2\2\2\u04cc\u04cd\7x"+
		"\2\2\u04cd\u04cf\5d\63\2\u04ce\u04cc\3\2\2\2\u04ce\u04cf\3\2\2\2\u04cf"+
		"\u04de\3\2\2\2\u04d0\u04d1\7G\2\2\u04d1\u04d2\7-\2\2\u04d2\u04d7\5d\63"+
		"\2\u04d3\u04d4\7\16\2\2\u04d4\u04d6\5d\63\2\u04d5\u04d3\3\2\2\2\u04d6"+
		"\u04d9\3\2\2\2\u04d7\u04d5\3\2\2\2\u04d7\u04d8\3\2\2\2\u04d8\u04dc\3\2"+
		"\2\2\u04d9\u04d7\3\2\2\2\u04da\u04db\7H\2\2\u04db\u04dd\5d\63\2\u04dc"+
		"\u04da\3\2\2\2\u04dc\u04dd\3\2\2\2\u04dd\u04df\3\2\2\2\u04de\u04d0\3\2"+
		"\2\2\u04de\u04df\3\2\2\2\u04df\u04fd\3\2\2\2\u04e0\u04e1\7v\2\2\u04e1"+
		"\u04e2\7\f\2\2\u04e2\u04e7\5d\63\2\u04e3\u04e4\7\16\2\2\u04e4\u04e6\5"+
		"d\63\2\u04e5\u04e3\3\2\2\2\u04e6\u04e9\3\2\2\2\u04e7\u04e5\3\2\2\2\u04e7"+
		"\u04e8\3\2\2\2\u04e8\u04ea\3\2\2\2\u04e9\u04e7\3\2\2\2\u04ea\u04f9\7\r"+
		"\2\2\u04eb\u04ec\7\16\2\2\u04ec\u04ed\7\f\2\2\u04ed\u04f2\5d\63\2\u04ee"+
		"\u04ef\7\16\2\2\u04ef\u04f1\5d\63\2\u04f0\u04ee\3\2\2\2\u04f1\u04f4\3"+
		"\2\2\2\u04f2\u04f0\3\2\2\2\u04f2\u04f3\3\2\2\2\u04f3\u04f5\3\2\2\2\u04f4"+
		"\u04f2\3\2\2\2\u04f5\u04f6\7\r\2\2\u04f6\u04f8\3\2\2\2\u04f7\u04eb\3\2"+
		"\2\2\u04f8\u04fb\3\2\2\2\u04f9\u04f7\3\2\2\2\u04f9\u04fa\3\2\2\2\u04fa"+
		"\u04fd\3\2\2\2\u04fb\u04f9\3\2\2\2\u04fc\u04b2\3\2\2\2\u04fc\u04e0\3\2"+
		"\2\2\u04fd\u008d\3\2\2\2\u04fe\u04ff\7\177\2\2\u04ff\u0502\5\u0090I\2"+
		"\u0500\u0501\7\16\2\2\u0501\u0503\5\u0090I\2\u0502\u0500\3\2\2\2\u0503"+
		"\u0504\3\2\2\2\u0504\u0502\3\2\2\2\u0504\u0505\3\2\2\2\u0505\u008f\3\2"+
		"\2\2\u0506\u050c\5\u00c0a\2\u0507\u050a\7\17\2\2\u0508\u050b\5\u009aN"+
		"\2\u0509\u050b\5L\'\2\u050a\u0508\3\2\2\2\u050a\u0509\3\2\2\2\u050b\u050d"+
		"\3\2\2\2\u050c\u0507\3\2\2\2\u050c\u050d\3\2\2\2\u050d\u0091\3\2\2\2\u050e"+
		"\u050f\7\177\2\2\u050f\u0510\5\u00c0a\2\u0510\u0093\3\2\2\2\u0511\u0512"+
		"\7\n\2\2\u0512\u0513\7\13\2\2\u0513\u0514\3\2\2\2\u0514\u051d\5\u00c0"+
		"a\2\u0515\u0516\5\u00c0a\2\u0516\u0518\7\n\2\2\u0517\u0519\7\u008e\2\2"+
		"\u0518\u0517\3\2\2\2\u0518\u0519\3\2\2\2\u0519\u051a\3\2\2\2\u051a\u051b"+
		"\7\13\2\2\u051b\u051d\3\2\2\2\u051c\u0511\3\2\2\2\u051c\u0515\3\2\2\2"+
		"\u051d\u0095\3\2\2\2\u051e\u051f\7\u0080\2\2\u051f\u0520\7\177\2\2\u0520"+
		"\u0521\7\n\2\2\u0521\u0522\5\u00a8U\2\u0522\u0523\7\13\2\2\u0523\u0526"+
		"\3\2\2\2\u0524\u0526\5\u00a2R\2\u0525\u051e\3\2\2\2\u0525\u0524\3\2\2"+
		"\2\u0526\u0097\3\2\2\2\u0527\u0528\5\u00a2R\2\u0528\u052b\5\u00aeX\2\u0529"+
		"\u052c\5\u009aN\2\u052a\u052c\5L\'\2\u052b\u0529\3\2\2\2\u052b\u052a\3"+
		"\2\2\2\u052c\u0099\3\2\2\2\u052d\u0532\5\u009cO\2\u052e\u0532\5b\62\2"+
		"\u052f\u0532\5`\61\2\u0530\u0532\5\66\34\2\u0531\u052d\3\2\2\2\u0531\u052e"+
		"\3\2\2\2\u0531\u052f\3\2\2\2\u0531\u0530\3\2\2\2\u0532\u009b\3\2\2\2\u0533"+
		"\u053a\5\u00a8U\2\u0534\u053a\5\u00a6T\2\u0535\u053a\5\u00a4S\2\u0536"+
		"\u053a\7]\2\2\u0537\u053a\5\u00a2R\2\u0538\u053a\5\6\4\2\u0539\u0533\3"+
		"\2\2\2\u0539\u0534\3\2\2\2\u0539\u0535\3\2\2\2\u0539\u0536\3\2\2\2\u0539"+
		"\u0537\3\2\2\2\u0539\u0538\3\2\2\2\u053a\u009d\3\2\2\2\u053b\u053c\5\u00a2"+
		"R\2\u053c\u009f\3\2\2\2\u053d\u053e\5\u00a2R\2\u053e\u00a1\3\2\2\2\u053f"+
		"\u0540\bR\1\2\u0540\u0546\5\u00c0a\2\u0541\u0542\7\f\2\2\u0542\u0543\5"+
		"\u00a2R\2\u0543\u0544\7\r\2\2\u0544\u0546\3\2\2\2\u0545\u053f\3\2\2\2"+
		"\u0545\u0541\3\2\2\2\u0546\u054c\3\2\2\2\u0547\u0548\f\4\2\2\u0548\u0549"+
		"\7\t\2\2\u0549\u054b\5\u00a2R\5\u054a\u0547\3\2\2\2\u054b\u054e\3\2\2"+
		"\2\u054c\u054a\3\2\2\2\u054c\u054d\3\2\2\2\u054d\u00a3\3\2\2\2\u054e\u054c"+
		"\3\2\2\2\u054f\u0554\t\f\2\2\u0550\u0551\7\21\2\2\u0551\u0553\5\u009c"+
		"O\2\u0552\u0550\3\2\2\2\u0553\u0556\3\2\2\2\u0554\u0552\3\2\2\2\u0554"+
		"\u0555\3\2\2\2\u0555\u00a5\3\2\2\2\u0556\u0554\3\2\2\2\u0557\u0558\t\r"+
		"\2\2\u0558\u00a7\3\2\2\2\u0559\u055c\7\u008e\2\2\u055a\u055c\5\u00aaV"+
		"\2\u055b\u0559\3\2\2\2\u055b\u055a\3\2\2\2\u055c\u00a9\3\2\2\2\u055d\u055f"+
		"\t\7\2\2\u055e\u055d\3\2\2\2\u055e\u055f\3\2\2\2\u055f\u0560\3\2\2\2\u0560"+
		"\u0563\7\u008e\2\2\u0561\u0563\7\20\2\2\u0562\u055e\3\2\2\2\u0562\u0561"+
		"\3\2\2\2\u0563\u00ab\3\2\2\2\u0564\u0565\t\16\2\2\u0565\u00ad\3\2\2\2"+
		"\u0566\u0568\5\u00b0Y\2\u0567\u0566\3\2\2\2\u0567\u0568\3\2\2\2\u0568"+
		"\u0569\3\2\2\2\u0569\u056a\7\17\2\2\u056a\u00af\3\2\2\2\u056b\u056c\t"+
		"\17\2\2\u056c\u00b1\3\2\2\2\u056d\u056e\t\3\2\2\u056e\u00b3\3\2\2\2\u056f"+
		"\u0570\t\20\2\2\u0570\u00b5\3\2\2\2\u0571\u0572\t\21\2\2\u0572\u00b7\3"+
		"\2\2\2\u0573\u0574\7\u0090\2\2\u0574\u00b9\3\2\2\2\u0575\u0576\t\22\2"+
		"\2\u0576\u00bb\3\2\2\2\u0577\u0578\t\23\2\2\u0578\u00bd\3\2\2\2\u0579"+
		"\u057a\5\u00c0a\2\u057a\u00bf\3\2\2\2\u057b\u057c\7\u008c\2\2\u057c\u00c1"+
		"\3\2\2\2\u057d\u057f\13\2\2\2\u057e\u057d\3\2\2\2\u057f\u0580\3\2\2\2"+
		"\u0580\u0581\3\2\2\2\u0580\u057e\3\2\2\2\u0581\u00c3\3\2\2\2\u0582\u0583"+
		"\5\u00dan\2\u0583\u00c5\3\2\2\2\u0584\u0585\5\u00dan\2\u0585\u00c7\3\2"+
		"\2\2\u0586\u0587\5\u00dan\2\u0587\u00c9\3\2\2\2\u0588\u0589\5\u00dan\2"+
		"\u0589\u00cb\3\2\2\2\u058a\u058b\5\u00dan\2\u058b\u00cd\3\2\2\2\u058c"+
		"\u058d\5\u00dan\2\u058d\u00cf\3\2\2\2\u058e\u058f\5\u00dan\2\u058f\u00d1"+
		"\3\2\2\2\u0590\u0591\5\u00dan\2\u0591\u00d3\3\2\2\2\u0592\u0593\5\u00da"+
		"n\2\u0593\u00d5\3\2\2\2\u0594\u0595\5\u00dan\2\u0595\u00d7\3\2\2\2\u0596"+
		"\u0597\t\24\2\2\u0597\u00d9\3\2\2\2\u0598\u05a1\7\u008d\2\2\u0599\u05a1"+
		"\7\u008c\2\2\u059a\u05a1\5\u00bc_\2\u059b\u05a1\7\u0090\2\2\u059c\u059d"+
		"\7\f\2\2\u059d\u059e\5\u00dan\2\u059e\u059f\7\r\2\2\u059f\u05a1\3\2\2"+
		"\2\u05a0\u0598\3\2\2\2\u05a0\u0599\3\2\2\2\u05a0\u059a\3\2\2\2\u05a0\u059b"+
		"\3\2\2\2\u05a0\u059c\3\2\2\2\u05a1\u00db\3\2\2\2\u00b5\u00e0\u00e3\u00e5"+
		"\u00f0\u00f5\u00fe\u0104\u0109\u010e\u0113\u011c\u0124\u012c\u0133\u0136"+
		"\u013e\u0143\u0149\u014c\u014e\u0154\u0158\u015f\u0165\u0170\u0178\u0181"+
		"\u0184\u018b\u018f\u0194\u019b\u019d\u01a1\u01a5\u01aa\u01bc\u01c8\u01cf"+
		"\u01d3\u01da\u01df\u01e4\u01e8\u01ed\u01ef\u01f5\u01fa\u01fe\u0203\u0205"+
		"\u020f\u0213\u0229\u0234\u023c\u024e\u0250\u0255\u025c\u0261\u0267\u026e"+
		"\u0275\u027e\u0294\u029d\u02aa\u02ad\u02b3\u02b5\u02be\u02c5\u02ca\u02ce"+
		"\u02f0\u02f3\u02fb\u0303\u0308\u030e\u0314\u0318\u0320\u0322\u0328\u032c"+
		"\u0335\u033d\u033f\u0347\u034c\u0358\u035b\u0361\u037f\u038a\u038c\u0392"+
		"\u0399\u039d\u03a2\u03ac\u03af\u03b5\u03b8\u03be\u03c7\u03cc\u03d1\u03d5"+
		"\u03d9\u03e0\u03e3\u03e5\u03f1\u03f8\u03fb\u03ff\u040a\u0414\u0421\u0429"+
		"\u042c\u0434\u043b\u0443\u044d\u0455\u0459\u0462\u0465\u0467\u046c\u0470"+
		"\u0473\u047a\u0482\u0486\u048a\u048d\u0493\u0496\u0498\u04a1\u04a7\u04aa"+
		"\u04ad\u04b4\u04bb\u04c4\u04c8\u04ca\u04ce\u04d7\u04dc\u04de\u04e7\u04f2"+
		"\u04f9\u04fc\u0504\u050a\u050c\u0518\u051c\u0525\u052b\u0531\u0539\u0545"+
		"\u054c\u0554\u055b\u055e\u0562\u0567\u0580\u05a0";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}