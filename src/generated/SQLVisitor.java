// Generated from D:/Desktop/chapter2/compiler/Compiler-1-7-2020/compiler-project2/src\SQL.g4 by ANTLR 4.8
package generated;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SQLParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface SQLVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SQLParser#parse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParse(SQLParser.ParseContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#error}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitError(SQLParser.ErrorContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#function_call}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_call(SQLParser.Function_callContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#argument_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgument_list(SQLParser.Argument_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#argument}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgument(SQLParser.ArgumentContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#function_def}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_def(SQLParser.Function_defContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#parameter_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameter_list(SQLParser.Parameter_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameter(SQLParser.ParameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#default_var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefault_var(SQLParser.Default_varContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBody(SQLParser.BodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#java_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJava_stmt(SQLParser.Java_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#not_need_scol}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNot_need_scol(SQLParser.Not_need_scolContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#need_scol}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNeed_scol(SQLParser.Need_scolContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#incriment_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIncriment_stmt(SQLParser.Incriment_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#print}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint(SQLParser.PrintContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#end_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnd_body(SQLParser.End_bodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#base_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBase_body(SQLParser.Base_bodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#for_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor_stmt(SQLParser.For_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#for_loop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor_loop(SQLParser.For_loopContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#for_each_loop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor_each_loop(SQLParser.For_each_loopContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#switch_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitch_stmt(SQLParser.Switch_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#case_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCase_stmt(SQLParser.Case_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#default_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefault_stmt(SQLParser.Default_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#if_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_stmt(SQLParser.If_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#else_if_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElse_if_stmt(SQLParser.Else_if_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#else_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElse_stmt(SQLParser.Else_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#if_inline}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_inline(SQLParser.If_inlineContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#ifinlin_side}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfinlin_side(SQLParser.Ifinlin_sideContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#while_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhile_stmt(SQLParser.While_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#do_while_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDo_while_stmt(SQLParser.Do_while_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#return_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturn_stmt(SQLParser.Return_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#sql_stmt_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSql_stmt_list(SQLParser.Sql_stmt_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#sql_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSql_stmt(SQLParser.Sql_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#create_table_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreate_table_stmt(SQLParser.Create_table_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#table_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_type(SQLParser.Table_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#table_path}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_path(SQLParser.Table_pathContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#create_type_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreate_type_stmt(SQLParser.Create_type_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#select_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect_stmt(SQLParser.Select_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#column_def}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_def(SQLParser.Column_defContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#type_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_name(SQLParser.Type_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#create_aggregation_function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreate_aggregation_function(SQLParser.Create_aggregation_functionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#jarPath}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJarPath(SQLParser.JarPathContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#jarName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJarName(SQLParser.JarNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#className}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassName(SQLParser.ClassNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#methodName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodName(SQLParser.MethodNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#returnType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnType(SQLParser.ReturnTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#parameters}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameters(SQLParser.ParametersContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#condition_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondition_exp(SQLParser.Condition_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#num_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNum_exp(SQLParser.Num_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(SQLParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#function_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_expr(SQLParser.Function_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#not_in_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNot_in_expr(SQLParser.Not_in_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#foreign_key_clause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForeign_key_clause(SQLParser.Foreign_key_clauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#foreign_on_condition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForeign_on_condition(SQLParser.Foreign_on_conditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#fk_target_column_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFk_target_column_name(SQLParser.Fk_target_column_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#indexed_column}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexed_column(SQLParser.Indexed_columnContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#table_constraint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_constraint(SQLParser.Table_constraintContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#table_constraint_primary_key}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_constraint_primary_key(SQLParser.Table_constraint_primary_keyContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#table_constraint_foreign_key}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_constraint_foreign_key(SQLParser.Table_constraint_foreign_keyContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#table_constraint_unique}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_constraint_unique(SQLParser.Table_constraint_uniqueContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#table_constraint_key}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_constraint_key(SQLParser.Table_constraint_keyContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#fk_origin_column_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFk_origin_column_name(SQLParser.Fk_origin_column_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#qualified_table_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQualified_table_name(SQLParser.Qualified_table_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#ordering_term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrdering_term(SQLParser.Ordering_termContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#result_column}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitResult_column(SQLParser.Result_columnContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#table_or_subquery}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_or_subquery(SQLParser.Table_or_subqueryContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#join_clause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoin_clause(SQLParser.Join_clauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#join_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoin_operator(SQLParser.Join_operatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#join_constraint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoin_constraint(SQLParser.Join_constraintContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#select_core}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect_core(SQLParser.Select_coreContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#multivar_def}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultivar_def(SQLParser.Multivar_defContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#multivar_def_sub}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultivar_def_sub(SQLParser.Multivar_def_subContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#var_def}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_def(SQLParser.Var_defContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#array_def}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray_def(SQLParser.Array_defContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#array_dec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray_dec(SQLParser.Array_decContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#var_deceleration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_deceleration(SQLParser.Var_decelerationContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#decliration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecliration(SQLParser.DeclirationContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#java_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJava_value(SQLParser.Java_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#boolean_var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolean_var(SQLParser.Boolean_varContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#num_var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNum_var(SQLParser.Num_varContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(SQLParser.VariableContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#string_val}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitString_val(SQLParser.String_valContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#boolean_val}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolean_val(SQLParser.Boolean_valContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#num_val}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNum_val(SQLParser.Num_valContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#signed_number}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSigned_number(SQLParser.Signed_numberContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#literal_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral_value(SQLParser.Literal_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#assign_op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssign_op(SQLParser.Assign_opContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#num_op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNum_op(SQLParser.Num_opContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#comparison_op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparison_op(SQLParser.Comparison_opContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#incriment_op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIncriment_op(SQLParser.Incriment_opContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#unary_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary_operator(SQLParser.Unary_operatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#error_message}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitError_message(SQLParser.Error_messageContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#column_alias}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_alias(SQLParser.Column_aliasContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#keyword}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKeyword(SQLParser.KeywordContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#java_function_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJava_function_name(SQLParser.Java_function_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#java_var_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJava_var_name(SQLParser.Java_var_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#unknown}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnknown(SQLParser.UnknownContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitName(SQLParser.NameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#function_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_name(SQLParser.Function_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#database_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDatabase_name(SQLParser.Database_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#source_table_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSource_table_name(SQLParser.Source_table_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#table_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_name(SQLParser.Table_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#new_table_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNew_table_name(SQLParser.New_table_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#column_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_name(SQLParser.Column_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#collation_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCollation_name(SQLParser.Collation_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#foreign_table}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForeign_table(SQLParser.Foreign_tableContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#index_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndex_name(SQLParser.Index_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#table_alias}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_alias(SQLParser.Table_aliasContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#any_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAny_name(SQLParser.Any_nameContext ctx);
}