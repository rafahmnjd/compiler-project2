// Generated from D:/Desktop/chapter2/compiler/Compiler-1-7-2020/compiler-project2/src\SQL.g4 by ANTLR 4.8
package generated;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SQLParser}.
 */
public interface SQLListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SQLParser#parse}.
	 * @param ctx the parse tree
	 */
	void enterParse(SQLParser.ParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#parse}.
	 * @param ctx the parse tree
	 */
	void exitParse(SQLParser.ParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#error}.
	 * @param ctx the parse tree
	 */
	void enterError(SQLParser.ErrorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#error}.
	 * @param ctx the parse tree
	 */
	void exitError(SQLParser.ErrorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#function_call}.
	 * @param ctx the parse tree
	 */
	void enterFunction_call(SQLParser.Function_callContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#function_call}.
	 * @param ctx the parse tree
	 */
	void exitFunction_call(SQLParser.Function_callContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#argument_list}.
	 * @param ctx the parse tree
	 */
	void enterArgument_list(SQLParser.Argument_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#argument_list}.
	 * @param ctx the parse tree
	 */
	void exitArgument_list(SQLParser.Argument_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#argument}.
	 * @param ctx the parse tree
	 */
	void enterArgument(SQLParser.ArgumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#argument}.
	 * @param ctx the parse tree
	 */
	void exitArgument(SQLParser.ArgumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#function_def}.
	 * @param ctx the parse tree
	 */
	void enterFunction_def(SQLParser.Function_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#function_def}.
	 * @param ctx the parse tree
	 */
	void exitFunction_def(SQLParser.Function_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#parameter_list}.
	 * @param ctx the parse tree
	 */
	void enterParameter_list(SQLParser.Parameter_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#parameter_list}.
	 * @param ctx the parse tree
	 */
	void exitParameter_list(SQLParser.Parameter_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#parameter}.
	 * @param ctx the parse tree
	 */
	void enterParameter(SQLParser.ParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#parameter}.
	 * @param ctx the parse tree
	 */
	void exitParameter(SQLParser.ParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#default_var}.
	 * @param ctx the parse tree
	 */
	void enterDefault_var(SQLParser.Default_varContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#default_var}.
	 * @param ctx the parse tree
	 */
	void exitDefault_var(SQLParser.Default_varContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#body}.
	 * @param ctx the parse tree
	 */
	void enterBody(SQLParser.BodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#body}.
	 * @param ctx the parse tree
	 */
	void exitBody(SQLParser.BodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#java_stmt}.
	 * @param ctx the parse tree
	 */
	void enterJava_stmt(SQLParser.Java_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#java_stmt}.
	 * @param ctx the parse tree
	 */
	void exitJava_stmt(SQLParser.Java_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#not_need_scol}.
	 * @param ctx the parse tree
	 */
	void enterNot_need_scol(SQLParser.Not_need_scolContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#not_need_scol}.
	 * @param ctx the parse tree
	 */
	void exitNot_need_scol(SQLParser.Not_need_scolContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#need_scol}.
	 * @param ctx the parse tree
	 */
	void enterNeed_scol(SQLParser.Need_scolContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#need_scol}.
	 * @param ctx the parse tree
	 */
	void exitNeed_scol(SQLParser.Need_scolContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#incriment_stmt}.
	 * @param ctx the parse tree
	 */
	void enterIncriment_stmt(SQLParser.Incriment_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#incriment_stmt}.
	 * @param ctx the parse tree
	 */
	void exitIncriment_stmt(SQLParser.Incriment_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#print}.
	 * @param ctx the parse tree
	 */
	void enterPrint(SQLParser.PrintContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#print}.
	 * @param ctx the parse tree
	 */
	void exitPrint(SQLParser.PrintContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#end_body}.
	 * @param ctx the parse tree
	 */
	void enterEnd_body(SQLParser.End_bodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#end_body}.
	 * @param ctx the parse tree
	 */
	void exitEnd_body(SQLParser.End_bodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#base_body}.
	 * @param ctx the parse tree
	 */
	void enterBase_body(SQLParser.Base_bodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#base_body}.
	 * @param ctx the parse tree
	 */
	void exitBase_body(SQLParser.Base_bodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#for_stmt}.
	 * @param ctx the parse tree
	 */
	void enterFor_stmt(SQLParser.For_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#for_stmt}.
	 * @param ctx the parse tree
	 */
	void exitFor_stmt(SQLParser.For_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#for_loop}.
	 * @param ctx the parse tree
	 */
	void enterFor_loop(SQLParser.For_loopContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#for_loop}.
	 * @param ctx the parse tree
	 */
	void exitFor_loop(SQLParser.For_loopContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#for_each_loop}.
	 * @param ctx the parse tree
	 */
	void enterFor_each_loop(SQLParser.For_each_loopContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#for_each_loop}.
	 * @param ctx the parse tree
	 */
	void exitFor_each_loop(SQLParser.For_each_loopContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#switch_stmt}.
	 * @param ctx the parse tree
	 */
	void enterSwitch_stmt(SQLParser.Switch_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#switch_stmt}.
	 * @param ctx the parse tree
	 */
	void exitSwitch_stmt(SQLParser.Switch_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#case_stmt}.
	 * @param ctx the parse tree
	 */
	void enterCase_stmt(SQLParser.Case_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#case_stmt}.
	 * @param ctx the parse tree
	 */
	void exitCase_stmt(SQLParser.Case_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#default_stmt}.
	 * @param ctx the parse tree
	 */
	void enterDefault_stmt(SQLParser.Default_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#default_stmt}.
	 * @param ctx the parse tree
	 */
	void exitDefault_stmt(SQLParser.Default_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#if_stmt}.
	 * @param ctx the parse tree
	 */
	void enterIf_stmt(SQLParser.If_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#if_stmt}.
	 * @param ctx the parse tree
	 */
	void exitIf_stmt(SQLParser.If_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#else_if_stmt}.
	 * @param ctx the parse tree
	 */
	void enterElse_if_stmt(SQLParser.Else_if_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#else_if_stmt}.
	 * @param ctx the parse tree
	 */
	void exitElse_if_stmt(SQLParser.Else_if_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#else_stmt}.
	 * @param ctx the parse tree
	 */
	void enterElse_stmt(SQLParser.Else_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#else_stmt}.
	 * @param ctx the parse tree
	 */
	void exitElse_stmt(SQLParser.Else_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#if_inline}.
	 * @param ctx the parse tree
	 */
	void enterIf_inline(SQLParser.If_inlineContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#if_inline}.
	 * @param ctx the parse tree
	 */
	void exitIf_inline(SQLParser.If_inlineContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#ifinlin_side}.
	 * @param ctx the parse tree
	 */
	void enterIfinlin_side(SQLParser.Ifinlin_sideContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#ifinlin_side}.
	 * @param ctx the parse tree
	 */
	void exitIfinlin_side(SQLParser.Ifinlin_sideContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#while_stmt}.
	 * @param ctx the parse tree
	 */
	void enterWhile_stmt(SQLParser.While_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#while_stmt}.
	 * @param ctx the parse tree
	 */
	void exitWhile_stmt(SQLParser.While_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#do_while_stmt}.
	 * @param ctx the parse tree
	 */
	void enterDo_while_stmt(SQLParser.Do_while_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#do_while_stmt}.
	 * @param ctx the parse tree
	 */
	void exitDo_while_stmt(SQLParser.Do_while_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#return_stmt}.
	 * @param ctx the parse tree
	 */
	void enterReturn_stmt(SQLParser.Return_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#return_stmt}.
	 * @param ctx the parse tree
	 */
	void exitReturn_stmt(SQLParser.Return_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#sql_stmt_list}.
	 * @param ctx the parse tree
	 */
	void enterSql_stmt_list(SQLParser.Sql_stmt_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#sql_stmt_list}.
	 * @param ctx the parse tree
	 */
	void exitSql_stmt_list(SQLParser.Sql_stmt_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#sql_stmt}.
	 * @param ctx the parse tree
	 */
	void enterSql_stmt(SQLParser.Sql_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#sql_stmt}.
	 * @param ctx the parse tree
	 */
	void exitSql_stmt(SQLParser.Sql_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#create_table_stmt}.
	 * @param ctx the parse tree
	 */
	void enterCreate_table_stmt(SQLParser.Create_table_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#create_table_stmt}.
	 * @param ctx the parse tree
	 */
	void exitCreate_table_stmt(SQLParser.Create_table_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#table_type}.
	 * @param ctx the parse tree
	 */
	void enterTable_type(SQLParser.Table_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#table_type}.
	 * @param ctx the parse tree
	 */
	void exitTable_type(SQLParser.Table_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#table_path}.
	 * @param ctx the parse tree
	 */
	void enterTable_path(SQLParser.Table_pathContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#table_path}.
	 * @param ctx the parse tree
	 */
	void exitTable_path(SQLParser.Table_pathContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#create_type_stmt}.
	 * @param ctx the parse tree
	 */
	void enterCreate_type_stmt(SQLParser.Create_type_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#create_type_stmt}.
	 * @param ctx the parse tree
	 */
	void exitCreate_type_stmt(SQLParser.Create_type_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#select_stmt}.
	 * @param ctx the parse tree
	 */
	void enterSelect_stmt(SQLParser.Select_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#select_stmt}.
	 * @param ctx the parse tree
	 */
	void exitSelect_stmt(SQLParser.Select_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#column_def}.
	 * @param ctx the parse tree
	 */
	void enterColumn_def(SQLParser.Column_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#column_def}.
	 * @param ctx the parse tree
	 */
	void exitColumn_def(SQLParser.Column_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#type_name}.
	 * @param ctx the parse tree
	 */
	void enterType_name(SQLParser.Type_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#type_name}.
	 * @param ctx the parse tree
	 */
	void exitType_name(SQLParser.Type_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#create_aggregation_function}.
	 * @param ctx the parse tree
	 */
	void enterCreate_aggregation_function(SQLParser.Create_aggregation_functionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#create_aggregation_function}.
	 * @param ctx the parse tree
	 */
	void exitCreate_aggregation_function(SQLParser.Create_aggregation_functionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#jarPath}.
	 * @param ctx the parse tree
	 */
	void enterJarPath(SQLParser.JarPathContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#jarPath}.
	 * @param ctx the parse tree
	 */
	void exitJarPath(SQLParser.JarPathContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#jarName}.
	 * @param ctx the parse tree
	 */
	void enterJarName(SQLParser.JarNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#jarName}.
	 * @param ctx the parse tree
	 */
	void exitJarName(SQLParser.JarNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#className}.
	 * @param ctx the parse tree
	 */
	void enterClassName(SQLParser.ClassNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#className}.
	 * @param ctx the parse tree
	 */
	void exitClassName(SQLParser.ClassNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#methodName}.
	 * @param ctx the parse tree
	 */
	void enterMethodName(SQLParser.MethodNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#methodName}.
	 * @param ctx the parse tree
	 */
	void exitMethodName(SQLParser.MethodNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#returnType}.
	 * @param ctx the parse tree
	 */
	void enterReturnType(SQLParser.ReturnTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#returnType}.
	 * @param ctx the parse tree
	 */
	void exitReturnType(SQLParser.ReturnTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#parameters}.
	 * @param ctx the parse tree
	 */
	void enterParameters(SQLParser.ParametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#parameters}.
	 * @param ctx the parse tree
	 */
	void exitParameters(SQLParser.ParametersContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#condition_exp}.
	 * @param ctx the parse tree
	 */
	void enterCondition_exp(SQLParser.Condition_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#condition_exp}.
	 * @param ctx the parse tree
	 */
	void exitCondition_exp(SQLParser.Condition_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#num_exp}.
	 * @param ctx the parse tree
	 */
	void enterNum_exp(SQLParser.Num_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#num_exp}.
	 * @param ctx the parse tree
	 */
	void exitNum_exp(SQLParser.Num_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(SQLParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(SQLParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#function_expr}.
	 * @param ctx the parse tree
	 */
	void enterFunction_expr(SQLParser.Function_exprContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#function_expr}.
	 * @param ctx the parse tree
	 */
	void exitFunction_expr(SQLParser.Function_exprContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#not_in_expr}.
	 * @param ctx the parse tree
	 */
	void enterNot_in_expr(SQLParser.Not_in_exprContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#not_in_expr}.
	 * @param ctx the parse tree
	 */
	void exitNot_in_expr(SQLParser.Not_in_exprContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#foreign_key_clause}.
	 * @param ctx the parse tree
	 */
	void enterForeign_key_clause(SQLParser.Foreign_key_clauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#foreign_key_clause}.
	 * @param ctx the parse tree
	 */
	void exitForeign_key_clause(SQLParser.Foreign_key_clauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#foreign_on_condition}.
	 * @param ctx the parse tree
	 */
	void enterForeign_on_condition(SQLParser.Foreign_on_conditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#foreign_on_condition}.
	 * @param ctx the parse tree
	 */
	void exitForeign_on_condition(SQLParser.Foreign_on_conditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#fk_target_column_name}.
	 * @param ctx the parse tree
	 */
	void enterFk_target_column_name(SQLParser.Fk_target_column_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#fk_target_column_name}.
	 * @param ctx the parse tree
	 */
	void exitFk_target_column_name(SQLParser.Fk_target_column_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#indexed_column}.
	 * @param ctx the parse tree
	 */
	void enterIndexed_column(SQLParser.Indexed_columnContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#indexed_column}.
	 * @param ctx the parse tree
	 */
	void exitIndexed_column(SQLParser.Indexed_columnContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#table_constraint}.
	 * @param ctx the parse tree
	 */
	void enterTable_constraint(SQLParser.Table_constraintContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#table_constraint}.
	 * @param ctx the parse tree
	 */
	void exitTable_constraint(SQLParser.Table_constraintContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#table_constraint_primary_key}.
	 * @param ctx the parse tree
	 */
	void enterTable_constraint_primary_key(SQLParser.Table_constraint_primary_keyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#table_constraint_primary_key}.
	 * @param ctx the parse tree
	 */
	void exitTable_constraint_primary_key(SQLParser.Table_constraint_primary_keyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#table_constraint_foreign_key}.
	 * @param ctx the parse tree
	 */
	void enterTable_constraint_foreign_key(SQLParser.Table_constraint_foreign_keyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#table_constraint_foreign_key}.
	 * @param ctx the parse tree
	 */
	void exitTable_constraint_foreign_key(SQLParser.Table_constraint_foreign_keyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#table_constraint_unique}.
	 * @param ctx the parse tree
	 */
	void enterTable_constraint_unique(SQLParser.Table_constraint_uniqueContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#table_constraint_unique}.
	 * @param ctx the parse tree
	 */
	void exitTable_constraint_unique(SQLParser.Table_constraint_uniqueContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#table_constraint_key}.
	 * @param ctx the parse tree
	 */
	void enterTable_constraint_key(SQLParser.Table_constraint_keyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#table_constraint_key}.
	 * @param ctx the parse tree
	 */
	void exitTable_constraint_key(SQLParser.Table_constraint_keyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#fk_origin_column_name}.
	 * @param ctx the parse tree
	 */
	void enterFk_origin_column_name(SQLParser.Fk_origin_column_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#fk_origin_column_name}.
	 * @param ctx the parse tree
	 */
	void exitFk_origin_column_name(SQLParser.Fk_origin_column_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#qualified_table_name}.
	 * @param ctx the parse tree
	 */
	void enterQualified_table_name(SQLParser.Qualified_table_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#qualified_table_name}.
	 * @param ctx the parse tree
	 */
	void exitQualified_table_name(SQLParser.Qualified_table_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#ordering_term}.
	 * @param ctx the parse tree
	 */
	void enterOrdering_term(SQLParser.Ordering_termContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#ordering_term}.
	 * @param ctx the parse tree
	 */
	void exitOrdering_term(SQLParser.Ordering_termContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#result_column}.
	 * @param ctx the parse tree
	 */
	void enterResult_column(SQLParser.Result_columnContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#result_column}.
	 * @param ctx the parse tree
	 */
	void exitResult_column(SQLParser.Result_columnContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#table_or_subquery}.
	 * @param ctx the parse tree
	 */
	void enterTable_or_subquery(SQLParser.Table_or_subqueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#table_or_subquery}.
	 * @param ctx the parse tree
	 */
	void exitTable_or_subquery(SQLParser.Table_or_subqueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#join_clause}.
	 * @param ctx the parse tree
	 */
	void enterJoin_clause(SQLParser.Join_clauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#join_clause}.
	 * @param ctx the parse tree
	 */
	void exitJoin_clause(SQLParser.Join_clauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#join_operator}.
	 * @param ctx the parse tree
	 */
	void enterJoin_operator(SQLParser.Join_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#join_operator}.
	 * @param ctx the parse tree
	 */
	void exitJoin_operator(SQLParser.Join_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#join_constraint}.
	 * @param ctx the parse tree
	 */
	void enterJoin_constraint(SQLParser.Join_constraintContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#join_constraint}.
	 * @param ctx the parse tree
	 */
	void exitJoin_constraint(SQLParser.Join_constraintContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#select_core}.
	 * @param ctx the parse tree
	 */
	void enterSelect_core(SQLParser.Select_coreContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#select_core}.
	 * @param ctx the parse tree
	 */
	void exitSelect_core(SQLParser.Select_coreContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#multivar_def}.
	 * @param ctx the parse tree
	 */
	void enterMultivar_def(SQLParser.Multivar_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#multivar_def}.
	 * @param ctx the parse tree
	 */
	void exitMultivar_def(SQLParser.Multivar_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#multivar_def_sub}.
	 * @param ctx the parse tree
	 */
	void enterMultivar_def_sub(SQLParser.Multivar_def_subContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#multivar_def_sub}.
	 * @param ctx the parse tree
	 */
	void exitMultivar_def_sub(SQLParser.Multivar_def_subContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#var_def}.
	 * @param ctx the parse tree
	 */
	void enterVar_def(SQLParser.Var_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#var_def}.
	 * @param ctx the parse tree
	 */
	void exitVar_def(SQLParser.Var_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#array_def}.
	 * @param ctx the parse tree
	 */
	void enterArray_def(SQLParser.Array_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#array_def}.
	 * @param ctx the parse tree
	 */
	void exitArray_def(SQLParser.Array_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#array_dec}.
	 * @param ctx the parse tree
	 */
	void enterArray_dec(SQLParser.Array_decContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#array_dec}.
	 * @param ctx the parse tree
	 */
	void exitArray_dec(SQLParser.Array_decContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#var_deceleration}.
	 * @param ctx the parse tree
	 */
	void enterVar_deceleration(SQLParser.Var_decelerationContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#var_deceleration}.
	 * @param ctx the parse tree
	 */
	void exitVar_deceleration(SQLParser.Var_decelerationContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#decliration}.
	 * @param ctx the parse tree
	 */
	void enterDecliration(SQLParser.DeclirationContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#decliration}.
	 * @param ctx the parse tree
	 */
	void exitDecliration(SQLParser.DeclirationContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#java_value}.
	 * @param ctx the parse tree
	 */
	void enterJava_value(SQLParser.Java_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#java_value}.
	 * @param ctx the parse tree
	 */
	void exitJava_value(SQLParser.Java_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#boolean_var}.
	 * @param ctx the parse tree
	 */
	void enterBoolean_var(SQLParser.Boolean_varContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#boolean_var}.
	 * @param ctx the parse tree
	 */
	void exitBoolean_var(SQLParser.Boolean_varContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#num_var}.
	 * @param ctx the parse tree
	 */
	void enterNum_var(SQLParser.Num_varContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#num_var}.
	 * @param ctx the parse tree
	 */
	void exitNum_var(SQLParser.Num_varContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(SQLParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(SQLParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#string_val}.
	 * @param ctx the parse tree
	 */
	void enterString_val(SQLParser.String_valContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#string_val}.
	 * @param ctx the parse tree
	 */
	void exitString_val(SQLParser.String_valContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#boolean_val}.
	 * @param ctx the parse tree
	 */
	void enterBoolean_val(SQLParser.Boolean_valContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#boolean_val}.
	 * @param ctx the parse tree
	 */
	void exitBoolean_val(SQLParser.Boolean_valContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#num_val}.
	 * @param ctx the parse tree
	 */
	void enterNum_val(SQLParser.Num_valContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#num_val}.
	 * @param ctx the parse tree
	 */
	void exitNum_val(SQLParser.Num_valContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#signed_number}.
	 * @param ctx the parse tree
	 */
	void enterSigned_number(SQLParser.Signed_numberContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#signed_number}.
	 * @param ctx the parse tree
	 */
	void exitSigned_number(SQLParser.Signed_numberContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#literal_value}.
	 * @param ctx the parse tree
	 */
	void enterLiteral_value(SQLParser.Literal_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#literal_value}.
	 * @param ctx the parse tree
	 */
	void exitLiteral_value(SQLParser.Literal_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#assign_op}.
	 * @param ctx the parse tree
	 */
	void enterAssign_op(SQLParser.Assign_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#assign_op}.
	 * @param ctx the parse tree
	 */
	void exitAssign_op(SQLParser.Assign_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#num_op}.
	 * @param ctx the parse tree
	 */
	void enterNum_op(SQLParser.Num_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#num_op}.
	 * @param ctx the parse tree
	 */
	void exitNum_op(SQLParser.Num_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#comparison_op}.
	 * @param ctx the parse tree
	 */
	void enterComparison_op(SQLParser.Comparison_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#comparison_op}.
	 * @param ctx the parse tree
	 */
	void exitComparison_op(SQLParser.Comparison_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#incriment_op}.
	 * @param ctx the parse tree
	 */
	void enterIncriment_op(SQLParser.Incriment_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#incriment_op}.
	 * @param ctx the parse tree
	 */
	void exitIncriment_op(SQLParser.Incriment_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#unary_operator}.
	 * @param ctx the parse tree
	 */
	void enterUnary_operator(SQLParser.Unary_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#unary_operator}.
	 * @param ctx the parse tree
	 */
	void exitUnary_operator(SQLParser.Unary_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#error_message}.
	 * @param ctx the parse tree
	 */
	void enterError_message(SQLParser.Error_messageContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#error_message}.
	 * @param ctx the parse tree
	 */
	void exitError_message(SQLParser.Error_messageContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#column_alias}.
	 * @param ctx the parse tree
	 */
	void enterColumn_alias(SQLParser.Column_aliasContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#column_alias}.
	 * @param ctx the parse tree
	 */
	void exitColumn_alias(SQLParser.Column_aliasContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#keyword}.
	 * @param ctx the parse tree
	 */
	void enterKeyword(SQLParser.KeywordContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#keyword}.
	 * @param ctx the parse tree
	 */
	void exitKeyword(SQLParser.KeywordContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#java_function_name}.
	 * @param ctx the parse tree
	 */
	void enterJava_function_name(SQLParser.Java_function_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#java_function_name}.
	 * @param ctx the parse tree
	 */
	void exitJava_function_name(SQLParser.Java_function_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#java_var_name}.
	 * @param ctx the parse tree
	 */
	void enterJava_var_name(SQLParser.Java_var_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#java_var_name}.
	 * @param ctx the parse tree
	 */
	void exitJava_var_name(SQLParser.Java_var_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#unknown}.
	 * @param ctx the parse tree
	 */
	void enterUnknown(SQLParser.UnknownContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#unknown}.
	 * @param ctx the parse tree
	 */
	void exitUnknown(SQLParser.UnknownContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#name}.
	 * @param ctx the parse tree
	 */
	void enterName(SQLParser.NameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#name}.
	 * @param ctx the parse tree
	 */
	void exitName(SQLParser.NameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#function_name}.
	 * @param ctx the parse tree
	 */
	void enterFunction_name(SQLParser.Function_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#function_name}.
	 * @param ctx the parse tree
	 */
	void exitFunction_name(SQLParser.Function_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#database_name}.
	 * @param ctx the parse tree
	 */
	void enterDatabase_name(SQLParser.Database_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#database_name}.
	 * @param ctx the parse tree
	 */
	void exitDatabase_name(SQLParser.Database_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#source_table_name}.
	 * @param ctx the parse tree
	 */
	void enterSource_table_name(SQLParser.Source_table_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#source_table_name}.
	 * @param ctx the parse tree
	 */
	void exitSource_table_name(SQLParser.Source_table_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#table_name}.
	 * @param ctx the parse tree
	 */
	void enterTable_name(SQLParser.Table_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#table_name}.
	 * @param ctx the parse tree
	 */
	void exitTable_name(SQLParser.Table_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#new_table_name}.
	 * @param ctx the parse tree
	 */
	void enterNew_table_name(SQLParser.New_table_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#new_table_name}.
	 * @param ctx the parse tree
	 */
	void exitNew_table_name(SQLParser.New_table_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#column_name}.
	 * @param ctx the parse tree
	 */
	void enterColumn_name(SQLParser.Column_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#column_name}.
	 * @param ctx the parse tree
	 */
	void exitColumn_name(SQLParser.Column_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#collation_name}.
	 * @param ctx the parse tree
	 */
	void enterCollation_name(SQLParser.Collation_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#collation_name}.
	 * @param ctx the parse tree
	 */
	void exitCollation_name(SQLParser.Collation_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#foreign_table}.
	 * @param ctx the parse tree
	 */
	void enterForeign_table(SQLParser.Foreign_tableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#foreign_table}.
	 * @param ctx the parse tree
	 */
	void exitForeign_table(SQLParser.Foreign_tableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#index_name}.
	 * @param ctx the parse tree
	 */
	void enterIndex_name(SQLParser.Index_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#index_name}.
	 * @param ctx the parse tree
	 */
	void exitIndex_name(SQLParser.Index_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#table_alias}.
	 * @param ctx the parse tree
	 */
	void enterTable_alias(SQLParser.Table_aliasContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#table_alias}.
	 * @param ctx the parse tree
	 */
	void exitTable_alias(SQLParser.Table_aliasContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#any_name}.
	 * @param ctx the parse tree
	 */
	void enterAny_name(SQLParser.Any_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#any_name}.
	 * @param ctx the parse tree
	 */
	void exitAny_name(SQLParser.Any_nameContext ctx);
}