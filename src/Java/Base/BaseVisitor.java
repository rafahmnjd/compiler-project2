package Java.Base;

import Java.AST.Defenations_and_Values.*;
import Java.AST.Defenations_and_Values.Values.Value;
import Java.AST.Defenations_and_Values.Values.javavalue.Java_value;
import Java.AST.Expression.*;
import Java.AST.Expression.JavaExpr.Condition_exp;
import Java.AST.Expression.JavaExpr.Num_exp;
import Java.AST.Functions.Body;
import Java.AST.Functions.Function_call;
import Java.AST.Functions.Function_def;
import Java.AST.Java_Stmt.*;
import Java.AST.Java_Stmt.Conditions.ElseIf;
import Java.AST.Java_Stmt.Conditions.If_inline;
import Java.AST.Java_Stmt.Conditions.If_stmt;
import Java.AST.Java_Stmt.Loops.*;
import Java.AST.Java_Stmt.Switch_Stmt.Case_stmt;
import Java.AST.Java_Stmt.Switch_Stmt.Switch_stmt;
import Java.AST.Parse;
import Java.AST.Sql.DDL.ColumnDef;
import Java.AST.Sql.DDL.Constraint.ForeignKeyClause;
import Java.AST.Sql.DDL.Constraint.ForeignKeyConstraint;
import Java.AST.Sql.DDL.Constraint.ForeignOnCondition;
import Java.AST.Sql.DDL.Constraint.TableConstraint;
import Java.AST.Sql.DDL.CreateAggFunction;
import Java.AST.Sql.DDL.CreateTableStmt;
import Java.AST.Sql.QueryStmt.JoinElement;
import Java.AST.Sql.QueryStmt.Join_clause;
import Java.AST.Sql.QueryStmt.SelectCoreOrValue;
import Java.AST.Sql.QueryStmt.SelectStmt;
import Java.AST.Sql.Statement;
import Java.AST.Sql.Values.*;
import Java.Main;
import Java.SymbolTable.AggregationFunction;
import Java.SymbolTable.Scope;
import Java.SymbolTable.Symbol;
import Java.SymbolTable.Type;
import generated.SQLBaseVisitor;
import generated.SQLParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static Java.AST.Sql.DDL.Constraint.ForeignKeyClause.Action;

public class BaseVisitor extends SQLBaseVisitor {

    @Override
    public Parse visitParse(SQLParser.ParseContext ctx) {
        System.out.println("visitParse");
        Parse p = new Parse();
        Scope parserScope = new Scope();
        parserScope.setParent(null);
        parserScope.setId("parser_Scope");
        if (ctx.function_call() != null) {
            List<Function_call> function_calls = new ArrayList<>();
            for (int i = 0; i < ctx.function_call().size(); i++) {
                function_calls.add(visitFunction_call(ctx.function_call(i), parserScope));
            }
            p.setFunctionsCall(function_calls);
        }
        if (ctx.function_def() != null) {
            List<Function_def> Function_defs = new ArrayList<>();
            for (int i = 0; i < ctx.function_def().size(); i++) {
                Function_def function_def = visitFunction_def(ctx.function_def(i), parserScope);
                Function_defs.add(function_def);
            }
            p.setFunctions(Function_defs);
        }
        if (ctx.sql_stmt_list() != null) {
            List<Statement> sqlStmts=new ArrayList<>();
            for(int st=0;st<ctx.sql_stmt_list().size();st++){
                sqlStmts.addAll(visitSql_stmt_list(ctx.sql_stmt_list(0)));
            }
            p.setSqlStmts(sqlStmts);
        }
        p.setLine(ctx.getStart().getLine()); //get line number
        p.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return p;
    }

    @Override
    public Object visitError(SQLParser.ErrorContext ctx) {
        return super.visitError(ctx);
    }

    @Override
    public List<Statement> visitSql_stmt_list(SQLParser.Sql_stmt_listContext ctx) {
        List<Statement> sqlStmt = new ArrayList<>();
        for (int i = 0; i < ctx.sql_stmt().size(); i++) {
            sqlStmt.add(visitSql_stmt(ctx.sql_stmt(i)));
        }
        return sqlStmt;
    }

    @Override
    public Statement visitSql_stmt(SQLParser.Sql_stmtContext ctx) {

        System.out.println("visitSql_stmt   ");
        if (ctx.select_stmt() != null)
            return visitSelect_stmt(ctx.select_stmt());

        if (ctx.create_table_stmt() != null)
            return visitCreate_table_stmt(ctx.create_table_stmt());

        if (ctx.create_type_stmt() != null)
             visitCreate_type_stmt(ctx.create_type_stmt());

        if(ctx.create_aggregation_function()!=null)
            return visitCreate_aggregation_function(ctx.create_aggregation_function());
        return new Statement();
    }


    @Override
    public CreateTableStmt visitCreate_table_stmt(SQLParser.Create_table_stmtContext ctx) {
        CreateTableStmt createTableStmt = new CreateTableStmt();
        createTableStmt.setLine(ctx.getStart().getLine()); //get line number
        createTableStmt.setCol(ctx.getStart().getCharPositionInLine()); // get col number

        Type tableType;
        String tablename = "";
        if (ctx.table_name() != null) {
            tablename = visitAny_name(ctx.table_name().any_name());
            if (typeExist(tablename) != null) {
                existBefore(tablename + " type",ctx.table_name().getStart().getLine(),
                        ctx.table_name().getStart().getCharPositionInLine()
                );
                return createTableStmt;
            }
        }
        tableType = new Type(tablename);
        if (ctx.column_def() != null && !ctx.column_def().isEmpty()) {
            for (int cd = 0; cd < ctx.column_def().size(); cd++) {
                ColumnDef coldef = visitColumn_def(ctx.column_def(cd));
                if (columnExist(coldef.getColumnName(), tableType)) {
                    existBefore(coldef.getColumnName() + " column",
                            ctx.column_def(cd).column_name().getStart().getLine(),
                            ctx.column_def(cd).column_name().getStart().getCharPositionInLine());
                } else {
                    tableType.addColumn(coldef.getColumnName(), coldef.getType());
                }
            }
            tableType.setTable(createTableStmt);
        }
        if(ctx.K_TYPE()!=null && ctx.table_type()!=null)
            createTableStmt.setFileType(visitString_val(ctx.table_type().string_val(),null));
        if(ctx.K_PATH()!=null && ctx.table_path()!=null)
            createTableStmt.setPath(visitString_val(ctx.table_path().string_val(),null));
        tableType.setLine(ctx.getStart().getLine());
        tableType.setCol(ctx.getStart().getCharPositionInLine());
        createTableStmt.setSembolTableType(tableType);
        Main.symbolTable.addType(tableType);
        createTableStmt.setName("createTableStmt");
        return createTableStmt;
    }


    @Override
    public SelectStmt visitSelect_stmt(SQLParser.Select_stmtContext ctx) {
        System.out.println("visitSelect_stmt");

        SelectStmt select = new SelectStmt();

        if (ctx.select_core() != null) {
            select.setSellectOrValues(visitSelect_core(ctx.select_core()));
        }
        if (ctx.K_ORDER() != null && ctx.K_BY() != null) {
            select.setORDER_BY(true);
            List<OrderingTerm> orderingTerms = new ArrayList<>();
            for (int ot = 0; ot < ctx.ordering_term().size(); ot++)
                orderingTerms.add(visitOrdering_term(ctx.ordering_term(ot)));
            select.setOrderingTerms(orderingTerms);
        }
        if (ctx.K_LIMIT() != null) {
            select.setK_LIMIT(true);
            select.setLimitExpression(visitExpr(ctx.expr(0)));
            if (ctx.expr().size() == 2) {
                select.setSecondExpression(visitExpr(ctx.expr(1)));
                if (ctx.K_OFFSET() != null)
                    select.setK_OFFSET(true);
                else
                    select.setComma(true);
            }
        }
        select.setName("Select");
        select.setLine(ctx.getStart().getLine()); //get line number
        select.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return select;

    }


    @Override
    public ColumnDef visitColumn_def(SQLParser.Column_defContext ctx) {
        ColumnDef columnDef = new ColumnDef();
        if (ctx.column_name() != null)
            columnDef.setColumnName(visitAny_name(ctx.column_name().any_name()));

        if (ctx.type_name() != null && !ctx.type_name().isEmpty()) {
            String typeNames;
            typeNames = visitAny_name(ctx.type_name().name().any_name());

            Type tp = typeExist(typeNames);
            if (tp == null) {
                undefined(typeNames + " Type");
                columnDef.setType(new Type(Type.NULL_CONST));
            } else {
                columnDef.setType(tp);
            }
        }

        columnDef.setLine(ctx.getStart().getLine()); //get line number
        columnDef.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return columnDef;
    }


    @Override
    public Expression visitExpr(SQLParser.ExprContext ctx) {
        Expression expression;

        if (ctx.expr().size() == 1) {
            if (ctx.unary_operator() != null && ctx.expr() != null) {
               return visitUnary_expression(ctx);
            }
            if (ctx.OPEN_PAR() != null && ctx.CLOSE_PAR() != null) {
                return visitExpr(ctx.expr(0));
            }
            if (ctx.not_in_expr() != null) {
                return visitNot_in_expr(ctx);
            }
        }
        if (ctx.function_expr() != null) {
            return visitFunction_expr(ctx.function_expr());
        }

        if (ctx.expr() != null && ctx.expr().size() == 2) {
            return (visitBinaryExpression(ctx));
        }

        expression = new Expression();

        if (ctx.literal_value() != null)
            expression.setLiteral_value(visitLiteral_value(ctx.literal_value()));
        if (ctx.column_name() != null) {
            expression.setColumn_name(visitAny_name(ctx.column_name().any_name()));
            if(ctx.table_name()!=null)
                expression.setTable_name(visitAny_name(ctx.table_name().any_name()));
            if(ctx.database_name()!=null)
                expression.setDataBaseName(visitAny_name(ctx.database_name().any_name()));
        }


        if (ctx.select_stmt() != null){
            if (ctx.K_EXISTS() != null) {
                expression.setExists(true);
                if (ctx.K_NOT() != null)
                    expression.setNot(true);
            }
            expression.setSelectStmt(visitSelect_stmt(ctx.select_stmt()));
        }
        System.out.println();
        expression.setLine(ctx.getStart().getLine()); //get line number
        expression.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return expression;

    }

    private BinaryExpression visitBinaryExpression(SQLParser.ExprContext ctx) {

        System.out.println("visit binaryExpression  ");
        BinaryExpression binaryExpression = new BinaryExpression();
        if (ctx.NOT_EQ2() != null) {
            binaryExpression.setOperator(ctx.NOT_EQ2().getSymbol().getText());
        }
        if (ctx.NOT_EQ1() != null) {
            binaryExpression.setOperator(ctx.NOT_EQ1().getSymbol().getText());
        }
        if (ctx.GT() != null) {
            binaryExpression.setOperator(ctx.GT().getSymbol().getText());
        }
        if (ctx.GT2() != null) {
            binaryExpression.setOperator(ctx.GT2().getSymbol().getText());
        }
        if (ctx.LT() != null) {
            binaryExpression.setOperator(ctx.LT().getSymbol().getText());
        }
        if (ctx.LT2() != null) {
            binaryExpression.setOperator(ctx.LT2().getSymbol().getText());
        }
        if (ctx.GT_EQ() != null) {
            binaryExpression.setOperator(ctx.GT_EQ().getSymbol().getText());
        }
        if (ctx.LT_EQ() != null) {
            binaryExpression.setOperator(ctx.LT_EQ().getSymbol().getText());
        }
        if (ctx.EQ() != null) {
            binaryExpression.setOperator(ctx.EQ().getSymbol().getText());
        }

        if (ctx.ASSIGN() != null) {
            binaryExpression.setOperator(ctx.ASSIGN().getSymbol().getText());
        }
        if (ctx.PLUS() != null) {
            binaryExpression.setOperator(ctx.PLUS().getSymbol().getText());
        }
        if (ctx.MINUS() != null) {
            binaryExpression.setOperator(ctx.MINUS().getSymbol().getText());
        }
        if (ctx.MOD() != null) {
            binaryExpression.setOperator(ctx.MOD().getSymbol().getText());
        }
        if (ctx.STAR() != null) {
            binaryExpression.setOperator(ctx.STAR().getSymbol().getText());
        }


        if (ctx.PIPE() != null) {
            binaryExpression.setOperator(ctx.PIPE().getSymbol().getText());
        }
        if (ctx.PIPE2() != null) {
            binaryExpression.setOperator(ctx.PIPE2().getSymbol().getText());
        }
        if (ctx.AMP() != null) {
            binaryExpression.setOperator(ctx.AMP().getSymbol().getText());
        }
        if (ctx.K_AND() != null) {
            binaryExpression.setOperator(ctx.K_AND().getSymbol().getText());
        }
        if (ctx.K_OR() != null) {
            binaryExpression.setOperator(ctx.K_OR().getSymbol().getText());
        }

        if (ctx.K_IS() != null) {
            if (ctx.K_NOT() != null)
                binaryExpression.setOperator(ctx.K_IS().getSymbol().getText());
            else
                binaryExpression.setOperator(ctx.K_IS().getSymbol().getText() + " " + ctx.K_NOT().getSymbol().getText());
        }
        if (ctx.K_LIKE() != null) {
            binaryExpression.setOperator(ctx.K_LIKE().getSymbol().getText());
        }
        if (ctx.K_EXISTS() != null) {
            binaryExpression.setOperator(ctx.K_EXISTS().getSymbol().getText());
        }
        if (ctx.K_IN() != null) {
            binaryExpression.setOperator(ctx.K_IN().getSymbol().getText());
        }
        if (ctx.K_MATCH() != null) {
            binaryExpression.setOperator(ctx.K_MATCH().getSymbol().getText());
        }
        if (ctx.K_GLOB() != null) {
            binaryExpression.setOperator(ctx.K_GLOB().getSymbol().getText());
        }
        if (ctx.K_REGEXP() != null) {
            binaryExpression.setOperator(ctx.K_REGEXP().getSymbol().getText());
        }

        if (ctx.expr(0) != null)
            binaryExpression.setLeftExpression(visitExpr(ctx.expr(0)));
        if (ctx.expr(1) != null)
            binaryExpression.setRightExpression(visitExpr(ctx.expr(1)));
        binaryExpression.setLine(ctx.getStart().getLine()); //get line number

        binaryExpression.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return binaryExpression;
    }

    private UnaryExpr visitUnary_expression(SQLParser.ExprContext ctx) {

        System.out.println("visit Unary Expression");
        UnaryExpr unaryExpr = new UnaryExpr();
        //unaryExpr.setType("Unary Expression");
        if (ctx.unary_operator().PLUS() != null)
            unaryExpr.setPlus(true);
        if (ctx.unary_operator().MINUS() != null)
            unaryExpr.setMinus(true);
        if (ctx.unary_operator().K_NOT() != null)
            unaryExpr.setNot(true);
        if (ctx.unary_operator().TILDE() != null)
            unaryExpr.setTilde(true);
        if (ctx.expr() != null)
            unaryExpr.setExpr(visitExpr(ctx.expr(0)));
        unaryExpr.setOperator();
        unaryExpr.setLine(ctx.getStart().getLine()); //get line number
        unaryExpr.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return unaryExpr;

    }


    public ExpressionIn visitNot_in_expr(SQLParser.ExprContext ctx) {
        ExpressionIn expressionIn = new ExpressionIn();
        if(ctx.expr(0)!=null)
            expressionIn.setLeft(visitExpr(ctx.expr(0)));
        if (ctx.not_in_expr().K_NOT() != null)
            expressionIn.setNotIn(true);
        if (ctx.not_in_expr().select_stmt() != null)
            expressionIn.setSelectStmt(visitSelect_stmt(ctx.not_in_expr().select_stmt()));
        else if (ctx.not_in_expr().expr() != null) {
            ArrayList<Expression> expressions = new ArrayList<>();
            for (int e = 0; e < ctx.not_in_expr().expr().size(); e++) {
                expressions.add(visitExpr(ctx.not_in_expr().expr(e)));
            }
            expressionIn.setRightExpressions(expressions);
        }
        if (ctx.not_in_expr().table_name() != null) {
            expressionIn.setTableName(visitAny_name(ctx.not_in_expr().table_name().any_name()));
            if (ctx.not_in_expr().database_name() != null)
                expressionIn.setDatabaseName(visitAny_name(ctx.not_in_expr().database_name().any_name()));
        }
        expressionIn.setLine(ctx.not_in_expr().getStart().getLine()); //get line number
        expressionIn.setCol(ctx.not_in_expr().getStart().getCharPositionInLine()); // get col number
        return expressionIn;

    }

    @Override
    public FunctionExpression visitFunction_expr(SQLParser.Function_exprContext ctx) {
        FunctionExpression functionExpression = new FunctionExpression();
        functionExpression.setFunctionName(visitAny_name(ctx.function_name().any_name()));
        if (ctx.STAR() != null)
            functionExpression.setStar(true);
        else if (ctx.expr() != null) {
            if (ctx.K_DISTINCT() != null)
                functionExpression.setK_DISTINCT(true);
            List<Expression> expressions = new ArrayList<>();
            for (int e = 0; e < ctx.expr().size(); e++) {
                expressions.add(visitExpr(ctx.expr(e)));
            }
            functionExpression.setExpressions(expressions);
        }
        functionExpression.setLine(ctx.getStart().getLine()); //get line number
        functionExpression.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return functionExpression;
    }


    @Override
    public ForeignKeyClause visitForeign_key_clause(SQLParser.Foreign_key_clauseContext ctx) {
        ForeignKeyClause foreignKeyClause = new ForeignKeyClause();

        //K_REFERENCES
        if (ctx.database_name() != null)
            foreignKeyClause.setRefDatabaseName(visitAny_name(ctx.database_name().any_name()));
        foreignKeyClause.setRefForeignTable(visitAny_name(ctx.foreign_table().any_name()));
        if (!ctx.fk_target_column_name().isEmpty()) {
            List<String> target_column_names = new ArrayList<>();
            for (int ftc = 0; ftc < ctx.fk_target_column_name().size(); ftc++)
                target_column_names.add(visitAny_name(ctx.fk_target_column_name(ftc).name().any_name()));
            foreignKeyClause.setRefFk_target_column_nameList(target_column_names);
        }

        //match condition
        if (!ctx.K_MATCH().isEmpty() && !ctx.name().isEmpty()) {
            List<String> matchs = new ArrayList<>();
            for (int mn = 0; mn < ctx.name().size(); mn++) {
                matchs.add(visitAny_name(ctx.name(mn).any_name()));
            }
            foreignKeyClause.setK_MATCH_names(matchs);
        }
        //on condition          //update-delete actions
        if (!ctx.foreign_on_condition().isEmpty()) {
            List<ForeignOnCondition> foreignOnConditions = new ArrayList<>();
            for (int on = 0; on < ctx.foreign_on_condition().size(); on++) {
                foreignOnConditions.add(visitForeign_on_condition(ctx.foreign_on_condition(on)));
            }
            foreignKeyClause.setForeignOnCondition(foreignOnConditions);
        }

        // K_DEFERRABLE
        if (ctx.K_NOT() == null) {
            foreignKeyClause.setK_DEFERRABLE(true);
        }
        if (ctx.K_INITIALLY() != null) {
            if (ctx.K_IMMEDIATE() != null)
                foreignKeyClause.setINITIALLY_IMMEDIATE(true);
            else if (ctx.K_DEFERRED() != null)
                foreignKeyClause.setINITIALLY_DEFERRED(true);
        }
        if (ctx.K_ENABLE() != null)
            foreignKeyClause.setENABLEed(true);
        foreignKeyClause.setLine(ctx.getStart().getLine()); //get line number
        foreignKeyClause.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return foreignKeyClause;
    }

    @Override
    public ForeignOnCondition visitForeign_on_condition(SQLParser.Foreign_on_conditionContext ctx) {
        ForeignOnCondition foreignOnCondition = new ForeignOnCondition();
        if (ctx.K_UPDATE() != null)
            foreignOnCondition.setK_UPDATE(true);
        else if (ctx.K_DELETE() != null)
            foreignOnCondition.setK_DELETE(true);


        if (ctx.K_SET() != null && ctx.K_DEFAULT() != null)
            foreignOnCondition.setAction(Action.SET_DEFAULT);
        else if (ctx.K_CASCADE() != null)
            foreignOnCondition.setAction(Action.CASCADE);
        else if (ctx.K_SET() != null && ctx.K_NULL() != null)
            foreignOnCondition.setAction(Action.SET_NULL);
        else if (ctx.K_NO() != null && ctx.K_ACTION() != null)
            foreignOnCondition.setAction(Action.NO_ACTION);
        else if (ctx.K_RESTRICT() != null)
            foreignOnCondition.setAction(Action.RESTRICT);
        foreignOnCondition.setLine(ctx.getStart().getLine()); //get line number
        foreignOnCondition.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return foreignOnCondition;
    }

    @Override
    public Indexed_column visitIndexed_column(SQLParser.Indexed_columnContext ctx) {
        Indexed_column indexed_column = new Indexed_column();
        indexed_column.setColumn_name(visitAny_name(ctx.column_name().any_name()));
        if (ctx.K_COLLATE() != null) {
            indexed_column.setK_COLLATE(true);
            indexed_column.setCollation_name(visitAny_name(ctx.collation_name().any_name()));
        }
        if (ctx.K_DESC() != null)
            indexed_column.setK_DESC(true);
        indexed_column.setLine(ctx.getStart().getLine()); //get line number
        indexed_column.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return indexed_column;
    }

    @Override
    public TableConstraint visitTable_constraint(SQLParser.Table_constraintContext ctx) {
        TableConstraint table_constraint = new TableConstraint();
        if (ctx.K_CONSTRAINT() != null) {
            table_constraint.setK_CONSTRAINT(true);
            table_constraint.setConstraintName(visitAny_name(ctx.name().any_name()));
        }

        if (ctx.table_constraint_primary_key() != null) {
            table_constraint.setK_PRIMARY(true);
            table_constraint.setIndexed_columns(visitTable_constraint_primary_key(ctx.table_constraint_primary_key()));
        } else if (ctx.table_constraint_unique() != null) {
            table_constraint.setK_UNIQUE(true);
            if (ctx.table_constraint_unique().name() != null)
                table_constraint.setUnique_keyName(visitAny_name(ctx.table_constraint_unique().name().any_name()));
            table_constraint.setIndexed_columns(visitTable_constraint_unique(ctx.table_constraint_unique()));
        } else if (ctx.table_constraint_key() != null) {
            table_constraint.setTable_constraint_key(true);
            if (ctx.table_constraint_key().name() != null)
                table_constraint.setUnique_keyName(visitAny_name(ctx.table_constraint_key().name().any_name()));
            table_constraint.setIndexed_columns(visitTable_constraint_key(ctx.table_constraint_key()));
        } else if (ctx.K_CHECK() != null) {
            table_constraint.setK_CHECK_expr(visitExpr(ctx.expr()));
        } else if (ctx.table_constraint_foreign_key() != null) {
            table_constraint.setForeignKeyConstraint(visitTable_constraint_foreign_key(ctx.table_constraint_foreign_key()));
        }
        table_constraint.setLine(ctx.getStart().getLine()); //get line number
        table_constraint.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return table_constraint;
    }

    @Override
    public ForeignKeyConstraint visitTable_constraint_foreign_key(SQLParser.Table_constraint_foreign_keyContext ctx) {
        ForeignKeyConstraint foreignKeyConstraint = new ForeignKeyConstraint();
        if (!ctx.fk_origin_column_name().isEmpty()) {
            List<String> foreignCulomnsName = new ArrayList<>();
            for (int fcm = 0; fcm < ctx.fk_origin_column_name().size(); fcm++) {
                foreignCulomnsName.add(visitAny_name(ctx.fk_origin_column_name(fcm).column_name().any_name()));
            }
            foreignKeyConstraint.setForeignCulomnsName(foreignCulomnsName);
        }
        if (ctx.foreign_key_clause() != null)
            foreignKeyConstraint.setForeignKeyClause(visitForeign_key_clause(ctx.foreign_key_clause()));
        foreignKeyConstraint.setLine(ctx.getStart().getLine()); //get line number
        foreignKeyConstraint.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return foreignKeyConstraint;
    }


    @Override
    public List<Indexed_column> visitTable_constraint_primary_key(SQLParser.Table_constraint_primary_keyContext ctx) {
        List<Indexed_column> indexed_columns = new ArrayList<>();
        if (!ctx.indexed_column().isEmpty()) {
            for (int ic = 0; ic < ctx.indexed_column().size(); ic++) {
                indexed_columns.add(visitIndexed_column(ctx.indexed_column(ic)));
            }
        }
        return indexed_columns;
    }

    @Override
    public List<Indexed_column> visitTable_constraint_unique(SQLParser.Table_constraint_uniqueContext ctx) {

        List<Indexed_column> indexed_columns = new ArrayList<>();
        if (!ctx.indexed_column().isEmpty()) {
            for (int ic = 0; ic < ctx.indexed_column().size(); ic++) {
                indexed_columns.add(visitIndexed_column(ctx.indexed_column(ic)));
            }
        }
        return indexed_columns;
    }

    @Override
    public List<Indexed_column> visitTable_constraint_key(SQLParser.Table_constraint_keyContext ctx) {
        List<Indexed_column> indexed_columns = new ArrayList<>();
        if (!ctx.indexed_column().isEmpty()) {
            for (int ic = 0; ic < ctx.indexed_column().size(); ic++) {
                indexed_columns.add(visitIndexed_column(ctx.indexed_column(ic)));
            }
        }
        return indexed_columns;
    }


    @Override
    public QualifiedTableName visitQualified_table_name(SQLParser.Qualified_table_nameContext ctx) {
        QualifiedTableName qualifiedTableName = new QualifiedTableName();
        if (ctx.table_name() != null)
            qualifiedTableName.setTable_name(visitAny_name(ctx.table_name().any_name()));
        if (ctx.database_name() != null)
            qualifiedTableName.setDatabase_name(visitAny_name(ctx.database_name().any_name()));
        if (ctx.K_INDEXED() != null) {
            qualifiedTableName.setK_INDEXED(true);
            qualifiedTableName.setIndex_name(visitAny_name(ctx.index_name().any_name()));
        }
        qualifiedTableName.setLine(ctx.table_name().getStart().getLine()); //get line number
        qualifiedTableName.setCol(ctx.table_name().getStart().getCharPositionInLine()); // get col number
        return qualifiedTableName;
    }

    @Override
    public OrderingTerm visitOrdering_term(SQLParser.Ordering_termContext ctx) {
        OrderingTerm orderingTerm = new OrderingTerm();
        if (ctx.K_DESC() != null)
            orderingTerm.setDESC(true);
        if (ctx.expr() != null)
            orderingTerm.setExpression(visitExpr(ctx.expr()));
        orderingTerm.setLine(ctx.getStart().getLine()); //get line number
        orderingTerm.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return orderingTerm;
    }

    @Override
    public Result_column visitResult_column(SQLParser.Result_columnContext ctx) {
        Result_column result_column = new Result_column();
        if (ctx.STAR() != null)
            result_column.setStar(true);
        if (ctx.table_name() != null)
            result_column.setTable_name(visitAny_name(ctx.table_name().any_name()));
        if (ctx.expr() != null)
            result_column.setExpression(visitExpr(ctx.expr()));
        if (ctx.column_alias() != null)
            result_column.setColumn_alias(visitColumn_alias(ctx.column_alias()));
        result_column.setLine(ctx.getStart().getLine()); //get line number
        result_column.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return result_column;
    }

    @Override
    public Table_or_subquery visitTable_or_subquery(SQLParser.Table_or_subqueryContext ctx) {

        System.out.println("visitTable_or_subquery");
        Table_or_subquery table_or_subquery = new Table_or_subquery();
        if (ctx.table_alias() != null)
            table_or_subquery.setTable_alias(visitTable_alias(ctx.table_alias()));
        if (ctx.table_name() != null) {
            QualifiedTableName q = new QualifiedTableName();
            q.setTable_name(visitAny_name(ctx.table_name().any_name()));
            if (ctx.database_name() != null)
                q.setDatabase_name(visitAny_name(ctx.database_name().any_name()));
            if (ctx.K_INDEXED() != null) {
                q.setK_INDEXED(true);
                q.setIndex_name(visitAny_name(ctx.index_name().any_name()));
            }
            q.setLine(ctx.table_name().getStart().getLine());
            q.setCol(ctx.table_name().getStart().getCharPositionInLine());
            table_or_subquery.setQualifiedTableName(q);
        } else if (!ctx.table_or_subquery().isEmpty()) {
            List<Table_or_subquery> table_or_subqueries = new ArrayList<>();
            Map<String,Type> cols=new HashMap<>();
            for (int ts = 0; ts < ctx.table_or_subquery().size(); ts++) {
                table_or_subqueries.add(visitTable_or_subquery(ctx.table_or_subquery(ts)));
            }
            table_or_subquery.setTable_or_subqueries(table_or_subqueries);
        } else if (ctx.select_stmt() != null) {
            table_or_subquery.setSellectStmt(visitSelect_stmt(ctx.select_stmt()));
        }
        table_or_subquery.setLine(ctx.getStart().getLine()); //get line number
        table_or_subquery.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return table_or_subquery;
    }

    @Override
    public Join_clause visitJoin_clause(SQLParser.Join_clauseContext ctx) {
        Join_clause join_stmt = new Join_clause();
        join_stmt.setTable_or_subquery(visitTable_or_subquery(ctx.table_or_subquery(0)));
        List<JoinElement> joinElements = new ArrayList<>();
        JoinElement joinElement;
        for (int i = 0; i < ctx.table_or_subquery().size() - 1; i++) {
            joinElement = new JoinElement();
            joinElement.setLeft_Table_or_subquery(visitTable_or_subquery(ctx.table_or_subquery(i + 1)));
            if (ctx.join_operator(i).COMMA() != null)
                joinElement.setOperator("COMMA");
            else if (ctx.join_operator(i).K_LEFT() != null)
                if (ctx.join_operator(i).K_OUTER() != null)
                    joinElement.setOperator("Left Outer");
                else
                    joinElement.setOperator("Left");
            else if (ctx.join_operator(i).K_INNER() != null)
                joinElement.setOperator("Inner");
            if (ctx.join_constraint(i).expr() != null)
                joinElement.setOnExpression(visitExpr(ctx.join_constraint(i).expr()));
            joinElements.add(joinElement);
        }
        join_stmt.setJoinRightElements(joinElements);
        join_stmt.setLine(ctx.getStart().getLine()); //get line number
        join_stmt.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return join_stmt;
    }

    @Override
    public SelectCoreOrValue visitSelect_core(SQLParser.Select_coreContext ctx) {
        SelectCoreOrValue select_core = new SelectCoreOrValue();
        if (ctx.K_VALUES() != null) {
            select_core.setK_VALUE(true);
            if (!ctx.expr().isEmpty())
                for (int ve = 0; ve < ctx.expr().size(); ve++)
                    select_core.addvaluesExpression(visitExpr(ctx.expr(ve)));
        } else {
            if(ctx.result_column()!=null){
                ArrayList<Result_column> result_columns=new ArrayList<>();
                for(int rc=0;rc<ctx.result_column().size();rc++){
                    result_columns.add(visitResult_column(ctx.result_column(rc)));
                }
                select_core.setResult_columns(result_columns);
            }
            if (ctx.K_DISTINCT() != null)
                select_core.setK_DISTINCT(true);
            else if (ctx.K_ALL() != null)
                select_core.setK_ALL(true);

            if (!ctx.table_or_subquery().isEmpty()){
                for (int ts = 0; ts < ctx.table_or_subquery().size(); ts++){
                    select_core.addTableOrSub__(visitTable_or_subquery(ctx.table_or_subquery(ts)));
                }
            }
            if(ctx.join_clause()!=null){
                select_core.setJoin_clause(visitJoin_clause(ctx.join_clause()));
            }



            int groupbyS = 0, groupbyE = ctx.expr().size() - 1;
            if (ctx.K_WHERE() != null) {
                select_core.setWhereExpression(visitExpr(ctx.expr(0)));
                groupbyS = 1;
            }
            if (ctx.K_GROUP() != null) {
                if (ctx.K_HAVING() != null) {
                    select_core.setHavingExpression(visitExpr(ctx.expr(groupbyE)));
                    groupbyE -= 1;
                }
                List<Expression> groupByExpressions = new ArrayList<>();
                for (int ge = groupbyS; ge < groupbyE; ge++) {
                    groupByExpressions.add(visitExpr(ctx.expr(ge)));
                }
                select_core.setGroupByExpressions(groupByExpressions);
            }


        }
        select_core.setLine(ctx.getStart().getLine()); //get line number
        select_core.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return select_core;
    }

    @Override
    public SignedNumber visitSigned_number(SQLParser.Signed_numberContext ctx) {

        SignedNumber signedNumber = new SignedNumber();
        if (ctx.PLUS() != null)
            signedNumber.setPlus(true);
        else if (ctx.MINUS() != null)
            signedNumber.setMinus(true);
        else if (ctx.STAR() != null)
            signedNumber.setStar(true);
        signedNumber.setOperator();

        if (ctx.NUMERIC_LITERAL() != null)
            signedNumber.setNumber(Double.parseDouble(ctx.NUMERIC_LITERAL().getText()));

        signedNumber.setValueType(Type.NUMBER_CONST);
        signedNumber.setLine(ctx.getStart().getLine()); //get line number
        signedNumber.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return signedNumber;
    }

    @Override
    public LiteralValue visitLiteral_value(SQLParser.Literal_valueContext ctx) {
        System.out.println("visit literal_value");
        LiteralValue literal_value = new LiteralValue();
        if (ctx.BLOB_LITERAL() != null)
            literal_value.setBLOB_LITERAL(ctx.BLOB_LITERAL().getText());
        if (ctx.K_CURRENT_DATE() != null)
            literal_value.setK_CURRENT_DATE(true);
        if (ctx.K_CURRENT_TIME() != null)
            literal_value.setK_CURRENT_TIME(true);
        if (ctx.K_CURRENT_TIMESTAMP() != null)
            literal_value.setK_CURRENT_TIMESTAMP(true);
        if (ctx.K_NULL() != null)
            literal_value.setK_NULL(true);
        if (ctx.NUMERIC_LITERAL() != null)
            literal_value.setNUMERICLITERAL(Double.parseDouble(ctx.NUMERIC_LITERAL().getText()));
        if (ctx.STRING_LITERAL() != null) {
            String s=ctx.STRING_LITERAL().getText();
            s=s.replace("\'","\"");
            literal_value.setSTRINGLITERA(s);

        }
        literal_value.setFinalValue();
        literal_value.setLine(ctx.getStart().getLine()); //get line number
        literal_value.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return literal_value;
    }


    @Override
    public String visitError_message(SQLParser.Error_messageContext ctx) {
        System.out.println("visitError_message");
        if (ctx.STRING_LITERAL() != null) {
            return ctx.STRING_LITERAL().getText();
        }


        return "";
    }

    @Override
    public String visitColumn_alias(SQLParser.Column_aliasContext ctx) {

        System.out.println("visitColumn_alias");
        String alias = "";
        if (ctx.IDENTIFIER() != null)
            alias = ctx.IDENTIFIER().getSymbol().getText();
        else if (ctx.VARNAME() != null)
            alias = ctx.VARNAME().getSymbol().getText();
        else if (ctx.STRING_LITERAL() != null)
            alias = ctx.STRING_LITERAL().getSymbol().getText();


        return alias;
    }

    @Override
    public String visitTable_alias(SQLParser.Table_aliasContext ctx) {
        System.out.println("visit Table_alias");
        String tableAlias = "";

        if (ctx.IDENTIFIER() != null)
            tableAlias = ctx.IDENTIFIER().getSymbol().getText();
        if (ctx.VARNAME() != null)
            tableAlias = ctx.VARNAME().getSymbol().getText();

        return tableAlias;
    }

    @Override
    public String visitAny_name(SQLParser.Any_nameContext ctx) {
//        System.out.println("visit Any_name");
        String name = "";

        if (ctx.IDENTIFIER() != null)
            name = ctx.IDENTIFIER().getSymbol().getText();
        if (ctx.VARNAME() != null)
            name = ctx.VARNAME().getSymbol().getText();
        if (ctx.STRING_LITERAL() != null)
            name = ctx.STRING_LITERAL().getSymbol().getText();
        if (ctx.keyword() != null) {
            name = ctx.keyword().getText();
        }
        return name;
    }

//////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////JAVA///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////


    public Function_call visitFunction_call(SQLParser.Function_callContext ctx, Scope currentScope) {
        Function_call function_call = new Function_call();
/*        Variable v;
        if (ctx.variable() != null) {
            v = visitVariable(ctx.variable(), currentScope);
            if (!v.isExist())
                undefined(v.getVar_name());
            //else
            function_call.setVariable(v);
        }*/

        String funName = ctx.java_function_name().java_var_name().VARNAME().getSymbol().getText();
         function_call.setFunction_name(funName);
            if (ctx.argument_list() != null) {
                List argument_list = new ArrayList();
                for (int i = 0; i < ctx.argument_list().argument().size(); i++) {
                    argument_list.add(visitArgument(ctx.argument_list().argument(i), currentScope));
                }
                function_call.setArguments(argument_list);
            }


        function_call.setLine(ctx.getStart().getLine()); //get line number
        function_call.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return function_call;
    }

/*    public ArrayList<Value> visitArgument_list(SQLParser.Argument_listContext ctx, Scope currentScope) {
        ArrayList<Value> argument_list = new ArrayList<>();
        for (int i = 0; i < ctx.argument().size(); i++) {
            argument_list.add(visitArgument(ctx.argument(i), currentScope));
        }
//  argument_list.setLine(ctx.getStart().getLine()); //get line number
//    argument_list.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return argument_list;
    }*/

    public Value visitArgument(SQLParser.ArgumentContext ctx, Scope currentScope) {
        Value argument;
        if (ctx.decliration() != null) {
            argument = visitDecliration(ctx.decliration(), currentScope);

            argument.setLine(ctx.getStart().getLine()); //get line number
            argument.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        } else argument = null;
//        else{
//            argument.setFunction_def(visitFunction_def(ctx.function_def()));
//        }
        return argument;
    }

    public Function_def visitFunction_def(SQLParser.Function_defContext ctx, Scope currentScope) {

        String fuName = ctx.java_function_name().java_var_name().VARNAME().getSymbol().getText();
        Function_def function_def = new Function_def();
        if (!funExist("fun_" + fuName)) {
            function_def.setFunction_name(fuName);
            Scope funScope = new Scope();
            funScope.setId("fun_" + fuName);
            funScope.setParent(currentScope);
            Main.symbolTable.addScope(funScope);
            function_def.setScope(funScope);

            if (ctx.parameter_list() != null) {
                function_def.setParameter_list(visitParameter_list(ctx.parameter_list(), funScope));
            }

            if (ctx.body() != null) {
                function_def.getBaseBody().setBody(visitBody(ctx.body(), funScope));
            }
            if (ctx.end_body() != null) {
                function_def.getBaseBody().setEndBody(visitEnd_body(ctx.end_body(), funScope));

            }
            function_def.setLine(ctx.getStart().getLine()); //get line number
            function_def.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        } else
            existBefore(fuName + " function",function_def.getLine(),function_def.getCol());
        return function_def;
    }

    public ArrayList<Variable> visitParameter_list(SQLParser.Parameter_listContext ctx, Scope currentScope) {
        ArrayList<Variable> parameter_list = new ArrayList<>();
        //  Scope currentScope = Main.symbolTable.getScopes().get(Main.symbolTable.getScopes().size() - 1 );
        if (ctx.parameter() != null) {
            for (int i = 0; i < ctx.parameter().size(); i++) {
                Variable parameter = visitVar_def(ctx.parameter(i).var_def(), currentScope);
                parameter.getMySymbol().setIsParam(true);
                parameter_list.add(parameter);
            }
        }
        if (ctx.default_var() != null) {
            for (int j = 0; j < ctx.default_var().size(); j++) {
                Variable default_var = visitDefault_var(ctx.default_var(j), currentScope);
                if (default_var.isExist()) {
                    existBefore(default_var.getVar_name(),ctx.default_var(j).getStart().getLine(),
                            ctx.default_var(j).getStart().getCharPositionInLine());
                } else {
                    default_var.getMySymbol().setIsParam(true);
                    parameter_list.add(default_var);
                }
            }
        }
        return parameter_list;
    }

//    public Variable visitParameter(SQLParser.ParameterContext ctx, Scope currentScope) {
//        Variable parameter = new Variable();
//        String parName = ctx.var_def().java_var_name().VARNAME().getSymbol().getText();
//        if (!symbolExist(parName, currentScope)) {
//            Symbol symbol = new Symbol();
//            symbol.setName(parName);
//            symbol.setIsParam(true);
//            symbol.setScope(currentScope);
//
//
//
//            parameter.setVar_name(parName);
//            parameter.setMySymbol(symbol);
//            parameter.setLine(ctx.getStart().getLine()); //get line number
//            parameter.setCol(ctx.getStart().getCharPositionInLine()); // get col number
//
//            symbol.setVariable(parameter);
//            currentScope.addSymbol(symbol.getName(), symbol);
//        }
//        else System.out.println("Error: " + parName + " is already exist");
//        return parameter;
//    }

    public Variable visitDefault_var(SQLParser.Default_varContext ctx, Scope currentScope) {
        String DefaultVarName = ctx.var_def().java_var_name().VARNAME().getSymbol().getText();
        Variable default_var = new Variable();
        default_var.setLine(ctx.getStart().getLine()); //get line number
        default_var.setCol(ctx.getStart().getCharPositionInLine()); // get col number


        Symbol symbol = symbolExist(DefaultVarName, currentScope);
        if (symbol == null) {
            symbol = new Symbol();
            symbol.setName(DefaultVarName);
            symbol.setScope(currentScope);

            default_var.setVar_name(DefaultVarName);
            default_var.setMySymbol(symbol);
            if (ctx.decliration() != null) {
                Value v = visitDecliration(ctx.decliration(), currentScope);
                default_var.setValue(v);
                default_var.setValueType(v.getValueType());
                symbol.setType(new Type(default_var.getValueType()));
            } else if (ctx.select_stmt() != null) {
                SelectStmt st = visitSelect_stmt(ctx.select_stmt());
                default_var.setSelectStmt(st);
                // TODO: 6/26/2020 type = select stmt return type.
                default_var.setValueType("selectStmt");
               // symbol.setType(st.getType());
            }

            symbol.setVariable(default_var);
            currentScope.addSymbol(DefaultVarName, symbol);
            System.out.println("new variable : " + DefaultVarName);
        } else {
            existBefore(DefaultVarName,ctx.var_def().java_var_name().getStart().getLine(),
                    ctx.var_def().java_var_name().getStart().getCharPositionInLine());
            default_var.setExist(true);
        }
        return default_var;
    }

    public BaseBody visitBase_body(SQLParser.Base_bodyContext ctx, Scope currentScope) {
        BaseBody baseBody = new BaseBody();

        if (ctx.java_stmt() != null) {
            baseBody.setJava_stmt(visitJava_stmt(ctx.java_stmt(), currentScope));
        }
        if (ctx.body() != null) {
            baseBody.setBody(visitBody(ctx.body(), currentScope));
        }
        if (ctx.end_body() != null) {
            baseBody.setEndBody(visitEnd_body(ctx.end_body(), currentScope));
        }
        return baseBody;
    }

    public EndBody visitEnd_body(SQLParser.End_bodyContext ctx, Scope currentScope) {
        EndBody endBody = new EndBody();
        if (ctx.K_BREAK() != null) {
            endBody.setK_break(true);
        }
        if (ctx.K_CONTINUE() != null) {
            endBody.setK_continue(true);
        }
        if (ctx.return_stmt() != null) {
            endBody.setReturn_stmt(visitReturn_stmt(ctx.return_stmt(), currentScope));
        }
        return endBody;
    }

    public Body visitBody(SQLParser.BodyContext ctx, Scope currentScope) {
        Body body = new Body();
        body.setScope(currentScope);
        if (!ctx.java_stmt().isEmpty()) {
            for (int i = 0; i < ctx.java_stmt().size(); i++) {
                body.add_java_stmt(visitJava_stmt(ctx.java_stmt(i), currentScope));
            }
        }
        if (ctx.body() != null) {
            Scope bodyScope = new Scope();
            bodyScope.setId("internalBod_." + Main.symbolTable.getStmtIdCounter());
            bodyScope.setParent(currentScope);
            body.setBody(visitBody(ctx.body(), bodyScope));
            Main.symbolTable.addScope(bodyScope);
        }
        body.setLine(ctx.getStart().getLine()); //get line number
        body.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return body;
    }

    public Java_stmt visitJava_stmt(SQLParser.Java_stmtContext ctx, Scope currentScope) {
        if (ctx.need_scol() != null) {
            return visitNeed_scol(ctx.need_scol(), currentScope);
        } else {
            return visitNot_need_scol(ctx.not_need_scol(), currentScope);
        }

    }

    public Java_stmt visitNot_need_scol(SQLParser.Not_need_scolContext ctx, Scope currentScope) {

        if (ctx.switch_stmt() != null) {
            return visitSwitch_stmt(ctx.switch_stmt(), currentScope);
        } else if (ctx.for_stmt() != null) {
            return visitFor_stmt(ctx.for_stmt(), currentScope);
        } else if (ctx.if_stmt() != null) {
            return visitIf_stmt(ctx.if_stmt(), currentScope);
        } else if (ctx.while_stmt() != null) {
            return visitWhile_stmt(ctx.while_stmt(), currentScope);
        }

        return null;
    }

    public Java_stmt visitNeed_scol(SQLParser.Need_scolContext ctx, Scope currentScope) {
        if (ctx.var_def() != null) {
            System.out.println("Warning at line: "+ctx.getStart().getLine()+" col: "+ctx.getStart().getCharPositionInLine()+
                    "\n un assigned variable");
            return visitVar_def(ctx.var_def(), currentScope);
        }
        if (ctx.default_var() != null) {
            return visitDefault_var(ctx.default_var(), currentScope);
        }
        if (ctx.var_deceleration() != null) {
            String varName = ctx.var_deceleration().variable().java_var_name().VARNAME().getSymbol().getText();
            Symbol symbol = symbolExist(varName, currentScope);
            if (symbol == null) {
                undefined(varName);
                return new Variable();
            } else

                return visitVar_deceleration(ctx.var_deceleration(), currentScope);

        }
        if (ctx.multivar_def() != null) {
            return visitMultivar_def(ctx.multivar_def(), currentScope);
        }
        if (ctx.function_call() != null) {
            return visitFunction_call(ctx.function_call(), currentScope);
        }
        if (ctx.print() != null) {
            return visitPrint(ctx.print(), currentScope);
        }
        if (ctx.incriment_stmt() != null)
            return visitIncriment_stmt(ctx.incriment_stmt(), currentScope);
        if (ctx.if_inline() != null) {
            return visitIf_inline(ctx.if_inline(), currentScope);
        }
        if (ctx.do_while_stmt() != null) {
            return visitDo_while_stmt(ctx.do_while_stmt(), currentScope);
        }
//        need_scol.setLine(ctx.getStart().getLine()); //get line number
//        need_scol.setCol(ctx.getStart().getCharPositionInLine()); // get col number
//        return need_scol;
        return null;
    }

    public Incriment_stmt visitIncriment_stmt(SQLParser.Incriment_stmtContext ctx, Scope currentScope) {

        Incriment_stmt incriment_stmt = new Incriment_stmt();

        incriment_stmt.setVariable(visitVariable(ctx.variable(), currentScope));
        if (!incriment_stmt.getVariable().isExist()) {
            undefined(incriment_stmt.getVariable().getVar_name());
        }
        if (!incriment_stmt.getVariable().getValueType().equals(Type.NUMBER_CONST)) {
            typeMustBe(incriment_stmt.getVariable().getVar_name(), Type.NUMBER_CONST);
        }

        incriment_stmt.setIncriment_op(visitIncriment_op(ctx.incriment_op()));

        incriment_stmt.setLine(ctx.getStart().getLine()); //get line number
        incriment_stmt.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return incriment_stmt;
    }

    public Print visitPrint(SQLParser.PrintContext ctx, Scope currentScope) {
        Print print = new Print();
        for (int i = 0; i < ctx.java_value().size(); i++) {
            print.add_Java_value(visitJava_value(ctx.java_value(i), currentScope));
        }
        print.setLine(ctx.getStart().getLine()); //get line number
        print.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return print;
    }

    public For_stmt visitFor_stmt(SQLParser.For_stmtContext ctx, Scope currentScope) {
        For_stmt for_stmt;


        if (ctx.for_loop() != null) {
            for_stmt = (visitFor_loop(ctx.for_loop(), currentScope));

        }
        else if (ctx.for_each_loop() != null) {
            for_stmt = (visitFor_each_loop(ctx.for_each_loop(), currentScope));
        }
        else
            for_stmt = new For_stmt();

        if (ctx.base_body() != null) {
            for_stmt.setBaseBody(visitBase_body(ctx.base_body(), for_stmt.getScope()));
        }

        for_stmt.setLine(ctx.getStart().getLine()); //get line number
        for_stmt.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return for_stmt;
    }

    public For_loop visitFor_loop(SQLParser.For_loopContext ctx, Scope currentScope) {

        Scope for_loopScope = new Scope();
        for_loopScope.setParent(currentScope);
        for_loopScope.setId("for_loopScope_" + Main.symbolTable.getStmtIdCounter());


        For_loop for_loop = new For_loop();
        for_loop.setLine(ctx.getStart().getLine()); //get line number
        for_loop.setCol(ctx.getStart().getCharPositionInLine()); // get col number


        Variable loopvar = visitVariable(ctx.variable(0), for_loopScope);

        if (ctx.K_VAR() != null) {
            if (loopvar.isExist()) {
                existBefore(loopvar.getVar_name(),ctx.variable(0).getStart().getLine(),
                        ctx.variable(0).getStart().getCharPositionInLine());
            }
        } else {
            if (!loopvar.isExist()) {
                undefined(loopvar.getVar_name());
            }
        }
        for_loop.setLoopVar(loopvar);

        if(!ctx.num_exp().isEmpty() && ctx.num_exp(0)!=null){
            for_loop.setStartValue(visitNum_exp(ctx.num_exp(0), for_loopScope));
            for_loop.getLoopVar().setValueType(Type.NUMBER_CONST);
            for_loop.getLoopVar().getMySymbol().setType(new Type(Type.NUMBER_CONST));
        }
        if(ctx.variable(1).java_var_name()!=null){
            String v2=ctx.variable(1).java_var_name().VARNAME().getSymbol().getText();
            if(!v2.equals(loopvar.getVar_name())){
                undefined(v2);
            }
        }
//        for_loop.setLoopCondition(visitVariable(ctx.variable(1), for_loopScope));
        for_loop.setComparison_op(visitComparison_op(ctx.comparison_op()));
        for_loop.setEndValue(visitNum_exp(ctx.num_exp(1), for_loopScope));

        if (ctx.variable(2) != null && ctx.assign_op().num_op() != null && ctx.num_exp(2)!=null) {
//            for_loop.setLoopCounter(visitVariable(ctx.variable(2), for_loopScope));
            String v2=ctx.variable(2).java_var_name().VARNAME().getSymbol().getText();
            if(!v2.equals(loopvar.getVar_name())){
                undefined(v2);
            }
            for_loop.setNum_op(visitNum_op(ctx.assign_op().num_op()));
            for_loop.setUpdateValue(visitNum_exp(ctx.num_exp(2), for_loopScope));
        }


        Main.symbolTable.addScope(for_loopScope);
        for_loop.setScope(for_loopScope);

        return for_loop;
    }

    public For_each visitFor_each_loop(SQLParser.For_each_loopContext ctx, Scope currentScope) {
        For_each for_each = new For_each();
        if (ctx.K_VAR() != null) {
            for_each.setK_var(true);
        }
        Scope for_eachScope = new Scope();
        for_eachScope.setParent(currentScope);
        for_eachScope.setId("for_eachScope_" + Main.symbolTable.getStmtIdCounter());
        if (ctx.java_var_name() != null) {
            String varName = ctx.java_var_name().VARNAME().getSymbol().getText();
            Variable firstvar = new Variable();
            Symbol symbol = symbolExist(varName, for_eachScope);
            if (symbol == null) {
                firstvar.setVar_name(varName);
                symbol = new Symbol();
                symbol.setName(varName);
                symbol.setIsParam(false);
                symbol.setScope(for_eachScope);
                symbol.setVariable(firstvar);
                for_eachScope.addSymbol(symbol.getName(), symbol);
                firstvar.setMySymbol(symbol);
            } else existBefore(varName,ctx.java_var_name().getStart().getLine(),
                    ctx.java_var_name().getStart().getCharPositionInLine());

            for_each.setFirstVar(firstvar);
        }
        if (ctx.variable() != null) {
            for_each.setVariable(visitVariable(ctx.variable(), for_eachScope));
        }
        for_each.setLine(ctx.getStart().getLine()); //get line number
        for_each.setCol(ctx.getStart().getCharPositionInLine()); // get col number

        Main.symbolTable.addScope(for_eachScope);
        for_each.setScope(for_eachScope);
        return for_each;
    }

    public Switch_stmt visitSwitch_stmt(SQLParser.Switch_stmtContext ctx, Scope currentScope) {
        Switch_stmt switch_stmt = new Switch_stmt();
        Scope switchScope = new Scope();
        switchScope.setParent(currentScope);
        switchScope.setId("switch_" + Main.symbolTable.getStmtIdCounter());

        if (ctx.variable() != null) {
            switch_stmt.setVariable(visitVariable(ctx.variable(), switchScope));
        }
        if (ctx.num_exp() != null) {
            switch_stmt.setNum_exp(visitNum_exp(ctx.num_exp(), switchScope));
        }
        if (ctx.string_val() != null) {
            switch_stmt.setString_val(visitString_val(ctx.string_val(), switchScope));
        }

        for (int i = 0; i < ctx.case_stmt().size(); i++) {
            switch_stmt.add_case_stmt(visitCase_stmt(ctx.case_stmt(i), switchScope));
        }
        if (ctx.default_stmt() != null) {
            switch_stmt.setDefault_stmt(visitDefault_stmt(ctx.default_stmt(), switchScope));
        }
        switch_stmt.setScope(switchScope);
        Main.symbolTable.addScope(switchScope);
        switch_stmt.setLine(ctx.getStart().getLine()); //get line number
        switch_stmt.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return switch_stmt;
    }

    public Case_stmt visitCase_stmt(SQLParser.Case_stmtContext ctx, Scope currentScope) {
        Case_stmt case_stmt = new Case_stmt();
        Scope caseScope = new Scope();
        caseScope.setParent(currentScope);
        caseScope.setId("case_" + Main.symbolTable.getStmtIdCounter());

        if (ctx.num_exp() != null) {
            case_stmt.setNum_exp(visitNum_exp(ctx.num_exp(), caseScope));
            case_stmt.setValueType(Type.NUMBER_CONST);
        }
        if (ctx.string_val() != null) {
            case_stmt.setString_val(visitString_val(ctx.string_val(), caseScope));
            case_stmt.setValueType(Type.STRING_CONST);
        }
        if (ctx.body() != null) {
            case_stmt.getBaseBody().setBody(visitBody(ctx.body(), caseScope));
        }
        if (ctx.end_body() != null) {
            case_stmt.getBaseBody().setEndBody(visitEnd_body(ctx.end_body(), caseScope));
        }

        Main.symbolTable.addScope(caseScope);
        case_stmt.setLine(ctx.getStart().getLine()); //get line number
        case_stmt.setCol(ctx.getStart().getCharPositionInLine()); //get col number
        return case_stmt;

    }

    public BaseBody visitDefault_stmt(SQLParser.Default_stmtContext ctx, Scope currentScope) {
        BaseBody default_stmt = new BaseBody();
        Scope defaultScope = new Scope();
        defaultScope.setParent(currentScope);
        defaultScope.setId("default_" + Main.symbolTable.getStmtIdCounter());
        if (ctx.body() != null) {
            default_stmt.setBody(visitBody(ctx.body(), defaultScope));
        }
        if (ctx.end_body() != null) {
            default_stmt.setEndBody((visitEnd_body(ctx.end_body(), defaultScope)));
        }


        Main.symbolTable.addScope(defaultScope);
        default_stmt.setLine(ctx.getStart().getLine()); //get line number
        default_stmt.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return default_stmt;
    }

    public If_stmt visitIf_stmt(SQLParser.If_stmtContext ctx, Scope currentScope) {
        If_stmt if_stmt = new If_stmt();
        Scope ifScope = new Scope();
        ifScope.setParent(currentScope);
        ifScope.setId("If_" + Main.symbolTable.getStmtIdCounter());

        if_stmt.setCondition_exp(visitCondition_exp(ctx.condition_exp(), ifScope));

        if (ctx.base_body() != null) {
            if_stmt.setBaseBody(visitBase_body(ctx.base_body(), ifScope));
            Main.symbolTable.addScope(ifScope);
            if_stmt.setScope(ifScope);
        }

        if (!ctx.else_if_stmt().isEmpty()) {
            ArrayList<ElseIf> elseIfs = new ArrayList<>();
            for (int i = 0; i < ctx.else_if_stmt().size(); i++) {
                elseIfs.add(visitElse_if_stmt(ctx.else_if_stmt(i), currentScope));
            }
            if_stmt.setElseIfStmts(elseIfs);
        }
        if (ctx.else_stmt() != null) {
            Scope elseScope = new Scope();
            elseScope.setParent(currentScope);
            elseScope.setId("ELSE_" + Main.symbolTable.getStmtIdCounter());
            if_stmt.setElseStmt(visitBase_body(ctx.else_stmt().base_body(), elseScope));
            Main.symbolTable.addScope(elseScope);
            if_stmt.setElseScope(elseScope);
        }
        if_stmt.setLine(ctx.getStart().getLine()); //get line number
        if_stmt.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return if_stmt;
    }

    public ElseIf visitElse_if_stmt(SQLParser.Else_if_stmtContext ctx, Scope currentScope) {
        ElseIf elseIf_stmt = new ElseIf();
        Scope elseIfScope = new Scope();
        elseIfScope.setParent(currentScope);
        elseIfScope.setId("ElseIf_" + Main.symbolTable.getStmtIdCounter());

        elseIf_stmt.setCondition_exp(visitCondition_exp(ctx.condition_exp(), elseIfScope));

        if (ctx.base_body() != null) {
            elseIf_stmt.setBaseBody(visitBase_body(ctx.base_body(), elseIfScope));
        }

        Main.symbolTable.addScope(elseIfScope);
        elseIf_stmt.setScope(elseIfScope);
        elseIf_stmt.setLine(ctx.getStart().getLine()); //get line number
        elseIf_stmt.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return elseIf_stmt;
    }

/*
    public BaseBody visitElse_stmt(SQLParser.Else_stmtContext ctx, Scope currentScope) {
        ElseIf an_else = new ElseIf();
        Scope ElseScope = new Scope();
        ElseScope.setParent(currentScope);
        ElseScope.setId("ElseIf." + Main.symbolTable.getStmtIdCounter() + currentScope.getId());

        if (ctx.body() != null) {
            an_else.setBody(visitBody(ctx.body(), currentScope));
        }
        if (ctx.K_BREAK() != null) {
            an_else.setK_break(true);
        }
        if (ctx.K_CONTINUE() != null) {
            an_else.setK_continue(true);
        }
        if (ctx.return_stmt() != null) {
            an_else.setReturn_stmt(visitReturn_stmt(ctx.return_stmt(), currentScope));
        }
        if (ctx.java_stmt() != null) {
            an_else.setJava_stmt(visitJava_stmt(ctx.java_stmt(), currentScope));
        }
        an_else.setLine(ctx.getStart().getLine()); //get line number
        an_else.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return an_else;
    }
*/

    public If_inline visitIf_inline(SQLParser.If_inlineContext ctx, Scope currentScope) {
        if (ctx.if_inline() != null) {
            return visitIf_inline(ctx.if_inline(), currentScope);
        }
        If_inline if_inline = new If_inline();
        if (ctx.condition_exp() != null) {
            if_inline.setConditionExp(visitCondition_exp(ctx.condition_exp(), currentScope));
        }
        if (ctx.ifinlin_side().size() == 2) {
            if_inline.setLeftValue(visitIfinlin_side(ctx.ifinlin_side(0), currentScope));
            if_inline.setRightValue(visitIfinlin_side(ctx.ifinlin_side(1), currentScope));
            if_inline.setValueType(if_inline.getLeftValue().getValueType());
        }
        if_inline.setLine(ctx.getStart().getLine()); //get line number
        if_inline.setCol(ctx.getStart().getCharPositionInLine()); // get col number

        return if_inline;
    }

    private Value visitIfinlin_side(SQLParser.Ifinlin_sideContext ctx, Scope currentScope) {
        if (ctx.ifinlin_side() != null)
            return visitIfinlin_side(ctx.ifinlin_side(), currentScope);
        if (ctx.java_value() != null)
            return visitJava_value(ctx.java_value(), currentScope);
        else if (ctx.if_inline() != null)
            return visitIf_inline(ctx.if_inline(), currentScope);
        else if (ctx.condition_exp() != null)
            return visitCondition_exp(ctx.condition_exp(), currentScope);
        else if (ctx.num_exp() != null)
            return visitNum_exp(ctx.num_exp(), currentScope);
        else if (ctx.var_deceleration() != null)
            return visitVar_deceleration(ctx.var_deceleration(), currentScope);
        return null;
    }

    public While_stmt visitWhile_stmt(SQLParser.While_stmtContext ctx, Scope currentScope) {
        While_stmt while_stmt = new While_stmt();
        Scope WhileScope = new Scope();
        WhileScope.setParent(currentScope);
        WhileScope.setId("While_" + Main.symbolTable.getStmtIdCounter());

        while_stmt.setCondition_exp(visitCondition_exp(ctx.condition_exp(), WhileScope));
        if (ctx.base_body() != null) {
            while_stmt.setBody(visitBase_body(ctx.base_body(), WhileScope));
        }

        Main.symbolTable.addScope(WhileScope);
        while_stmt.setScope(WhileScope);
        while_stmt.setLine(ctx.getStart().getLine()); //get line number
        while_stmt.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return while_stmt;
    }

    public Do_while_stmt visitDo_while_stmt(SQLParser.Do_while_stmtContext ctx, Scope currentScope) {
        Do_while_stmt do_while_stmt = new Do_while_stmt();
        Scope doWhileScope = new Scope();
        doWhileScope.setParent(currentScope);
        doWhileScope.setId("doWhile_" + Main.symbolTable.getStmtIdCounter());

        do_while_stmt.setCondition_exp(visitCondition_exp(ctx.condition_exp(), doWhileScope));
        if (ctx.base_body() != null) {
            do_while_stmt.setBody(visitBase_body(ctx.base_body(), doWhileScope));
        }
        do_while_stmt.setLine(ctx.getStart().getLine()); //get line number
        do_while_stmt.setCol(ctx.getStart().getCharPositionInLine()); // get col number

        Main.symbolTable.addScope(doWhileScope);
        do_while_stmt.setScope(doWhileScope);
        return do_while_stmt;
    }

    public Return_stmt visitReturn_stmt(SQLParser.Return_stmtContext ctx, Scope currentScope) {
        Return_stmt return_stmt = new Return_stmt();
        if (ctx.java_value() != null) {
            return_stmt.setValue(visitJava_value(ctx.java_value(), currentScope));
        }
        if (ctx.if_inline() != null) {
            return_stmt.setValue(visitIf_inline(ctx.if_inline(), currentScope));
        }
        if (ctx.num_exp() != null) {
            return_stmt.setValue(visitNum_exp(ctx.num_exp(), currentScope));
        }
        if (ctx.condition_exp() != null) {
            return_stmt.setValue(visitCondition_exp(ctx.condition_exp(), currentScope));
        }
        return_stmt.setLine(ctx.getStart().getLine()); //get line number
        return_stmt.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return return_stmt;
    }

    public Condition_exp visitCondition_exp(SQLParser.Condition_expContext ctx, Scope currentScope) {
        if (ctx.OPEN_PAR() != null && ctx.CLOSE_PAR() != null) {
            if (ctx.condition_exp() != null && ctx.condition_exp(0) != null) {
                return visitCondition_exp(ctx.condition_exp(0), currentScope);
            }
        }
        Condition_exp condition_exp = new Condition_exp();
        condition_exp.setValueType(Type.BOOLEAN_CONST);
        if (ctx.function_call() != null) {
            Function_call fc = visitFunction_call(ctx.function_call(), currentScope);
            condition_exp.setFunction_call(fc);
        } else if (ctx.boolean_val() != null) {
            if (ctx.boolean_val().K_TRUE() != null)
                condition_exp.setBoolean_val(true);
            else if (ctx.boolean_val().K_FALSE() != null)
                condition_exp.setBoolean_val(false);
        } else if (ctx.boolean_var() != null) {
            Variable bv = visitVariable(ctx.boolean_var().variable(), currentScope);
            condition_exp.setBoolean_var(bv);
        } else if (ctx.condition_exp().size() == 2) {
            if (ctx.AMP() != null) {
                condition_exp.setOperator(ctx.AMP().getSymbol().getText());
            } else if (ctx.AMP2() != null) {
                condition_exp.setOperator(ctx.AMP2().getSymbol().getText());
            } else if (ctx.PIPE() != null) {
                condition_exp.setOperator(ctx.PIPE().getSymbol().getText());
            } else if (ctx.PIPE2() != null) {
                condition_exp.setOperator(ctx.PIPE2().getSymbol().getText());
            }
            condition_exp.setLeft_cond(visitCondition_exp(ctx.condition_exp(0), currentScope));
            condition_exp.setRight_cond(visitCondition_exp(ctx.condition_exp(1), currentScope));
        }

        if (ctx.EQ() != null) {
            condition_exp.setOperator(ctx.EQ().getSymbol().getText());
        }
        if (ctx.NOT_EQ1() != null) {
            condition_exp.setOperator(ctx.NOT_EQ1().getSymbol().getText());
        }
        if (ctx.NOT_EQ2() != null) {
            condition_exp.setOperator(ctx.NOT_EQ2().getSymbol().getText());
        }

        if (ctx.num_exp().size() == 2) {
            if (ctx.GT() != null) {
                condition_exp.setOperator(ctx.GT().getSymbol().getText());
            } else if (ctx.GT_EQ() != null) {
                condition_exp.setOperator(ctx.GT_EQ().getSymbol().getText());
            } else if (ctx.LT() != null) {
                condition_exp.setOperator(ctx.LT().getSymbol().getText());
            } else if (ctx.LT_EQ() != null) {
                condition_exp.setOperator(ctx.LT_EQ().getSymbol().getText());
            }
            condition_exp.setLeftSide(visitNum_exp(ctx.num_exp(0), currentScope));
            condition_exp.setRightSide(visitNum_exp(ctx.num_exp(1), currentScope));
        } else if (ctx.java_value().size() == 2) {
            Java_value JVLeft = visitJava_value(ctx.java_value(0), currentScope),
                    JVRight = visitJava_value(ctx.java_value(1), currentScope);


            condition_exp.setLeftSide(JVLeft);
            condition_exp.setRightSide(JVRight);

        }
        if (ctx.java_value().size() == 1 && ctx.num_exp().size() == 1) {
            Java_value jv = visitJava_value(ctx.java_value(0), currentScope);
            if (ctx.children.indexOf(ctx.java_value(0)) == 0) {
                condition_exp.setLeftSide(jv);
                condition_exp.setRightSide(visitNum_exp(ctx.num_exp(0), currentScope));
            } else if (ctx.children.indexOf(ctx.java_value(0)) == 3) {
                condition_exp.setRightSide(visitJava_value(ctx.java_value(0), currentScope));
                condition_exp.setLeftSide(visitNum_exp(ctx.num_exp(0), currentScope));
            }

        }

        condition_exp.setLine(ctx.getStart().getLine()); //get line number
        condition_exp.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return condition_exp;
    }

    // TODO: 6/26/2020 make visit numExpression return value my be its better i do not know
    public Num_exp visitNum_exp(SQLParser.Num_expContext ctx, Scope currentScope) {

        if (ctx.OPEN_PAR() != null && ctx.CLOSE_PAR() != null)
            return visitNum_exp(ctx.num_exp(0), currentScope);

        Num_exp num_exp = new Num_exp();
        num_exp.setValueType(Type.NUMBER_CONST);

        if (ctx.num_val() != null) {
            num_exp.setValue(visitNum_val(ctx.num_val()));
        } else if (ctx.function_call() != null) {
            num_exp.setValue(visitFunction_call(ctx.function_call(), currentScope));
        } else if (ctx.num_exp().size() == 2) {
            if (ctx.STAR() != null) {
                num_exp.setOperator(ctx.STAR().getSymbol().getText());
            } else if (ctx.PLUS() != null) {
                num_exp.setOperator(ctx.PLUS().getSymbol().getText());
            } else if (ctx.MINUS() != null) {
                num_exp.setOperator(ctx.MINUS().getSymbol().getText());
            } else if (ctx.DIV() != null) {
                num_exp.setOperator(ctx.DIV().getSymbol().getText());
            } else if (ctx.MOD() != null) {
                num_exp.setOperator(ctx.MOD().getSymbol().getText());
            }
            num_exp.setLeft(visitNum_exp(ctx.num_exp(0), currentScope));
            num_exp.setRight(visitNum_exp(ctx.num_exp(1), currentScope));
        }
        if (ctx.num_var() != null) {
            Variable numv = visitVariable(ctx.num_var().variable(), currentScope);

            if (!numv.isExist())
                undefined(numv.getVar_name());
            else if (numv.getValueType().equals(Type.NUMBER_CONST)) {
                num_exp.setNumVar(numv);
                if (ctx.incriment_op() != null) {
                    num_exp.setIncriment_op(visitIncriment_op(ctx.incriment_op()));
                }
                //postfix increment
                if (ctx.children.indexOf(ctx.num_var()) == 0) {
                    num_exp.setPreIncOp(false);
                }
                //prefix increment
                if (ctx.children.indexOf(ctx.num_var()) == 1) {
                    num_exp.setPreIncOp(true);
                }
            } else
                typeMustBe(numv.getVar_name(), Type.NUMBER_CONST);
        }
        num_exp.setLine(ctx.getStart().getLine()); //get line number
        num_exp.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return num_exp;
    }

    public Multivar_def visitMultivar_def(SQLParser.Multivar_defContext ctx, Scope currentScope) {
        Multivar_def multivar_def = new Multivar_def();
        Variable v;
        for (int i = 0; i < ctx.multivar_def_sub().size(); i++) {
            String varName = ctx.multivar_def_sub(i).java_var_name().VARNAME().getSymbol().getText();
            Symbol symbol = symbolExist(varName, currentScope);
            if (symbol == null) {

                symbol = new Symbol();
                v = new Variable();

                v.setVar_name(varName);
                symbol.setName(varName);

                if (ctx.multivar_def_sub(i).decliration() != null) {
                    Value val = visitDecliration(ctx.multivar_def_sub(i).decliration(), currentScope);
                    v.setValue(val);
                    v.setValueType(val.getValueType());
                    symbol.setType(new Type(val.getValueType()));
                } else if (ctx.multivar_def_sub(i).select_stmt() != null) {
                    SelectStmt st = visitSelect_stmt(ctx.multivar_def_sub(i).select_stmt());
                    v.setSelectStmt(st);
                    v.setValueType("selectStmt");
                    symbol.setType(new Type("selectStmt"));
                }
                v.setMySymbol(symbol);
                symbol.setVariable(v);

                symbol.setScope(currentScope);
                currentScope.addSymbol(varName, symbol);

                multivar_def.add_var_name(v);
            } else {
                v = symbol.getVariable();
                v.setExist(true);
                existBefore(varName,ctx.multivar_def_sub(i).java_var_name().getStart().getLine(),
                        ctx.multivar_def_sub(i).java_var_name().getStart().getCharPositionInLine());
            }
        }
        multivar_def.setLine(ctx.getStart().getLine()); //get line number
        multivar_def.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return multivar_def;
    }

    public Variable visitVar_def(SQLParser.Var_defContext ctx, Scope currentScope) {

        Variable var_def = new Variable();
        var_def.setLine(ctx.getStart().getLine()); //get line number
        var_def.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        String varName = "Error No Name";
        if (ctx.java_var_name() != null) {
            varName = ctx.java_var_name().VARNAME().getSymbol().getText();
            var_def.setVar_name(varName);
        }
        Symbol symbol = symbolExist(varName, currentScope);
        if (symbol == null) {
            symbol = new Symbol();
            symbol.setName(varName);
            symbol.setIsParam(false);
            symbol.setScope(currentScope);
            symbol.setVariable(var_def);
            currentScope.addSymbol(symbol.getName(), symbol);

            var_def.setMySymbol(symbol);
            System.out.println("new variable : " + varName);
        } else {
            var_def = symbol.getVariable();
            var_def.setExist(true);
            existBefore(varName,ctx.java_var_name().getStart().getLine(),
                    ctx.java_var_name().getStart().getCharPositionInLine());
        }
        return var_def;
    }

    public Variable visitVar_deceleration(SQLParser.Var_decelerationContext ctx, Scope currentScope) {

        Variable var_deceleration;

        var_deceleration = (visitVariable(ctx.variable(), currentScope));
        if (!var_deceleration.isExist()) {
            undefined(var_deceleration.getVar_name());
        }
        if (ctx.decliration() != null) {
            Value v = visitDecliration(ctx.decliration(), currentScope);

            //if variable is already declared
            if (var_deceleration.getValueType().equals(Type.NULL_CONST)) {
                Type tp = new Type(v.getValueType());
                var_deceleration.setValue(v);
                var_deceleration.setValueType(tp.getName());
                var_deceleration.getMySymbol().setType(tp);
            } else if (v.getValueType().equals(var_deceleration.getValueType())) {
                var_deceleration.setValue(v);
            } else {
                typeMustBe(var_deceleration.getVar_name(),
                        var_deceleration.getValueType());
            }
        }
        //if v is the first value;
        else if (ctx.select_stmt() != null) {
            SelectStmt st = visitSelect_stmt(ctx.select_stmt());
            if (var_deceleration.getValueType().equals(Type.NULL_CONST)) {
                Type tp = new Type("selectStmt");
                var_deceleration.setSelectStmt(st);
                var_deceleration.setValueType(tp.getName());
                var_deceleration.getMySymbol().setType(tp);
            } else if (var_deceleration.getValueType().equals("selectStmt")) {
                var_deceleration.setSelectStmt(st);
            } else typeMustBe(var_deceleration.getVar_name(),
                    var_deceleration.getValueType());
        }
        var_deceleration.setLine(ctx.getStart().getLine()); //get line number
        var_deceleration.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return var_deceleration;
    }

    public Value visitDecliration(SQLParser.DeclirationContext ctx, Scope currentScope) {
        Value decliration;
        if (ctx.num_exp() != null) {
            decliration = visitNum_exp(ctx.num_exp(), currentScope);
        } else if (ctx.condition_exp() != null) {
            decliration = visitCondition_exp(ctx.condition_exp(), currentScope);
        } else if (ctx.if_inline() != null) {
            decliration = visitIf_inline(ctx.if_inline(), currentScope);
            // TODO: 6/21/2020 set declaration value = if inline value
            //      decliration.setValueType();
        } else if (ctx.java_value() != null) {
            decliration = visitJava_value(ctx.java_value(), currentScope);
        }
/*        else if(ctx.select_stmt()!=null){
            decliration =visitSelect_stmt(ctx.select_stmt())
        }*/
        else
            return null;

        decliration.setLine(ctx.getStart().getLine()); //get line number
        decliration.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return decliration;
    }

    public Java_value visitJava_value(SQLParser.Java_valueContext ctx, Scope currentScope) {
        Java_value java_value = new Java_value();
        Function_call function_call;

        if (ctx.num_val() != null) {
            java_value.setNum_val(visitNum_val(ctx.num_val()));
            java_value.setValueType(Type.NUMBER_CONST);
            currentScope.setReturnType(java_value.getValueType());

        } else if (ctx.boolean_val() != null) {
            if (ctx.boolean_val().K_TRUE() != null)
                java_value.setBooleanVal(new Boolean(true));
            else if (ctx.boolean_val().K_FALSE() != null)
                java_value.setBooleanVal(new Boolean(false));
            java_value.setValueType(Type.BOOLEAN_CONST);
            currentScope.setReturnType(java_value.getValueType());

        } else if (ctx.string_val() != null) {
            java_value.setStringVal(visitString_val(ctx.string_val(), currentScope));
            java_value.setValueType(Type.STRING_CONST);
            currentScope.setReturnType(java_value.getValueType());

        } else if (ctx.function_call() != null) {
            // TODO: 6/21/2020   type = function return type
            function_call = visitFunction_call(ctx.function_call(), currentScope);
            String s=funExist_fun("fun_"+function_call.getFunction_name()).getReturnType();
            java_value.setValueType(s);
           // java_value.equals()unction_call;
        } else if (ctx.variable() != null) {
            // type is already stored in variable
            Variable v = visitVariable(ctx.variable(), currentScope);
            if (!v.isExist()) {
                undefined(v.getVar_name());
                //return v; else return null
            }
            return (v);
        } else if (ctx.K_NULL() != null) {
            java_value.setValueType(Type.NULL_CONST);
        }

        java_value.setLine(ctx.getStart().getLine()); //get line number
        java_value.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return java_value;
    }

    public Variable visitVariable(SQLParser.VariableContext ctx, Scope currentScope) {
        if (ctx.OPEN_PAR() != null && ctx.CLOSE_PAR() != null) {
            return visitVariable(ctx.variable(0), currentScope);
        }

        String varName;
        Symbol symbol;
        Variable variable = new Variable();
        if (ctx.java_var_name() != null) {
            varName = ctx.java_var_name().VARNAME().getSymbol().getText();
        } else if (ctx.variable().size() == 2) {
            variable.setFather(visitVariable(ctx.variable(0), currentScope));
            varName = ctx.variable(1).java_var_name().VARNAME().getSymbol().getText();
        } else return null;

        symbol = symbolExist(varName, currentScope);
        if (symbol == null) {
            symbol = new Symbol();
            symbol.setName(varName);
            symbol.setScope(currentScope);


            variable.setVar_name(varName);
            variable.setMySymbol(symbol);

            symbol.setVariable(variable);
            currentScope.addSymbol(symbol.getName(), symbol);

        } else {
            variable = symbol.getVariable();
            variable.setExist(true);
        }

        variable.setLine(ctx.getStart().getLine()); //get line number
        variable.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return variable;
    }

    @Override
    public Incriment_op visitIncriment_op(SQLParser.Incriment_opContext ctx) {
        Incriment_op incriment_op = new Incriment_op();
        if (ctx.MINUS2() != null) {
            incriment_op.setMinusminus(true);
        }
        if (ctx.PLUS2() != null) {
            incriment_op.setPlusplus(true);
        }
        incriment_op.setLine(ctx.getStart().getLine()); //get line number
        incriment_op.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return incriment_op;
    }

    public String visitString_val(SQLParser.String_valContext ctx, Scope currentScope) {
        StringBuilder string_val = new StringBuilder();
        if (ctx.STRING_LITERAL() != null) {
            string_val.append(ctx.STRING_LITERAL().getSymbol().getText());
        }
        if (ctx.IDENTIFIER() != null) {
            string_val.append(ctx.IDENTIFIER().getSymbol().getText());
        }
        if (!ctx.java_value().isEmpty()) {
            for (int i = 0; i < ctx.java_value().size(); i++) {
                string_val.append(visitJava_value(ctx.java_value(i), currentScope));
            }
        }
//"
        return string_val.toString();
    }

    @Override
    public SignedNumber visitNum_val(SQLParser.Num_valContext ctx) {

        SignedNumber num_val = new SignedNumber();

        if (ctx.NUMERIC_LITERAL() != null) {
            num_val.setNumber(Double.parseDouble(ctx.NUMERIC_LITERAL().getSymbol().getText()));
            num_val.setValueType(Type.NUMBER_CONST);
        } else if (ctx.signed_number() != null) {
            num_val = (visitSigned_number(ctx.signed_number()));
        }

        num_val.setLine(ctx.getStart().getLine()); //get line number
        num_val.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return num_val;
    }


    @Override
    public Num_op visitNum_op(SQLParser.Num_opContext ctx) {
        Num_op num_op = new Num_op();
        if (ctx.PLUS() != null) {
            num_op.setPlus(true);
        }
        if (ctx.MINUS() != null) {
            num_op.setMinus(true);
        }
        if (ctx.MOD() != null) {
            num_op.setMod(true);
        }
        if (ctx.STAR() != null) {
            num_op.setStar(true);
        }
        if (ctx.DIV() != null) {
            num_op.setDiv(true);
        }
        num_op.setLine(ctx.getStart().getLine()); //get line number
        num_op.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return num_op;
    }

    @Override
    public Comparison_op visitComparison_op(SQLParser.Comparison_opContext ctx) {
        Comparison_op comparison_op = new Comparison_op();
        if (ctx.LT() != null) {
            comparison_op.setLt(true);
        }
        if (ctx.LT_EQ() != null) {
            comparison_op.setLt_eq(true);
        }
        if (ctx.GT() != null) {
            comparison_op.setGt(true);
        }
        if (ctx.GT_EQ() != null) {
            comparison_op.setGt_eq(true);
        }
        comparison_op.setLine(ctx.getStart().getLine()); //get line number
        comparison_op.setCol(ctx.getStart().getCharPositionInLine()); // get col number
        return comparison_op;
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public Type visitCreate_type_stmt(SQLParser.Create_type_stmtContext ctx) {
        Type t;
        String typeName = "";
        if (ctx.table_name() != null) {
            typeName = visitAny_name(ctx.table_name().any_name());
        }
        t = typeExist(typeName);

        if (t != null) {
            existBefore(typeName + " Type",t.getLine(),t.getCol());
//            t.print(2);
        } else {
            t = new Type(typeName);
        }
        if (ctx.column_def() != null && !ctx.column_def().isEmpty()) {
            for (int cd = 0; cd < ctx.column_def().size(); cd++) {
                ColumnDef columnDef = visitColumn_def(ctx.column_def(cd));
                if (columnExist(columnDef.getColumnName(), t)) {
                    existBefore(columnDef.getColumnName() + " column",
                            ctx.column_def(cd).column_name().getStart().getLine(),
                            ctx.column_def(cd).column_name().getStart().getCharPositionInLine());
                } else {
                    t.addColumn(columnDef.getColumnName(), columnDef.getType());
                }
            }
        }
        t.setLine(ctx.getStart().getLine());
        t.setCol(ctx.getStart().getCharPositionInLine());
        Main.symbolTable.addType(t);
        return t;
    }

    @Override
    public CreateAggFunction visitCreate_aggregation_function(SQLParser.Create_aggregation_functionContext ctx) {
        AggregationFunction function = new AggregationFunction();
        if (ctx.function_name() != null) {
            function.setAggregationFunctionName(visitAny_name(ctx.function_name().any_name()));
        }
        if (ctx.jarPath() != null) {
           function.setJarPath(visitString_val(ctx.jarPath().string_val(),null));
        }
        if (ctx.className() != null) {
            function.setClassName(visitAny_name(ctx.className().any_name()));
        }
        if (ctx.methodName() != null) {
            function.setMethodName(visitAny_name(ctx.methodName().any_name()));
        }
        if (ctx.returnType() != null) {
            function.setReturnType(visitAny_name(ctx.returnType().any_name()));
        }
        if (ctx.parameters() != null) {
            function.setParams(visitParameters(ctx.parameters()));
        }
        CreateAggFunction cf=new CreateAggFunction();
        cf.setFunction(function);
        Main.symbolTable.addAggFun(function);
        cf.setLine(ctx.getStart().getLine());
        cf.setCol(ctx.getStart().getCharPositionInLine());
        return cf;
    }

    @Override
    public ArrayList<Type> visitParameters(SQLParser.ParametersContext ctx) {
        ArrayList<Type> parameters=new ArrayList<>();

        if(ctx.any_name()!=null){
            for(int p=0;p<ctx.any_name().size();p++){
                String tName=visitAny_name(ctx.any_name(p));
                Type t=typeExist(tName);
                if(t!=null){
                    parameters.add(t);
                }
                else{
                    parameters.add(new Type(tName));
                }
            }
        }
        return parameters;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////


    private Symbol symbolExist(String symName, Scope scope) {
        Symbol s = scope.getSymbolMap().get(symName);
        if (s != null)
            return s;
        else if (scope.getParent() != null)
            return symbolExist(symName, scope.getParent());
        else
            return null;
    }

    private Type typeExist(String typeName) {
        ArrayList<Type> typs = Main.symbolTable.getDeclaredTypes();
        for (Type t : typs) {
            if (t.getName().equals(typeName))
                return t;
        }
        return null;
    }

    private boolean columnExist(String name, Type t) {
        return t.getColumns().get(name) != null;
    }

    /// Error prints;
    private void undefined(String varName) {
        System.out.println("Error: (" + varName + ") is not defined before");
    }


    private void notEqualType() {
        System.out.println("Error: right side type must be the same of the left side type");
    }

    private void existBefore(String varName,int line,int col) {
        System.out.println("Error :(" + varName + ") is already exist. \n at line: "+line+" and column : "+col+".");
    }

    private void typeMustBe(String varName, String type) {
        System.out.println("Error (" + varName + ") type must be " + type);
    }
    private boolean funExist(String funId) {
        ArrayList<Scope> scopes = Main.symbolTable.getScopes();
        for (Scope s : scopes) {
            if (s.getId().equals(funId))
                return true;
        }
        return false;
    }

    private Scope funExist_fun(String funId) {
        ArrayList<Scope> scopes = Main.symbolTable.getScopes();
        for (Scope s : scopes) {
            if (s.getId().equals(funId))
                return s;
        }
        return null;
    }

}
