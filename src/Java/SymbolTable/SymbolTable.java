package Java.SymbolTable;

import java.util.ArrayList;


public class SymbolTable {

    private int stmtIdCounter=0;
    private ArrayList<Scope> scopes = new ArrayList<Scope>();

    private ArrayList<Type> declaredTypes = new ArrayList<Type>();
    private ArrayList<AggregationFunction> declaredAggregationFunction = new ArrayList<AggregationFunction>();


    public ArrayList<Scope> getScopes() {
        return scopes;
    }

    public void setScopes(ArrayList<Scope> scopes) {
        this.scopes = scopes;
    }

    public ArrayList<Type> getDeclaredTypes() {
        return declaredTypes;
    }

    public int getStmtIdCounter() {
        stmtIdCounter++;
        return stmtIdCounter;
    }

    public void setStmtIdCounter(int stmtIdCounter) {
        this.stmtIdCounter = stmtIdCounter;
    }

    public void setDeclaredTypes(ArrayList<Type> declaredTypes) {
        this.declaredTypes = declaredTypes;
    }

    public ArrayList<AggregationFunction> getDeclaredAggregationFunction() {
        return declaredAggregationFunction;
    }
    public void setDeclaredAggregationFunction(ArrayList<AggregationFunction> declaredAggregationFunction) {
        this.declaredAggregationFunction = declaredAggregationFunction;
    }


    public void addScope(Scope scope){
        this.scopes.add(scope);
    }
    public void addType(Type type){
        this.declaredTypes.add(type);
    }

    public void addAggFun(AggregationFunction function){
        this.declaredAggregationFunction.add(function);
    }


    public  void print(){
        System.out.println("\n Symbol Table:");
        System.out.println("\tTypes :");
        for(Type t:declaredTypes){
            t.print(2);
        }
        System.out.println("\tstmtIdCounter = "+stmtIdCounter);
        for (Scope s:scopes) {
            s.print(1);
            System.out.println();
        }
        System.out.println("\t aggregation functions");
        for (AggregationFunction ag : declaredAggregationFunction){
            ag.print(2);
        }
    }

}
