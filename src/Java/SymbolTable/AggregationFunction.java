package Java.SymbolTable;

import java.util.ArrayList;
public  class AggregationFunction extends Type {
    protected String AggregationFunctionName;
    protected String JarPath;
    protected String ClassName;
    protected String MethodName;
    protected String returnType;

    private ArrayList<Type> params = new ArrayList<>();


    public String getAggregationFunctionName() {
        return AggregationFunctionName;
    }

    public void setAggregationFunctionName(String aggregationFunctionName) {
        AggregationFunctionName = aggregationFunctionName;
    }

    public String getJarPath() {
        return JarPath;
    }

    public void setJarPath(String jarPath) {
        JarPath = jarPath;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public String getMethodName() {
        return MethodName;
    }

    public void setMethodName(String methodName) {
        MethodName = methodName;
    }

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    public ArrayList<Type> getParams() {
        return params;
    }

    public void setParams(ArrayList<Type> params) {
        this.params = params;
    }
    private String spaces(int i) {
        String s = "";
        for (int x = 0; x < i; x++)
            s += "\t";
        return s;
    }
    @Override
    public void print(int i) {
        System.out.println(spaces(i)+"name : "+AggregationFunctionName);
        System.out.println(spaces(i)+"parameters : ");
        for (Type p:params) {
            System.out.println(spaces(i+1)+p.getName());
        }
    }
}