package Java.SymbolTable;

import Java.AST.Defenations_and_Values.Variable;

public class Symbol {

    private String name=null;
    private Type type=null;
    private Scope scope=null;
    private Variable variable=null;

    public Variable getVariable() {
        return variable;
    }

    public void setVariable(Variable variable) {
        this.variable = variable;
    }

    private boolean isParam;



    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public Scope getScope() {
        return scope;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public boolean getIsParam() {
        return isParam;
    }

    public void setIsParam(boolean param) {
        isParam = param;
    }
    private String spaces(int i) {
        String s = "";
        for (int x = 0; x < i; x++)
            s += "\t";
        return s;
    }
    public void print(int i) {
        System.out.println(spaces(i)+"Symbol "+name);
        System.out.println(spaces(i+2)+"is Param = "+isParam);
        if(type!=null){
            type.print(i+2);
        }
        else
            System.out.println(spaces(i+2)+"Type: undefined");
    }
}
