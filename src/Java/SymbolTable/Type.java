package Java.SymbolTable;

import Java.AST.Node;
import Java.AST.Sql.DDL.CreateTableStmt;

import java.util.HashMap;
import java.util.Map;

public class Type extends Node {



    private String name = null;
    private Map<String, Type> columns = new HashMap<String, Type>();
    private CreateTableStmt table=null;

    public static final String AGG_FUN_CONST ="aggregation_function" ;
    public static final String RESULT_COL_CONST ="result_col" ;
    public final static String NUMBER_CONST = "number";
    public final static String STRING_CONST = "string";
    public final static String BOOLEAN_CONST = "boolean";
    public final static String NULL_CONST = "null";


    public Type() {
        this.name = NULL_CONST;
        this.columns = new HashMap<String, Type>();
    }

    public Type(String name) {
        this.name = name;
    }


    public CreateTableStmt getTable() {
        return table;
    }

    public void setTable(CreateTableStmt table) {
        this.table = table;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Type> getColumns() {
        return columns;
    }

    public void addColumn(String s, Type t) {

//        if(t.isPrimary())
            columns.put(s,t);
//        else
//            columns.putAll(t.flat(s));
    }
    public  void addColumns(Map<String, Type>  cols){
        columns.putAll(cols);
    }

    public void setColumns(Map<String, Type> columns) {
        this.columns = columns;
    }

    private String spaces(int i) {
        String s = "";
        for (int x = 0; x < i; x++)
            s += "\t";
        return s;
    }

    public void print(int i) {
        System.out.println(spaces(i) + "Type name :" + name);
        if (!columns.isEmpty()) {
            System.out.println(spaces(i) + "columns :");
            for (String k : columns.keySet()) {
                System.out.println(spaces(i) + "\tcolumn name : " + k);
                columns.get(k).print(i + 1);
                System.out.println();
            }
        }
    }

    public Map<String, Type> flat(String colName) {
        Map<String, Type> flatCols = new HashMap<>();

        for (String k : this.columns.keySet()) {
            Type tt = this.columns.get(k);
            if (tt.isPrimary()) {
                flatCols.put(colName + "$" + k, tt);
            } else {
                flatCols.putAll(tt.flat(colName + "$" + k));
            }
        }

        return flatCols;

    }
    public Map<String, Type> flat() {
        Map<String, Type> flatCols = new HashMap<>();

        for (String k : this.columns.keySet()) {
            Type tt = this.columns.get(k);
            if (tt.isPrimary()) {
                flatCols.put(k, tt);
            } else {
                flatCols.putAll(tt.flat(k));
            }
        }

        return flatCols;

    }
    public Map<String, Type> flat0() {
        Map<String, Type> flatCols = new HashMap<>();
        for (String k : this.columns.keySet()) {
            Type tt = this.columns.get(k);
            if (tt.isPrimary()) {
                flatCols.put(k, tt);
            } else {
                flatCols.putAll(tt.flat0());
            }
        }

        return flatCols;

    }
    public boolean isPrimary() {
        if (this.columns.isEmpty())
            return true;
        return false;
    }

    public void addToName(String added_name) {
        this.name+=added_name;
    }

    public void addNamePrev(String var_name) {
        this.name= var_name+this.name;
    }
}
