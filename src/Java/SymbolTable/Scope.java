package Java.SymbolTable;

import java.util.LinkedHashMap;
import java.util.Map;

public class Scope {

    private String id;
    private Scope parent;
    private Map<String, Symbol> symbolMap = new LinkedHashMap<String, Symbol>();
    String returnType=Type.NULL_CONST;


    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    //    private String returnType=Type.NULL_CONST;
//
//    public String getReturnType() {
//        return returnType;
//    }
//
//    public void setReturnType(String returnType) {
//        this.returnType = returnType;
//    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Scope getParent() {
        return parent;
    }

    public void setParent(Scope parent) {
        this.parent = parent;
    }

    public Map<String, Symbol> getSymbolMap() {
        return symbolMap;
    }

    public void setSymbolMap(Map<String, Symbol> symbolMap) {
        this.symbolMap = symbolMap;
    }

    public void addSymbol(String name, Symbol symbol) {
        this.symbolMap.put(name, symbol);
    }

    private String spaces(int i) {
        String s = "";
        for (int x = 0; x < i; x++)
            s += "\t";
        return s;
    }

    public void print(int i) {
        String newId = id.substring(0,3);
        if(newId.contains("fun")) {
            System.out.println("--------------------------------------");
        }
        System.out.println(spaces(i) + "Scope:" + id);
        System.out.println(spaces(i)+"\tparent:"+parent.id);
        for (Symbol s:symbolMap.values() ) {
            s.print(i+1);
        }
    }
}
