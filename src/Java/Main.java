package Java;

import Java.AST.Parse;
import Java.AST.Visitor.BaseASTVisitor;
import Java.Base.BaseVisitor;
import Java.GenerationCode.ClassGenerator;
import Java.SymbolTable.SymbolTable;
import Java.SymbolTable.Type;
import generated.SQLLexer;
import generated.SQLParser;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;

import static org.antlr.v4.runtime.CharStreams.fromFileName;

public class Main {
    public static SymbolTable symbolTable = new SymbolTable();




    public static void main(String[] args) {
        symbolTable.addType(new Type(Type.NULL_CONST));
        symbolTable.addType(new Type(Type.NUMBER_CONST));
        symbolTable.addType(new Type(Type.BOOLEAN_CONST));
        symbolTable.addType(new Type(Type.STRING_CONST));
        try {
            String source = "samples\\samples.txt";
            CharStream cs = fromFileName(source);
            SQLLexer lexer = new SQLLexer(cs);
            CommonTokenStream token  = new CommonTokenStream(lexer);
            SQLParser parser = new SQLParser(token);
            ParseTree tree = parser.parse();

            System.out.println("AST Tree :");
            Parse p = (Parse) new BaseVisitor().visit(tree);
            p.accept(new BaseASTVisitor(),1);

            symbolTable.print();


            ClassGenerator classGenerator=new ClassGenerator();
            classGenerator.generateClass();

//            facultyClass.load();
//            for (facultyClass facultyClassObj:facultyClass.arraylist ) {
//                facultyClassObj.print();
//            }
//            FunctionExe functionExe=new
//                    FunctionExe();
//            System.out.println(functionExe.sum(new ArrayList<>(Arrays.asList(1.0, 2.0, 3.0, 12.0))));
//            System.out.println(functionExe.avg(new ArrayList<>(Arrays.asList(1.0, 2.0, 3.0, 12.0))));
        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (InvocationTargetException e) {
//            e.printStackTrace();
//        } catch (NoSuchMethodException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
