package Java.AST.Visitor;

import Java.AST.Defenations_and_Values.Variable;
import Java.AST.Expression.*;
import Java.AST.Expression.JavaExpr.Condition_exp;
import Java.AST.Expression.JavaExpr.Num_exp;
import Java.AST.Functions.Function_call;
import Java.AST.Functions.Function_def;
import Java.AST.Java_Stmt.Return_stmt;
import Java.AST.Parse;
import Java.AST.Sql.QueryStmt.JoinElement;
import Java.AST.Sql.QueryStmt.SelectCoreOrValue;
import Java.AST.Sql.QueryStmt.SelectStmt;
import Java.AST.Sql.Statement;
import Java.AST.Sql.Values.*;

public interface ASTVisitor {
    public void visit(Parse p);
    public void visit(Statement stmt);
    public void visit(SelectStmt SelectStmt);
    public void visit(SelectCoreOrValue selectCoreOrValue);
    public void visit(Table_or_subquery table_or_subquery);
    public void visit(QualifiedTableName qualifiedTableName);
    public void visit(Function_def function_def);
    public void visit(Function_call functionCall);
    public void visit(Variable variable);
    public void visit(Num_exp num_exp);
    public void visit(Condition_exp condition_exp);

    public void visit(Return_stmt return_stmt);
    public void visit(Expression expression);
    public void visit(FunctionExpression functionExpression);
    public void visit(BinaryExpression binaryExpression);
    public void visit(ExpressionIn expressionIn);
    public void visit(UnaryExpr unaryExpr);
    public void visit(LiteralValue literal_value);
    public  void visit(Result_column result_column);
    public void visit(SignedNumber signed_number);

    public void visit(JoinElement joinElement);



    /*    public void visit(Insert_stmt insert_stmt);
*/



}
