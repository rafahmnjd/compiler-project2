package Java.AST.Visitor;

import Java.AST.Defenations_and_Values.Variable;
import Java.AST.Expression.*;
import Java.AST.Expression.JavaExpr.Condition_exp;
import Java.AST.Expression.JavaExpr.Num_exp;
import Java.AST.Functions.Function_call;
import Java.AST.Functions.Function_def;
import Java.AST.Java_Stmt.Return_stmt;
import Java.AST.Node;
import Java.AST.Parse;
import Java.AST.Sql.QueryStmt.JoinElement;
import Java.AST.Sql.QueryStmt.SelectCoreOrValue;
import Java.AST.Sql.QueryStmt.SelectStmt;
import Java.AST.Sql.Statement;
import Java.AST.Sql.Values.*;
import Java.Main;
import Java.SymbolTable.AggregationFunction;
import Java.SymbolTable.Scope;
import Java.SymbolTable.Type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BaseASTVisitor implements ASTVisitor {


    @Override
    public void visit(Statement stmt) {
    }

    @Override
    public void visit(SelectStmt selectStmt) {

    }

    @Override
    public void visit(SelectCoreOrValue selectCoreOrValue) {
        Map<String, Type> cols = new HashMap<>();
        if (selectCoreOrValue.getResult_columns() != null) {
            for (int rc = 0; rc < selectCoreOrValue.getResult_columns().size(); rc++) {
                Result_column recol = selectCoreOrValue.getResult_columns().get(rc);
                if (recol.isStar()) {
//                    cols=selectCoreOrValue.getTables().flat();
                }
                if (recol.getExpression() != null) {
                    String colName = recol.getExpression().getColumn_name();
                }
            }
        }
        selectCoreOrValue.getType().setColumns(cols);
    }

    @Override
    public void visit(Table_or_subquery table_or_subquery) {


    }

    @Override
    public void visit(QualifiedTableName qualifiedTableName) {
        String tableName = qualifiedTableName.getTable_name();
        Type type = TypeExist(tableName);
        if (type == null) {
            undefined(tableName + " Table", qualifiedTableName);
        } else if (type.getTable()!=null) {
            System.out.println("warning at line:" + qualifiedTableName.getLine() + " and col:" + qualifiedTableName.getCol()
                    + "\n(" + tableName + ") is just a type but not a table so you cant have any data");
        }
        qualifiedTableName.setType(type);
    }

    @Override
    public void visit(Function_def function_def) {

    }

    @Override
    public void visit(Variable variable) {
        //System.out.println("parser visit variable");

    }

    @Override
    public void visit(Function_call functionCall) {
        Scope funs = funExist(functionCall.getFunction_name());
        if (funs == null) {
            undeclaredFun(functionCall);
        } else
            functionCall.setFunScope(funs);


    }


    /* @Override
     public void visit(Insert_stmt insert_stmt) {
         String s=insert_stmt.addSpase();
         s+="ast insertStmt :{";
         if(insert_stmt.getDataBaseName()!="")
             s=s.concat( insert_stmt.addSpase()+"\tDatabase: "+insert_stmt.getDataBaseName());

         s=s.concat(insert_stmt.addSpase()+"\tTable: "+insert_stmt.getTableName());
         for(int i=0;i<insert_stmt.getColumnsName().size();i++)
             s=s.concat(insert_stmt.addSpase()+"\tcolumn: "+insert_stmt.getColumnsName().get(i));
         s=s.concat(insert_stmt.addSpase()+"\tIs Default Value: "+insert_stmt.isDefaultValues());
         s=s+insert_stmt.addSpase()+"}\n";
         System.out.println(s);
     }

     @Override
     public void visit(FunctionExpression functionExpression) {
         String s="";
         s+=functionExpression.addSpase();
         s+="ast Function in expr:\n";
         if(functionExpression.getFunctionName()!=null)
             s+=functionExpression.addSpase()+"\t name="+functionExpression.getFunctionName()+"\n";
         if(!functionExpression.isK_DISTINCT())
             s+="\tvalue : * \n";
         else if (functionExpression.isK_DISTINCT())
             s+="\t is K_DISTINCT = true\n";
         System.out.println(s);
     }

     @Override
     public void visit(BinaryExpression binaryExpression) {
         StringBuilder s= new StringBuilder();
         for (int l=0;l<level;l++)
             s.append("  ");
         s.append("ast Binarry Expr{ \n");
         for (int l=0;l<level;l++)
             s.append("  ");
         s.append("\toperator: ").append(binaryExpression.getOperator()).append("\n");
         System.out.println(s);
     }

     @Override
     public void visit(ExpressionIn expressionIn) {
         StringBuilder s= new StringBuilder();
         for (int l=0;l<level;l++)
             s.append("  ");
         s.append("ast Expression in: \n");
         for (int l=0;l<level;l++)
             s.append("  ");
         if(expressionIn.isNotIn())
             s.append("Not ");
         s.append("IN:\n");
         for (int l=0;l<level;l++)
             s.append("  ");
         if(expressionIn.getDatabaseName()!=null)
             s.append("\t database name : ").append(expressionIn.getDatabaseName()).append('\n');
         if(expressionIn.getTableName()!=null)
             s.append("\t table name : ").append(expressionIn.getTableName()).append("\n");
         System.out.println(s);
     }

     @Override
     public void visit(UnaryExpr unaryExpr,int level) {
         String s="";
         for (int l=0;l<level;l++)
             s+=("  ");

         unaryExpr.setOperator();
         s+="ast UnaryExpr:";
         s+="\t unary operator : " +unaryExpr.getOperator()+"\n";
         System.out.println(s);
     }

     @Override
     public void visit(LiteralValue literal_value, int level) {

         String s="";
         for (int l=0;l<level;l++)
             s+=("  ");

         s+="ast literal value:\n";
         s+=literal_value.getFinalValue();
         System.out.println(s);
     }*/

    @Override
    public void visit(Result_column result_column) {


        if (result_column.getTable_name()!=null) {
            result_column.setType(TypeExist(result_column.getTable_name()));
            if (result_column.getType() == null) {
                undefined(result_column.getTable_name() + " Type", result_column);
            }
        }
    }

    /*
         @Override
         public void visit(SignedNumber signed_number, int level) {
             String s="";
             for (int l=0;l<level;l++)
                 s+=("  ");

             s+="ast signed number:\n";
             signed_number.setOperator();
             s+="\toperator: "+signed_number.getOperator();
             s+="\n\tnumber: "+signed_number.getNumber();
             System.out.println(s);
         }

         @Override
         public void visit(Expression expression,int level) {
             String s="";
             for (int l=0;l<level;l++)
                 s+=("  ");

             s += "ast Expression {\n";
             if(expression.getDataBaseName()!=null){
                 for (int l=0;l<level;l++)
                     s+=("  ");
                 s = s.concat("  Database: "+expression.getDataBaseName()+"\n");
             }
             if(expression.getTable_name()!=null){
                 for (int l=0;l<level;l++)
                     s+=("  ");
                 s=s.concat("Table: "+expression.getTable_name()+"\n");
             }
             if(expression.getColumn_name()!=null){
                 for (int l=0;l<level;l++)
                 s+=("  ");
                 s+=" Column: "+expression.getColumn_name()+"\n";}
     //        System.out.println(s);
             for (int l=0;l<level;l++)
                 s+=("  ");

             if(expression.getSellectStmt()!=null) {
                 if (expression.isExists()) {
                     if(expression.isNot())
                         s+="not";
                     s += " exist";
                 }
                 s+="select stmt";
             }
             System.out.println(s);
         }
     */
    @Override
    public void visit(Parse p) {

    }

    @Override
    public void visit(Num_exp num_exp) {

    }

    @Override
    public void visit(Condition_exp condition_exp) {

    }

    @Override
    public void visit(Return_stmt return_stmt) {

    }

    @Override
    public void visit(Expression expression) {
        String colN = expression.getColumn_name();
        expression.setType(new Type(colN));
        if (colN != null) {
            if (expression.getTable_name() != null) {
                Type t = TypeExist(expression.getTable_name());
                if (t != null) {
                    Type colt = columnExist(colN, t);
                    if (colt != null) {
                        expression.setType(colt);
//                  expression.getType().getColumns().put(colN,colt);
                    } else
                        undefined(colN + " column", expression);
                } else
                    undefined(expression.getTable_name() + " table/type", expression);
            }
        }
    }


    @Override
    public void visit(FunctionExpression functionExpression) {
        AggregationFunction ag=aggFunExist(functionExpression.getFunctionName());
        functionExpression.setColumn_name(functionExpression.getFunctionName());
        if(ag!=null){
//            functionExpression.setAggregationFunction(ag);
            ag.setName(Type.AGG_FUN_CONST);
            functionExpression.setType(ag);

        }
        else
            undefined(functionExpression.getFunctionName(),functionExpression);
    }

    @Override
    public void visit(BinaryExpression binaryExpression) {

    }

    @Override
    public void visit(ExpressionIn expressionIn) {

    }

    @Override
    public void visit(UnaryExpr unaryExpr) {

    }

    @Override
    public void visit(LiteralValue literal_value) {

    }


    @Override
    public void visit(SignedNumber signed_number) {

    }

    @Override
    public void visit(JoinElement joinElement) {

    }

    private Scope funExist(String funId) {
        ArrayList<Scope> scopes = Main.symbolTable.getScopes();
        for (Scope s : scopes) {
            if (s.getId().equals(funId))
                return s;
        }
        return null;
    }

    private AggregationFunction aggFunExist(String name) {

        for (AggregationFunction ag : Main.symbolTable.getDeclaredAggregationFunction()) {
            if (ag.getAggregationFunctionName().equals(name)) {
                return ag;
            }
        }
        return null;
    }

    private Type TypeExist(String tName) {
        Main.symbolTable.getScopes();
        for (Type type : Main.symbolTable.getDeclaredTypes()) {
            if (type.getName().equals(tName)) {
                return type;
            }
        }
        return null;
    }

    private Type columnExist(String column_name, Type t) {
        if (t.getColumns().get(column_name) != null)
            return t;
        if (t.getColumns() != null) {
            for (Type type : t.getColumns().values()) {
                return columnExist(column_name, type);
            }
            return null;
        }
        return null;
    }

    public void undeclaredFun(Function_call n) {
        System.out.println("Error at line:" + n.getLine() + " and col: " + n.getCol() + "\n(" + n.getFunction_name() + ") is undeclared method");
    }

    private void existBefore(String varName, Node n) {
        System.out.println("Error at line:" + n.getLine() + " and column: " + n.getCol() + "\n(" + varName + ") is already exist.");
    }

    private void undefined(String nodeName, Node n) {
        System.out.println("Error at line: " + n.getLine() + " and col: " + n.getCol() + "\n(" + nodeName + ") is not defined before");
    }
}
