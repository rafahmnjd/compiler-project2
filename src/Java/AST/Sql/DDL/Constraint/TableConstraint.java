package Java.AST.Sql.DDL.Constraint;

import Java.AST.Expression.Expression;
import Java.AST.Node;
import Java.AST.Sql.Values.Indexed_column;
import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class TableConstraint extends Node {
    boolean K_CONSTRAINT=false; String constraintName,constraint_keyName,unique_keyName;
    boolean K_PRIMARY=false,K_UNIQUE=false, table_constraint_key =false;


    public void setUnique_keyName(String unique_keyName) {
        this.unique_keyName = unique_keyName;
    }

    //TableConstraintKey table_constraint_key;
    ForeignKeyConstraint foreignKeyConstraint;
    Expression K_CHECK_expr;
    List<Indexed_column> indexed_columns=new ArrayList<>();
/*     String name;*/

    public List<Indexed_column> getIndexed_columns() { return indexed_columns; }
    public void setIndexed_columns(List<Indexed_column> indexed_columns) { this.indexed_columns = indexed_columns;}
    public void setK_CONSTRAINT(boolean k_CONSTRAINT) {K_CONSTRAINT = k_CONSTRAINT;}
    public void setConstraintName(String constraintName) {
        this.constraintName = constraintName;
        this.setS(this.addSpase());
    }
    public boolean isK_PRIMARY() {
        return K_PRIMARY;
    }
    public void setK_PRIMARY(boolean k_PRIMARY) {
        K_PRIMARY = k_PRIMARY;
    }

    public boolean isK_UNIQUE() {
        return K_UNIQUE;
    }
    public void setK_UNIQUE(boolean k_UNIQUE) {
        K_UNIQUE = k_UNIQUE;
    }

    public String getConstraint_keyName() {
        return constraint_keyName;
    }
    public void setConstraint_keyName(String constraint_keyName) {this.constraint_keyName = constraint_keyName;}

    public boolean isTable_constraint_key() {return table_constraint_key;}
    public void setTable_constraint_key(boolean table_constraint_key) {this.table_constraint_key = table_constraint_key;}



    public ForeignKeyConstraint getForeignKeyConstraint() {
        return foreignKeyConstraint;
    }
    public void setForeignKeyConstraint(ForeignKeyConstraint foreignKeyConstraint) {
        this.foreignKeyConstraint = foreignKeyConstraint;}

    public Expression getK_CHECK_expr() {
        return K_CHECK_expr;
    }
    public void setK_CHECK_expr(Expression k_CHECK_expr) {
        K_CHECK_expr = k_CHECK_expr;
    }

    @Override
    public void accept(ASTVisitor astVisitor, int level) {
        this.setLevel(level);
        this.setS(this.addSpase()+"Table constraint :");
        if(this.constraintName!=null&&this.constraintName!=null)
            this.setS(this.addSpase()+"  Constraint name : "+this.constraintName);
        if(this.constraintName!="")
            this.setS(this.addSpase()+"  Constraint name : "+this.constraintName);
    }
}
