package Java.AST.Sql.DDL.Constraint;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateAndTime  {

    public  String getFullCurrentDate(){
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        return(formatter.format(date));
    }
    public String getCurrentDate(){

        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        return(formatter.format(date));
    }
    public String getCurrentTime(){
        SimpleDateFormat formatter= new SimpleDateFormat("HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        return(formatter.format(date));
    }

}
