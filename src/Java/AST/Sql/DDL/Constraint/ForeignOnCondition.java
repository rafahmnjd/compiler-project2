package Java.AST.Sql.DDL.Constraint;

import Java.AST.Node;

public class ForeignOnCondition extends Node {
    boolean  K_DELETE , K_UPDATE ; ForeignKeyClause.Action action;


    public boolean isK_DELETE() {
        return K_DELETE;
    }

    public void setK_DELETE(boolean k_DELETE) {
        K_DELETE = k_DELETE;
    }

    public boolean isK_UPDATE() {
        return K_UPDATE;
    }

    public void setK_UPDATE(boolean k_UPDATE) {
        K_UPDATE = k_UPDATE;
    }

    public ForeignKeyClause.Action getAction() {
        return action;
    }

    public void setAction(ForeignKeyClause.Action action) {
        this.action = action;
    }
}
