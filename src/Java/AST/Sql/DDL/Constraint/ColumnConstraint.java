package Java.AST.Sql.DDL.Constraint;

import Java.AST.Expression.Expression;
import Java.AST.Node;

public class ColumnConstraint extends Node {
    String ConstraintName;
    boolean K_PRIMARY_K_KEY=false,K_AUTOINCREMENT=false,K_DSEK=false;//ASK by default
    ForeignKeyClause column_constraint_foreign_key;
    boolean Null=false,notNull=false,K_UNIQUE=false;
    Expression CHECKExpr;
    ColumnDefault columnDefault;
    String collation_name;

    public String getConstraintName() {
        return ConstraintName;
    }

    public void setConstraintName(String constraintName) {
        ConstraintName = constraintName;
    }

    public boolean isK_PRIMARY_K_KEY() {
        return K_PRIMARY_K_KEY;
    }

    public void setK_PRIMARY_K_KEY(boolean k_PRIMARY_K_KEY) {
        K_PRIMARY_K_KEY = k_PRIMARY_K_KEY;
    }

    public boolean isK_AUTOINCREMENT() {
        return K_AUTOINCREMENT;
    }

    public void setK_AUTOINCREMENT(boolean k_AUTOINCREMENT) {
        K_AUTOINCREMENT = k_AUTOINCREMENT;
    }

    public boolean isK_DSEK() {
        return K_DSEK;
    }

    public void setK_DSEK(boolean k_DSEK) {
        K_DSEK = k_DSEK;
    }

    public ForeignKeyClause getColumn_constraint_foreign_key() {
        return column_constraint_foreign_key;
    }

    public void setColumn_constraint_foreign_key(ForeignKeyClause column_constraint_foreign_key) {
        this.column_constraint_foreign_key = column_constraint_foreign_key;
    }

    public boolean isNull() {
        return Null;
    }

    public void setNull(boolean aNull) {
        Null = aNull;
    }

    public boolean isNotNull() {
        return notNull;
    }

    public void setNotNull(boolean notNull) {
        this.notNull = notNull;
    }

    public boolean isK_UNIQUE() {
        return K_UNIQUE;
    }

    public void setK_UNIQUE(boolean k_UNIQUE) {
        K_UNIQUE = k_UNIQUE;
    }

    public Expression getCHECKExpr() {
        return CHECKExpr;
    }

    public void setCHECKExpr(Expression CHECKExpr) {
        this.CHECKExpr = CHECKExpr;
    }

    public ColumnDefault getColumnDefault() {
        return columnDefault;
    }

    public void setColumnDefault(ColumnDefault columnDefault) {
        this.columnDefault = columnDefault;
    }

    public String getCollation_name() {
        return collation_name;
    }

    public void setCollation_name(String collation_name) {
        this.collation_name = collation_name;
    }
}
