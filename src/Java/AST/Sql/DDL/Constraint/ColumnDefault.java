package Java.AST.Sql.DDL.Constraint;

import Java.AST.Expression.Expression;
import Java.AST.Node;
import Java.AST.Sql.Values.LiteralValue;
import Java.AST.Sql.Values.SignedNumber;

import java.util.ArrayList;
import java.util.List;

public class ColumnDefault extends Node {
    LiteralValue literalValue;
    SignedNumber signed_number;
    Expression expression;
    boolean K_NEXTVAL=false;
    String leftName;

    public String getLeftName() {
        return leftName;
    }

    public void setLeftName(String leftName) {
        this.leftName = leftName;
    }

    List<String> name=new ArrayList<>();

    public LiteralValue getLiteralValue() {
        return literalValue;
    }

    public void setLiteralValue(LiteralValue literalValue) {
        this.literalValue = literalValue;
    }

    public SignedNumber getSigned_number() {
        return signed_number;
    }

    public void setSigned_number(SignedNumber signed_number) {
        this.signed_number = signed_number;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public boolean isK_NEXTVAL() {
        return K_NEXTVAL;
    }

    public void setK_NEXTVAL(boolean k_NEXTVAL) {
        K_NEXTVAL = k_NEXTVAL;
    }

    public List<String> getName() {
        return name;
    }

    public void setName(List<String> name) {
        this.name = name;
    }
}
