package Java.AST.Sql.DDL.Constraint;

import Java.AST.Node;

import java.util.List;


public class ForeignKeyClause extends Node {
    //references:
    String refDatabaseName, refForeignTable;
    List<String> refFk_target_column_nameList ;
    public String getRefDatabaseName() {
        return refDatabaseName;
    }
    public void setRefDatabaseName(String refDatabaseName) {
        this.refDatabaseName = refDatabaseName;
    }
    public String getRefForeignTable() {
        return refForeignTable;
    }
    public void setRefForeignTable(String refForeignTable) {
        this.refForeignTable = refForeignTable;
    }
    public List<String> getRefFk_target_column_nameList() {
        return refFk_target_column_nameList;
    }
    public void setRefFk_target_column_nameList(List<String> refFk_target_column_nameList) {
        this.refFk_target_column_nameList = refFk_target_column_nameList;
    }

    //Match condition
    List<String> K_MATCH_names;
    public List<String> getK_MATCH_names() {return K_MATCH_names;}
    public void setK_MATCH_names(List<String> k_MATCH_names) { K_MATCH_names = k_MATCH_names;}

    // ON condition
    List<ForeignOnCondition> foreignOnCondition ;

    public enum Action{
        SET_NULL, SET_DEFAULT,CASCADE,RESTRICT, NO_ACTION
    }
    public void setForeignOnCondition(List<ForeignOnCondition> foreignOnCondition) {
        this.foreignOnCondition = foreignOnCondition;
    }
    public List<ForeignOnCondition> getForeignOnCondition() {
        return foreignOnCondition;
    }


    //  DEFERRABLE
    boolean DEFERRABLE=false, INITIALLY_DEFERRED=false ,INITIALLY_IMMEDIATE=false, ENABLEed=false;

    public boolean isDEFERRABLE() {
        return DEFERRABLE;
    }
    public void setK_DEFERRABLE(boolean EFERRABLE) {
        DEFERRABLE = DEFERRABLE;
    }
    public void setDEFERRABLE(boolean DEFERRABLE) {
        this.DEFERRABLE = DEFERRABLE;
    }
    public boolean isINITIALLY_DEFERRED() {
        return INITIALLY_DEFERRED;
    }
    public void setINITIALLY_DEFERRED(boolean INITIALLY_DEFERRED) {
        this.INITIALLY_DEFERRED = INITIALLY_DEFERRED;
    }
    public boolean isINITIALLY_IMMEDIATE() {
        return INITIALLY_IMMEDIATE;
    }
    public void setINITIALLY_IMMEDIATE(boolean INITIALLY_IMMEDIATE) {
        this.INITIALLY_IMMEDIATE = INITIALLY_IMMEDIATE;
    }
    public boolean isENABLEed() {
        return ENABLEed;
    }
    public void setENABLEed(boolean ENABLEed) {
        this.ENABLEed = ENABLEed;
    }
}
