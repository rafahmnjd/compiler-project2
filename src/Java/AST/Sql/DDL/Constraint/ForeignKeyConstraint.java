package Java.AST.Sql.DDL.Constraint;

import Java.AST.Node;

import java.util.ArrayList;
import java.util.List;

public class ForeignKeyConstraint extends Node {
    List<String> foreignCulomnsName=new ArrayList<>();
    ForeignKeyClause foreignKeyClause;

    public List<String> getForeignCulomnsName() {
        return foreignCulomnsName;
    }

    public void setForeignCulomnsName(List<String> foreignCulomnsName) {
        this.foreignCulomnsName = foreignCulomnsName;
    }

    public ForeignKeyClause getForeignKeyClause() {
        return foreignKeyClause;
    }

    public void setForeignKeyClause(ForeignKeyClause foreignKeyClause) {
        this.foreignKeyClause = foreignKeyClause;
    }
}
