package Java.AST.Sql.DDL;

import Java.AST.Sql.Statement;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Type;

public class CreateTableStmt extends Statement {

    boolean  IF_NOT_EXISTS = false;
    Type sembolTableType;
    String fileType,path;


    public Type getSembolTableType() {
        return sembolTableType;
    }

    public void setSembolTableType(Type sembolTableType) {
        this.sembolTableType = sembolTableType;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isIF_NOT_EXISTS() {
        return IF_NOT_EXISTS;
    }

    public void setIF_NOT_EXISTS(boolean IF_NOT_EXISTS) {
        this.IF_NOT_EXISTS = IF_NOT_EXISTS;
    }


    @Override
    public void accept(ASTVisitor astVisitor, int level) {
      this.level=level;
        System.out.println(this.addSpase() + "create table_stmt :");
        astVisitor.visit(this);
        if(this.sembolTableType!=null){
            this.sembolTableType.setTable(this);
        }

    }
}
