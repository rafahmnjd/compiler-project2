package Java.AST.Sql.DDL;

import Java.AST.Functions.Parameter_list;
import Java.AST.Sql.Statement;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.AggregationFunction;

public class CreateAggFunction extends Statement {
AggregationFunction function;


    public AggregationFunction getFunction() {
        return function;
    }

    public void setFunction(AggregationFunction function) {
        this.function = function;
    }

    @Override
    public void accept(ASTVisitor astVisitor, int level) {
        super.accept(astVisitor, level);
    }
}
