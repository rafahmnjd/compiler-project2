package Java.AST.Sql.DDL;

import Java.AST.Node;
import Java.AST.Sql.DDL.Constraint.ColumnConstraint;
import Java.AST.Sql.Values.TypeName;
import Java.SymbolTable.Type;

import java.util.List;

public class ColumnDef extends Node {
String columnName;
Type type;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
