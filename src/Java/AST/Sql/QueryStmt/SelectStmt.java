package Java.AST.Sql.QueryStmt;

import Java.AST.Expression.Expression;
import Java.AST.Sql.Statement;
import Java.AST.Sql.Values.OrderingTerm;
import Java.AST.Sql.Values.SQLValue;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Type;

import java.util.ArrayList;
import java.util.List;

public class SelectStmt extends Statement implements SQLValue {

    SelectCoreOrValue sellectOrValues;
    boolean ORDER_BY,K_LIMIT,K_OFFSET;
    List<OrderingTerm> orderingTerms=new ArrayList<>();
    Expression limitExpression, secondExpression;
    boolean comma;

    public  boolean oneTable=false;

    public Type getType() {
        Type t=new Type(sellectOrValues.getType().getName());
        t.setColumns(sellectOrValues.getType().flat());
        return t ;
    }

    public Expression getLimitExpression() {
        return limitExpression;
    }
    public void setLimitExpression(Expression limitExpression) {
        this.limitExpression = limitExpression;
    }

    public Expression getSecondExpression() {
        return secondExpression;
    }
    public void setSecondExpression(Expression secondExpression) {
        this.secondExpression = secondExpression;
    }

    public SelectCoreOrValue getSellectOrValues() {
        return sellectOrValues;
    }
    public void setSellectOrValues(SelectCoreOrValue sellectOrValues) {
        this.sellectOrValues = sellectOrValues;
    }

    public boolean isORDER_BY() {
        return ORDER_BY;
    }
    public void setORDER_BY(boolean ORDER_BY) {
        this.ORDER_BY = ORDER_BY;
    }

    public boolean isK_LIMIT() {
        return K_LIMIT;
    }
    public void setK_LIMIT(boolean k_LIMIT) {
        K_LIMIT = k_LIMIT;
    }

    public boolean isK_OFFSET() {
        return K_OFFSET;
    }

    public void setK_OFFSET(boolean k_OFFSET) {
        K_OFFSET = k_OFFSET;
    }

    public List<OrderingTerm> getOrderingTerms() {
        return orderingTerms;
    }

    public void setOrderingTerms(List<OrderingTerm> orderingTerms) {
        this.orderingTerms = orderingTerms;
    }

    public void setComma(boolean b) {
        this.comma=b;
    }

    @Override
    public void accept(ASTVisitor astVisitor, int l){
        this.level=l;
        astVisitor.visit(this);


        if(sellectOrValues!=null){
            sellectOrValues.accept(astVisitor,l+1);
            if(sellectOrValues.tables.size()==1){
                oneTable=true;
            }
//             this.getType().print(0);
        }
    }

}
