/*-
 * #%L
 * JSQLParser library
 * %%
 * Copyright (C) 2004 - 2019 JSQLParser
 * %%
 * Dual licensed under GNU LGPL 2.1 or Apache License 2.0
 * #L%
 */
package Java.AST.Sql.QueryStmt;

import Java.AST.Node;
import Java.AST.Sql.Values.Table_or_subquery;
import Java.AST.Visitor.ASTVisitor;
import Java.Main;
import Java.SymbolTable.Type;

import java.util.*;

public class Join_clause extends Node {

    Table_or_subquery table_or_subquery = null;
    List<JoinElement> joinRightElements = null;
    Type type;
    private ArrayList<Type> tables;

    public Type getType() {
        return type;
    }



    public Table_or_subquery getTable_or_subquery() {
        return table_or_subquery;
    }

    public void setTable_or_subquery(Table_or_subquery table_or_subquery) {
        this.table_or_subquery = table_or_subquery;
    }

    public List<JoinElement> getJoinRightElements() {
        return joinRightElements;
    }

    public void setJoinRightElements(List<JoinElement> joinRightElements) {
        this.joinRightElements = joinRightElements;
    }

    @Override
    public void accept(ASTVisitor astVisitor, int level) {
        this.tables=new ArrayList<>();
        this.type=new Type("");
        this.level = level;
        System.out.println(addSpase()+"join clause{");

        if (joinRightElements != null) {
            System.out.println(addSpase()+" _join elements:{");
            for (int jr = joinRightElements.size()-1; jr >=0 ; jr--) {
                joinRightElements.get(jr).accept(astVisitor, level + 1);
                tables.add(joinRightElements.get(jr).getType());
                this.type.addToName("_join_"+joinRightElements.get(jr).getType().getName());
/*                Type t=new Type(this.type.getName());
                t.setColumns(joinRightElements.get(jr).getType().flat(joinRightElements.get(jr).getType().getName()));
                if(jr+1!=getJoinRightElements().size()){
                   t.addColumns(joinRightElements.get(jr+1).getType().getColumns());
                }*/
//                Main.symbolTable.addType(t);
//                getJoinRightElements().get(jr).setType(t);
            }
            System.out.println(addSpase() + " }");
        }
        if (table_or_subquery != null) {
            System.out.println(addSpase() + " _table_or_subquery:{");
            table_or_subquery.accept(astVisitor, level + 1);
            tables.add(table_or_subquery.getType());
            type.addNamePrev("_"+table_or_subquery.getType().getName());
            System.out.println(addSpase() + " }");
        }
        System.out.println(addSpase() + "}");
        if(tables!=null){
            for (Type t:tables) {
               this.type.addColumns(t.flat(t.getName()));
            }
            Main.symbolTable.addType(this.type);
        }
    }

    public ArrayList<Type> getTables() {
        return  this.tables;
    }
}
