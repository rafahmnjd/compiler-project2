package Java.AST.Sql.QueryStmt;

import Java.AST.Expression.Expression;
import Java.AST.Sql.Statement;
import Java.AST.Sql.Values.Result_column;
import Java.AST.Sql.Values.Table_or_subquery;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Type;

import java.util.ArrayList;
import java.util.List;

public class SelectCoreOrValue extends Statement {
    public ArrayList<Type> tables = new ArrayList<>();
    Type type = null;

    boolean K_DISTINCT = false, K_ALL = false, K_SELECT = false, K_VALUE = false;
    List<Result_column> result_columns = null;
    List<Table_or_subquery> table_or_subQueries = null;
    Join_clause join_clause = null;
    Expression whereExpression = null,
            havingExpression = null;
    List<Expression> groupByExpressions = null,
            valuesExpressionList = null;


    public void setType(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public void addTableOrSub__(Table_or_subquery ts) {
        if (table_or_subQueries == null)
            table_or_subQueries = new ArrayList<>();
        table_or_subQueries.add(ts);
    }

    public void addvaluesExpression(Expression e) {
        if (valuesExpressionList == null)
            valuesExpressionList = new ArrayList<>();
        valuesExpressionList.add(e);
    }

    public boolean isK_DISTINCT() {
        return K_DISTINCT;
    }

    public void setK_DISTINCT(boolean k_DISTINCT) {
        K_DISTINCT = k_DISTINCT;
    }

    public boolean isK_ALL() {
        return K_ALL;
    }

    public void setK_ALL(boolean k_ALL) {
        K_ALL = k_ALL;
    }

    public List<Result_column> getResult_columns() {
        return result_columns;
    }

    public void setResult_columns(List<Result_column> result_columns) {
        this.result_columns = result_columns;
    }

    public List<Table_or_subquery> getTable_or_subQueries() {
        return table_or_subQueries;
    }

    public Join_clause getJoin_clause() {
        return join_clause;
    }

    public void setJoin_clause(Join_clause join_clause) {
        this.join_clause = join_clause;
    }

    public Expression getWhereExpression() {
        return whereExpression;
    }

    public void setWhereExpression(Expression whereExpression) {
        this.whereExpression = whereExpression;
    }

    public Expression getHavingExpression() {
        return havingExpression;
    }

    public void setHavingExpression(Expression havingExpression) {
        this.havingExpression = havingExpression;
    }

    public List<Expression> getGroupByExpressions() {
        return groupByExpressions;
    }

    public void setGroupByExpressions(List<Expression> groupByExpressions) {
        this.groupByExpressions = groupByExpressions;
    }

    public List<Expression> getValuesExpressionList() {
        return valuesExpressionList;
    }

    public void setValuesExpressionList(List<Expression> valuesExpressionList) {
        this.valuesExpressionList = valuesExpressionList;
    }

    public void setK_VALUE(boolean b) {
        K_VALUE = b;
    }


    @Override
    public void accept(ASTVisitor astVisitor, int level) {
        this.level = level;
        this.type = new Type("");
        if (!K_VALUE) {
            System.out.println(addSpase() + "SelectCore:{");
            if (K_DISTINCT)
                System.out.println(addSpase() + "   -type = DISTINCT");
            else if (K_ALL)
                System.out.println(addSpase() + "   -type = ALL");

            if (table_or_subQueries != null) {
                System.out.println(addSpase() + "    -table_or_subQueries:{");
                for (int rc = 0; rc < table_or_subQueries.size(); rc++) {
                    table_or_subQueries.get(rc).accept(astVisitor, level + 2);
                    tables.add(table_or_subQueries.get(rc).getType());
                    this.type.addToName("_" + table_or_subQueries.get(rc).getType().getName());
                }
                System.out.println(addSpase() + "     }");
            }
            if (join_clause != null) {
                System.out.println(addSpase() + "     -join_clause:{");
                join_clause.accept(astVisitor, level + 2);
                tables.addAll(join_clause.getTables());
                type.addToName(join_clause.getType().getName());
                System.out.println(addSpase() + "     }");
            }
            if (whereExpression != null) {
                System.out.println(addSpase() + "     -whereExpression:{");
                whereExpression.accept(astVisitor, level + 2);
                System.out.println(addSpase() + "     }");
                if(tables.size()==1){
                    whereExpression.setTable_name(tables.get(0).getName());
                }
            }
            if (groupByExpressions != null) {
                System.out.println(addSpase() + "     -groupByExpressions list:{");
                for (int rc = 0; rc < groupByExpressions.size(); rc++) {
                    groupByExpressions.get(rc).accept(astVisitor, level + 2);
                }
                System.out.println(addSpase() + "     }");
            }

            if (havingExpression != null) {
                System.out.println(addSpase() + "     -havingExpression:{");
                havingExpression.accept(astVisitor, level + 2);
                System.out.println(addSpase() + "     }");
            }
            if (result_columns != null) {
                System.out.println(addSpase() + "     -result columns :{");
                for (int rc = 0; rc < result_columns.size(); rc++) {
                    result_columns.get(rc).accept(astVisitor, level + 2);
                    if (result_columns.get(rc).isStar()) {
                        if (result_columns.get(rc).getTable_name() != null && result_columns.get(rc).getType() != null)
                            this.type.addColumn(result_columns.get(rc).getTable_name(),result_columns.get(rc).getType());
                        else {
                            if (!tables.isEmpty()) {
                                if (tables.size() == 1) {
                                    this.type.addColumn(tables.get(0).getName(),tables.get(0));
                                } else {
                                    for (Type t : tables) {
                                        this.type.addColumn(t.getName(),t);
                                    }
                                }
                            }
                        }
                    }
                    else if (result_columns.get(rc).getExpression() != null) {
                        if(result_columns.get(rc).getFinal_column_name()!=null){
                            Type rt=result_columns.get(rc).getType();
                            if(rt!=null && rt.getName().equals(Type.RESULT_COL_CONST)){
                                String resultName=result_columns.get(rc).getExpression().getColumn_name();
                                if(resultName!=null){
                                    Type t = getColType(resultName);
                                    if(tables.size()==1)
                                        this.type.addColumn(tables.get(0).getName()+"$"+result_columns.get(rc).getFinal_column_name(), t);
                                    else
                                        this.type.addColumn(result_columns.get(rc).getFinal_column_name(), t);

                                }
                            }
                            else {
                                this.type.addColumn(result_columns.get(rc).getFinal_column_name(), rt);
                            }
                        }
                    }
                }
                System.out.println(addSpase() + "     }");
            }
            System.out.println(addSpase() + "}");
        } else if (K_VALUE) {
            System.out.println("Values:{");
            if (valuesExpressionList != null) {
                System.out.println(addSpase() + "     -valuesExpressionList:{");
                for (int rc = 0; rc < valuesExpressionList.size(); rc++) {
                    valuesExpressionList.get(rc).accept(astVisitor, level + 2);
                }
                System.out.println(addSpase() + "     }");
            }
            System.out.println(addSpase() + "}");
        }
    }

    private Type getColType(String rcName) {
        for (Type t : tables) {
            Type ct = t.flat0().get(rcName);
            if (ct != null)
                return ct;
        }
        return null;
    }
}
