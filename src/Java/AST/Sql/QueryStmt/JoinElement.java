package Java.AST.Sql.QueryStmt;

import Java.AST.Expression.Expression;
import Java.AST.Node;
import Java.AST.Sql.Values.Table_or_subquery;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Type;

public class JoinElement extends Node {
    Expression onExpression;
    String operator=",";         //left outer ,left, inner,coma','.
    Table_or_subquery left_Table_or_subquery;

    Type type=null;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Expression getOnExpression() {
        return onExpression;
    }

    public void setOnExpression(Expression onExpression) {
        this.onExpression = onExpression;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Table_or_subquery getLeft_Table_or_subquery() {
        return left_Table_or_subquery;
    }

    public void setLeft_Table_or_subquery(Table_or_subquery left_Table_or_subquery) {
        this.left_Table_or_subquery = left_Table_or_subquery;

    }

    @Override
    public void accept(ASTVisitor astVisitor, int level) {
       this.level=level;

        System.out.println(addSpase()+"joinElement:{");
        System.out.println(addSpase()+" -left_Table_or_subquery{");
        if(left_Table_or_subquery!=null){
            left_Table_or_subquery.accept(astVisitor,level+2);
            this.setType(left_Table_or_subquery.getType());
        }
        System.out.println(addSpase()+" }");
        System.out.println(addSpase()+" -onExpression:{");
        onExpression.accept(astVisitor,level+2);
        System.out.println(addSpase()+" }");
        System.out.println(addSpase()+" -operator : "+operator);
        astVisitor.visit(this);

        System.out.println(addSpase()+"}");

    }
}
