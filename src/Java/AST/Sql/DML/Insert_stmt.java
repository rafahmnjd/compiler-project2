package Java.AST.Sql.DML;

import Java.AST.Expression.Expression;
import Java.AST.Sql.QueryStmt.SelectStmt;
import Java.AST.Sql.Statement;
import Java.AST.Visitor.ASTVisitor;

import java.util.List;

public class Insert_stmt extends Statement {

    private String dataBaseName="";
    private String tableName="";
    private List<String> columnsName ;
    private List<Expression> values =null;
    private SelectStmt selectStmt =null;
    private boolean defaultValues=false;

    public String getDataBaseName() {return dataBaseName;}
    public void setDataBaseName(String dataBaseName) {this.dataBaseName = dataBaseName;}

    public boolean isDefaultValues() { return defaultValues; }
    public void setDefaultValues(boolean defaultValues) { this.defaultValues = defaultValues; }

    public String getTableName() {
        return tableName;
    }
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<String> getColumnsName() {
        return columnsName;
    }
    public void setColumnsName(List<String> columnsName) {
        this.columnsName = columnsName;
    }

    public List<Expression> getValues() {
        return values;
    }
    public void setValues(List<Expression> values) {
        this.values = values;
    }

    public SelectStmt getSelectStmt() {
        return selectStmt;
    }
    public void setSelectStmt(SelectStmt selectStmt) {
        this.selectStmt = selectStmt;
    }

    @Override
    public void accept(ASTVisitor astVisitor,int l) {
      //  astVisitor.visit(this,l+1);
        if(this.selectStmt !=null){
           // sellectStmt.accept(astVisitor,l+1);
        }
        if(this.values!=null) {
            for(int lx=0;lx<l;lx++)
                System.out.print("  ");
            System.out.print("values : {\n");
            for (int v = 0; v < values.size(); v++) {
                values.get(v).accept(astVisitor,l+2);
            }
            for(int lx=0;lx<l;lx++)
                    System.out.print("  ");
            System.out.print("}\n");
        }
    }
}
