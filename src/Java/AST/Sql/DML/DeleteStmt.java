package Java.AST.Sql.DML;

import Java.AST.Expression.Expression;
import Java.AST.Sql.Statement;
import Java.AST.Sql.Values.QualifiedTableName;

public class DeleteStmt extends Statement {
    QualifiedTableName fromQualifiedTableName;
    Expression whereExpression;

    public QualifiedTableName getFromQualifiedTableName() {
        return fromQualifiedTableName;
    }

    public void setFromQualifiedTableName(QualifiedTableName fromQualifiedTableName) {
        this.fromQualifiedTableName = fromQualifiedTableName;
    }

    public Expression getWhereExpression() {
        return whereExpression;
    }

    public void setWhereExpression(Expression whereExpression) {
        this.whereExpression = whereExpression;
    }
}
