package Java.AST.Sql.DML;

import Java.AST.Expression.Expression;
import Java.AST.Sql.Statement;
import Java.AST.Sql.Values.QualifiedTableName;

import java.util.List;

public class UpdateStmt extends Statement {
    QualifiedTableName qualifiedTableName;
    List<String> columnNameStringList;// for each column there is an expr to set its value
    List<Expression> columnValueExpressionList;
    Expression whereExpression; //last expr if K_WHERE exist

    public QualifiedTableName getQualifiedTableName() {

        return qualifiedTableName;
    }

    public void setQualifiedTableName(QualifiedTableName qualifiedTableName) {
        this.qualifiedTableName = qualifiedTableName;
    }

    public List<String> getColumnNameStringList() {
        return columnNameStringList;
    }

    public void setColumnNameStringList(List<String> columnNameStringList) {
        this.columnNameStringList = columnNameStringList;
    }

    public List<Expression> getColumnValueExpressionList() {
        return columnValueExpressionList;
    }

    public void setColumnValueExpressionList(List<Expression> columnValueExpressionList) {
        this.columnValueExpressionList = columnValueExpressionList;
    }

    public Expression getWhereExpression() {
        return whereExpression;
    }

    public void setWhereExpression(Expression whereExpression) {
        this.whereExpression = whereExpression;
    }
}
