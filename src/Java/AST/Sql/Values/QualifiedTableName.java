package Java.AST.Sql.Values;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.Main;
import Java.SymbolTable.Type;

public class QualifiedTableName extends Node implements SQLValue{
    String database_name="", table_name="",index_name="";
    boolean K_INDEXED=false;
    Type type=null;

    public Type getType() {
        return type;
    }
    public void setType(Type type) {
        this.type = type;
    }
    public String getDatabase_name() { return database_name;}
    public void setDatabase_name(String database_name) { this.database_name = database_name; }
    public void setTable_name(String table_name){this.table_name=table_name;}
    public String getTable_name(){return this.table_name;}
    public String getIndex_name() {return index_name;}
    public void setIndex_name(String index_name) {this.index_name = index_name;}
    public boolean isK_INDEXED() {return K_INDEXED; }
    public void setK_INDEXED(boolean k_INDEXED) { K_INDEXED = k_INDEXED;}

    @Override
    public void accept(ASTVisitor astVisitor, int level) {
        this.level=level;
        astVisitor.visit(this);
        System.out.println(addSpase()+"QualifiedTableName:{");
        if(!database_name.equals(""))
            System.out.println(addSpase()+" -database_name : "+database_name);
        if(!table_name.equals("")){
            System.out.println(addSpase()+" -table_name : "+table_name);
/*            for (Type t:Main.symbolTable.getDeclaredTypes() ) {
                if(t.getName().equals(table_name))
                    this.type=t;
            }*/
        }
        if(!index_name.equals(""))
            System.out.println(addSpase()+" -index_name : "+index_name);
        System.out.println(addSpase()+"}");
    }
}
