package Java.AST.Sql.Values;

import Java.AST.Node;
import Java.AST.Sql.DDL.Constraint.DateAndTime;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Type;

public class LiteralValue extends Node implements SQLValue {

    boolean K_NULL = false;
    boolean K_CURRENT_TIME = false;
    boolean K_CURRENT_DATE = false;
    boolean K_CURRENT_TIMESTAMP = false;
    Double NUMERICLITERAL=null;
    String STRINGLITERA, BLOB_LITERAL;
    String dateAndTime, finalValue;
    Type type = null;

    public Type getType() {
        return type;
    }
    public void setType(Type type) {
        this.type = type;
    }

    public void setK_NULL(boolean k_NULL) {
        K_NULL = k_NULL;
    }

    public void setK_CURRENT_TIME(boolean k_CURRENT_TIME) {
        K_CURRENT_TIME = k_CURRENT_TIME;
    }

    public void setK_CURRENT_DATE(boolean k_CURRENT_DATE) {
        K_CURRENT_DATE = k_CURRENT_DATE;
    }

    public void setK_CURRENT_TIMESTAMP(boolean k_CURRENT_TIMESTAMP) {
        K_CURRENT_TIMESTAMP = k_CURRENT_TIMESTAMP;
    }

    public void setNUMERICLITERAL(double NUMERICLITERAL) {
        this.NUMERICLITERAL = NUMERICLITERAL;
    }

    public void setSTRINGLITERA(String STRINGLITERA) {
        this.STRINGLITERA = STRINGLITERA;
    }

    public void setBLOB_LITERAL(String BLOB_LITERAL) {
        this.BLOB_LITERAL = BLOB_LITERAL;
    }

    public String getFinalValue() {
        return finalValue;
    }

    public void setFinalValue() {
        if (K_NULL) {
            finalValue = null;
            this.type = new Type(Type.NULL_CONST);
        } else if (NUMERICLITERAL != null) {
            finalValue = NUMERICLITERAL + "";
            this.type = new Type(Type.NUMBER_CONST);
        } else {
            this.type = new Type(Type.STRING_CONST);
            if (K_CURRENT_TIME) {
                finalValue = new DateAndTime().getCurrentTime();
            } else if (K_CURRENT_DATE)
                finalValue = new DateAndTime().getCurrentDate();
            else if (K_CURRENT_TIMESTAMP)
                finalValue = new DateAndTime().getFullCurrentDate();
            else if (!STRINGLITERA.equals(""))
                finalValue = STRINGLITERA;
            else if (!BLOB_LITERAL.equals(""))
                finalValue = BLOB_LITERAL;
        }
    }

    @Override
    public void accept(ASTVisitor astVisitor, int l) {
        System.out.println("literal_value = " + this.finalValue);
    }
}
