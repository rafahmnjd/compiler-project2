package Java.AST.Sql.Values;

import Java.AST.Node;
import Java.AST.Sql.QueryStmt.Join_clause;
import Java.AST.Sql.QueryStmt.SelectStmt;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Type;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Table_or_subquery extends Node implements SQLValue{
    QualifiedTableName qualifiedTableName;
    List<Table_or_subquery> table_or_subqueries;
    Join_clause join_clause;
    SelectStmt sellect_stmt;
    String table_alias="";
    Type type=null;


    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public QualifiedTableName getQualifiedTableName() {
        return qualifiedTableName;
    }

    public void setQualifiedTableName(QualifiedTableName qualifiedTableName) {
        this.qualifiedTableName = qualifiedTableName;
    }

    public SelectStmt getSellect_stmt() {
        return sellect_stmt;
    }

    public void setSellect_stmt(SelectStmt sellect_stmt) {
        this.sellect_stmt = sellect_stmt;
    }

    public String getTable_alias() { return table_alias; }
    public void setTable_alias(String table_alias) {this.table_alias = table_alias; }
    public List<Table_or_subquery> getTable_or_subqueries() { return table_or_subqueries;}
    public void setTable_or_subqueries(List<Table_or_subquery> table_or_subqueries) {
        this.table_or_subqueries = table_or_subqueries;}

    public Join_clause getJoin_clause() {return join_clause;}
    public void setJoin_clause(Join_clause join_clause) {this.join_clause = join_clause;}

    public void setSellectStmt(SelectStmt sellect_stmt){
        this.sellect_stmt = sellect_stmt;}
    public SelectStmt getSellectStmt(){return this.sellect_stmt;}

    @Override
    public void accept(ASTVisitor astVisitor, int level) {
        this.level=level;

        System.out.println(addSpase()+"TableOrSubQuery:{");
        if(qualifiedTableName!=null){
            System.out.println(addSpase()+"     qualifiedTableName :");
            qualifiedTableName.accept(astVisitor,level+3);
            this.type=qualifiedTableName.getType();
        }

        if(table_or_subqueries!=null){
            this.type=new Type();
            String tName="";
            Map<String,Type> cols=new HashMap<>();

            System.out.println(addSpase()+"     TableOrSubQueryList:{");
            for(int ts=0;ts<table_or_subqueries.size();ts++){
                table_or_subqueries.get(ts).accept(astVisitor,level+3);
                tName+=table_or_subqueries.get(ts).getType().getName()+",";
                cols.put(table_or_subqueries.get(ts).getType().getName(),table_or_subqueries.get(ts).getType());
                this.type.addToName(table_or_subqueries.get(ts).getType().getName()+"_");
            }
            this.type.setName(this.type.getName().substring(0,this.type.getName().length()-1));
            System.out.println(addSpase()+" }");
        }
        if(join_clause!=null){
            join_clause.accept(astVisitor,level+2);
            this.type=join_clause.getType();

        }
        if(sellect_stmt!=null){
            sellect_stmt.accept(astVisitor,level+2);
            this.type=sellect_stmt.getType();
        }
        if(table_alias!=null&&table_alias!=""){
            System.out.println(addSpase()+"     table_alias : "+table_alias);
            this.type.setName(table_alias);
        }
        System.out.println(addSpase()+"}");
        astVisitor.visit(this);
    }
}

