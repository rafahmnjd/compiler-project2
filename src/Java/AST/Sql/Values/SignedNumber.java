package Java.AST.Sql.Values;


import Java.AST.Defenations_and_Values.Values.Value;

public class SignedNumber extends Value implements SQLValue{

    boolean minus,plus,star;
    Double number = null;
    String operator;

    public String getOperator() {
        return operator;
    }

    public void setOperator() {
        if(minus)
        this.operator ="-";
        if(plus)
            this.operator="+";
        if(star)
            this.operator="*";
    }

    public boolean isMinus() {
        return minus;
    }

    public void setMinus(boolean minus) {
        this.minus = minus;
    }

    public boolean isPlus() {
        return plus;
    }

    public void setPlus(boolean plus) {
        this.plus = plus;
    }

    public boolean isStar() {
        return star;
    }

    public void setStar(boolean star) {
        this.star = star;
    }

    public double getNumber() {
        return number;
    }

    public void setNumber(double number) {
        this.number = number;
    }

//    @Override
//    public void accept(ASTVisitor astVisitor,int level) {
//        this.level=level;
//      //  astVisitor.visit(this,l+1);
//    }
    @Override
    public String toString() {
        return (this.operator+" "+number.toString());
    }
}
