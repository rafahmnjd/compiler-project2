package Java.AST.Sql.Values;

import Java.AST.Node;

public class TypeName extends Node implements SQLValue{
    String name="",any_name1,any_name2;
    SignedNumber signed_number1=null,signed_number2=null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAny_name1() {
        return any_name1;
    }

    public void setAny_name1(String any_name1) {
        this.any_name1 = any_name1;
    }

    public String getAny_name2() {
        return any_name2;
    }

    public void setAny_name2(String any_name2) {
        this.any_name2 = any_name2;
    }

    public SignedNumber getSigned_number1() {
        return signed_number1;
    }

    public void setSigned_number1(SignedNumber signed_number1) {
        this.signed_number1 = signed_number1;
    }

    public SignedNumber getSigned_number2() {
        return signed_number2;
    }

    public void setSigned_number2(SignedNumber signed_number2) {
        this.signed_number2 = signed_number2;
    }

    public String getFullName(){
        String fullname=name+"("+signed_number1+any_name1;
        if(signed_number2!=null){
            fullname+=signed_number2+any_name2;
        }
        fullname+=")";
        return fullname;
    }
}
