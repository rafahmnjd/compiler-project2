package Java.AST.Sql.Values;

import Java.AST.Node;

public class Indexed_column extends Node implements SQLValue{
    private String  column_name,collation_name;
    private boolean K_COLLATE , K_DESC=false;   //ASC by default.

    public String getColumn_name() {
        return column_name;
    }
    public void setColumn_name(String column_name) {
        this.column_name = column_name;
    }

    public String getCollation_name() {
        return collation_name;
    }
    public void setCollation_name(String collation_name) {
        this.collation_name = collation_name;
    }

    public boolean isK_COLLATE() {
        return K_COLLATE;
    }
    public void setK_COLLATE(boolean k_COLLATE) {
        K_COLLATE = k_COLLATE;
    }

    public boolean isK_DESC() {
        return K_DESC;
    }
    public void setK_DESC(boolean k_DESC) {
        K_DESC = k_DESC;
    }
}
