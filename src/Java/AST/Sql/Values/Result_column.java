package Java.AST.Sql.Values;

import Java.AST.Expression.Expression;
import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Type;

public class Result_column extends Node implements SQLValue{

    boolean star;
    String  table_name=null,
            final_column_name=null,
            column_alias=null;
    Expression expression=null ;
    Type type;


    public Type getType() {
        return this.type;
    }
    public void setType(Type type) {
        this.type = type;
    }
    public boolean isStar() {
        return star;
    }
    public void setStar(boolean star) {
        this.star = star;
    }
    public String getTable_name() {
        return table_name;
    }
    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }
    public String getColumn_alias() {
        return column_alias;
    }
    public void setColumn_alias(String column_alias) {
        this.column_alias = column_alias;
    }
    public Expression getExpression() {
        return expression;
    }
    public void setExpression(Expression expression) {
        this.expression = expression;
    }
    public String getFinal_column_name() {
        return final_column_name;
    }
    public void setFinal_column_name(String final_column_name) {
        this.final_column_name = final_column_name;
    }



    @Override
    public void accept(ASTVisitor astVisitor,int l) {

        this.level=l;
        astVisitor.visit(this);
        System.out.println(addSpase()+"ResultColumn:{");
        if(table_name!=null)
            System.out.println(addSpase()+" -fromTable_name : "+table_name);
        if(star)
            System.out.println(addSpase()+" -All columns ");
        if(this.expression!=null) {
            this.expression.accept(astVisitor , l + 1);
            this.type=expression.getType();
            if(this.type.getName().equals(Type.RESULT_COL_CONST)){
                if(this.expression.getColumn_name()!=null){
//                    this.type.setName(expression.getColumn_name());
                    this.final_column_name=expression.getColumn_name();
                }
                if(this.expression.getTable_name()!=null){
//                    this.type.addNamePrev(this.expression.getTable_name()+"__");
                    this.final_column_name=this.expression.getTable_name()+"$"+this.final_column_name;
                }
            }
            else if(this.type!=null && !this.type.getName().equals(Type.NULL_CONST)){
                if(this.expression.getColumn_name()!=null){
                    this.final_column_name=this.expression.getColumn_name();
                }
            }

            if (column_alias != null){
                System.out.println(addSpase() + "-column_alias : " + column_alias);
                this.final_column_name=this.column_alias;
            }
        }
        System.out.println(addSpase()+"}");
    }
}
