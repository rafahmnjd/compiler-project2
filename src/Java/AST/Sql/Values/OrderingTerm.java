package Java.AST.Sql.Values;

import Java.AST.Expression.Expression;
import Java.AST.Node;

public class OrderingTerm extends Node implements SQLValue{
    Expression expression;
    boolean DESC=false;         //ASC by default.

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public boolean isDESC() {
        return DESC;
    }

    public void setDESC(boolean DESC) {
        this.DESC = DESC;
    }

}
