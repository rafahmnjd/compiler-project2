package Java.AST;

import Java.AST.Visitor.ASTVisitor;

public class Node {

    private int line;
    private int col;
    private String s;


    public String getS() {
        return s;
    }
    public void setS(String s) {
        this.s =this.s+ s;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    protected int level; //tree level
    public void setLine(int line) {
        this.line = line;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public int getLine() {
        return this.line;
    }

    public int getCol() {
        return this.col;
    }


    public  String addSpase(){
        String s="";
        int l = this.level;
        while(l>0){
            s=s+"   ";
            l--;
        }
        return s;
    }
     public void accept(ASTVisitor astVisitor, int level){

     }

}
