package Java.AST.Defenations_and_Values;

import Java.AST.Defenations_and_Values.Values.Value;
import Java.AST.Defenations_and_Values.Values.javavalue.Java_value;
import Java.AST.Java_Stmt.Java_stmt;
import Java.AST.Sql.QueryStmt.SelectStmt;
import Java.AST.Visitor.ASTVisitor;
import Java.Main;
import Java.SymbolTable.Symbol;
import Java.SymbolTable.Type;

public class Variable extends Java_value implements Java_stmt {
    String var_name=null;
    Value value=null;
    SelectStmt selectStmt;
    Variable father=null; //father of variable json: (v.x)


    Symbol mySymbol=null;
    boolean exist=false;

    //index
    // Num_exp index=null;//array-jsonObject  index
//    Java_value java_value=null; //array-jsonObject  index


    public SelectStmt getSelectStmt() {
        return selectStmt;
    }
    public void setSelectStmt(SelectStmt selectStmt) {
        this.selectStmt = selectStmt;
    }

    public String getVar_name() {
        return var_name;
    }
    public void setVar_name(String var_name) {
        this.var_name = var_name;
    }

    public Value getValue() {
        return value;
    }
    public void setValue(Value value) {
        this.value = value;
    }

    public Variable getFather() { return father;  }
    public void setFather(Variable father) { this.father = father; }


    public boolean isExist() {
        return exist;
    }
    public void setExist(boolean exist) {
        this.exist = exist;
    }

    public void setMySymbol(Symbol mySymbol) {
        this.mySymbol = mySymbol;
    }
    public Symbol getMySymbol() {
        return mySymbol;
    }


/*
    public Variable getProperty() {  return property; }
    public void setProperty(Variable property) {   this.property = property; }
//    public void setArrayType(boolean arrayType) { this.arrayType = arrayType;  }

//    public Java_value getJava_value() {
//        return java_value;
//    }
//    public void setJava_value(Java_value java_value) {
//        this.java_value = java_value;
//    }
//    public Num_exp getIndex() {
//        return index;
//    }
//    public void setIndex(Num_exp index) {
//        this.index = index;
//    }


//    public boolean isArrayType() {
//        return arrayType;
//    }
*/

    @Override
    public void accept(ASTVisitor astVisitor,  int level){
        this.level=level;
        System.out.println(this.addSpase() + "variable :{");

        if(var_name!=null) {
            if (father != null) {
                System.out.println(this.addSpase() + " -father:{");
                this.father.accept(astVisitor,level + 1);
                System.out.println(this.addSpase() + "}");
                System.out.println(this.addSpase() + " -expression : .");
                System.out.println(this.addSpase() + " -property :");
                System.out.println(this.addSpase() + " -name : " + this.var_name);
                System.out.println(this.addSpase() + "}");
            } else
                System.out.println(this.addSpase() + " -name : " + this.var_name);
        }
        if(this.value!=null){
            System.out.println(this.addSpase()+ "   -value : " + this.value.getValueType());
        }
        if(this.selectStmt!=null){
            this.selectStmt.accept(astVisitor,level+1);
            this.getMySymbol().setType(this.selectStmt.getType());
            this.getMySymbol().getType().setName(this.var_name);
            this.setValueType(this.mySymbol.getType().getName());
            Main.symbolTable.addType(this.mySymbol.getType());
        }
        System.out.println(this.addSpase()+ "}");
    }

    @Override
    public String toString() { return this.value.toString(); }

    @Override
    public void javaStmtAccept(ASTVisitor astVisitor, int level) {
            this.accept(astVisitor,level);
    }
}
