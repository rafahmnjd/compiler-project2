package Java.AST.Defenations_and_Values.Values;

import Java.AST.Node;
import Java.SymbolTable.Type;

abstract public class Value extends Node {
public String valueType= Type.NULL_CONST;


    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

}
