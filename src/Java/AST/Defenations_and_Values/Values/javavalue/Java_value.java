package Java.AST.Defenations_and_Values.Values.javavalue;

import Java.AST.Defenations_and_Values.Values.Value;
import Java.AST.Sql.Values.SignedNumber;
import Java.AST.Visitor.ASTVisitor;

public class Java_value extends Value {

    String stringVal = null;
    Boolean booleanVal = null;
    SignedNumber num_val=null;

    public SignedNumber getNum_val() {
        return num_val;
    }

    public void setNum_val(SignedNumber num_val) {
        this.num_val = num_val;
    }

    public String getStringVal() {
        return stringVal;
    }

    public void setStringVal(String stringVal) {
        this.stringVal = stringVal;
    }

    public Boolean getBooleanVal() {
        return booleanVal;
    }

    public void setBooleanVal(Boolean booleanVal) {
        this.booleanVal = booleanVal;
    }


    @Override
    public void accept(ASTVisitor astVisitor, int level) {

        this.level = level;
        if (this.booleanVal != null) {
            System.out.print("\n" + addSpase() + "boolean value:");
            System.out.print(this.booleanVal.toString() + "\n");
        }
        if(this.stringVal!=null){
            System.out.print("\n" + addSpase() + "String value:");
            System.out.print(this.stringVal + "\n");
        }


    }

}
