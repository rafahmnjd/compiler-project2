package Java.AST.Defenations_and_Values.Values.javavalue;

import Java.AST.Node;
import Java.AST.Sql.Values.SignedNumber;
import Java.AST.Visitor.ASTVisitor;

public class Num_val extends Node {
    Double numeric_literal=null;
    SignedNumber signed_number=null;

    public SignedNumber getSigned_number() {
        return signed_number;
    }

    public void setSigned_number(SignedNumber signed_number) {
        this.signed_number = signed_number;
    }

    public double getNumeric_literal() {
        return numeric_literal;
    }

    public void setNumeric_literal(double numeric_literal) {
        this.numeric_literal = numeric_literal;
    }

    @Override
    public String toString() {
        if(numeric_literal!=null)
            return numeric_literal.toString();
         if(signed_number!=null)
             return signed_number.toString();
         return "null";
    }
    @Override
    public void accept(ASTVisitor astVisitor,  int level) {
        this.level=level;
        System.out.println(addSpase()+"Num value:");
        if(numeric_literal!=null)
            System.out.println(addSpase()+"   -type = numerical_literal");
        else
            System.out.println(addSpase()+"   -type = signed_number");
        System.out.println(addSpase()+"   -value = "+this.toString());

    }

}
