package Java.AST.Defenations_and_Values.Values.Json;

import Java.AST.Defenations_and_Values.Values.Value;
import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;

public class Json_array extends Value {
//    List<Java_value> java_values=new ArrayList<>();
//    List<Json_container>json_containers=new ArrayList<>();
//
    public void add_java_value(Value java_value){
       // java_values.add(java_value);
        values.add(java_value);
    }
//    public void add_json_container(Json_container json_container){
//        json_containers.add(json_container);
//    }
    ArrayList<Value> values=null;

    public ArrayList<Value> getValues() { return values; }
//    public void setValues(ArrayList<Value> values) { this.values = values; }


    @Override
    public void accept(ASTVisitor astVisitor, int level) {
        this.level=level;
        System.out.println(this.addSpase()+"json array: [");
        for (int v=0;v<values.size();v++){
            System.out.println(this.addSpase()+"   -index : "+v);
            System.out.println(this.addSpase()+"   -value = {");
            values.get(v).accept(astVisitor,level+2);
            System.out.println(addSpase()+"    }");
        }
        System.out.println(addSpase()+"]");
    }
}
