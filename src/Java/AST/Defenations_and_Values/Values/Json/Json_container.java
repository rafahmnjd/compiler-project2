package Java.AST.Defenations_and_Values.Values.Json;

import Java.AST.Defenations_and_Values.Values.Value;
import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Json_container extends Value {
    ArrayList<String> var_name=new ArrayList<>();
    Map<Integer,Value> java_valueMap=new HashMap<>();
/*    Map<Integer,Java_value> java_valueMap;
    Map<Integer,Json_container>json_containerMap;
    Map<Integer,Json_array>json_arrayMap;*/


    public ArrayList<String> getVar_name() {
        return var_name;
    }
    public void add_var_name(String name){
        var_name.add(name);
    }
    public  void add_java_value(int i, Value j){
        if(java_valueMap==null)
            java_valueMap=new HashMap<>();
        this.java_valueMap.put(new Integer(i),j);
    }
/*    public  void add_jsonArray(int i, Json_array j){
        if(json_arrayMap==null)
            json_arrayMap=new HashMap<>();
        this.json_arrayMap.put(new Integer(i),j);
    }
    public  void add_json_container(int i, Json_container j){
        if(json_containerMap==null)
            json_containerMap=new HashMap<>();
        this.json_containerMap.put(new Integer(i),j);
    }*/
/*    public void setVar_name(ArrayList<String> var_name) {
        this.var_name = var_name;
    }

    public Map<String, Java_value> getJava_valueMap() {
        return java_valueMap;
    }
    public void setJava_valueMap(Map<String, Java_value> java_valueMap) {
        this.java_valueMap = java_valueMap;
    }
    public Map<String, Json_container> getJson_containerMap() {
        return json_containerMap;
    }
    public void setJson_containerMap(Map<String, Json_container> json_containerMap) {
        this.json_containerMap = json_containerMap;
    }
    public Map<String, Json_array> getJson_arrayMap() {
        return json_arrayMap;
    }
    public void setJson_arrayMap(Map<String, Json_array> json_arrayMap) {
        this.json_arrayMap = json_arrayMap;
    }
    */
/*
    ArrayList<Java_value> java_valueList=new ArrayList<>();
    ArrayList<Json_container> json_containerList=new ArrayList<>();
    ArrayList<Json_array>json_arrayList=new ArrayList<>();



    public void add_java_value(int indx,Java_value java_value){
        java_valueList.add(indx,java_value);
    }
    public void add_json_container(int indx,Json_container json_container){
        json_containerList.add(indx,json_container);
    }
    public void add_json_array(int indx,Json_array json_array){
        json_arrayList.add(indx,json_array);
    }
*/
@Override
public void accept(ASTVisitor astVisitor, int level) {
    this.level=level;
    System.out.println(this.addSpase()+"json array: {");
    for (int v=0;v<var_name.size();v++){
        System.out.println(this.addSpase()+"   -index = "+var_name.get(v));
        System.out.println(this.addSpase()+"   -value = {");
        java_valueMap.get(v).accept(astVisitor,level+2);
        System.out.println(addSpase()+"    }");
    }
    System.out.println(addSpase()+"}");
}
}
