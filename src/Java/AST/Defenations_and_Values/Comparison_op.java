package Java.AST.Defenations_and_Values;

import Java.AST.Node;

public class Comparison_op extends Node {
    boolean lt, gt, gt_eq, lt_eq;

    public boolean isLt() {
        return lt;
    }

    public void setLt(boolean lt) {
        this.lt = lt;
    }

    public boolean isGt() {
        return gt;
    }

    public void setGt(boolean gt) {
        this.gt = gt;
    }

    public boolean isGt_eq() {
        return gt_eq;
    }

    public void setGt_eq(boolean gt_eq) {
        this.gt_eq = gt_eq;
    }

    public boolean isLt_eq() {
        return lt_eq;
    }

    public void setLt_eq(boolean lt_eq) {
        this.lt_eq = lt_eq;
    }
}
