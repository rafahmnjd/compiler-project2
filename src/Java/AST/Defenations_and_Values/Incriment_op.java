package Java.AST.Defenations_and_Values;

import Java.AST.Node;

public class Incriment_op extends Node {
    boolean minusminus , plusplus;

    public boolean isMinusminus() {
        return minusminus;
    }

    public void setMinusminus(boolean minusminus) {
        this.minusminus = minusminus;
    }

    public boolean isPlusplus() {
        return plusplus;
    }

    public void setPlusplus(boolean plusplus) {
        this.plusplus = plusplus;
    }
}
