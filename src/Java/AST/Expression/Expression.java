package Java.AST.Expression;

import Java.AST.Node;
import Java.AST.Sql.QueryStmt.SelectStmt;
import Java.AST.Sql.Values.LiteralValue;
import Java.AST.Visitor.ASTVisitor;
import Java.Main;
import Java.SymbolTable.Type;

public class Expression extends Node {

    private LiteralValue literal_value;
    private String dataBaseName=null,
            table_name=null,column_name=null;
    private boolean not,exists;
    private SelectStmt selectStmt;
    protected Type type=new Type();

    public Type getType() {
        return type;
    }
    public void setType(Type type) {
        this.type = type;
    }

    public String getDataBaseName() {
        return dataBaseName;
    }
    public void setDataBaseName(String dataBaseName) {
        this.dataBaseName = dataBaseName;
    }

    public String getTable_name() {
        return table_name;
    }
    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public String getColumn_name() {
        return column_name;
    }
    public void setColumn_name(String column_name) {
        this.column_name = column_name;
    }


    public LiteralValue getLiteral_value() {
        return literal_value;
    }
    public void setLiteral_value(LiteralValue literal_value) {
        this.literal_value = literal_value;
    }


    public boolean isNot() {
        return not;
    }
    public void setNot(boolean not) {
        this.not = not;
    }

    public boolean isExists() {
        return exists;
    }
    public void setExists(boolean exists) {
        this.exists = exists;
    }

    public SelectStmt getSelectStmt() {
        return selectStmt;
    }
    public void setSelectStmt(SelectStmt selectStmt) {
        this.selectStmt = selectStmt;
    }

    @Override
    public void accept(ASTVisitor astVisitor,int l) {
        this.level=l;
//        astVisitor.visit(this);
        System.out.println(addSpase()+"visit Expr:");
        if(this.literal_value!=null) {
            this.literal_value.accept(astVisitor, l + 1);
            this.type=this.literal_value.getType();
        }
        else if(this.selectStmt !=null) {
            this.selectStmt.accept(astVisitor, l + 1);
            this.type=this.selectStmt.getType();
            this.selectStmt.accept(astVisitor,level+1);
                this.type=this.selectStmt.getType();
            this.type.addNamePrev("selectStmt"+Main.symbolTable.getStmtIdCounter());
            Main.symbolTable.addType(this.type);
        }
        if(column_name!=null){
            System.out.println(addSpase()+"     columnName = "+column_name);
            if(table_name!=null){
                System.out.println(addSpase()+"     tableName = "+table_name);
            }
            this.type= new Type(Type.RESULT_COL_CONST);
//            astVisitor.visit(this);
        }
    }
}
