package Java.AST.Expression;

import Java.AST.Visitor.ASTVisitor;

public class BinaryExpression extends Expression{
    String operator;
   // EnumMap<ExprOperator,Boolean> opMap=new EnumMap<ExprOperator, Boolean>(ExprOperator.class);
    Expression rightExpression,leftExpression;
    public Expression getRightExpression() {
        return rightExpression;
    }

    public void setRightExpression(Expression rightExpression) {
        this.rightExpression = rightExpression;
    }

    public Expression getLeftExpression() {
        return leftExpression;
    }

    public void setLeftExpression(Expression leftExpression) {
        this.leftExpression = leftExpression;
    }


    public void setOperator(String operator) {
        this.operator=operator;
    }

    public String getOperator() {
        return operator;
    }

    @Override
    public void accept(ASTVisitor astVisitor,int l) {
       // astVisitor.visit(this,l+1);
        if (this.leftExpression!=null) {
            for(int lx=0;lx<=l;lx++)
                System.out.print("  ");
            System.out.print("left expr:{\n ");
            this.leftExpression.accept(astVisitor,l+1);
            for(int lx=0;lx<l;lx++)
                System.out.print("  ");
            System.out.print("}\n");
        }
        if (this.rightExpression!=null) {
            for(int lx=0;lx<l;lx++)
                System.out.print("  ");
            System.out.print("right expr:{\n ");
            this.rightExpression.accept(astVisitor,l+1);
            for(int lx=0;lx<l;lx++)
                System.out.print("  ");
            System.out.print("}\n");
        }
    }
}
