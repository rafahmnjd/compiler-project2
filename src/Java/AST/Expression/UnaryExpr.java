package Java.AST.Expression;

import Java.AST.Visitor.ASTVisitor;

public class UnaryExpr extends  Expression {
    boolean plus,minus,not, tilde;
    Expression expr;
    String operator="";

    public boolean isPlus() {
        return plus;
    }

    public void setPlus(boolean plus) {
        this.plus = plus;
    }

    public boolean isMinus() {
        return minus;
    }

    public void setMinus(boolean minus) {
        this.minus = minus;
    }

    public boolean isNot() {
        return not;
    }

    public void setNot(boolean not) {
        this.not = not;
    }

    public boolean isTilde() {
        return tilde;
    }

    public void setTilde(boolean tilde) {
        this.tilde = tilde;
    }

    public Expression getExpr() {
        return expr;
    }

    public void setExpr(Expression expr) {
        this.expr = expr;
    }

    public void setOperator() {
        if(plus)
            operator="+";
        if(minus)
            operator="-";
        if(not)
            operator="NOT";
        if(tilde)
            operator="~";
        //super.setOperator();
    }
    public  String getOperator(){
        return operator;
    }

    @Override
    public void accept(ASTVisitor astVisitor,int l) {
      //  astVisitor.visit(this,l+1);
        if (this.expr!=null){
            System.out.println();
                this.expr.accept(astVisitor,l+1);
        }
    }
}
