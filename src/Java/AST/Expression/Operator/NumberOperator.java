package Java.AST.Expression.Operator;

public enum NumberOperator {
    STAR("*"),DIV("/"),MOD("%"),                // '*' , '/' , '%'
    PLUS("+"),MINUS("-"),                                   // '+' ,'-'
    LT2("<<"),GT2(">>"),PIPE("|"),AMP("&"),    //'<<','>>','|','&'
    LT("<"),LT_EQ("<="),GT(">"),GT_EQ(">=");

    private String value;
    NumberOperator(String value){
        this.value =value;
    }
    public String getValue() {
        return value;
    }
}
