package Java.AST.Expression.Operator;

public enum ExprOperator {
    PIPE2("||"),AMP2("&&"),                 // '||','&&'
    STAR("*"),DIV("/"),MOD("%"),                // '*' , '/' , '%'
    PLUS("+"),MINUS("-"),                                   // '+' ,'-'
    LT2("<<"),GT2(">>"),PIPE("|"),AMP("&"),    //'<<','>>','|','&'
    LT("<"),LT_EQ("<="),GT(">"),GT_EQ(">="),                   //'<','<=','>','>='
    ASSIGN("="),EQ("=="),NOT_EQ1("!="),NOT_EQ2("<>"),       //'=','==','!=','<>'
    IS ("IS"), IS_NOT("IS_NOT") ,IN("IN") ,LIKE("LIKE") ,GLOB("GLOB") , MATCH("MATCH") ,REGEXP("REGEXP"),
    AND("AND"),OR("OR");
    private String value;
    ExprOperator(String value){
        this.value =value;
    }

    public String getValue() {
        return value;
    }

}
