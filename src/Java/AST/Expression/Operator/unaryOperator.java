package Java.AST.Expression.Operator;

public class unaryOperator extends Operator {
    boolean plus,minus,not, tilde;

    public boolean  isPlus(){ return plus; }
    public void     setPlus(boolean plus){ this.plus = plus; }
    public boolean  isMinus(){ return minus; }
    public void     setMinus(boolean minus) { this.minus = minus; }
    public boolean  isNot() { return not; }
    public void     setNot(boolean not) { this.not = not; }
    public boolean  isTilde() { return tilde;}
    public void     setTilde(boolean tilde) { this.tilde = tilde; }


    @Override
    public void setOperator() {
             if(plus)   operator="+";
        else if(minus)  operator="-";
        else if(not)    operator="NOT";
        else if(tilde)  operator="~";
    }

    @Override
    public String getOperator() { return operator; }
}
