package Java.AST.Expression.JavaExpr;

import Java.AST.Defenations_and_Values.Values.Value;
import Java.AST.Defenations_and_Values.Variable;
import Java.AST.Functions.Function_call;
import Java.AST.Visitor.ASTVisitor;

public class Condition_exp extends javaExpr {
    Function_call function_call;
    Variable boolean_var;
    boolean boolean_val;
    String operator;
    Condition_exp right_cond,left_cond;

    Value rightSide=null,leftSide=null;

    public Value getRightSide() {
        return rightSide;
    }

    public void setRightSide(Value rightSide) {
        this.rightSide = rightSide;
    }

    public Value getLeftSide() {
        return leftSide;
    }

    public void setLeftSide(Value leftSide) {
        this.leftSide = leftSide;
    }

    //    Num_exp right_num,left_num;
//    Java_value right_java_value,left_java_value;

    public Function_call getFunction_call() {
        return function_call;
    }

    public void setFunction_call(Function_call function_call) {
        this.function_call = function_call;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Variable getBoolean_var() {
        return boolean_var;
    }

    public void setBoolean_var(Variable boolean_var) {
        this.boolean_var = boolean_var;
    }

    public boolean getBoolean_val() {
        return boolean_val;
    }

    public void setBoolean_val(boolean boolean_val) {
        this.boolean_val = boolean_val;
    }

    public Condition_exp getRight_cond() {
        return right_cond;
    }

    public void setRight_cond(Condition_exp right_cond) {
        this.right_cond = right_cond;
    }

    public Condition_exp getLeft_cond() {
        return left_cond;
    }

    public void setLeft_cond(Condition_exp left_cond) {
        this.left_cond = left_cond;
    }

/*
    public Num_exp getRight_num() {
        return right_num;
    }

    public void setRight_num(Num_exp right_num) {
        this.right_num = right_num;
    }

    public Num_exp getLeft_num() {
        return left_num;
    }

    public void setLeft_num(Num_exp left_num) {
        this.left_num = left_num;
    }

    public Java_value getRight_java_value() {
        return right_java_value;
    }

    public void setRight_java_value(Java_value right_java_value) {
        this.right_java_value = right_java_value;
    }

    public Java_value getLeft_java_value() {
        return left_java_value;
    }

    public void setLeft_java_value(Java_value left_java_value) {
        this.left_java_value = left_java_value;
    }
*/

    @Override
    public void accept(ASTVisitor astVisitor, int level) {
        this.level=level;
        astVisitor.visit(this);
    }
}
