package Java.AST.Expression.JavaExpr;

import Java.AST.Defenations_and_Values.Incriment_op;
import Java.AST.Defenations_and_Values.Values.Value;
import Java.AST.Defenations_and_Values.Variable;
import Java.AST.Visitor.ASTVisitor;

public class Num_exp extends javaExpr {

    Value value;
//    SignedNumber num_val;
//    Function_call function_call;
    Num_exp right ,left;
    Variable numVar;
    String operator;
    Incriment_op incriment_op;
    boolean preIncOp=false;



    public Value getValue() {
        return value;
    }
    public void setValue(Value value) {
        this.value = value;
    }
    public boolean isPreIncOp() {
        return preIncOp;
    }
    public void setPreIncOp(boolean preIncOp) {
        this.preIncOp = preIncOp;
    }
    public Variable getNumVar() {
        return numVar;
    }
    public void setNumVar(Variable numVar) {
        this.numVar = numVar;
    }
/*    public SignedNumber getNum_val() {
        return num_val;
    }
    public void setNum_val(SignedNumber num_val) {
        this.num_val = num_val;
    }*/
    public Incriment_op getIncriment_op() {
        return incriment_op;
    }
    public void setIncriment_op(Incriment_op incriment_op) {
        this.incriment_op = incriment_op;
    }
/*    public Function_call getFunction_call() {
        return function_call;
    }
    public void setFunction_call(Function_call function_call) {
        this.function_call = function_call;
    }*/
    public Num_exp getRight() {
        return right;
    }
    public void setRight(Num_exp right) {
        this.right = right;
    }
    public Num_exp getLeft() {
        return left;
    }
    public void setLeft(Num_exp left) {
        this.left = left;
    }
    public String getOperator() {
        return operator;
    }
    public void setOperator(String operator) {
        this.operator = operator;
    }



    @Override
    public void accept(ASTVisitor astVisitor, int level) {
        this.level=level;
        astVisitor.visit(this);
    }
}
