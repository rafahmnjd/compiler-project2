package Java.AST.Expression;

import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.AggregationFunction;
import Java.SymbolTable.Type;


import java.util.List;

public class FunctionExpression extends  Expression {
    String functionName="";
    boolean K_DISTINCT,star;
    List<Expression> expressions;
//    AggregationFunction aggregationFunction;


//    public AggregationFunction getAggregationFunction() {
//        return aggregationFunction;
//    }

//    public void setAggregationFunction(AggregationFunction aggregationFunction) {
//        this.aggregationFunction = aggregationFunction;
//    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public boolean isK_DISTINCT() {
        return K_DISTINCT;
    }

    public void setK_DISTINCT(boolean k_DISTINCT) {
        K_DISTINCT = k_DISTINCT;
    }

    public boolean isStar() {
        return star;
    }

    public void setStar(boolean star) {
        this.star = star;
    }

    public List<Expression> getExpressions() {
        return expressions;
    }

    public void setExpressions(List<Expression> expressions) {
        this.expressions = expressions;
    }

    @Override
    public void accept(ASTVisitor astVisitor,int l) {
        this.level=l;
        astVisitor.visit(this);
        if (this.expressions!=null){
            System.out.println(addSpase()+"expressions:{");
            for(int e=0;e<this.expressions.size();e++){
                System.out.print(addSpase()+"  expr"+e+" :\n");
                this.expressions.get(e).accept(astVisitor,l+3);
            }
            System.out.print(addSpase()+"}\n");
        }



    }
}
