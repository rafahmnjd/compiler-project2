package Java.AST.Expression;

import Java.AST.Sql.QueryStmt.SelectStmt;
import Java.AST.Visitor.ASTVisitor;

import java.util.List;

public class ExpressionIn extends Expression{

    //String type="";


    boolean NotIn;
    Expression left;
    List<Expression> rightExpressions;
    SelectStmt selectStmt;
    String databaseName, tableName;


    public Expression getLeft() {
        return left;
    }

    public void setLeft(Expression left) {
        this.left = left;
    }

    public boolean isNotIn() {
        return NotIn;
    }

    public void setNotIn(boolean notIn) {
        NotIn = notIn;
    }

    public List<Expression> getRightExpressions() {
        return rightExpressions;
    }

    public void setRightExpressions(List<Expression> rightExpressions) {
        this.rightExpressions = rightExpressions;
    }

    public SelectStmt getSelectStmt() {
        return selectStmt;
    }

    public void setSelectStmt(SelectStmt selectStmt) {
        this.selectStmt = selectStmt;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public void accept(ASTVisitor astVisitor,int l) {
      //  astVisitor.visit(this,l+1);
        if(this.left!=null)
            this.left.accept(astVisitor,l+1);
        if(isNotIn())
        {
            System.out.println( "expr not in expr");
        }
        else{
            System.out.println("expr in expr");
        }
        if (this.rightExpressions!=null){
            for(int re=0;re<this.rightExpressions.size();re++){
                this.rightExpressions.get(re).accept(astVisitor,l);
            }
        }
        if (this.selectStmt !=null);
        //    this.sellectStmt.accept(astVisitor,l+1);

    }

/*    public String getType() {
        return  type;
    }*/
}
