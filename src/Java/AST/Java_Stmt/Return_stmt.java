package Java.AST.Java_Stmt;

import Java.AST.Defenations_and_Values.Values.Value;
import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Scope;
import Java.SymbolTable.Type;

public class Return_stmt extends Node implements Java_stmt {
    Value value;
    Scope parent;

    public Scope getParent() {
        return parent;
    }

    public void setParent(Scope parent) {
        this.parent = parent;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    @Override
    public void accept(ASTVisitor astVisitor, int level) {
        this.level=level;
        astVisitor.visit(this);
    }

    @Override
    public void javaStmtAccept(ASTVisitor astVisitor, int level) {

    }
}
