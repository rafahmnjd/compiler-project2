package Java.AST.Java_Stmt;

import Java.AST.Defenations_and_Values.Values.javavalue.Java_value;
import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class Print extends Node  implements Java_stmt{
    List<Java_value> java_values=new ArrayList<>();

    public void add_Java_value(Java_value java_value){
        java_values.add(java_value);
    }

    public List<Java_value> getJava_values() {
        return java_values;
    }

    public void setJava_values(List<Java_value> java_values) {
        this.java_values = java_values;
    }

    @Override
    public void javaStmtAccept(ASTVisitor astVisitor, int level) {

    }
}
