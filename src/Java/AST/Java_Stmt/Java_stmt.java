package Java.AST.Java_Stmt;

import Java.AST.Visitor.ASTVisitor;

public interface Java_stmt {
    public void javaStmtAccept(ASTVisitor astVisitor,int level);

}
