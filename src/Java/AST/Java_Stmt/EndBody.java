package Java.AST.Java_Stmt;

import Java.AST.Node;

public class EndBody extends Node {

    boolean k_break,k_continue;
    Return_stmt return_stmt=null;

    public boolean isK_break() {
        return k_break;
    }

    public void setK_break(boolean k_break) {
        this.k_break = k_break;
    }

    public boolean isK_continue() {
        return k_continue;
    }

    public void setK_continue(boolean k_continue) {
        this.k_continue = k_continue;
    }

    public Return_stmt getReturn_stmt() {
        return return_stmt;
    }

    public void setReturn_stmt(Return_stmt return_stmt) {
        this.return_stmt = return_stmt;
    }
}
