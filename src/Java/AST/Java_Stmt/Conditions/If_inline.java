package Java.AST.Java_Stmt.Conditions;

import Java.AST.Defenations_and_Values.Values.Value;
import Java.AST.Expression.JavaExpr.Condition_exp;
import Java.AST.Java_Stmt.Java_stmt;
import Java.AST.Visitor.ASTVisitor;

public class If_inline extends Value implements Java_stmt {

    Condition_exp conditionExp;
    Value rightValue, leftValue;


    public Condition_exp getConditionExp() {
        return conditionExp;
    }

    public void setConditionExp(Condition_exp conditionExp) {
        this.conditionExp = conditionExp;
    }

    public Value getRightValue() {
        return rightValue;
    }

    public void setRightValue(Value rightValue) {
        this.rightValue = rightValue;
    }

    public Value getLeftValue() {
        return leftValue;
    }

    public void setLeftValue(Value leftValue) {
        this.leftValue = leftValue;
    }

    void test(){
        int x=0,y=9,z=20;
//         x==20? return x : return y;

    }

    @Override
    public void javaStmtAccept(ASTVisitor astVisitor, int level) {

    }
}
