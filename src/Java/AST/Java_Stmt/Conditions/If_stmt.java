package Java.AST.Java_Stmt.Conditions;

import Java.AST.Expression.JavaExpr.Condition_exp;
import Java.AST.Java_Stmt.BaseBody;
import Java.AST.Java_Stmt.Java_stmt;
import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Scope;

import java.util.ArrayList;
import java.util.List;

public class If_stmt extends Node implements Java_stmt{
    Condition_exp condition_exp;
    BaseBody baseBody=new BaseBody();
    BaseBody elseStmt;
    List<ElseIf> elseIfStmts=new ArrayList<>();
    Scope scope=null, elseScope=null;

    public BaseBody getBaseBody() {
        return baseBody;
    }

    public void setBaseBody(BaseBody baseBody) {
        this.baseBody = baseBody;
    }

    public Condition_exp getCondition_exp() {
        return condition_exp;
    }

    public void setCondition_exp(Condition_exp condition_exp) {
        this.condition_exp = condition_exp;
    }

    public BaseBody getElseStmt() {
        return elseStmt;
    }

    public void setElseStmt(BaseBody elseStmt) {
        this.elseStmt = elseStmt;
    }

    public List<ElseIf> getElseIfStmts() {
        return elseIfStmts;
    }

    public void setElseIfStmts(List<ElseIf> elseIfStmts) {
        this.elseIfStmts = elseIfStmts;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public void setElseScope(Scope elseScope) {
        this.elseScope=elseScope;
    }

    @Override
    public void javaStmtAccept(ASTVisitor astVisitor, int level) {

    }
}
