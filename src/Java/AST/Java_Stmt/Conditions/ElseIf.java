package Java.AST.Java_Stmt.Conditions;

import Java.AST.Expression.JavaExpr.Condition_exp;
import Java.AST.Java_Stmt.BaseBody;
import Java.AST.Node;
import Java.SymbolTable.Scope;

public class ElseIf extends Node {
    Condition_exp condition_exp;
    BaseBody baseBody=new BaseBody();
    Scope scope=null;

    public Condition_exp getCondition_exp() {
        return condition_exp;
    }

    public void setCondition_exp(Condition_exp condition_exp) {
        this.condition_exp = condition_exp;
    }

    public BaseBody getBaseBody() {
        return baseBody;
    }

    public void setBaseBody(BaseBody baseBody) {
        this.baseBody = baseBody;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }
}
