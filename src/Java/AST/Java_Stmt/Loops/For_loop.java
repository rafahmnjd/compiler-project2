package Java.AST.Java_Stmt.Loops;

import Java.AST.Defenations_and_Values.Comparison_op;
import Java.AST.Defenations_and_Values.Num_op;
import Java.AST.Defenations_and_Values.Variable;
import Java.AST.Expression.JavaExpr.Num_exp;
import Java.AST.Java_Stmt.Java_stmt;
import Java.AST.Visitor.ASTVisitor;

public class For_loop extends For_stmt  {
    Variable loopVar/*,loopCondition,loopCounter*/;
    //header operations
    Num_exp startValue,endValue,updateValue;
    Comparison_op comparison_op;
    //update exp //+= , -= , *= , /=;
    Num_op num_op;


/*    Body body;
    boolean k_break,k_continue;
    Return_stmt return_stmt;
    Java_stmt java_stmt;*/


    public Variable getLoopVar() {
        return loopVar;
    }

    public void setLoopVar(Variable loopVar) {
        this.loopVar = loopVar;
    }
/*
    public Variable getLoopCondition() {
        return loopCondition;
    }

    public void setLoopCondition(Variable loopCondition) {
        this.loopCondition = loopCondition;
    }

    public Variable getLoopCounter() {
        return loopCounter;
    }

    public void setLoopCounter(Variable loopCounter) {
        this.loopCounter = loopCounter;
    }*/

    public Num_exp getStartValue() {
        return startValue;
    }

    public void setStartValue(Num_exp startValue) {
        this.startValue = startValue;
    }

    public Num_exp getEndValue() {
        return endValue;
    }

    public void setEndValue(Num_exp endValue) {
        this.endValue = endValue;
    }

    public Num_exp getUpdateValue() {
        return updateValue;
    }

    public void setUpdateValue(Num_exp updateValue) {
        this.updateValue = updateValue;
    }

    public Comparison_op getComparison_op() {
        return comparison_op;
    }

    public void setComparison_op(Comparison_op comparison_op) {
        this.comparison_op = comparison_op;
    }

    public Num_op getNum_op() {
        return num_op;
    }

    public void setNum_op(Num_op num_op) {
        this.num_op = num_op;
    }

    public  void test(){
        int x=0;
        for(int i=0;i<90;i=9){
            i+=10;
        }
    }
    @Override
    public void javaStmtAccept(ASTVisitor astVisitor, int level) {
        this.accept(astVisitor,level);
    }

    @Override
    public void accept(ASTVisitor astVisitor, int level) {
        if(startValue!=null) {}
            if(endValue!=null){}
                if(updateValue!=null){}
                if(this.baseBody!=null){
                    baseBody.accept(astVisitor,level+1);
                }
    }
}
