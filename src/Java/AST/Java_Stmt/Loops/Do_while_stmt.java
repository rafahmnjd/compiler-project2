package Java.AST.Java_Stmt.Loops;

import Java.AST.Expression.JavaExpr.Condition_exp;
import Java.AST.Java_Stmt.BaseBody;
import Java.AST.Java_Stmt.Java_stmt;
import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Scope;

public class Do_while_stmt extends Node implements Java_stmt{
    Condition_exp condition_exp;
    BaseBody body=new BaseBody();
    Scope doWhileScope;

    public Condition_exp getCondition_exp() {
        return condition_exp;
    }

    public void setCondition_exp(Condition_exp condition_exp) {
        this.condition_exp = condition_exp;
    }

    public BaseBody getBody() {
        return body;
    }

    public void setBody(BaseBody body) {
        this.body = body;
    }


    public void setScope(Scope doWhileScope) {
        this.doWhileScope=doWhileScope;
    }
    public Scope getScope(){
        return doWhileScope;
    }

    @Override
    public void javaStmtAccept(ASTVisitor astVisitor, int level) {

    }
}
