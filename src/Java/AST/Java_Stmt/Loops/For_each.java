package Java.AST.Java_Stmt.Loops;

import Java.AST.Defenations_and_Values.Variable;
import Java.AST.Java_Stmt.Java_stmt;

public class For_each extends For_stmt {
    Variable firstVar;
    Variable variable;
    boolean k_var;

    public boolean isK_var() {
        return k_var;
    }

    public void setK_var(boolean k_var) {
        this.k_var = k_var;
    }

    public Variable getFirstVar() {
        return firstVar;
    }

    public void setFirstVar(Variable var_name) {
        this.firstVar = var_name;
    }

    public Variable getVariable() {
        return variable;
    }

    public void setVariable(Variable variable) {
        this.variable = variable;
    }
}
