package Java.AST.Java_Stmt.Loops;

import Java.AST.Java_Stmt.BaseBody;
import Java.AST.Java_Stmt.Java_stmt;
import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Scope;

public class For_stmt extends Node implements Java_stmt{

    BaseBody baseBody;
    Scope scope;

    public Scope getScope() {
        return scope;
    }
    public void setScope(Scope scope) {
        this.scope = scope;
    }
    public BaseBody getBaseBody() {
        return baseBody;
    }
    public void setBaseBody(BaseBody baseBody) {
        this.baseBody = baseBody;
    }

    @Override
    public void javaStmtAccept(ASTVisitor astVisitor, int level) {
        this.accept(astVisitor,level);
    }

}
