package Java.AST.Java_Stmt;

import Java.AST.Defenations_and_Values.Incriment_op;
import Java.AST.Defenations_and_Values.Variable;
import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;

public class Incriment_stmt extends Node implements Java_stmt {
    Variable variable;
    Incriment_op incriment_op;

    boolean pre_op=false; //v++ by default (++v has pre_op)

    public Variable getVariable() {
        return variable;
    }

    public void setVariable(Variable variable) {
        this.variable = variable;
    }

    public Incriment_op getIncriment_op() {
        return incriment_op;
    }

    public void setIncriment_op(Incriment_op incriment_op) {
        this.incriment_op = incriment_op;
    }

    @Override
    public void javaStmtAccept(ASTVisitor astVisitor, int level) {
        this.accept(astVisitor,level);
    }

    @Override
    public void accept(ASTVisitor astVisitor, int level) {

    }
}
