package Java.AST.Java_Stmt;

import Java.AST.Functions.Body;
import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;

public class BaseBody extends Node {
    Body body = null;
    EndBody endBody = null;
    Java_stmt java_stmt = null;


    public EndBody getEndBody() {
        return endBody;
    }

    public void setEndBody(EndBody endBody) {
        this.endBody = endBody;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Java_stmt getJava_stmt() {
        return java_stmt;
    }

    public void setJava_stmt(Java_stmt java_stmt) {
        this.java_stmt = java_stmt;
    }

    @Override
    public void accept(ASTVisitor astVisitor, int level) {

        this.level=level;

        if (java_stmt!=null){
            java_stmt.javaStmtAccept(astVisitor, level+1);
        }
        if(body!=null){
            body.accept(astVisitor,level+1);
        }
        if(endBody!=null)
            endBody.accept(astVisitor,level+1);
    }

}
