package Java.AST.Java_Stmt.Switch_Stmt;

import Java.AST.Defenations_and_Values.Variable;
import Java.AST.Expression.JavaExpr.Num_exp;
import Java.AST.Java_Stmt.BaseBody;
import Java.AST.Java_Stmt.Java_stmt;
import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Scope;

import java.util.ArrayList;
import java.util.List;

public class Switch_stmt extends Node implements Java_stmt {
    Variable variable;
    Num_exp num_exp;
    String string_val;
    Scope scope=null;
    List<Case_stmt> case_stmts=new ArrayList<>();
    BaseBody default_stmt;

    public void add_case_stmt(Case_stmt case_stmt){
        case_stmts.add(case_stmt);
    }

    public Variable getVariable() {
        return variable;
    }

    public void setVariable(Variable variable) {
        this.variable = variable;
    }

    public Num_exp getNum_exp() {
        return num_exp;
    }

    public void setNum_exp(Num_exp num_exp) {
        this.num_exp = num_exp;
    }

    public String getString_val() {
        return string_val;
    }

    public void setString_val(String string_val) {
        this.string_val = string_val;
    }

    public List<Case_stmt> getCase_stmts() {
        return case_stmts;
    }

    public void setCase_stmts(List<Case_stmt> case_stmts) {
        this.case_stmts = case_stmts;
    }

    public BaseBody getDefault_stmt() {
        return default_stmt;
    }

    public void setDefault_stmt(BaseBody default_stmt) {
        this.default_stmt = default_stmt;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    @Override
    public void javaStmtAccept(ASTVisitor astVisitor, int level) {

    }
}
