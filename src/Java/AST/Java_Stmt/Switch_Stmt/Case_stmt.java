package Java.AST.Java_Stmt.Switch_Stmt;

import Java.AST.Expression.JavaExpr.Num_exp;
import Java.AST.Java_Stmt.BaseBody;
import Java.AST.Node;

public class Case_stmt extends Node {
    Num_exp num_exp;
    String string_val;
    BaseBody baseBody=new BaseBody();

    String valueType;


    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public Num_exp getNum_exp() {
        return num_exp;
    }

    public void setNum_exp(Num_exp num_exp) {
        this.num_exp = num_exp;
    }

    public String getString_val() {
        return string_val;
    }

    public void setString_val(String string_val) {
        this.string_val = string_val;
    }

    public BaseBody getBaseBody() {
        return baseBody;
    }

    public void setBaseBody(BaseBody baseBody) {
        this.baseBody = baseBody;
    }
}
