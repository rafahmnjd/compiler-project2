package Java.AST.Java_Stmt.Switch_Stmt;

import Java.AST.Functions.Body;
import Java.AST.Java_Stmt.Return_stmt;
import Java.AST.Node;
import Java.SymbolTable.Scope;

public class Default_stmt extends Node {
    Body body;
    Return_stmt return_stmt;
    boolean k_break,k_continue;
    Scope defaultScope;

    public boolean isK_break() {
        return k_break;
    }

    public void setK_break(boolean k_break) {
        this.k_break = k_break;
    }

    public boolean isK_continue() {
        return k_continue;
    }

    public void setK_continue(boolean k_continue) {
        this.k_continue = k_continue;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Return_stmt getReturn_stmt() {
        return return_stmt;
    }

    public void setReturn_stmt(Return_stmt return_stmt) {
        this.return_stmt = return_stmt;
    }

    public void setScope(Scope defaultScope) {
        this.defaultScope=defaultScope;
    }
}
