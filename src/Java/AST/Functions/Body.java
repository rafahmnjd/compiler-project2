package Java.AST.Functions;

import Java.AST.Java_Stmt.Java_stmt;
import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Scope;

import java.util.ArrayList;
import java.util.List;

public class Body extends Node {
    Body body;
    Scope scope;
    List<Java_stmt> java_stmts=new ArrayList<>();

    public void add_java_stmt(Java_stmt java_stmt){
        java_stmts.add(java_stmt);
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public List<Java_stmt> getJava_stmts() {
        return java_stmts;
    }

    public void setJava_stmts(List<Java_stmt> java_stmts) {
        this.java_stmts = java_stmts;
    }

    public void setScope(Scope funScope) {
        this.scope=funScope;
    }


    @Override
    public void accept(ASTVisitor astVisitor, int level) {
        if(java_stmts!=null){
            for (int js=0;js<java_stmts.size();js++){
                java_stmts.get(js).javaStmtAccept(astVisitor,level+1);
            }
        }
        if(body!=null){
            body.accept(astVisitor,level+1);
        }
    }
}
