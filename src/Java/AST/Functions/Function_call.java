package Java.AST.Functions;

import Java.AST.Defenations_and_Values.Values.Value;
import Java.AST.Defenations_and_Values.Values.javavalue.Java_value;
import Java.AST.Defenations_and_Values.Variable;
import Java.AST.Java_Stmt.Java_stmt;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Scope;

import java.util.List;

public class Function_call extends Java_value implements Java_stmt {
    Variable variable;
    String function_name;
    List<Value> argument_list;
    Scope scope=null, //where called.
            funScope=null;//where defined.
    Scope definFun=null;

    public Scope getDefinFun() {
        return definFun;
    }

    public void Scope (Scope definFun) {
        this.definFun = definFun;
    }

    public List<Value> getArguments() {
        return argument_list;
    }


    public List<Value> getArgument_list() {
        return argument_list;
    }

    public void setArgument_list(List<Value> argument_list) {
        this.argument_list = argument_list;
    }

    public Scope getScope() {
        return scope;
    }

    public Scope getFunScope() {
        return funScope;
    }

    public void setFunScope(Scope funScope) {
        this.funScope = funScope;
    }

    public void setArguments(List<Value> arguments) {
        this.argument_list = arguments;
    }

    public void add(Value argument){
        argument_list.add(argument);
    }

    public Variable getVariable() {
        return variable;
    }

    public void setVariable(Variable variable) {
        this.variable = variable;
    }

    public String getFunction_name() {
        return function_name;
    }

    public void setFunction_name(String function_name) {
        this.function_name = function_name;
    }


    @Override
    public void accept(ASTVisitor astVisitor, int level) {
        this.level=level;
        astVisitor.visit(this);
        System.out.println(this.addSpase()+"function call:{");
        if(variable!=null){
            System.out.println(this.addSpase()+"    -function Parent:{");
            this.variable.accept(astVisitor,level+2);
            System.out.println(addSpase()+"     }");
        }
        System.out.println(this.addSpase()+ "    -function name: "+function_name);
        if(argument_list!=null && !argument_list.isEmpty()){
            System.out.println(this.addSpase()+"    -argument list:{");
            for(int arg=0;arg<argument_list.size();arg++){
                System.out.println(addSpase()+"         arg num = "+(arg+1));
               this.argument_list.get(arg).accept(astVisitor,level+3);
            }
            System.out.println(addSpase()+"    }");
        }
        System.out.println(this.addSpase()+"}");
    }


    public void setScope(Scope currentScope) {
        this.scope=currentScope;
    }

    @Override
    public void javaStmtAccept(ASTVisitor astVisitor, int level) {
        this.accept(astVisitor,level);
    }
}

