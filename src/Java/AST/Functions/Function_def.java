package Java.AST.Functions;

import Java.AST.Defenations_and_Values.Variable;
import Java.AST.Java_Stmt.BaseBody;
import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Scope;

import java.util.ArrayList;

public class Function_def extends Node {
    String function_name;
    ArrayList<Variable> parameter_list;
    BaseBody baseBody=new BaseBody();
    Scope scope =null;


    public BaseBody getBaseBody() {
        return baseBody;
    }

    public void setBaseBody(BaseBody baseBody) {
        this.baseBody = baseBody;
    }



    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public Scope getScope() {
        return scope;
    }


    public String getFunction_name() {
        return function_name;
    }

    public void setFunction_name(String function_name) {
        this.function_name = function_name;
    }

    public ArrayList<Variable> getParameter_list() {
        return parameter_list;
    }

    public void setParameter_list(ArrayList<Variable> parameter_list) {
        this.parameter_list = parameter_list;
    }

    @Override
    public void accept(ASTVisitor astVisitor, int level) {
        this.level=level;
        if(parameter_list!=null){
        for(int i=0;i<parameter_list.size();i++){
            parameter_list.get(i).accept(astVisitor,level+1);
        }
    }
        if(baseBody!=null){
            baseBody.accept(astVisitor,level+1);
        }
}
}
