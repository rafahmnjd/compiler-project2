package Java.AST.Functions;

import Java.AST.Defenations_and_Values.Variable;
import Java.AST.Node;

import java.util.ArrayList;
import java.util.List;

public class Parameter_list extends Node {
    List<Variable> parameterList=new ArrayList<>();
    List<Variable> default_vars=new ArrayList<>();

    public void add_parameter(Variable parameter){
        parameterList.add(parameter);
    }

    public void add_default_var(Variable default_var){
        default_vars.add(default_var);
    }

    public List<Variable> getParameterList() {
        return parameterList;
    }

    public void setParameterList(List<Variable> parameterList) {
        this.parameterList = parameterList;
    }

    public List<Variable> getDefault_vars() {
        return default_vars;
    }

    public void setDefault_vars(List<Variable> default_vars) {
        this.default_vars = default_vars;
    }
}
