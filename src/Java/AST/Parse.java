package Java.AST;

import Java.AST.Functions.Function_call;
import Java.AST.Functions.Function_def;
import Java.AST.Sql.Statement;
import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class Parse extends Node{
    private List<Statement> sqlStmts =null;
    private List<Function_def> functions = null;
    private List<Function_call> functionsCall = new ArrayList<Function_call>();



    public List<Function_call> getFunctionsCall() {return functionsCall;}
    public void setFunctionsCall(List<Function_call> functionsCall) { this.functionsCall = functionsCall; }

    public void addQuery(Statement query){
        this.sqlStmts.add(query);
    }
    public void addFunction(Function_def function){this.functions.add(function);}

    public void setSqlStmts(List<Statement> sqlStmts) {
        this.sqlStmts = sqlStmts;
    }
    public void setFunctions(List<Function_def> functions) {
        this.functions = functions;
    }

    public List<Statement> getSqlStmts() {
        return sqlStmts;
    }


    public List<Function_def> getFunctions() {
        return functions;
    }


    @Override
    public String toString(){
        if(sqlStmts!=null && !sqlStmts.isEmpty())
            return "\n sql stmts = "+ getSqlStmts().get(0).getName();
        return "\n";
    }

    @Override
    public void accept(ASTVisitor astVisitor, int level ){

        this.level=level;

        System.out.println(this.addSpase() + "Parser :");
        astVisitor.visit(this);

        if(this.sqlStmts!=null &&!this.sqlStmts.isEmpty()) {
//            System.out.println(this.addSpase() + " Sql stmt list :");
            for (int i = 0; i < this.sqlStmts.size(); i++) {
                this.sqlStmts.get(i).accept(astVisitor, level + 1);
            }
//            System.out.println(this.addSpase() + " ");
        }
        if(this.functionsCall!=null &&!this.functionsCall.isEmpty()) {
            System.out.println(this.addSpase() + " functions Call list :{");
            for (int i = 0; i < this.functionsCall.size(); i++) {

                this.functionsCall.get(i).accept(astVisitor,level + 1);
            }
            System.out.println(this.addSpase() + " }");
        }
        if(this.functions!=null&&!this.functions.isEmpty()){
            for (int f=0;f<functions.size();f++){

                this.functions.get(f).accept(astVisitor,level+1);
            }
        }
    }

}
