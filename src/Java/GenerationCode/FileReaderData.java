package Java.GenerationCode;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class FileReaderData {
    ArrayList<Object> arr = new ArrayList<>();

    public void loadData(String type, String path) {
   /* String extension = "";
        int i = file.getName().lastIndexOf('.');
        if (i >= 0) {
            extension = file.getName().substring(i + 1);
        }*/
        if (type.equals("csv")) {
            readCsvFile(path);
        } else if (type.equals("json")) {
            readJsonFile(path);
        }
    }

    public void readCsvFile(String path) {
        // String csvFile = file.getPath();
        String csvFile = path;
        String line = "";
        String cvsSplitBy = ",";

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] dataArray = line.split(cvsSplitBy);
                for (int n = 0; n < dataArray.length; n++) {
                    System.out.println(dataArray[n]);
//                    arr.set(n, dataArray.get(n));
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void readJsonFile(String path) {
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(path)) {
            Object obj = jsonParser.parse(reader);
            JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);

//            usersClass uc=new usersClass();

            JSONArray cols = (JSONArray) jsonObject.get("usersClass");
            for (Object c : cols) {
                System.out.println(c + "\n");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}