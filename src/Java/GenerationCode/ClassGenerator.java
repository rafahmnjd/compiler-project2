package Java.GenerationCode;

import Java.AST.Defenations_and_Values.Variable;
import Java.AST.Expression.BinaryExpression;
import Java.AST.Expression.Expression;
import Java.AST.Expression.ExpressionIn;
import Java.AST.Expression.FunctionExpression;
import Java.AST.Sql.QueryStmt.JoinElement;
import Java.AST.Sql.QueryStmt.Join_clause;
import Java.AST.Sql.QueryStmt.SelectCoreOrValue;
import Java.AST.Sql.QueryStmt.SelectStmt;
import Java.AST.Sql.Values.Result_column;
import Java.GenerationCode.GeneratedClasses.FunctionExe;
import Java.Main;
import Java.SymbolTable.AggregationFunction;
import Java.SymbolTable.Scope;
import Java.SymbolTable.Symbol;
import Java.SymbolTable.Type;
import com.google.gson.internal.bind.util.ISO8601Utils;

import javax.json.Json;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class ClassGenerator {

    String main_import = "", main_class_header = "", main_variables = "", main_body = "", main_class_functions = "";
    String jenerateMap = "", havingMap = "", inExpr = "";

    public void generateClass() {
        generateTypes();
        generateAggFunctions();
        generateMain();
    }

    public void generateMain() {
        String filename = "src\\Java\\GenerationCode\\GeneratedClasses\\Main.java";
        try {
            FileWriter myWriter = new FileWriter(filename);
            main_import = "package Java.GenerationCode.GeneratedClasses;\n\n";
            main_import += "\n" +
                    "import java.lang.reflect.InvocationTargetException;\n" +
                    "import java.net.MalformedURLException;\n" +
                    "import java.util.ArrayList;\n" +
                    "import java.util.HashMap;\n\n";
            main_class_header = "public class Main { \n";
            main_variables += "    static HashMap<String,ArrayList> map=new HashMap<>();\n";
            for (int i = 0; i < Main.symbolTable.getScopes().size(); i++) {
                for (Symbol s : Main.symbolTable.getScopes().get(i).getSymbolMap().values()) {
                    main_variables += getnt(s.getType()) + " " + s.getName() + " ; \n";
                }
            }
            main_body = generateMainMethod();
            String main = main_import + main_class_header + main_variables + main_body + main_class_functions + "\n}";
            myWriter.write(main);
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }

    private String generateMainMethod() {
        String mainStruct = "", mainBody = "{\n";
        mainStruct += "\n\tpublic static void main(String[] args)\n";
        for (Scope scope : Main.symbolTable.getScopes()) {
            for (String symbolKey : scope.getSymbolMap().keySet()) {
                Symbol symbol = scope.getSymbolMap().get(symbolKey);
                Variable sv = symbol.getVariable();
                if (sv != null) {
//                    mainBody += "\t\tArrayList<" + sv.getValueType() + "Class> " + sv.getVar_name() + " = new ArrayList<>();\n";
                    SelectStmt st = sv.getSelectStmt();
                    mainBody += getSelectStmtCode(st, symbol);
                }
            }

        }

        mainBody += "\n\t }";
        return mainStruct + mainBody;
    }

    private String getSelectStmtCode(SelectStmt st, Symbol sy) {
        jenerateMap = "";
        ArrayList<String> finalExprs = new ArrayList<>();
        String forLops = "", forEnd = "", addObj = "", cond = "";
        String resultList = sy.getType().getName() + "ResultList";
        String resultTables = sy.getName() + "ResultTables";
        String select = "\n";
        main_variables += "\t\tstatic ArrayList<" + sy.getType().getName() + "Class> " + resultList + " = new ArrayList<>();\n";
        main_variables += "\t\tstatic  HashMap<String,ArrayList> " + resultTables + "= new HashMap<>();\n";


        if (st != null) {
            SelectCoreOrValue scv = st.getSellectOrValues();
            if (scv != null) {
                HashMap<String, String[]> variables = new HashMap<>();
                for (String key : sy.getType().getColumns().keySet()) {
                    if (!sy.getType().getColumns().get(key).getName().equals(Type.AGG_FUN_CONST))
                        variables.put(key, key.split("\\$"));
                }

                if (scv.tables != null && !scv.tables.isEmpty()) {
                    for (Type t : scv.tables) {
                        if (t.getTable() != null)
                            select += "\t\t" + t.getName() + "Class.load();\n";
                    }
                }
                if (scv.getWhereExpression() != null) {
                    finalExprs.add("(" + getExprCode(scv.getWhereExpression(), scv.tables.size(), scv.tables.get(0).getName()) + ")");
                }
                if (scv.getJoin_clause() != null) {
                    for (JoinElement je : scv.getJoin_clause().getJoinRightElements()) {
                        finalExprs.add("(" + getExprCode(je.getOnExpression(), scv.tables.size(), scv.tables.get(0).getName()) + ")");
                    }
                }

                if (scv.tables != null && !scv.tables.isEmpty()) {
                    for (Type t : scv.tables) {
                        select+="\t "+resultTables+".put(\""+ t.getName()+"\",new ArrayList<"+ t.getName()+"Class>());\n";
                        forLops += "for(" + t.getName() + "Class " + t.getName() + " : " + t.getName() + "Class.arraylist){\n";
                        addObj+= "\t "+resultTables+".get(\""+t.getName()+"\").add("+t.getName()+");\n";
                        for (String key : variables.keySet()) {
                            if (variables.get(key)[0].equals(t.getName())) {
                                addObj += "            obj." + key + " = " + variables.get(key)[0];
                                for (int i = 1; i < variables.get(key).length; i++) {
                                    addObj += "." + variables.get(key)[i];
                                }
                                addObj += ";\n";
                            }
                        }
                        forEnd += "}\n";
                    }
                    if (!finalExprs.isEmpty()) {
                        cond += "if((" + finalExprs.get(0) + ")";
                        for (int w = 1; w < finalExprs.size(); w++) {
                            cond += " && (" + finalExprs.get(w) + ")";
                        }
                        cond += ")";
                    }
                    select += "\n" + forLops + cond + "{\n" +
                            "               " + sy.getType().getName() + "Class obj = new " + sy.getType().getName() + "Class();" +
                            "\n             " + addObj + "\n" +
                            "\n             " + resultList + ".add(obj);" +
                            "\n            }" +
                            "\n         " + forEnd;

                }
                select += "\n       " + sy.getType().getName() + "Class.arraylist=" + resultList + ";";

                if (scv.getGroupByExpressions() != null && !scv.getGroupByExpressions().isEmpty()) {

                }
                else {
                    boolean haveRows = false;
                    for (Result_column rc : scv.getResult_columns()) {
                        if (rc.getType() instanceof AggregationFunction) {
                            String[] fun = new String[3];
                            fun[0] = "fun";
                            fun[1] = getntAf((AggregationFunction) rc.getType());
                            fun[2] = getExprCode(rc.getExpression(), 1, sy.getType().getName());
                            variables.put(rc.getColumn_alias(), fun);

                        } else
                            haveRows = true;
                    }
                    if (!haveRows) {
                        select += jenerateMap;
                        for (String key : variables.keySet()) {
                            if (variables.get(key)[0].equals("fun")) {
                                select += "\n\t " + variables.get(key)[1] + " " + key + " = " + variables.get(key)[2] + ";";
                                select += "\n\tSystem.out.println(\""+key+" = \"+" + key + ");";
                            }
                        }
                    }
                    else
                        select += "\n       " + sy.getType().getName() + "Class.printAll();\n";
                }
            }
        }
        select += "\t\t//_____________________________________________________________________\n\n";
        return select;
    }


    private String getExprCode(Expression expr, int s, String defT) {
        String code = "";
        Class<? extends Expression> c = expr.getClass();

        if (c.equals(BinaryExpression.class)) {
            String l = "", o = "", r = "";
            l = getExprCode(((BinaryExpression) expr).getLeftExpression(), s, defT);
            r = getExprCode(((BinaryExpression) expr).getRightExpression(), s, defT);
            o = ((BinaryExpression) expr).getOperator();

            if (o.equals("like")) {
                o = ".contains(" + r + ")";
                return l + o;
            }
            if (o.equals("=") || o.equals("==") || o.equals("is") || o.equals("IS")) {
                return l + ".equals(" + r + ")";
            }
            if (o.equals("!=") || o.equals("<>") || o.equals("is not") || o.equals("IS NOT")) {
                return "!(" + l + ".equals(" + r + "))";
            }
            if (o.equals("and") || o.equals("AND") || o.equals("&"))
                o = " && ";
            else if (o.equals("or") || o.equals("OR") || o.equals("|"))
                o = "||";
            else if (o.equals("<<"))
                o = "<";
            else if (o.equals(">>"))
                o = ">";
            code = l + o + r;
            return code;
        }
        if (c.equals(ExpressionIn.class)) {
            String l = getExprCode(((ExpressionIn) expr).getLeft(), s, defT);
            code += "(";
            if (((ExpressionIn) expr).isNotIn())
                code += "!";
            for (Expression rexp : ((ExpressionIn) expr).getRightExpressions()) {
                code += "(" + l + " == " + getExprCode(rexp, s, defT) + ")||";
            }
            code = code.substring(0, (code.length() - 2));
            code += ")";
            return code;
        }
        if (c.equals(FunctionExpression.class)) {

            FunctionExpression fe = (FunctionExpression) expr;
            code += fe.getFunctionName() + "(";
            if (fe.isStar()) {
                code += "\"*\")";
                jenerateMap += "\n\t map.put(\"*\"," + defT + "Class.arraylist);\n";
            } else {
                for (int i = 0; i < fe.getExpressions().size(); i++) {
                    Expression ex = fe.getExpressions().get(i);
                    String exCo = getExprCode(ex, s, defT);
                    code += "\"" + exCo + "\",";
                    String colT = getnt(((AggregationFunction) fe.getType()).getParams().get(i));
                    String[] classN = exCo.split("\\.");

//                    jenerateMap+="\n        map.put(\"movies.profitability\",new ArrayList());\n" +
//                            "        for (moviesClass movies :(ArrayList<moviesClass>) clResultTables.get(\"movies\") ) {\n" +
//                            "           map.get(\"movies.profitability\").add(movies.profitability);\n" +
//                            "        }";


                    jenerateMap += "\n\tif(map.get(\"" + exCo + "\")==null){\n" +
                            "   \tmap.put(\"" + exCo + "\",new ArrayList<" + colT + ">());\n" +
                            "   \tfor(" + classN[1] + "Class " + classN[1] + " : (ArrayList<" + classN[1] + "Class>) "+classN[0]+"ResultTables.get(\""+classN[1]+"\")){\n" +
                            "       \tmap.get(\"" + exCo + "\").add(";
                    for (int j = 1; j <classN.length ; j++) {
                        jenerateMap+=classN[j]+".";
                    }
                    jenerateMap=jenerateMap.substring(0,jenerateMap.length()-1);
                     jenerateMap+=");\n" +
                            "   }\n" +
                            "}\n";

                }
                code = code.substring(0, code.length() - 1) + ")";
            }
            return code;
        }

        if (expr.getLiteral_value() != null) {
            return expr.getLiteral_value().getFinalValue();
        }
        if (expr.getColumn_name() != null) {
            String col = "", table = "", database = "";
            col = expr.getColumn_name();
            if (expr.getTable_name() != null) {
                table = expr.getTable_name();
                if (expr.getDataBaseName() != null) {
                    database = expr.getDataBaseName() + ".";
                    if (s == 1) {
                        table += ".";
                        code += defT + ".";
                    } else {
                        table += "$";
                    }
                    code += database + table + col;
                } else {
                    table += ".";
                    if (s == 1) {
                        code = defT + "." + table + col;
                    } else
                        code = table + col;
                }
            } else {

                code = defT + "." + col;
            }
            return code;
        }
        return code;
    }

    private void generateAggFunctions() {
        if (Main.symbolTable.getDeclaredAggregationFunction().isEmpty())
            return;
        try {
            String filename = "src\\Java\\GenerationCode\\GeneratedClasses\\FunctionExe.java";
            FileWriter myWriter = new FileWriter(filename);
            String FunctionExeHeader = "package Java.GenerationCode.GeneratedClasses;\n" +
                    "import java.io.File;\n" +
                    "import java.lang.reflect.InvocationTargetException;\n" +
                    "import java.lang.reflect.Method;\n" +
                    "import java.net.MalformedURLException;\n" +
                    "import java.net.URL;\n" +
                    "import java.net.URLClassLoader;\n" +
                    "import java.util.ArrayList;\n" +
                    "import java.util.Arrays;\n" +
                    "import java.util.List;\n";
            String FunctionExeClass = "\n\n public class FunctionExe {\n";
            for (AggregationFunction ag : Main.symbolTable.getDeclaredAggregationFunction()) {
                FunctionExeClass += "\n" + getMethod(ag) + "\n";


            }
            FunctionExeClass += "\n}";
            String FunctionExe = FunctionExeHeader + FunctionExeClass;
            myWriter.write(FunctionExe);
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    private String getMethod(AggregationFunction ag) {
        main_class_functions += "\n static " + getntAf(ag) + " " + ag.getAggregationFunctionName() + "(";

        String method = "\n";
        String[] jarpathsplit = ag.getJarPath().split("/");

        String jarName = jarpathsplit[jarpathsplit.length - 1];
        CharSequence jarpath;
        String returnType = getntAf(ag);
        jarpath = ag.getJarPath().subSequence(0, ag.getJarPath().indexOf(jarName));
        method += "\t static public " + returnType + " " + ag.getAggregationFunctionName() + "(";
        if (!ag.getParams().isEmpty()) {
            int ind = 0;
            for (Type ptn : ag.getParams()) {
                if (ind == 0) {
                    method += "ArrayList<" + getnt(ptn) + "> myNumbers";
                    main_class_functions += " String col ";
                } else {
                    method += ",ArrayList<" + getnt(ptn) + "> myNumbers" + ind;
                    main_class_functions += "String col" + ind;
                }
                ind++;
            }
            main_class_functions += "){\n";
            String callArg = "";
            ind = 0;
            for (Type ptn : ag.getParams()) {
                if (ind == 0) {
                    main_class_functions += "ArrayList<" + getnt(ptn) + "> colList = map.get(col);\n";
                    callArg += "colList";
                } else {
                    main_class_functions += "ArrayList<" + getnt(ptn) + "> colList" + ind + " = map.get(col" + ind + ");\n";
                    callArg += ",colList" + ind;
                }
                ind++;
            }
            main_class_functions += "" + getntAf(ag) + " r = null;\n" +
                    "try {\n" +
                    "r= FunctionExe." + ag.getAggregationFunctionName() + "(" + callArg + ");\n" +
                    "     } catch (ClassNotFoundException e) {\n" +
                    "         e.printStackTrace();\n" +
                    "     } catch (NoSuchMethodException e) {\n" +
                    "         e.printStackTrace();\n" +
                    "     } catch (InvocationTargetException e) {\n" +
                    "         e.printStackTrace();\n" +
                    "     } catch (IllegalAccessException e) {\n" +
                    "         e.printStackTrace();\n" +
                    "     } catch (MalformedURLException e) {\n" +
                    "         e.printStackTrace();\n" +
                    "     }\n" +
                    "return r;\n" +
                    "}\n";

            //Type ptn = ag.getParams().get(0);

        }
        method += ") throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, MalformedURLException {\n" +
                "\n" +
                "        String JarPath=" + jarpath + "\";\n" +
                "        String JarName = \"" + jarName + ";\n" +
                "        String ClassName = " + ag.getClassName() + ";\n" +
                "        String MethodName = " + ag.getMethodName() + ";\n" +
                "\n" +
                "        URLClassLoader myClassLoader = new URLClassLoader(\n" +
                "                new URL[]{new File(JarPath + JarName).toURI().toURL()},\n" +
                "                FunctionExe.class.getClassLoader()\n" +
                "        );\n" +
                "\n" +
                "        Class<?> myClass = Class.forName(ClassName, true, myClassLoader);\n" +
                "        Method mySingeltonGetterMethod = myClass.getMethod(\"get\" + ClassName,\n" +
                "                null);\n" +
                "        Object myObject = mySingeltonGetterMethod.invoke(null);\n" +
                "\n" +
                "     /*   System.out.println(MethodName+\" = \"+myObject.getClass().getMethod(MethodName, List.class)\n" +
                "                .invoke(myObject, myNumbers));*/\n" +
                "        return (" + returnType + ") myObject.getClass().getMethod(MethodName, List.class)\n" +
                "                .invoke(myObject, myNumbers);\n" +
                "    }\n";
        return method;
    }

    public void generateTypes() {
        for (Type t : Main.symbolTable.getDeclaredTypes()) {
            if (!t.isPrimary()) {
                try {
                    String filename = "src\\Java\\GenerationCode\\GeneratedClasses\\" + t.getName() + "Class" + ".java";
                    FileWriter myWriter = new FileWriter(filename);
                    String prev = "package Java.GenerationCode.GeneratedClasses;\n\n";
                    prev += "import java.util.ArrayList;\n" +
                            "import org.json.simple.JSONObject;\n" +
                            "import org.json.simple.JSONArray;\n" +
                            "import java.io.*;\n" +
                            "import java.nio.file.Files;\n" +
                            "import org.json.simple.JSONObject;\n" +
                            "import org.json.simple.JSONArray;\n" +
                            "import java.nio.file.Paths;\n" +
                            "import org.apache.commons.csv.CSVFormat;\n" +
                            "import org.apache.commons.csv.CSVRecord;\n" +
                            "import org.apache.commons.csv.CSVParser;\n";
                    String code = "\n";
                    code += "public class " + t.getName() + "Class" + "{ \n";
                    code += getTableCol(t);
                    code += "\t static public ArrayList<" + t.getName() + "Class> arraylist = new ArrayList<>();\n";

                    if (t.getTable() != null) {
                        prev += "import java.io.BufferedReader;\n" +
                                "import java.io.FileNotFoundException;\n" +
                                "import java.io.FileReader;\n" +
                                "import java.io.IOException;\n" +
                                "\n" +
                                "import org.json.simple.parser.JSONParser;\n" +
                                "import org.json.simple.parser.ParseException;\n\n";

                        code += getArrayLoader(t, (t.getName() + "Class"));
                        code += getTablePars(t, (t.getName() + "Class"));
                        code += getTableParsCsv(t, (t.getName() + "Class"));
                    } else code += getTypePars(t, (t.getName() + "Class"));
                    code += getPrintAll(t);
                    code += getPrintMethod(t);
                    myWriter.write(prev + code);
                    myWriter.close();
                } catch (IOException e) {
                    System.out.println("An error occurred.");
                    e.printStackTrace();
                }
            }
        }
    }

    private String getTypePars(Type t, String className) {
        String parsMethod = "\n\n";
        parsMethod += " public static " + className + " pars(JSONObject data) {\n" +
                "     " + className + " obj=new " + className + "();\n";
        for (String k : t.getColumns().keySet()) {
            Type kt = t.getColumns().get(k);
            if (kt.isPrimary()) {
                String nt = getnt(kt);
                if (!nt.equals("Double"))
                    parsMethod += "            obj." + k + "=(" + nt + ") data.get(\"" + k + "\");\n";
                else

                    parsMethod += "            obj." + k + "=(" + nt + ") data.get(\"" + k + "\");\n";
            } else if (kt.getTable() != null && !kt.getName().equals(Type.NULL_CONST)) {
                parsMethod += "            obj." + k + "=" + kt.getName() + "Class.pars((JSONArray) data.get(\"" + k + "\")).get(0);\n";
            } else if (!kt.getName().equals(Type.NULL_CONST)) {
                parsMethod += "            obj." + k + "=" + kt.getName() + "Class.pars((JSONObject) data.get(\"" + k + "\"));\n";

            }
        }

        parsMethod += "     return obj;\n" +
                "    }\n";
        return parsMethod;
    }

    private String getTablePars(Type t, String className) {
        String parsMethod = "\n\n";

        parsMethod += "    public static ArrayList<" + className + "> pars(JSONArray jdata){\n" +
                "        ArrayList<" + className + "> data = new ArrayList<>();\n" +
                "        for(int i=0;i<jdata.size();i++) {\n" +
                "            JSONObject jRow= (JSONObject) jdata.get(i);\n" +
                "            " + className + " obj=new " + className + "();\n";
        for (String k : t.getColumns().keySet()) {
            Type kt = t.getColumns().get(k);
            if (kt.isPrimary()) {
//                if (kt.getName().equals(Type.NUMBER_CONST))
//                    parsMethod += "            obj." + k + "=Double.parseDouble((String) jRow.get(\"" + k + "\"));\n";
//                else
//                    parsMethod += "            obj." + k + "=(" + getnt(kt.getName()) + ") jRow.get(\"" + k + "\");\n";
                String nt = getnt(kt);
                if (!nt.equals("Double"))
                    parsMethod += "            obj." + k + "=(" + nt + ") jRow.get(\"" + k + "\");\n";
                else
                    parsMethod += "            obj." + k + "=(" + nt + ") Double.valueOf(jRow.get(\"" + k + "\").toString());\n";
            } else if (kt.getTable() != null && !kt.getName().equals(Type.NULL_CONST)) {
                parsMethod += "            obj." + k + "=" + kt.getName() + "Class.pars((JSONArray) jRow.get(\"" + k + "\")).get(0);\n";
            } else if (!kt.getName().equals(Type.NULL_CONST)) {
                parsMethod += "            obj." + k + "=" + kt.getName() + "Class.pars((JSONObject) jRow.get(\"" + k + "\"));\n";

            }

        }
        parsMethod +=
                "            data.add(obj);\n" +
                        "        }\n" +
                        "        return data;\n" +
                        "    }\n";

        return parsMethod;
    }

    private String getTableParsCsv(Type t, String className) {
        String parsMethod = "\n\n";

        parsMethod += "    public static ArrayList<" + className + "> parsCsv(CSVParser csvParser){\n" +
                "        ArrayList<" + className + "> data = new ArrayList<>();\n" +
                "       for (CSVRecord csvRecord : csvParser) {\n" +
                "            " + className + " obj=new " + className + "();\n";
        for (String k : t.getColumns().keySet()) {
            Type kt = t.getColumns().get(k);
            if (kt.isPrimary()) {
                if (kt.getName().equals(Type.NUMBER_CONST))
                    parsMethod += "            obj." + k + "=Double.parseDouble(csvRecord.get(\"" + k + "\"));\n";
                else
                    parsMethod += "            obj." + k + "= csvRecord.get(\"" + k + "\");\n";
            }
        }
        parsMethod +=
                "            data.add(obj);\n" +
                        "        }\n" +
                        "        return data;\n" +
                        "    }\n";

        return parsMethod;
    }

    private String getPrintMethod(Type t) {
        String myprint = "\n public void print(){\n";

        for (String k : t.getColumns().keySet()) {
            if (t.getColumns().get(k).isPrimary())
                myprint += "                System.out.print(\"\\t\\t " + k + "\"+\"=\" +this." + k + ");\n";
            else
                myprint += "                System.out.println(\"" + k + "\"+\"=\");\n" +
                        "                this." + k + ".print();\n";
        }
        myprint += "       }\n";
        myprint += "  }\n";
        return myprint;
    }

    private String getArrayLoader(Type t, String className) {
        String result = "\n";
        result += "\t static public String path=" + t.getTable().getPath() + ";\n" +
                "\t static public String type=" + t.getTable().getFileType() + ";\n\n\n";
        result += "\t static public void load(){\n";
        result += "if( type.equals(\"json\")) {\n";

        result += "\n\t\tJSONParser jsonParser = new JSONParser();\n" +
                "\n" +
                "        try (FileReader reader = new FileReader(path)) {\n" +
                "            JSONObject jsonTable = (JSONObject) jsonParser.parse(reader);\n" +
                "            JSONArray jdata = (JSONArray) jsonTable.get(\"" + t.getName() + "\");\n" +
                "            " + className + ".arraylist=pars(jdata);\n";


        result +=
                "        } catch (FileNotFoundException e) {\n" +
                        "            e.printStackTrace();\n" +
                        "        } catch (IOException e) {\n" +
                        "            e.printStackTrace();\n" +
                        "        } catch (ParseException e) {\n" +
                        "            e.printStackTrace();\n" +
                        "        }";

        result += "\n\t }\n\n";
        result += "else{" +
                "\n" +
                "            try (\n" +
                "                    Reader reader = Files.newBufferedReader(Paths.get(path));\n" +
                "                    CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT\n" +
                "                            .withFirstRecordAsHeader()\n" +
                "                            .withIgnoreHeaderCase()\n" +
                "                            .withTrim());\n" +
                "            )" +
                "{\n" +
                "            " + className + ".arraylist=parsCsv(csvParser);\n" +
                "            } catch (IOException e) {\n" +
                "                e.printStackTrace();\n" +
                "            }" +
                " }\n";
        result += "}\n";
        return result;
    }

    private String getTableCol(Type t) {
        String col = "\n";
        for (String k : t.getColumns().keySet()) {
            col += "\t public " + getnt(t.getColumns().get(k)) + " " + k + ";\n";
        }
        return col;
    }

    private String getnt(Type t) {
        if (t.getName().equals("object") || t.getName().equals("Object"))
            return "Object";
        if (t.getName().equals(Type.BOOLEAN_CONST))
            return "Boolean";
        if (t.getName().equals(Type.NUMBER_CONST))
            return "Double";
        if (t.getName().equals(Type.STRING_CONST))
            return "String";
        if (t.getName().equals(Type.AGG_FUN_CONST)) {
            AggregationFunction aft = (AggregationFunction) t;
            return getntAf(aft);
        }
        return t.getName() + "Class";
    }

    private String getntAf(AggregationFunction aft) {
        if (aft.getReturnType().equals("Object") || aft.getReturnType().equals("object"))
            return "Object";
        if (aft.getReturnType().equals(Type.BOOLEAN_CONST))
            return "Boolean";
        if (aft.getReturnType().equals(Type.NUMBER_CONST))
            return "Double";
        if (aft.getReturnType().equals(Type.STRING_CONST))
            return "String";
        return aft.getReturnType() + "Class";
    }

    private String getPrintAll(Type t) {
        String printAll = "";
        printAll +=
                "\n" +
                        "    public static void printAll() {\n" +
                        "        System.out.println();" +
                        "        for (" + t.getName() + "Class obj:arraylist ) {\n" +
                        "            obj.print();\n" +
                        "            System.out.println(\"\\n__________________________________________________________________________________________________________\");\n     " +
                        "        }\n" +
                        "    }";
        return printAll;
    }

    private boolean isTable(String name) {
        for (Type t : Main.symbolTable.getDeclaredTypes()) {
            if (t.getName() == name) {
                if (t.getTable() != null) {
                    return true;
                }
            }
        }
        return false;
    }
}

