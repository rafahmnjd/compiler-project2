package Java.GenerationCode.GeneratedClasses;

import java.util.ArrayList;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import java.io.*;
import java.nio.file.Files;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import java.nio.file.Paths;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.CSVParser;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class moviesClass{ 

	 public Double audience_score;
	 public Double profitability;
	 public Double year;
	 public String lead_studio;
	 public String name;
	 public String genre;
	 public Double rotten_tomato;
	 public Double worldwide_gross;
	 static public ArrayList<moviesClass> arraylist = new ArrayList<>();

	 static public String path="D://Desktop/chapter2/compiler/jsonFiles/movies.csv";
	 static public String type="csv";


	 static public void load(){
if( type.equals("json")) {

		JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(path)) {
            JSONObject jsonTable = (JSONObject) jsonParser.parse(reader);
            JSONArray jdata = (JSONArray) jsonTable.get("movies");
            moviesClass.arraylist=pars(jdata);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
	 }

else{
            try (
                    Reader reader = Files.newBufferedReader(Paths.get(path));
                    CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
                            .withFirstRecordAsHeader()
                            .withIgnoreHeaderCase()
                            .withTrim());
            ){
            moviesClass.arraylist=parsCsv(csvParser);
            } catch (IOException e) {
                e.printStackTrace();
            } }
}


    public static ArrayList<moviesClass> pars(JSONArray jdata){
        ArrayList<moviesClass> data = new ArrayList<>();
        for(int i=0;i<jdata.size();i++) {
            JSONObject jRow= (JSONObject) jdata.get(i);
            moviesClass obj=new moviesClass();
            obj.audience_score=(Double) Double.valueOf(jRow.get("audience_score").toString());
            obj.profitability=(Double) Double.valueOf(jRow.get("profitability").toString());
            obj.year=(Double) Double.valueOf(jRow.get("year").toString());
            obj.lead_studio=(String) jRow.get("lead_studio");
            obj.name=(String) jRow.get("name");
            obj.genre=(String) jRow.get("genre");
            obj.rotten_tomato=(Double) Double.valueOf(jRow.get("rotten_tomato").toString());
            obj.worldwide_gross=(Double) Double.valueOf(jRow.get("worldwide_gross").toString());
            data.add(obj);
        }
        return data;
    }


    public static ArrayList<moviesClass> parsCsv(CSVParser csvParser){
        ArrayList<moviesClass> data = new ArrayList<>();
       for (CSVRecord csvRecord : csvParser) {
            moviesClass obj=new moviesClass();
            obj.audience_score=Double.parseDouble(csvRecord.get("audience_score"));
            obj.profitability=Double.parseDouble(csvRecord.get("profitability"));
            obj.year=Double.parseDouble(csvRecord.get("year"));
            obj.lead_studio= csvRecord.get("lead_studio");
            obj.name= csvRecord.get("name");
            obj.genre= csvRecord.get("genre");
            obj.rotten_tomato=Double.parseDouble(csvRecord.get("rotten_tomato"));
            obj.worldwide_gross=Double.parseDouble(csvRecord.get("worldwide_gross"));
            data.add(obj);
        }
        return data;
    }

    public static void printAll() {
        System.out.println();        for (moviesClass obj:arraylist ) {
            obj.print();
            System.out.println("\n__________________________________________________________________________________________________________");
             }
    }
 public void print(){
                System.out.print("\t\t audience_score"+"=" +this.audience_score);
                System.out.print("\t\t profitability"+"=" +this.profitability);
                System.out.print("\t\t year"+"=" +this.year);
                System.out.print("\t\t lead_studio"+"=" +this.lead_studio);
                System.out.print("\t\t name"+"=" +this.name);
                System.out.print("\t\t genre"+"=" +this.genre);
                System.out.print("\t\t rotten_tomato"+"=" +this.rotten_tomato);
                System.out.print("\t\t worldwide_gross"+"=" +this.worldwide_gross);
       }
  }
