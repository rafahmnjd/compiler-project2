package Java.GenerationCode.GeneratedClasses;

import java.util.ArrayList;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import java.io.*;
import java.nio.file.Files;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import java.nio.file.Paths;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.CSVParser;

public class _movies_join_in_qClass{ 

	 public Double movies$audience_score;
	 public Double movies$profitability;
	 public String movies$genre;
	 public Double movies$rotten_tomato;
	 public Double movies$year;
	 public String movies$name;
	 public Double movies$worldwide_gross;
	 public Double in_q$movies$year;
	 public String movies$lead_studio;
	 static public ArrayList<_movies_join_in_qClass> arraylist = new ArrayList<>();


 public static _movies_join_in_qClass pars(JSONObject data) {
     _movies_join_in_qClass obj=new _movies_join_in_qClass();
            obj.movies$audience_score=(Double) data.get("movies$audience_score");
            obj.movies$profitability=(Double) data.get("movies$profitability");
            obj.movies$genre=(String) data.get("movies$genre");
            obj.movies$rotten_tomato=(Double) data.get("movies$rotten_tomato");
            obj.movies$year=(Double) data.get("movies$year");
            obj.movies$name=(String) data.get("movies$name");
            obj.movies$worldwide_gross=(Double) data.get("movies$worldwide_gross");
            obj.in_q$movies$year=(Double) data.get("in_q$movies$year");
            obj.movies$lead_studio=(String) data.get("movies$lead_studio");
     return obj;
    }

    public static void printAll() {
        System.out.println();        for (_movies_join_in_qClass obj:arraylist ) {
            obj.print();
            System.out.println("\n__________________________________________________________________________________________________________");
             }
    }
 public void print(){
                System.out.print("\t\t movies$audience_score"+"=" +this.movies$audience_score);
                System.out.print("\t\t movies$profitability"+"=" +this.movies$profitability);
                System.out.print("\t\t movies$genre"+"=" +this.movies$genre);
                System.out.print("\t\t movies$rotten_tomato"+"=" +this.movies$rotten_tomato);
                System.out.print("\t\t movies$year"+"=" +this.movies$year);
                System.out.print("\t\t movies$name"+"=" +this.movies$name);
                System.out.print("\t\t movies$worldwide_gross"+"=" +this.movies$worldwide_gross);
                System.out.print("\t\t in_q$movies$year"+"=" +this.in_q$movies$year);
                System.out.print("\t\t movies$lead_studio"+"=" +this.movies$lead_studio);
       }
  }
