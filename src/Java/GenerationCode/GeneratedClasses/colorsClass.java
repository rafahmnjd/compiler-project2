package Java.GenerationCode.GeneratedClasses;

import java.util.ArrayList;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import java.io.*;
import java.nio.file.Files;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import java.nio.file.Paths;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.CSVParser;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class colorsClass{ 

	 public String color;
	 public Double id;
	 public String value;
	 static public ArrayList<colorsClass> arraylist = new ArrayList<>();

	 static public String path="D://Desktop/chapter2/compiler/jsonFiles/colors.json";
	 static public String type="json";


	 static public void load(){
if( type.equals("json")) {

		JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(path)) {
            JSONObject jsonTable = (JSONObject) jsonParser.parse(reader);
            JSONArray jdata = (JSONArray) jsonTable.get("colors");
            colorsClass.arraylist=pars(jdata);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
	 }

else{
            try (
                    Reader reader = Files.newBufferedReader(Paths.get(path));
                    CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
                            .withFirstRecordAsHeader()
                            .withIgnoreHeaderCase()
                            .withTrim());
            ){
            colorsClass.arraylist=parsCsv(csvParser);
            } catch (IOException e) {
                e.printStackTrace();
            } }
}


    public static ArrayList<colorsClass> pars(JSONArray jdata){
        ArrayList<colorsClass> data = new ArrayList<>();
        for(int i=0;i<jdata.size();i++) {
            JSONObject jRow= (JSONObject) jdata.get(i);
            colorsClass obj=new colorsClass();
            obj.color=(String) jRow.get("color");
            obj.id=(Double) Double.valueOf(jRow.get("id").toString());
            obj.value=(String) jRow.get("value");
            data.add(obj);
        }
        return data;
    }


    public static ArrayList<colorsClass> parsCsv(CSVParser csvParser){
        ArrayList<colorsClass> data = new ArrayList<>();
       for (CSVRecord csvRecord : csvParser) {
            colorsClass obj=new colorsClass();
            obj.color= csvRecord.get("color");
            obj.id=Double.parseDouble(csvRecord.get("id"));
            obj.value= csvRecord.get("value");
            data.add(obj);
        }
        return data;
    }

    public static void printAll() {
        System.out.println();        for (colorsClass obj:arraylist ) {
            obj.print();
            System.out.println("\n__________________________________________________________________________________________________________");
             }
    }
 public void print(){
                System.out.print("\t\t color"+"=" +this.color);
                System.out.print("\t\t id"+"=" +this.id);
                System.out.print("\t\t value"+"=" +this.value);
       }
  }
