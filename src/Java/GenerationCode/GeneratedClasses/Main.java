package Java.GenerationCode.GeneratedClasses;


import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;

public class Main { 
    static HashMap<String,ArrayList> map=new HashMap<>();
in_qClass in_q ; 
clClass cl ; 
		static ArrayList<in_qClass> in_qResultList = new ArrayList<>();
		static  HashMap<String,ArrayList> in_qResultTables= new HashMap<>();
		static ArrayList<clClass> clResultList = new ArrayList<>();
		static  HashMap<String,ArrayList> clResultTables= new HashMap<>();

	public static void main(String[] args)
{

		moviesClass.load();
	 in_qResultTables.put("movies",new ArrayList<moviesClass>());

for(moviesClass movies : moviesClass.arraylist){
if(((movies.year<2008.0))){
               in_qClass obj = new in_qClass();
             	 in_qResultTables.get("movies").add(movies);
            obj.movies$year = movies.year;


             in_qResultList.add(obj);
            }
         }

       in_qClass.arraylist=in_qResultList;
       in_qClass.printAll();
		//_____________________________________________________________________


		moviesClass.load();
	 clResultTables.put("in_q",new ArrayList<in_qClass>());
	 clResultTables.put("movies",new ArrayList<moviesClass>());

for(in_qClass in_q : in_qClass.arraylist){
for(moviesClass movies : moviesClass.arraylist){
if(((in_q.movies$year.equals(movies.year)))){
               clClass obj = new clClass();
             	 clResultTables.get("in_q").add(in_q);
	 clResultTables.get("movies").add(movies);


             clResultList.add(obj);
            }
         }
}

       clClass.arraylist=clResultList;
	 map.put("*",clClass.arraylist);

	if(map.get("cl.movies.profitability")==null){
   	map.put("cl.movies.profitability",new ArrayList<Double>());
   	for(moviesClass movies : (ArrayList<moviesClass>) clResultTables.get("movies")){
       	map.get("cl.movies.profitability").add(movies.profitability);
   }
}

	 Double avg_ = avg("cl.movies.profitability");
	System.out.println("avg_ = "+avg_);
	 Double cnt = count("*");
	System.out.println("cnt = "+cnt);		//_____________________________________________________________________


	 }
 static Double sum( String col ){
ArrayList<Double> colList = map.get(col);
Double r = null;
try {
r= FunctionExe.sum(colList);
     } catch (ClassNotFoundException e) {
         e.printStackTrace();
     } catch (NoSuchMethodException e) {
         e.printStackTrace();
     } catch (InvocationTargetException e) {
         e.printStackTrace();
     } catch (IllegalAccessException e) {
         e.printStackTrace();
     } catch (MalformedURLException e) {
         e.printStackTrace();
     }
return r;
}

 static Double avg( String col ){
ArrayList<Double> colList = map.get(col);
Double r = null;
try {
r= FunctionExe.avg(colList);
     } catch (ClassNotFoundException e) {
         e.printStackTrace();
     } catch (NoSuchMethodException e) {
         e.printStackTrace();
     } catch (InvocationTargetException e) {
         e.printStackTrace();
     } catch (IllegalAccessException e) {
         e.printStackTrace();
     } catch (MalformedURLException e) {
         e.printStackTrace();
     }
return r;
}

 static Double max( String col ){
ArrayList<Double> colList = map.get(col);
Double r = null;
try {
r= FunctionExe.max(colList);
     } catch (ClassNotFoundException e) {
         e.printStackTrace();
     } catch (NoSuchMethodException e) {
         e.printStackTrace();
     } catch (InvocationTargetException e) {
         e.printStackTrace();
     } catch (IllegalAccessException e) {
         e.printStackTrace();
     } catch (MalformedURLException e) {
         e.printStackTrace();
     }
return r;
}

 static Double min( String col ){
ArrayList<Double> colList = map.get(col);
Double r = null;
try {
r= FunctionExe.min(colList);
     } catch (ClassNotFoundException e) {
         e.printStackTrace();
     } catch (NoSuchMethodException e) {
         e.printStackTrace();
     } catch (InvocationTargetException e) {
         e.printStackTrace();
     } catch (IllegalAccessException e) {
         e.printStackTrace();
     } catch (MalformedURLException e) {
         e.printStackTrace();
     }
return r;
}

 static Double std( String col ){
ArrayList<Double> colList = map.get(col);
Double r = null;
try {
r= FunctionExe.std(colList);
     } catch (ClassNotFoundException e) {
         e.printStackTrace();
     } catch (NoSuchMethodException e) {
         e.printStackTrace();
     } catch (InvocationTargetException e) {
         e.printStackTrace();
     } catch (IllegalAccessException e) {
         e.printStackTrace();
     } catch (MalformedURLException e) {
         e.printStackTrace();
     }
return r;
}

 static Double count( String col ){
ArrayList<Object> colList = map.get(col);
Double r = null;
try {
r= FunctionExe.count(colList);
     } catch (ClassNotFoundException e) {
         e.printStackTrace();
     } catch (NoSuchMethodException e) {
         e.printStackTrace();
     } catch (InvocationTargetException e) {
         e.printStackTrace();
     } catch (IllegalAccessException e) {
         e.printStackTrace();
     } catch (MalformedURLException e) {
         e.printStackTrace();
     }
return r;
}

}