package Java.GenerationCode.GeneratedClasses;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


 public class FunctionExe {


	 static public Double sum(ArrayList<Double> myNumbers) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, MalformedURLException {

        String JarPath="D://Desktop/chapter2/compiler/JarsFiles/";
        String JarName = "CommonAggregations.jar";
        String ClassName = "CommonAggregations";
        String MethodName = "Sum";

        URLClassLoader myClassLoader = new URLClassLoader(
                new URL[]{new File(JarPath + JarName).toURI().toURL()},
                FunctionExe.class.getClassLoader()
        );

        Class<?> myClass = Class.forName(ClassName, true, myClassLoader);
        Method mySingeltonGetterMethod = myClass.getMethod("get" + ClassName,
                null);
        Object myObject = mySingeltonGetterMethod.invoke(null);

     /*   System.out.println(MethodName+" = "+myObject.getClass().getMethod(MethodName, List.class)
                .invoke(myObject, myNumbers));*/
        return (Double) myObject.getClass().getMethod(MethodName, List.class)
                .invoke(myObject, myNumbers);
    }



	 static public Double avg(ArrayList<Double> myNumbers) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, MalformedURLException {

        String JarPath="D://Desktop/chapter2/compiler/JarsFiles/";
        String JarName = "CommonAggregations.jar";
        String ClassName = "CommonAggregations";
        String MethodName = "avg";

        URLClassLoader myClassLoader = new URLClassLoader(
                new URL[]{new File(JarPath + JarName).toURI().toURL()},
                FunctionExe.class.getClassLoader()
        );

        Class<?> myClass = Class.forName(ClassName, true, myClassLoader);
        Method mySingeltonGetterMethod = myClass.getMethod("get" + ClassName,
                null);
        Object myObject = mySingeltonGetterMethod.invoke(null);

     /*   System.out.println(MethodName+" = "+myObject.getClass().getMethod(MethodName, List.class)
                .invoke(myObject, myNumbers));*/
        return (Double) myObject.getClass().getMethod(MethodName, List.class)
                .invoke(myObject, myNumbers);
    }



	 static public Double max(ArrayList<Double> myNumbers) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, MalformedURLException {

        String JarPath="D://Desktop/chapter2/compiler/JarsFiles/";
        String JarName = "CommonAggregations.jar";
        String ClassName = "CommonAggregations";
        String MethodName = "max";

        URLClassLoader myClassLoader = new URLClassLoader(
                new URL[]{new File(JarPath + JarName).toURI().toURL()},
                FunctionExe.class.getClassLoader()
        );

        Class<?> myClass = Class.forName(ClassName, true, myClassLoader);
        Method mySingeltonGetterMethod = myClass.getMethod("get" + ClassName,
                null);
        Object myObject = mySingeltonGetterMethod.invoke(null);

     /*   System.out.println(MethodName+" = "+myObject.getClass().getMethod(MethodName, List.class)
                .invoke(myObject, myNumbers));*/
        return (Double) myObject.getClass().getMethod(MethodName, List.class)
                .invoke(myObject, myNumbers);
    }



	 static public Double min(ArrayList<Double> myNumbers) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, MalformedURLException {

        String JarPath="D://Desktop/chapter2/compiler/JarsFiles/";
        String JarName = "CommonAggregations.jar";
        String ClassName = "CommonAggregations";
        String MethodName = "min";

        URLClassLoader myClassLoader = new URLClassLoader(
                new URL[]{new File(JarPath + JarName).toURI().toURL()},
                FunctionExe.class.getClassLoader()
        );

        Class<?> myClass = Class.forName(ClassName, true, myClassLoader);
        Method mySingeltonGetterMethod = myClass.getMethod("get" + ClassName,
                null);
        Object myObject = mySingeltonGetterMethod.invoke(null);

     /*   System.out.println(MethodName+" = "+myObject.getClass().getMethod(MethodName, List.class)
                .invoke(myObject, myNumbers));*/
        return (Double) myObject.getClass().getMethod(MethodName, List.class)
                .invoke(myObject, myNumbers);
    }



	 static public Double std(ArrayList<Double> myNumbers) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, MalformedURLException {

        String JarPath="D://Desktop/chapter2/compiler/JarsFiles/";
        String JarName = "CommonAggregations.jar";
        String ClassName = "CommonAggregations";
        String MethodName = "std";

        URLClassLoader myClassLoader = new URLClassLoader(
                new URL[]{new File(JarPath + JarName).toURI().toURL()},
                FunctionExe.class.getClassLoader()
        );

        Class<?> myClass = Class.forName(ClassName, true, myClassLoader);
        Method mySingeltonGetterMethod = myClass.getMethod("get" + ClassName,
                null);
        Object myObject = mySingeltonGetterMethod.invoke(null);

     /*   System.out.println(MethodName+" = "+myObject.getClass().getMethod(MethodName, List.class)
                .invoke(myObject, myNumbers));*/
        return (Double) myObject.getClass().getMethod(MethodName, List.class)
                .invoke(myObject, myNumbers);
    }



	 static public Double count(ArrayList<Object> myNumbers) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, MalformedURLException {

        String JarPath="D://Desktop/chapter2/compiler/JarsFiles/";
        String JarName = "count.jar";
        String ClassName = "Count";
        String MethodName = "count";

        URLClassLoader myClassLoader = new URLClassLoader(
                new URL[]{new File(JarPath + JarName).toURI().toURL()},
                FunctionExe.class.getClassLoader()
        );

        Class<?> myClass = Class.forName(ClassName, true, myClassLoader);
        Method mySingeltonGetterMethod = myClass.getMethod("get" + ClassName,
                null);
        Object myObject = mySingeltonGetterMethod.invoke(null);

     /*   System.out.println(MethodName+" = "+myObject.getClass().getMethod(MethodName, List.class)
                .invoke(myObject, myNumbers));*/
        return (Double) myObject.getClass().getMethod(MethodName, List.class)
                .invoke(myObject, myNumbers);
    }


}