package Java.GenerationCode.GeneratedClasses;

import java.util.ArrayList;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import java.io.*;
import java.nio.file.Files;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import java.nio.file.Paths;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.CSVParser;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class FacultiesClass{ 

	 public addressClass Address;
	 public String name;
	 public Double id;
	 static public ArrayList<FacultiesClass> arraylist = new ArrayList<>();

	 static public String path="D://Desktop/chapter2/compiler/jsonFiles/Uni.json";
	 static public String type="json";


	 static public void load(){
if( type.equals("json")) {

		JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(path)) {
            JSONObject jsonTable = (JSONObject) jsonParser.parse(reader);
            JSONArray jdata = (JSONArray) jsonTable.get("Faculties");
            FacultiesClass.arraylist=pars(jdata);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
	 }

else{
            try (
                    Reader reader = Files.newBufferedReader(Paths.get(path));
                    CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
                            .withFirstRecordAsHeader()
                            .withIgnoreHeaderCase()
                            .withTrim());
            ){
            FacultiesClass.arraylist=parsCsv(csvParser);
            } catch (IOException e) {
                e.printStackTrace();
            } }
}


    public static ArrayList<FacultiesClass> pars(JSONArray jdata){
        ArrayList<FacultiesClass> data = new ArrayList<>();
        for(int i=0;i<jdata.size();i++) {
            JSONObject jRow= (JSONObject) jdata.get(i);
            FacultiesClass obj=new FacultiesClass();
            obj.Address=addressClass.pars((JSONObject) jRow.get("Address"));
            obj.name=(String) jRow.get("name");
            obj.id=(Double) Double.valueOf(jRow.get("id").toString());
            data.add(obj);
        }
        return data;
    }


    public static ArrayList<FacultiesClass> parsCsv(CSVParser csvParser){
        ArrayList<FacultiesClass> data = new ArrayList<>();
       for (CSVRecord csvRecord : csvParser) {
            FacultiesClass obj=new FacultiesClass();
            obj.name= csvRecord.get("name");
            obj.id=Double.parseDouble(csvRecord.get("id"));
            data.add(obj);
        }
        return data;
    }

    public static void printAll() {
        System.out.println();        for (FacultiesClass obj:arraylist ) {
            obj.print();
            System.out.println("\n__________________________________________________________________________________________________________");
             }
    }
 public void print(){
                System.out.println("Address"+"=");
                this.Address.print();
                System.out.print("\t\t name"+"=" +this.name);
                System.out.print("\t\t id"+"=" +this.id);
       }
  }
