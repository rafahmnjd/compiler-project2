package Java.GenerationCode;

import Java.SymbolTable.Scope;
import Java.SymbolTable.Symbol;
import Java.SymbolTable.Type;;
import java.io.FileWriter;
import java.util.ArrayList;
import java.io.IOException;  // Import the IOException class to handle errors

public class GeneratingClasses {
    ArrayList<Type> types = new ArrayList();
    ArrayList<Scope> scopes = new ArrayList();
    FileReaderData fileReaderData = new FileReaderData();

    public void setTypes(ArrayList<Type> types) {
        this.types = types;
    }

    public void setScopes(ArrayList<Scope> scopes) {
        this.scopes = scopes;
    }

    public void generatingClass() {

        for (int i = 0; i < types.size(); i++) {
            try {
                String filename = "src\\Java\\GenerationCode\\GenerationClass\\" + types.get(i).getName() + "Class" + ".java";
                //File myObj = new File(filename);
                FileWriter myWriter = new FileWriter(filename);
                String first = "package Java.GenerationCode.GenerationClass;\n\n";
                String second = "public class " + types.get(i).getName() + "Class" + "{ \n";
                for (String k : types.get(i).getColumns().keySet()) {
                    if (types.get(i).getColumns().get(k).getName().equals(Type.STRING_CONST)) {
                        second += "\t public String ";
                        second += k + ";\n";
                    } else if (types.get(i).getColumns().get(k).getName().equals(Type.NUMBER_CONST)) {
                        second += "\t public double ";
                        second += k + ";\n";
                    } else {
                        second += "\t public " + types.get(i).getColumns().get(k).getName() + "Class" + " ";
                        second += k + ";\n";
                        // first+="import "+types.get(i).getColumns().get(k).getName()+";\n";
                    }
                }
                if (types.get(i).getTable()!=null) {
                    second += "\t public String ";
                    second += "type , path ;\n";
                    second += "\t public static ArrayList" + "<" + types.get(i).getName() + "Class" + "> " + types.get(i).getName() + " = new ArrayList();";
                    first += "import java.util.ArrayList;\n\n";
                }

                second += "\n}";
                myWriter.write(first);
                myWriter.write(second);
                myWriter.close();
            } catch (IOException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
            }

        }

    }

    public void generatingMain() throws IOException {
        String filename = "src\\Java\\GenerationCode\\GenerationClass\\Main.java";
        try {
            FileWriter myWriter = new FileWriter(filename);
            String main = "package Java.GenerationCode.GenerationClass;\n\n";
            main += "public class Main { \n";
            for (int i = 0; i < scopes.size(); i++) {
                for (Symbol s : scopes.get(i).getSymbolMap().values()) {
                    if (s.getType().getName().equals(Type.NUMBER_CONST)) {
                        main += "\t double " + s.getName() + " ; \n";
                    } else if (s.getType().getName().equals(Type.STRING_CONST)) {
                        main += "\t String " + s.getName() + " ; \n";
                    } else {
                        main += "\t" + s.getType().getName() + "Class " + s.getName() + " ; \n";
                    }
                }
            }
            main += "  public static void main(String[] args) {\n";


            generatingClass();

            fileReaderData.loadData("json","samples.txt\\data.json");


            main += "\n } \n }";
            myWriter.write(main);
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }

}
