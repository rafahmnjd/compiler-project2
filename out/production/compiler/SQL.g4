/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 by Bart Kiers
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Project      : sqlite-parser; an ANTLR4 grammar for SQLite
 *                https://github.com/bkiers/sqlite-parser
 * Developed by : Bart Kiers, bart@big-o.nl
 */
grammar SQL;

parse
 : ( sql_stmt_list |function_def|function_call(';')?| error )* EOF
 ;

error
 : UNEXPECTED_CHAR
   {
     throw new RuntimeException("UNEXPECTED_CHAR=" + $UNEXPECTED_CHAR.text);
   }
 ;


function_call: (variable'.')? java_function_name '(' (argument_list?) ')';
argument_list: argument (',' argument)* ;
argument:decliration
//|function_def
;

function_def
:K_FUNCTION? java_function_name '(' parameter_list? ')''{' (body? (end_body ';')?)'}'
;
parameter_list
:   parameter (',' parameter)*
|   default_var(','default_var)*
|   (parameter (',' parameter)*)(','default_var)*;
parameter:var_def;
default_var: var_def ASSIGN (decliration|select_stmt);

/*body
:('{'(body)?'}')
 |(java_stmt)+ (body)?
 |body (java_stmt)+
 //|java_stmt+
;*/

body
:('{'(body)?'}')
 |(java_stmt)+ (body)?
 |body (java_stmt)+
 //|java_stmt+
;

java_stmt:
need_scol';'
|not_need_scol
;

not_need_scol:
switch_stmt
|for_stmt
|if_stmt
|while_stmt
;
need_scol:
var_def
|default_var
|var_deceleration
|multivar_def
|function_call
|print
|incriment_stmt
|if_inline
|do_while_stmt
;

incriment_stmt
:variable incriment_op
|incriment_op variable
;
print:
K_PRINT '(' ((java_value) ('+'java_value )*)?')'
;

end_body:(K_BREAK|K_CONTINUE|return_stmt);

base_body:'{' (body? (end_body ';')?)'}'
           |  (java_stmt|(end_body ';'))
;

for_stmt:
(for_loop|for_each_loop) ((base_body)|';');

    for_loop:
      K_FOR '(' (K_VAR)?
      variable '=' num_exp ';'
      variable comparison_op  num_exp ';'
      variable  assign_op num_exp
      ')'
    ;
    for_each_loop:
    K_FOR '(' K_VAR? java_var_name ':' variable ')'
    ;

switch_stmt
: K_SWITCH '(' (variable|num_exp|string_val ) ')' '{'(case_stmt)* (default_stmt)?'}'
;
case_stmt:
K_CASE  (num_exp|string_val) ':' (  '{' (body? (end_body ';')?)'}'
                                     |  (body? (end_body ';')?)
                                 )
;
default_stmt:
K_DEFAULT ':'(  '{' (body? (end_body ';')?)'}'
                  | (body? (end_body ';')?)
              )
;

if_stmt:
 K_IF '(' (condition_exp)')'
 (base_body)
 (else_if_stmt)* (else_stmt)?
;

else_if_stmt:
K_ELSE K_IF '(' (condition_exp)')'(base_body);

else_stmt:
K_ELSE (base_body ) ;

if_inline
: (condition_exp)'?'(ifinlin_side ':' ifinlin_side)
| '('if_inline')'
;

ifinlin_side
:java_value|num_exp|condition_exp
|if_inline
|var_deceleration
|'('ifinlin_side')'
;


while_stmt
: K_WHILE '('condition_exp')'
(base_body| ';')
;

do_while_stmt
: K_DO (base_body )
  K_WHILE '('condition_exp')'
;

return_stmt
: K_RETURN
('('java_value')'
   | (java_value|if_inline|num_exp|condition_exp)
 );


/////////////////////////////////////////////////////////////////////////////////////////////////


sql_stmt_list
 : ';'* sql_stmt ( ';'+ sql_stmt )* ';'*
 ;

sql_stmt
 : create_table_stmt
 | create_type_stmt
 | select_stmt
 | create_aggregation_function
/* | delete_stmt
 | alter_table_stmt
 | drop_table_stmt
 | factored_select_stmt
 | insert_stmt
 | update_stmt*/
 ;

 create_table_stmt
 : K_CREATE  K_TABLE ( K_IF K_NOT K_EXISTS )? table_name
   ( '(' column_def ( ',' column_def )* ')')
   K_TYPE ASSIGN table_type ','
   K_PATH ASSIGN table_path
 ;
table_type:string_val;
table_path:string_val;
 create_type_stmt
  : K_CREATE  K_TYPE ( K_IF K_NOT K_EXISTS )?
    table_name '(' column_def (',' column_def )* ')'
  ;

 select_stmt
 :  select_core //select_or_values
   ( K_ORDER K_BY ordering_term ( ',' ordering_term )* )?
   ( K_LIMIT expr ( ( K_OFFSET | ',' ) expr )? )?
 ;

 column_def : column_name ( type_name ) ;
 type_name
  : name ( '(' signed_number (any_name)? ')'
          | '(' signed_number (any_name)? ',' signed_number (any_name)? ')' )?
  ;

  create_aggregation_function
  : K_CREATE K_AGGREGATION K_FUNCTION
    function_name
   '('
        jarPath ',' className ',' methodName ',' returnType ',' parameters
   ')'
  ;

  jarPath : string_val;
  jarName:string_val;
  className : any_name;
  methodName : any_name;
  returnType : any_name ;
  parameters:  '['(any_name (','any_name)*)?']';
/*select_or_values
 : K_SELECT ( K_DISTINCT | K_ALL )? result_column ( ',' result_column )*
   ( K_FROM ( table_or_subquery ( ',' table_or_subquery )* | join_clause ) )?
   ( K_WHERE expr )?
   ( K_GROUP K_BY expr ( ',' expr )* ( K_HAVING expr )? )?
 | K_VALUES '(' expr ( ',' expr )* ')' ( ',' '(' expr ( ',' expr )* ')' )*
 ; // the same of select_core!!
column_def : column_name ( type_name ) ;
type_name
 : name ( '(' signed_number (any_name)? ')'
         | '(' signed_number (any_name)? ',' signed_number (any_name)? ')' )?
 ;*/
/*
 alter_table_stmt
  : K_ALTER K_TABLE  ( database_name '.' )? source_table_name
    ( K_RENAME K_TO new_table_name
    | alter_table_add
    | alter_table_add_constraint
    | K_ADD K_COLUMN? column_def
    )
  ;
*/
/*delete_stmt
 :  K_DELETE K_FROM qualified_table_name
   ( K_WHERE expr )?
 ;
drop_table_stmt
 : K_DROP K_TABLE ( K_IF K_EXISTS )? ( database_name '.' )? table_name
 ;

factored_select_stmt
 : select_core
   ( K_ORDER K_BY ordering_term ( ',' ordering_term )* )?
   ( K_LIMIT expr ( ( K_OFFSET | ',' ) expr )? )?
 ; // the same of select_stmt !!

insert_stmt
   :   K_INSERT  K_INTO
   ( database_name '.' )? table_name ( '(' column_name ( ',' column_name )* ')' )?
   ( K_VALUES '(' expr ( ',' expr )* ')' ( ',' '(' expr ( ',' expr )* ')' )*
   | select_stmt
   | K_DEFAULT K_VALUES
   )
 ;*/
/*
update_stmt
:  K_UPDATE  qualified_table_name
   K_SET column_name '=' expr ( ',' column_name '=' expr )* ( K_WHERE expr )?
 ;
*/
/*column_constraint
 : ( K_CONSTRAINT name )?
   ( column_constraint_primary_key
   | column_constraint_foreign_key
   | column_constraint_not_null
   | column_constraint_null
   | K_UNIQUE
   | K_CHECK '(' expr ')'
   | column_default
   | K_COLLATE collation_name
   )
 ;*/
/*column_constraint_primary_key : K_PRIMARY K_KEY ( K_ASC | K_DESC )?  K_AUTOINCREMENT?;
column_constraint_foreign_key: foreign_key_clause;
column_constraint_not_null : K_NOT K_NULL;
column_constraint_null: K_NULL ;
column_default: K_DEFAULT (column_default_value | '(' expr ')' | K_NEXTVAL '(' expr ')' | any_name )  ( '::' any_name+ )?;
column_default_value : ( signed_number | literal_value ) ;*/
/*
alter_table_add_constraint
 : K_ADD K_CONSTRAINT any_name table_constraint
 ;

alter_table_add
 : K_ADD table_constraint
 ;
*/
/*
    SQLite understands the following binary operators, in order from highest to
    lowest precedence:

    ||
    *    /    %
    +    -
    <<   >>   &    |
    <    <=   >    >=
    =    ==   !=   <>   IS   IS NOT   IN   LIKE   GLOB   MATCH   REGEXP
    AND
    OR
*/

/*JAVA EXPR*/

condition_exp:
  (function_call|boolean_var|boolean_val)
  | condition_exp ( '&&'|'&'| '||'|'|' ) condition_exp
  | num_exp ( '<' | '<=' | '>' | '>=' ) num_exp
  | (num_exp|java_value)( '==' | '!='| '<>')(num_exp|java_value)
  | condition_exp ( '==' | '!='| '<>') condition_exp
  | '!' (condition_exp|function_call|boolean_var)
  |'(' (condition_exp|function_call|boolean_var) ')'
  ;

num_exp
: (num_val|function_call)
| num_exp ('*' | '/' | '%' ) num_exp
| num_exp ( '+' | '-' ) num_exp
| num_var (incriment_op)?
| incriment_op num_var
| '(' num_exp ')'
;

/*END JAVA EXPR*/

expr
 : literal_value
 | ( ( database_name '.' )? table_name '.' )? column_name
 | unary_operator expr
 | expr '||' expr
 | expr ( '*' | '/' | '%' ) expr
 | expr ( '+' | '-' ) expr
 | expr ( '<<' | '>>' | '&' | '|' ) expr
 | expr ( '<' | '<=' | '>' | '>=' ) expr
 | expr ( '=' | '==' | '!=' | '<>' | K_IS | K_IS K_NOT | K_IN | K_LIKE | K_GLOB | K_MATCH | K_REGEXP ) expr
 | expr K_AND expr
 | expr K_OR expr
 | function_expr
 | '(' expr ')'
 |  expr not_in_expr
 | ( ( K_NOT )? K_EXISTS )? '(' select_stmt ')'
 ;

function_expr: function_name '(' ( K_DISTINCT? expr ( ',' expr )* | '*' )? ')';
 not_in_expr
  :K_NOT? K_IN ( '(' ( select_stmt| expr ( ',' expr )*)?')'
               | ( database_name '.' )? table_name
               )
  ;

foreign_key_clause
 : K_REFERENCES ( database_name '.' )? foreign_table ( '(' fk_target_column_name ( ',' fk_target_column_name )* ')' )?
   ( (foreign_on_condition| K_MATCH name ) )*
   ( K_NOT? K_DEFERRABLE ( K_INITIALLY K_DEFERRED | K_INITIALLY K_IMMEDIATE )? K_ENABLE? )?
 ;

foreign_on_condition:
 K_ON ( K_DELETE | K_UPDATE )
  ( K_SET K_NULL
  | K_SET K_DEFAULT
  | K_CASCADE
  | K_RESTRICT
  | K_NO K_ACTION
  )
 ;
fk_target_column_name : name;   //any name

indexed_column
 : column_name ( K_COLLATE collation_name )? ( K_ASC | K_DESC )?
 ;
table_constraint
 : ( K_CONSTRAINT name )?
   (  table_constraint_primary_key//=K_PRIMARY K_KEY +table_constraint_key -name not exsist!!
    | table_constraint_key
    | table_constraint_unique//= K_UNIQU + table_constraint_key
    | K_CHECK '(' expr ')'
    | table_constraint_foreign_key
   )
 ;
table_constraint_primary_key
 : K_PRIMARY K_KEY '(' indexed_column ( ',' indexed_column )* ')';
table_constraint_foreign_key
 : K_FOREIGN K_KEY '(' fk_origin_column_name ( ',' fk_origin_column_name )* ')' foreign_key_clause ;
table_constraint_unique
 : K_UNIQUE K_KEY? name? '(' indexed_column ( ',' indexed_column )* ')' ;
table_constraint_key
: K_KEY name? '(' indexed_column ( ',' indexed_column )* ')';

fk_origin_column_name
 : column_name //any name
 ;
qualified_table_name
 : ( database_name '.' )? table_name ( K_INDEXED K_BY index_name
                                     | K_NOT K_INDEXED )?
 ;
ordering_term
 : expr  ( K_ASC | K_DESC )?
 ;
result_column
 : '*'
 | table_name '.' '*'
 | expr ( K_AS? column_alias )?
 ;

table_or_subquery
 : ( database_name '.' )? table_name ( K_AS? table_alias )?( K_INDEXED K_BY index_name| K_NOT K_INDEXED )?
 | '(' ( table_or_subquery ( ',' table_or_subquery )*| join_clause )')' ( K_AS? table_alias )?
 | '(' select_stmt ')' ( K_AS? table_alias )?
 ;

join_clause
 : table_or_subquery ( join_operator table_or_subquery join_constraint )*
 ;
join_operator
 : ','
 |  ( K_LEFT K_OUTER? | K_INNER  )? K_JOIN
 ;
join_constraint
    : ( K_ON expr)
 ;

select_core
 : K_SELECT ( K_DISTINCT | K_ALL )? result_column ( ',' result_column )*
   ( K_FROM ( table_or_subquery ( ',' table_or_subquery )* | join_clause ) )?
   ( K_WHERE expr )?
   ( K_GROUP K_BY expr ( ',' expr )* ( K_HAVING expr )? )?
 | K_VALUES '(' expr ( ',' expr )* ')' ( ',' '(' expr ( ',' expr )* ')' )*
 ;




 /* JAVA CODE :*/
multivar_def:
K_VAR (multivar_def_sub)(','(multivar_def_sub))+
;

multivar_def_sub:
(java_var_name/*|array_def*/)(ASSIGN (decliration|select_stmt))?
;

var_def         : K_VAR (java_var_name);
//|array_def);
array_def       : ('[' ']') java_var_name|java_var_name ('['(NUMERIC_LITERAL)?']');

array_dec :
 (K_NEW K_VAR ('['num_val']')) | variable
;

var_deceleration : variable assign_op (decliration|select_stmt) ;

decliration:
//:K_NEW K_VAR(('['num_val']')
//             |('[' ']''{' java_value(',' java_value)*'}') // i think there is no need for this line
////             |('(' (argument_list)  ')')
//             )
//|json_container
//|json_array
//|
java_value
|num_exp
|condition_exp
|if_inline
;


//json_container
//:'{' (java_var_name ':'( java_value| json_container| json_array) ',')* (java_var_name ':'( java_value| json_container| json_array)) '}'
//;
//json_array
// :'[' ((java_value | json_container)',')* (java_value | json_container)']';

java_value
 : num_val
 | boolean_val
 | string_val
 | K_NULL
 | variable
 | function_call
 ;


boolean_var:variable;
num_var:variable;


 variable:
 java_var_name
 | variable '.' variable
// | variable ('['(java_value|num_exp)']')
 // TODO: IT MUST BE'[' AN EXPRESSION OR NUM EXPRESSION OR JAVA VALUE']' LIK n[7+k]
 | '(' variable ')'
 ;

/*end java code*/
string_val     :(STRING_LITERAL|IDENTIFIER)('+' (java_value))*;  //STRING VALUE
boolean_val    :K_FALSE |K_TRUE ;                                //boolean value
num_val        :NUMERIC_LITERAL|signed_number;                   //NUMBER VALUE
signed_number  : ( ( '+' | '-' )? NUMERIC_LITERAL | '*' ) ;

literal_value
 : NUMERIC_LITERAL | STRING_LITERAL | BLOB_LITERAL
 | K_NULL | K_CURRENT_TIME | K_CURRENT_DATE | K_CURRENT_TIMESTAMP ;


assign_op       : (num_op)? ASSIGN;
num_op          : STAR|DIV|MOD|PLUS|MINUS;
comparison_op   : LT| GT| GT_EQ| LT_EQ;
incriment_op    : '++'|'--';
unary_operator  : '-' | '+' | '~' | K_NOT ;
error_message   : STRING_LITERAL;
column_alias    : IDENTIFIER |VARNAME | STRING_LITERAL ;


keyword
 : K_ACTION     | K_ADD         | K_ALL             | K_ALTER       | K_AND
 | K_AS         | K_ASC         | K_AUTOINCREMENT   | K_BY          | K_CASCADE
 | K_CASE       | K_CHECK       | K_COLLATE         | K_COLUMN      | K_CONSTRAINT
 | K_CREATE     | K_CROSS       | K_CURRENT_DATE    | K_CURRENT_TIME| K_CURRENT_TIMESTAMP
 | K_DATABASE   | K_DEFAULT     | K_DEFERRABLE      | K_DEFERRED    | K_DELETE
 | K_DESC       | K_DISTINCT    | K_DROP            | K_EACH        | K_ELSE
 | K_ENABLE     | K_EXISTS      | K_FOR             | K_FOREIGN     | K_FROM
 | K_GLOB       | K_GROUP       | K_HAVING          | K_IF          | K_IMMEDIATE
 | K_IN         | K_INDEXED     | K_INITIALLY       | K_INNER       | K_INSERT
 | K_INTO       | K_IS          | K_ISNULL          | K_JOIN        | K_KEY
 | K_LEFT       | K_LIKE        | K_LIMIT           | K_MATCH       | K_NO
 | K_NOT        | K_NOTNULL     | K_NULL            | K_OF          | K_OFFSET
 | K_ON         | K_OR          | K_ORDER           | K_OUTER       | K_PRIMARY
 | K_QUERY      | K_REFERENCES  | K_REGEXP          | K_RENAME      | K_REPLACE
 | K_RESTRICT   | K_RIGHT       | K_ROW             | K_SELECT      | K_SET
 | K_TABLE      | K_THEN        | K_TO              | K_UNION       | K_UNIQUE
 | K_UPDATE     | K_VALUES      | K_VIEW            | K_WHERE       | K_NEXTVAL
 | K_METHODNAME
 | K_CLASSNAME
 | K_JARPATH    |K_AGGREGATION

  //java key word:

  | K_VAR        | K_NEW        | K_TRUE            | K_FALSE       | K_FUNCTION
  | K_RETURN     | K_WHILE      | K_SWITCH          | K_BREAK       | K_CONTINUE
  | K_DO;

/*JAVA CODE*/

//java variable name
java_function_name:java_var_name;
java_var_name: VARNAME;

/*End */


//tO DO check all names below


unknown: (.)+?;

// all any name are String //
name : any_name ;
function_name: any_name ;
database_name : any_name ;
source_table_name : any_name ;
table_name : any_name ;
new_table_name : any_name ;
column_name : any_name ;
collation_name : any_name ;
foreign_table : any_name ;
index_name : any_name ;

table_alias: IDENTIFIER|VARNAME ;

any_name
 : IDENTIFIER| VARNAME| keyword| STRING_LITERAL| '(' any_name ')'
 ;

  SCOL      : ';';
  DOT       : '.';
  OPA       : '[';
  CLA       : ']';
  OPEN_PAR  : '(';
  CLOSE_PAR : ')';
  COMMA     : ',';
  ASSIGN    : '=';
  STAR      : '*';
  PLUS      : '+';
  PLUS2     :'++';
  MINUS2    :'--';
  MINUS     : '-';
  TILDE     : '~';
  AMP2      :'&&';
  PIPE2     :'||';
  DIV       : '/';
  MOD       : '%';
  LT2       :'<<';
  GT2       :'>>';
  AMP       : '&';
  PIPE      : '|';
  LT        : '<';
  LT_EQ     :'<=';
  GT        : '>';
  GT_EQ     :'>=';
  EQ        :'==';
  NOT_EQ1   :'!=';
  NOT_EQ2   :'<>';

// http://www.sqlite.org/lang_keywords.html
K_ACTION            : A C T I O N;
K_ADD               : A D D;
K_ALL               : A L L;
K_ALTER             : A L T E R;
K_AND               : A N D;
K_AS                : A S;
K_ASC               : A S C;
K_AUTOINCREMENT     : A U T O I N C R E M E N T;
K_BY                : B Y;
K_CASCADE           : C A S C A D E;
K_CHECK             : C H E C K;
K_COLLATE           : C O L L A T E;
K_COLUMN            : C O L U M N;
K_CONSTRAINT        : C O N S T R A I N T;
K_CREATE            : C R E A T E;
K_CROSS             : C R O S S;
K_CURRENT_DATE      : C U R R E N T '_' D A T E;
K_CURRENT_TIME      : C U R R E N T '_' T I M E;
K_CURRENT_TIMESTAMP : C U R R E N T '_' T I M E S T A M P;
K_DATABASE          : D A T A B A S E;
K_DEFAULT           : D E F A U L T;
K_DEFERRABLE        : D E F E R R A B L E;
K_DEFERRED          : D E F E R R E D;
K_DELETE            : D E L E T E;
K_DESC              : D E S C;
K_DISTINCT          : D I S T I N C T;
K_DROP              : D R O P;
K_EACH              : E A C H;
K_ELSE              : E L S E;
K_ENABLE            : E N A B L E;
K_FOR               : F O R;
K_FOREIGN           : F O R E I G N;
K_FROM              : F R O M;
K_GLOB              : G L O B;
K_GROUP             : G R O U P;
K_HAVING            : H A V I N G;
K_IF                : I F;
K_IMMEDIATE         : I M M E D I A T E;
K_IN                : I N;
K_INDEXED           : I N D E X E D;
K_INITIALLY         : I N I T I A L L Y;
K_INNER             : I N N E R;
K_INSERT            : I N S E R T;
K_INTO              : I N T O;
K_IS                : I S;
K_ISNULL            : I S N U L L;
K_JOIN              : J O I N;
K_KEY               : K E Y;
K_LEFT              : L E F T;
K_LIKE              : L I K E;
K_LIMIT             : L I M I T;
K_MATCH             : M A T C H;
K_NEXTVAL           : N E X T V A L;
K_NO                : N O;
K_NOT               : N O T;
K_NOTNULL           : N O T N U L L;
K_NULL              : N U L L;
K_OF                : O F;
K_OFFSET            : O F F S E T;
K_ON                : O N;
K_OR                : O R;
K_ORDER             : O R D E R;
K_OUTER             : O U T E R;
K_PRIMARY           : P R I M A R Y;
K_QUERY             : Q U E R Y;
K_REFERENCES        : R E F E R E N C E S;
K_REGEXP            : R E G E X P;
K_RENAME            : R E N A M E;
K_REPLACE           : R E P L A C E;
K_RESTRICT          : R E S T R I C T;
K_RIGHT             : R I G H T;
K_ROW               : R O W;
K_SELECT            : S E L E C T;
K_SET               : S E T;
K_TABLE             : T A B L E;
K_TYPE              : T Y P E;
K_THEN              : T H E N;
K_TO                : T O;
K_UNION             : U N I O N;
K_UNIQUE            : U N I Q U E;
K_UPDATE            : U P D A T E;
K_VALUES            : V A L U E S;
K_VIEW              : V I E W;
K_WHERE             : W H E R E;
K_METHODNAME        : M E T H O D N A M E;
K_CLASSNAME         : C L A S S N A M E;
K_JARPATH           : J A R P A T H;
K_AGGREGATION       : A G G R E G A T I O N ;


K_PATH :P A T H;

  //java key word:
  K_PRINT     : P R I N T ;
  K_VAR       : V A R;
  K_NEW       : N E W;
  K_TRUE      : T R U E;
  K_FALSE     : F A L S E;
  K_FUNCTION  :  F U N C T I O N;
  K_RETURN    : R E T U R N ;
  K_WHILE     :W H I L E;
  K_SWITCH    :S W I T C H;
  K_CASE      :C A S E ;
  K_BREAK     :B R E A K;
  K_CONTINUE  :C O N T I N U E;
  K_DO        :D O;
  K_EXISTS    :E X I S T S;
  //java variable

  VARNAME:[a-zA-Z_][a-zA-Z_0-9]*  ;

  IDENTIFIER
  : '"' .*? '"'
  | '`' (~'`' | '``')* '`'
 ;
  NUMERIC_LITERAL
   :DIGIT+ ( '.' DIGIT* )? ( E [-+]?DIGIT+ )?
   | '.'DIGIT+ ( E [-+]?DIGIT+)?
   ;

  BIND_PARAMETER: '?' DIGIT*   | [@$] (IDENTIFIER|VARNAME)   ;
  STRING_LITERAL: '\'' ( ~'\'' | '\'\'' )* '\''   ;
  BLOB_LITERAL: X STRING_LITERAL;

  MULTILINE_COMMENT : '/*' .*? ( '*/' | EOF ) -> channel(HIDDEN)   ;
  LINE_COMMENT : '//' .*? ( '\n' | EOF ) -> channel(HIDDEN)   ;
  SPACES: [ \u000B\t\r\n] -> channel(HIDDEN) ;
  UNEXPECTED_CHAR: .;


  fragment DIGIT : [0-9];

  fragment A : [aA];
  fragment B : [bB];
  fragment C : [cC];
  fragment D : [dD];
  fragment E : [eE];
  fragment F : [fF];
  fragment G : [gG];
  fragment H : [hH];
  fragment I : [iI];
  fragment J : [jJ];
  fragment K : [kK];
  fragment L : [lL];
  fragment M : [mM];
  fragment N : [nN];
  fragment O : [oO];
  fragment P : [pP];
  fragment Q : [qQ];
  fragment R : [rR];
  fragment S : [sS];
  fragment T : [tT];
  fragment U : [uU];
  fragment V : [vV];
  fragment W : [wW];
  fragment X : [xX];
  fragment Y : [yY];
  fragment Z : [zZ];
